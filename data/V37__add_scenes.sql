USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


SET IDENTITY_INSERT Scene ON
GO

INSERT INTO Scene (Id, Filename, Height, Width) VALUES (9, '1601A0E0-E3CB-4D40-BBF3-7120681F54C0', 406, 500)
INSERT INTO Scene (Id, Filename, Height, Width) VALUES (10, 'FA8D3BD1-FEF2-4C2D-89F5-A599AA31D70C', 406, 500)
INSERT INTO Scene (Id, Filename, Height, Width) VALUES (11, '280CD0D7-2FF8-40A7-BA4B-FA39F638EA06', 406, 500)
INSERT INTO Scene (Id, Filename, Height, Width) VALUES (12, '52BF0CB3-CFC1-49D3-962F-5278290F7F6C', 406, 500)
INSERT INTO Scene (Id, Filename, Height, Width) VALUES (13, '457D8083-965B-458B-9FBF-DC245A06C161', 406, 500)
GO

SET IDENTITY_INSERT Scene OFF
GO

SET IDENTITY_INSERT SceneProduct ON
GO

INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (11, 9, '042')
INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (12, 10, '043')
INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (13, 11, '640')
INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (14, 12, '084')
INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (15, 13, 'A1W')
GO

SET IDENTITY_INSERT SceneProduct OFF
GO

SET IDENTITY_INSERT ScenePlacement ON
GO

INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (9, 9, 105, 43, 390, 52, 388, 297, 105, 271, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (10, 10, 136, 52, 364, 45, 364, 198, 136, 210, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (11, 11, 176, 24, 441, 23, 433, 475, 171, 461, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (12, 12, 88, 197, 414, 196, 416, 407, 87, 408, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (13, 13, 117, 82, 321, 40, 398, 348, 178, 392, 0.95)
GO

SET IDENTITY_INSERT ScenePlacement OFF
GO

UPDATE Scene Set Height = 406, Width = 500 WHERE Id = 8
GO
