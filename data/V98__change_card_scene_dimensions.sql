USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

UPDATE ScenePlacement SET TopRightX = 978, TopRightY = 146, BottomRightX = 972, BottomRightY = 538, BottomLeftX = 253, BottomLeftY = 525 WHERE ID = 1
GO
