USE [design_profile]
GO

-- Enable paper bags
UPDATE Template SET UseForLogoCenter = 1 WHERE ProductId = 'HXP'
GO

-- Update Mug PFID
UPDATE Template SET ProductId = 'A0Y' WHERE ProductId = 'A14'
GO

UPDATE SceneProduct SET ProductId = 'A0Y' WHERE ProductId = 'A14'
GO

-- Update Stickers PFID
UPDATE Template SET ProductId = 'ABF' WHERE ProductId = 'AFB'
GO

UPDATE SceneProduct SET ProductId = 'ABF' WHERE ProductId = 'AFB'
GO
