USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

WITH cteBc AS (
	SELECT pr.* FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pl.Id = pr.ScenePlacementId LEFT JOIN Scene s ON pl.SceneId = s.Id WHERE ProductId = 'B73' AND Filename = '3E9F049C-5F92-4E4D-9D03-E15C4125B639'
)
UPDATE cteBc SET ProductId = '___'
GO
