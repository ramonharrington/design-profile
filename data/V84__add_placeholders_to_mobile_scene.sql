USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @SceneId INT
SELECT @SceneId = Id FROM Scene WHERE SceneName = 'holiday_mobile'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 193, TooltipY = 779 WHERE ProductId = 'A0Y'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 619, TooltipY = 947 WHERE ProductId = 'BQ8'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 467, TooltipY = 2056 WHERE ProductId = 'HJA'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 189, TooltipY = 2500 WHERE ProductId = 'BR3'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 601, TooltipY = 2864 WHERE ProductId = 'BPT'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 197, TooltipY = 3093 WHERE ProductId = '220'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 503, TooltipY = 4076 WHERE ProductId = 'EAD'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 373, TooltipY = 4707 WHERE ProductId = '645'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 245, TooltipY = 5166 WHERE ProductId = '679'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 583, TooltipY = 5409 WHERE ProductId = '218'
GO
