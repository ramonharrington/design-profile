USE [design_profile]
GO

SET IDENTITY_INSERT Font ON
GO

INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (11, 'Arial', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (12, 'Armitage', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (13, 'Athelas Rg', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (14, 'BaileywickJF Curly', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (15, 'Balloon Bd BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (16, 'BankGothic Md BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (17, 'BaroqueTextJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (18, 'Baskerville BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (19, 'BatangChe', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (20, 'Beatnik', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (21, 'BeignetJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (22, 'BernhardMod BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (23, 'Big Limbo BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (24, 'Biographer', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (25, 'BookmanJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (26, 'Borges Titulo CE Blanca', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (27, 'Borges Titulo CE Hueca', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (28, 'BoxerScriptJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (29, 'Bravissima Script', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (30, 'Bree', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (31, 'BrushScript BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (32, 'BuenaParkJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (33, 'CabernetJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (34, 'Calla Web Titling', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (35, 'Calligraph421 BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (36, 'CapitalPoster', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (37, 'Comic Sans MS', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (38, 'ComicPro JY', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (39, 'CopprplGoth BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (40, 'Coquette', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (41, 'CornerStoreJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (42, 'Courier New', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (43, 'CousinBethJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (44, 'Crete', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (45, 'Cuisine', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (46, 'DandelionJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (47, 'De Soto', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (48, 'De Soto Engraved', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (49, 'DebonairJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (50, 'DomCasual BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (51, 'Dotum', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (52, 'DotumChe', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (53, 'DulcimerJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (54, 'EloquentJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (55, 'EmpyreanJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (56, 'English157 BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (57, 'Espresso', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (58, 'FairyTaleJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (59, 'FanfareJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (60, 'FangSong_GB2312', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (61, 'FenwayParkJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (62, 'FunnyBoneJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (63, 'Futura Bk BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (64, 'FuzzGuitarJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (65, 'FZShuTi', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (66, 'FZYaoTi', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (67, 'Georgia', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (68, 'Goldenbook', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (69, 'GoldenStateJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (70, 'GoudyOlSt BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (71, 'Grandma', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (72, 'GuedelScriptJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (73, 'Gulim', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (74, 'GulimChe', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (75, 'Gungsuh', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (76, 'GungsuhChe', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (77, 'HandelGothic BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (78, 'HellenicWideJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (79, 'HolidayTimesJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (80, 'HooliganJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (81, 'HorrorFlickJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (82, 'HucklebuckJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (83, 'Humanst521 BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (84, 'Impact', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (85, 'Integrity JY Lining 2', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (86, 'JeffrianaJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (87, 'JohannaWhimsyJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (88, 'JohnAndrewJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (89, 'KaiTi_GB2312', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (90, 'Kandal', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (91, 'Kandal Black', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (92, 'Kennedy Sm Caps Book GD', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (93, 'KonTikiEnchantedJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (94, 'Kuenstler165 BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (95, 'KugelhopfJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (96, 'LedenbachJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (97, 'LFT Etica', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (98, 'LiSu', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (99, 'LuxRoyaleJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (100, 'MagicSpellJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (101, 'MartiniJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (102, 'MaryHelenJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (103, 'Melanie BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (104, 'MingLiU', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (105, 'Missy BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (106, 'MisterEarl BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (107, 'Mostra Nuova', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (108, 'MS Gothic', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (109, 'MS Mincho', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (110, 'MS PGothic', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (111, 'MS PMincho', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (112, 'NSimSun', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (113, 'Parisian BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (114, 'Pill Web 300mg', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (115, 'Playbill BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (116, 'PMingLiU', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (117, 'Politica', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (118, 'Proxima Nova Black', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (119, 'Proxima Nova Rg', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (120, 'RandolphJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (121, 'Refrigerator Deluxe', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (122, 'Roger', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (123, 'Ronnia Cond', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (124, 'Roundhand BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (125, 'SaharanJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (126, 'ScriptoramaJF Hostess', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (127, 'SilverThreadJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (128, 'SimHei', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (129, 'SimSun', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (130, 'SouthlandJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (131, 'STCaiyun', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (132, 'StellaAnnJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (133, 'Stencil', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (134, 'StephanieMarieJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (135, 'STXihei', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (136, 'STXingkai', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (137, 'STXinwei', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (138, 'STZhongsong', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (139, 'Swis721 BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (140, 'Tahoma', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (141, 'Times New Roman', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (142, 'Trebuchet MS', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (143, 'TypoUpright BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (144, 'VAGRounded BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (145, 'ValentinaJoyJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (146, 'VarsityScriptJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (147, 'Verdana', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (148, 'WalcottGothicHollywoodJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (149, 'WeddingText BT', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (150, 'WesleyJF', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (151, 'Wingdings', '', '', 0)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (152, 'Zag', '', '', 0)

GO

SET IDENTITY_INSERT Font OFF
GO

UPDATE Font SET SpritePosition = (ID - 1) * 20 WHERE ID >= 11
GO
