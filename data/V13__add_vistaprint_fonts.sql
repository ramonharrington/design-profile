USE [design_profile]
GO

ALTER TABLE Font ADD SpritePosition SMALLINT NULL
GO

UPDATE Font SET Name = 'AcroterionJF', Url = '', Tags = '', SpritePosition = 0 WHERE Id = 1
UPDATE Font SET Name = 'AdageScriptJF', Url = '', Tags = '', SpritePosition = 20 WHERE Id = 2
UPDATE Font SET Name = 'Adelle', Url = '', Tags = '', SpritePosition = 40 WHERE Id = 3
UPDATE Font SET Name = 'AdLib BT', Url = '', Tags = '', SpritePosition = 60 WHERE Id = 4
UPDATE Font SET Name = 'Aetna JY Newstyle', Url = '', Tags = '', SpritePosition = 80 WHERE Id = 5

SET IDENTITY_INSERT Font ON
GO

INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (6, 'AlpengeistJF', '', '', 100)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (7, 'Amazone BT', '', '', 120)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (8, 'AndantinoJF', '', '', 140)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (9, 'AnnabelleJF', '', '', 160)
INSERT INTO Font (ID, Name, Url, Tags, SpritePosition) VALUES (10, 'Apertura Web', '', '', 180)
GO

SET IDENTITY_INSERT Font OFF
GO
