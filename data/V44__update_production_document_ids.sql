USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

UPDATE Template SET TemplateId = 'J26XV-G4A18-3J4' WHERE TemplateId = 'XVXQQ-G4A83-4M8'
UPDATE Template SET TemplateId = 'M26XV-G4A11-9G4' WHERE TemplateId = 'TVXQQ-G4A64-0P7'
GO

UPDATE TemplateItems SET template_id = 'J26XV-G4A18-3J4' WHERE template_id = 'XVXQQ-G4A83-4M8'
UPDATE TemplateItems SET template_id = 'M26XV-G4A11-9G4' WHERE template_id = 'TVXQQ-G4A64-0P7'
GO
