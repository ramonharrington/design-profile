USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


SET IDENTITY_INSERT Scene ON
GO

INSERT INTO Scene (Id, Filename, Height, Width) VALUES (18, 'D1CAD638-CA77-4D2C-9B32-7C5D93D3C455', 432, 500)
INSERT INTO Scene (Id, Filename, Height, Width) VALUES (19, 'A9B9BEEC-70DC-409B-85BA-5AB389A517BB', 319, 500)
INSERT INTO Scene (Id, Filename, Height, Width) VALUES (20, 'F545885F-F185-421A-ACEE-D7F65B6D35B1', 459, 500)
INSERT INTO Scene (Id, Filename, Height, Width) VALUES (21, 'DDC151BE-8075-4E09-8328-5A74DB57D33B', 547, 500)
GO

SET IDENTITY_INSERT Scene OFF
GO

SET IDENTITY_INSERT SceneProduct ON
GO

INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (20, 18, '357')
INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (21, 19, '276')
INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (22, 20, '083')
INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (23, 21, 'B06')
GO

SET IDENTITY_INSERT SceneProduct OFF
GO

SET IDENTITY_INSERT ScenePlacement ON
GO

INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (22, 18, 34, 172, 154, 137, 228, 363, 95, 402, 0.95, '#FF0000')
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (23, 18, 67, 137, 189, 140, 171, 376, 36, 372, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (24, 18, 219, 84, 338, 66, 387, 286, 257, 305, 0.95)

INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (25, 19, 125, 97, 291, 28, 400, 211, 229, 292, 0.95, '#00FF00')
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (26, 19, 130, 76, 296, 7, 408, 189, 234, 271, 0.95, '#FF0000')
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (27, 19, 227, 52, 409, 89, 351, 300, 157, 260, 0.95)

INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (28, 20, 149, 56, 269, 24, 279, 63, 159, 97, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (29, 20, 43, 128, 163, 96, 173, 135, 53, 169, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (30, 20, 161, 95, 281, 63, 291, 102, 171, 136, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (31, 20, 53, 166, 173, 134, 183, 173, 63, 207, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (32, 20, 64, 204, 184, 172, 194, 211, 74, 245, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (33, 20, 75, 242, 195, 210, 205, 249, 85, 283, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (34, 20, 85, 280, 205, 248, 215, 287, 95, 321, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (35, 20, 96, 318, 216, 286, 226, 325, 106, 359, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (36, 20, 107, 357, 227, 325, 237, 364, 117, 398, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (37, 20, 118, 395, 238, 363, 248, 402, 128, 436, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (38, 20, 238, 361, 358, 329, 368, 368, 248, 402, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (39, 20, 169, 133, 289, 101, 299, 140, 179, 174, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (40, 20, 230, 321, 350, 289, 360, 328, 240, 362, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (41, 20, 241, 360, 361, 328, 371, 367, 251, 401, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (42, 20, 146, 113, 268, 113, 268, 154, 146, 154, 0.95)

INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (43, 21, 158, 166, 332, 166, 332, 340, 158, 340, 0.95)
GO

SET IDENTITY_INSERT ScenePlacement OFF
GO
