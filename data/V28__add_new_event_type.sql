USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


SET IDENTITY_INSERT PreviewEventType ON
INSERT INTO PreviewEventType (Id, Name) VALUES (3, 'ViewPortfolio')
SET IDENTITY_INSERT PreviewEventType OFF
GO

UPDATE PreviewEventType SET Name = 'GoToStudio' WHERE ID = 1
UPDATE PreviewEventType SET Name = 'AddToPortfolio' WHERE ID = 2
GO
