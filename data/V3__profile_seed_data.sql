SET IDENTITY_INSERT Industry OFF
GO


SET IDENTITY_INSERT Account ON
INSERT INTO Account (Id, Email, ShopperKey, FirstName, LastName, Created) VALUES (1, 'mlert1@vp.com', 15292767, 'Manny', 'Lertpatthanakul', '2017-07-21')
INSERT INTO Account (Id, Email, ShopperKey, FirstName, LastName, Created) VALUES (2, 'fmartell1@vp.com', 15258719, 'Fernando', 'Martell', '2017-07-21')
SET IDENTITY_INSERT Account OFF
GO


SET IDENTITY_INSERT Business ON
INSERT INTO Business (Id, AccountId, Name, IndustryId1, IndustryId2, IndustryId3, Tagline, Address1, Address2, City, State, Postal, Phone1, Phone2, Email, Created) VALUES (1, 1, 'Guns n Eggs', 0, 22, NULL, 'Something for everyone', '101 Farm Lane', '', 'Boonies', 'NH', '00000', '603-555-1234', NULL, 'taxfreeordie@nhfarms.com', '2017-07-21')
INSERT INTO Business (Id, AccountId, Name, IndustryId1, IndustryId2, IndustryId3, Tagline, Address1, Address2, City, State, Postal, Phone1, Phone2, Email, Created) VALUES (2, 2, 'Beards Unlimited', 4, NULL, NULL, 'The only limit is your manliness', '10 Matching Lane', '', 'Worcester', 'MA', '00000', '508-555-1234', NULL, 'fm@responsiblebeards.com', '2017-07-21')
SET IDENTITY_INSERT Business OFF
GO


SET IDENTITY_INSERT Profile ON
INSERT INTO Profile (Id, BusinessId, Name, Created, Settings) VALUES (1, 1, 'Black and Yellow', '2017-07-21', '')
INSERT INTO Profile (Id, BusinessId, Name, Created, Settings) VALUES (2, 2, 'Classic', '2017-07-21', '')
SET IDENTITY_INSERT Profile OFF
GO
