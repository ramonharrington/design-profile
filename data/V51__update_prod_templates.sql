USE [design_profile]
GO

UPDATE SceneProduct SET ProductId = 'A4E' WHERE ProductId = 'A1W'
GO

-- Add new mug template
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('5BTW9-2A779-8C7', 'A14', 'Mug', NULL, 'local', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('5BTW9-2A779-8C7', 'A14', 'Mug', NULL, 'dev', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('XKVJ4-H4A78-8N3', 'A14', 'Mug', NULL, 'test', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('XKVJ4-H4A78-8N3', 'A14', 'Mug', NULL, 'prod', 1, 1, 0)
GO

INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('5BTW9-2A779-8C7', 'item_311', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('XKVJ4-H4A78-8N3', 'item_311', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO

-- Remove unused mug scene
DECLARE @SceneId INT
DECLARE @ScenePlacementId INT

SELECT @SceneId = Id FROM Scene WHERE Filename = 'CBF4CBF9-BADD-4E46-9047-63ABAD0F6426'
SELECT @ScenePlacementId = Id FROM ScenePlacement WHERE SceneId = @SceneId
DELETE FROM PreviewTracking WHERE SceneId = @SceneId
DELETE FROM SceneProduct WHERE ScenePlacementId = @ScenePlacementId
DELETE FROM ScenePlacement WHERE Id = @ScenePlacementId
DELETE FROM Scene WHERE Id = @SceneId
GO


-- Add Mug
DECLARE @SceneId INT
DECLARE @ScenePlacementId INT

INSERT INTO Scene (Filename, Width, Height) VALUES ('BC0A4A46-A6F2-4D53-9BFC-5DE80CD7A037', 860, 522)
SELECT @SceneId = @@IDENTITY

INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, TopCenterX, TopCenterY, RightMiddleX, RightMiddleY, BottomCenterX, BottomCenterY, LeftMiddleX, LeftMiddleY)
	VALUES (@SceneId, 327, 176, 582, 176, 573, 354, 328, 354, 0.95, 459, 195, NULL, NULL, 458, 386, NULL, NULL)
SELECT @ScenePlacementId = @@IDENTITY

INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'A14')
GO
