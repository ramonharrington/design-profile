USE [design_profile]
GO

-- Postcard magnet white
--UPDATE ScenePlacement SET TopLeftX = 1016, TopLeftY = 1008, TopRightX = 1104, TopRightY = 991, BottomRightX = 1125, BottomRightY = 1106, BottomLeftX = 1036, BottomLeftY = 1123 WHERE ID = 73
UPDATE ScenePlacement SET TopLeftX = 1012, TopLeftY = 1004, TopRightX = 1107, TopRightY = 988, BottomRightX = 1128, BottomRightY = 1109, BottomLeftX = 1032, BottomLeftY = 1127 WHERE TooltipX = 1045 AND TooltipY = 1155 AND MaskColor IS NULL

-- Postcard magnet red
--UPDATE ScenePlacement SET TopLeftX = 1007, TopLeftY = 991, BottomLeftX = 997, BottomRightX = 1088, BottomRightY = 1116 WHERE TooltipX = 1045 and MaskColor = '#FF0000'
UPDATE ScenePlacement SET TopLeftX = 1006, TopLeftY = 990, BottomLeftX = 995, BottomRightX = 1090, BottomRightY = 1118 WHERE TooltipX = 1045 AND TooltipY = 1155 AND MaskColor = '#FF0000'

-- Postcard magnet blue
--UPDATE ScenePlacement SET TopLeftX = 999, TopLeftY = 982, TopRightX = 1089, TopRightY = 989, BottomRightX = 1080, BottomRightY = 1106, BottomLeftX = 990, BottomLeftY = 1099 WHERE TooltipX = 1045 and MaskColor = '#0000FF'
UPDATE ScenePlacement SET TopLeftX = 997, TopLeftY = 980, TopRightX = 1092, TopRightY = 987, BottomRightX = 1083, BottomRightY = 1107, BottomLeftX = 987, BottomLeftY = 1099 WHERE TooltipX = 1045 AND TooltipY = 1155 AND MaskColor = '#0000FF'

-- Postcard magnet green
--UPDATE ScenePlacement SET TopLeftX = 991, TopLeftY = 988, BottomRightX = 1058, BottomRightY = 1120, BottomLeftX = 969, BottomLeftY = 1104 WHERE TooltipX = 1045 and MaskColor = '#00FF00'
UPDATE ScenePlacement SET TopLeftX = 989, TopLeftY = 986, BottomRightX = 1058, BottomRightY = 1121, BottomLeftX = 966, BottomLeftY = 1105 WHERE TooltipX = 1045 AND TooltipY = 1155 AND MaskColor = '#00FF00'


-- Mouse pad correction
--UPDATE ScenePlacement SET TopLeftX = 714, TopLeftY = 1275, TopRightX = 866, TopRightY = 1222, BottomRightX = 912, BottomRightY = 1350, BottomLeftX = 757, BottomLeftY = 1403 WHERE TooltipX = 845 AND TooltipY = 1401
UPDATE ScenePlacement SET TopLeftX = 714, TopLeftY = 1275, TopRightX = 867, TopRightY = 1221, BottomRightX = 914, BottomRightY = 1351, BottomLeftX = 757, BottomLeftY = 1407 WHERE TooltipX = 845 AND TooltipY = 1401


-- calendar magnet white
--UPDATE ScenePlacement SET BottomLeftX = 966, BottomLeftY = 1507 WHERE TooltipX = 1025 AND TooltipY = 1553 AND MaskColor IS NULL
UPDATE ScenePlacement SET BottomLeftX = 962, BottomLeftY = 1505 WHERE TooltipX = 1025 AND TooltipY = 1553 AND MaskColor IS NULL


-- mug for brandshop
--UPDATE ScenePlacement SET TopLeftX = 327, TopLeftY = 176, TopRightX = 582, TopRightY = 176, BottomRightX = 573, BottomRightY = 354, BottomLeftX = 328, BottomLeftY = 354, TopCenterX = 459, TopCenterY = 195, BottomCenterX = 458, BottomCenterY = 386 WHERE TopCenterX = 459 AND TopCenterY = 195 AND BottomCenterX = 458 AND BottomCenterY = 386
UPDATE ScenePlacement SET TopLeftX = 280, TopLeftY = 139, TopRightX = 623, TopRightY = 138, BottomRightX = 616, BottomRightY = 391, BottomLeftX = 287, BottomLeftY = 391, TopCenterX = 461, TopCenterY = 206, BottomCenterX = 468, BottomCenterY = 450 WHERE TopCenterX = 459 AND TopCenterY = 195 AND BottomCenterX = 458 AND BottomCenterY = 386
GO
