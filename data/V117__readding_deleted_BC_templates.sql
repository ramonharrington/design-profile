USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

UPDATE Template SET TemplateId = 'T40JB-2A664-7G6' WHERE TemplateId = 'MG3HB-2A964-2C3'
UPDATE Template SET TemplateId = 'V40JB-2A877-9R8' WHERE TemplateId = 'RH3HB-2A783-0E5'
GO

UPDATE TemplateItems SET template_id = 'T40JB-2A664-7G6' WHERE template_id = 'MG3HB-2A964-2C3'
UPDATE TemplateItems SET template_id = 'V40JB-2A877-9R8' WHERE template_id = 'RH3HB-2A783-0E5'
GO



