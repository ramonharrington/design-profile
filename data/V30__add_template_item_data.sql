USE [design_profile]
GO

INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'4G5J9-2A464-4H2', N'item_661', N'{"ImageUsage":"logo","Recolor":"complete","Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'4G5J9-2A464-4H2', N'vpls_text_3', N'{"Recolor":"complete","Color":"secondary","Font":"secondary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'4G5J9-2A464-4H2', N'vpls_text_4', N'{"Recolor":null,"Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'4G5J9-2A464-4H2', N'vpls_shape_5', N'{"Recolor":null,"Color":"secondary","Font":"secondary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'4G5J9-2A464-4H2', N'vpls_line_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'6G5J9-2A718-0M8', N'vpls_rect_1', N'{"Recolor":"complete","Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'6G5J9-2A718-0M8', N'item_395', N'{"ImageUsage":"logo","Recolor":"complete","Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'6G5J9-2A718-0M8', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'6G5J9-2A718-0M8', N'vpls_img_1', N'{"ImageUsage":"logo","Recolor":"complete","Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'6G5J9-2A718-0M8', N'vpls_rect_1', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'6G5J9-2A718-0M8', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7G5J9-2A758-4O1', N'item_348', N'{"ImageUsage":"logo","Recolor":"hue","Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7G5J9-2A758-4O1', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7G5J9-2A758-4O1', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'6G5J9-2A718-0M8', N'vpls_rect_2', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'8G5J9-2A061-3J7', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'8G5J9-2A061-3J7', N'item_402', N'{"ImageUsage":"logo","Recolor":"complete","Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'8G5J9-2A061-3J7', N'vpls_text_2', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'8G5J9-2A061-3J7', N'vpls_shape_3', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'9G5J9-2A523-2P2', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'9G5J9-2A523-2P2', N'item_342', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'9G5J9-2A523-2P2', N'vpls_line_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'BG5J9-2A247-5K4', N'vpls_img_2', N'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'BG5J9-2A247-5K4', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'BG5J9-2A247-5K4', N'item_361', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'BG5J9-2A247-5K4', N'vpls_text_3', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'BG5J9-2A247-5K4', N'vpls_shape_4', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'BG5J9-2A247-5K4', N'item_650', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'BG5J9-2A247-5K4', N'item_730', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'BG5J9-2A247-5K4', N'vpls_line_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_img_1', N'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'item_706', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_imagearea_4', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_40', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_39', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_38', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_37', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_36', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_35', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_34', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_33', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_32', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_31', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_30', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_29', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_28', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_27', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_26', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_25', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_24', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_23', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_22', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_21', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_20', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_19', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_18', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_17', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_16', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_15', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_text_1', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_40', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_39', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_38', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_37', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_36', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_35', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_34', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_33', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_32', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_31', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_30', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_29', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_28', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_27', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_26', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_25', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_24', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_23', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_22', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_21', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_20', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_19', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_18', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_17', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_16', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_shape_15', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_text_1', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_img_4', N'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_rect_1', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_text_1', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'CG5J9-2A741-7Q8', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'DG5J9-2A834-3N7', N'item_374', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'DG5J9-2A834-3N7', N'vpls_text_1', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'DG5J9-2A834-3N7', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'DG5J9-2A834-3N7', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'DG5J9-2A834-3N7', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'DG5J9-2A834-3N7', N'item_685', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'FG5J9-2A147-1I3', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'FG5J9-2A147-1I3', N'item_400', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'FG5J9-2A147-1I3', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'FG5J9-2A147-1I3', N'vpls_text_4', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'item_607', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_4', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_5', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_6', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_7', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_8', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_9', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_line_1', N'{"Recolor":"complete","Color":"bw","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'item_985', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_img_11', N'{"ImageUsage":"none","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_1', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_4', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_5', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_6', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_7', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_8', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_9', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GG5J9-2A288-6P4', N'vpls_text_10', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'HG5J9-2A734-9J6', N'vpls_img_1', N'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'HG5J9-2A734-9J6', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'HG5J9-2A734-9J6', N'vpls_oval_1', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'HG5J9-2A734-9J6', N'item_470', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'HG5J9-2A734-9J6', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'HG5J9-2A734-9J6', N'vpls_imagearea_3', N'{"Recolor":null,"Color":null,"Font":""}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'HG5J9-2A734-9J6', N'vpls_img_2', N'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'HG5J9-2A734-9J6', N'vpls_text_1', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'JG5J9-2A689-6M4', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'JG5J9-2A689-6M4', N'item_396', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'item_681', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'item_484', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_4', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_5', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_line_1', N'{"Recolor":"complete","Color":"bw","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_img_10', N'{"ImageUsage":"none","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_1', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_4', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_5', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_6', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_7', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_8', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KG5J9-2A931-4J3', N'vpls_text_9', N'{"Recolor":null,"Color":null,"Font":"none"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'LG5J9-2A244-2O8', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'LG5J9-2A244-2O8', N'item_366', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'LG5J9-2A244-2O8', N'vpls_text_2', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'LG5J9-2A244-2O8', N'vpls_shape_3', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'MG5J9-2A547-0K4', N'item_264', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'NG5J9-2A210-3R8', N'item_422', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'NG5J9-2A210-3R8', N'vpls_text_2', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'NG5J9-2A210-3R8', N'vpls_text_3', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'NG5J9-2A210-3R8', N'vpls_text_4', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'NG5J9-2A210-3R8', N'vpls_text_5', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
