USE [design_profile]
GO

SET IDENTITY_INSERT Scene ON
GO

INSERT INTO Scene (Id, Filename) VALUES (4, '16D4D290-ADD6-435F-BA91-49E4C8E94F3C')
GO

SET IDENTITY_INSERT Scene OFF
GO

INSERT INTO SceneProduct (SceneId, ProductId) VALUES (4, '018')
INSERT INTO SceneProduct (SceneId, ProductId) VALUES (4, 'BLN')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (4, 48, 96, 828, 86, 847, 422, 40, 425, 0.95)
GO
