USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE Scene ADD MinViewportWidth INT NULL
GO

UPDATE Scene SET MinViewportWidth = 880 WHERE SceneName = 'holiday' AND Width = 1440
UPDATE Scene SET MinViewportWidth = 415 WHERE SceneName = 'holiday' AND Width = 828
GO

DECLARE @SceneId INT
DECLARE @ScenePlacementId INT

INSERT INTO Scene (Filename, Height, Width, SceneName, MinViewportWidth) VALUES ('F18EE1A3-470C-43FC-BBDD-E680399A9F0C', 2828, 414, 'holiday', 0)
SELECT @SceneId = @@IDENTITY

-- Mug
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 60, 238, 138, 246, 128, 335, 49, 326, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'A0Y')

-- Coaster
-- blue
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 272, 348, 367, 316, 399, 411, 304, 442, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')
-- red
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 233, 304, 334, 294, 344, 393, 243, 403, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')
-- white
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 252, 272, 349, 299, 323, 396, 225, 369, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')

-- Men's shirt
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 127, 632, 265, 618, 280, 769, 141, 783, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'HJA')

-- Puzzle
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 42, 1090, 187, 1110, 170, 1225, 26, 1205, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BR3')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 44, 1127, 192, 1100, 216, 1213, 68, 1239, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BR3')


-- Canvas
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 243, 1170, 396, 1188, 369, 1403, 218, 1385, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BPT')

-- Photo Magnet
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 51, 1382, 133, 1397, 112, 1511, 29, 1494, 0.95, '#00FF00')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 59, 1376, 147, 1384, 138, 1498, 50, 1490, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 67, 1386, 155, 1393, 146, 1508, 58, 1500, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 76, 1402, 162, 1386, 184, 1498, 97, 1515, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')

-- Women's T-shirt
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 142, 1718, 264, 1695, 288, 1828, 166, 1851, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'EAD')

-- Calendar Poster
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 77, 2095, 377, 2147, 341, 2349, 42, 2297, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '645')

-- Mouse pad
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 20, 2431, 183, 2402, 208, 2540, 44, 2568, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '679')

-- Calendar magnet
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 238, 2551, 358, 2532, 370, 2613, 250, 2632, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 242, 2563, 361, 2553, 368, 2630, 248, 2640, 0.95, '#00FF00')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 244, 2568, 364, 2568, 364, 2647, 244, 2647, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 239, 2579, 358, 2597, 346, 2675, 227, 2657, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
GO
