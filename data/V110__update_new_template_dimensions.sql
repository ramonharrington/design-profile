USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

UPDATE SceneProduct SET TemplateLeftEdge = 0.27, TemplateRightEdge = 0.73, TemplateTopEdge = 0.5 WHERE ProductId = 'FGU'
GO

UPDATE ScenePlacement SET MaskColor = '#FF0000' WHERE MaskColor = '#E00808'
UPDATE ScenePlacement SET MaskColor = '#00FF00' WHERE MaskColor = '#05A34A'
GO

WITH cte AS (
	SELECT pl.* FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pl.Id = pr.ScenePlacementId WHERE pr.ProductId = '622' AND pl.MaskColor = '#0000FF'
)
UPDATE cte SET TopLeftX = 310, TopLeftY = 220, BottomLeftX = 250, BottomLeftY = 242
GO
