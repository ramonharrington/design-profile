USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @SceneId INT
SELECT @SceneId = Id FROM Scene WHERE SceneName = 'holiday' AND MinViewportWidth = 0

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 96, TooltipY = 389 WHERE ProductId = 'A0Y'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 309, TooltipY = 473 WHERE ProductId = 'BQ8'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 233, TooltipY = 1028 WHERE ProductId = 'HJA'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 94, TooltipY = 1250 WHERE ProductId = 'BR3'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 300, TooltipY = 1432 WHERE ProductId = 'BPT'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 98, TooltipY = 1546 WHERE ProductId = '220'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 251, TooltipY = 2038 WHERE ProductId = 'EAD'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 186, TooltipY = 2353 WHERE ProductId = '645'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 122, TooltipY = 2583 WHERE ProductId = '679'

;WITH cteSceneElements AS (
	SELECT pl.*, pr.ProductId FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pr.ScenePlacementId = pl.Id
	WHERE SceneId = @SceneId
)
UPDATE cteSceneElements SET TooltipX = 291, TooltipY = 2704 WHERE ProductId = '218'
GO
