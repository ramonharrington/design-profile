USE [design_profile]
GO

UPDATE Template SET TemplateId = 'XRPX9-2A177-8L1' WHERE ProductId = 'EAD' AND Environment IN ('local', 'dev')
UPDATE Template SET TemplateId = 'GWDZ6-H4A14-1D9', UseForLogoCenter = 1 WHERE ProductId = 'EAD' AND Environment IN ('test', 'prod')
GO

INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('XRPX9-2A177-8L1', 'item_718', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('GWDZ6-H4A14-1D9', 'item_718', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO

UPDATE Template SET TemplateId = '14QX9-2A492-6M2' WHERE TemplateId = '5BTW9-2A779-8C7'
UPDATE TemplateItems SET Template_Id = '14QX9-2A492-6M2' WHERE Template_Id = '5BTW9-2A779-8C7'
GO
