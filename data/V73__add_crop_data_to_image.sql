USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE Image ADD CropTop [float] NULL, CropRight [float] NULL, CropBottom [float] NULL, CropLeft [float] NULL
GO
