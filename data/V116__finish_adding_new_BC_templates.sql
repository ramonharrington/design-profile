USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter, rank, Height, Width, UseForBCsLogoFlow, UseInCarousel)
VALUES
('MG3HB-2A964-2C3', 'B73', 'Business Card 3', NULL, 'local', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('MG3HB-2A964-2C3', 'B73', 'Business Card 3', NULL, 'dev', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('PBMRZ-L4A53-9W7', 'B73', 'Business Card 3', NULL, 'prod', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('PBMRZ-L4A53-9W7', 'B73', 'Business Card 3', NULL, 'test', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('RH3HB-2A783-0E5', 'B73', 'Business Card 4', NULL, 'local', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('RH3HB-2A783-0E5', 'B73', 'Business Card 4', NULL, 'dev', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('FLTVZ-L4A68-5H2', 'B73', 'Business Card 4', NULL, 'prod', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('FLTVZ-L4A68-5H2', 'B73', 'Business Card 4', NULL, 'test', 0, 0, 0, 0, NULL, NULL, 1, 1)
GO

INSERT INTO TemplateItems  (template_id, item_id, configuration)
VALUES
('MG3HB-2A964-2C3', 'buildPathBackgroundRectangle', '{"Recolor":null,"Color":null,"Font":null}')
,('MG3HB-2A964-2C3', 'item_796', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
,('MG3HB-2A964-2C3', 'vpls_text_5', '{"Recolor":"complete","Color":"primary","Font":null}')
,('MG3HB-2A964-2C3', 'vpls_text_6', '{"Recolor":null,"Color":null,"Font":null}')
,('MG3HB-2A964-2C3', 'vpls_text_7', '{"Recolor":null,"Color":null,"Font":null}')
,('MG3HB-2A964-2C3', 'vpls_text_8', '{"Recolor":null,"Color":null,"Font":null}')
,('MG3HB-2A964-2C3', 'vpls_text_9', '{"Recolor":null,"Color":null,"Font":null}')
,('MG3HB-2A964-2C3', 'vpls_text_10', '{"Recolor":null,"Color":null,"Font":null}')
,('MG3HB-2A964-2C3', 'vpls_text_11', '{"Recolor":null,"Color":null,"Font":null}')
,('MG3HB-2A964-2C3', 'vpls_line_1', '{"Recolor":"complete","Color":"primary","Font":null}')
,('RH3HB-2A783-0E5', 'buildPathBackgroundRectangle', '{"Recolor":null,"Color":null,"Font":null}')
,('RH3HB-2A783-0E5', 'item_2142', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
,('RH3HB-2A783-0E5', 'vpls_text_3', '{"Recolor":"complete","Color":"primary","Font":null}')
,('RH3HB-2A783-0E5', 'vpls_text_4', '{"Recolor":null,"Color":null,"Font":null}')
,('RH3HB-2A783-0E5', 'vpls_text_5', '{"Recolor":"complete","Color":"primary","Font":null}')
,('RH3HB-2A783-0E5', 'vpls_text_6', '{"Recolor":null,"Color":null,"Font":null}')
,('RH3HB-2A783-0E5', 'vpls_text_7', '{"Recolor":null,"Color":null,"Font":null}')
,('RH3HB-2A783-0E5', 'vpls_text_8', '{"Recolor":null,"Color":null,"Font":null}')
,('RH3HB-2A783-0E5', 'vpls_text_9', '{"Recolor":null,"Color":null,"Font":null}')
,('RH3HB-2A783-0E5', 'vpls_text_10', '{"Recolor":null,"Color":null,"Font":null}')
,('RH3HB-2A783-0E5', 'vpls_text_11', '{"Recolor":null,"Color":null,"Font":null}')
,('PBMRZ-L4A53-9W7', 'buildPathBackgroundRectangle', '{"Recolor":null,"Color":null,"Font":null}')
,('PBMRZ-L4A53-9W7', 'item_796', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
,('PBMRZ-L4A53-9W7', 'vpls_text_5', '{"Recolor":"complete","Color":"primary","Font":null}')
,('PBMRZ-L4A53-9W7', 'vpls_text_6', '{"Recolor":null,"Color":null,"Font":null}')
,('PBMRZ-L4A53-9W7', 'vpls_text_7', '{"Recolor":null,"Color":null,"Font":null}')
,('PBMRZ-L4A53-9W7', 'vpls_text_8', '{"Recolor":null,"Color":null,"Font":null}')
,('PBMRZ-L4A53-9W7', 'vpls_text_9', '{"Recolor":null,"Color":null,"Font":null}')
,('PBMRZ-L4A53-9W7', 'vpls_text_10', '{"Recolor":null,"Color":null,"Font":null}')
,('PBMRZ-L4A53-9W7', 'vpls_text_11', '{"Recolor":null,"Color":null,"Font":null}')
,('PBMRZ-L4A53-9W7', 'vpls_line_1', '{"Recolor":"complete","Color":"primary","Font":null}')
,('FLTVZ-L4A68-5H2', 'buildPathBackgroundRectangle', '{"Recolor":null,"Color":null,"Font":null}')
,('FLTVZ-L4A68-5H2', 'item_2142', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
,('FLTVZ-L4A68-5H2', 'vpls_text_3', '{"Recolor":"complete","Color":"primary","Font":null}')
,('FLTVZ-L4A68-5H2', 'vpls_text_4', '{"Recolor":null,"Color":null,"Font":null}')
,('FLTVZ-L4A68-5H2', 'vpls_text_5', '{"Recolor":"complete","Color":"primary","Font":null}')
,('FLTVZ-L4A68-5H2', 'vpls_text_6', '{"Recolor":null,"Color":null,"Font":null}')
,('FLTVZ-L4A68-5H2', 'vpls_text_7', '{"Recolor":null,"Color":null,"Font":null}')
,('FLTVZ-L4A68-5H2', 'vpls_text_8', '{"Recolor":null,"Color":null,"Font":null}')
,('FLTVZ-L4A68-5H2', 'vpls_text_9', '{"Recolor":null,"Color":null,"Font":null}')
,('FLTVZ-L4A68-5H2', 'vpls_text_10', '{"Recolor":null,"Color":null,"Font":null}')
,('FLTVZ-L4A68-5H2', 'vpls_text_11', '{"Recolor":null,"Color":null,"Font":null}')
GO

