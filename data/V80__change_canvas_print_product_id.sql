USE [design_profile]
GO

update template set productid = 'BPT', templateid = 'T4L3B-2A107-6V7' where templateid = 'KX1Z9-2A215-1D9'
update template set productid = 'BPT', templateid = 'WTWLQ-H4A14-7H7' where templateid = 'X9FML-H4A05-2F3'

update sceneproduct set productid = 'BPT' where productid = 'K6U'

update templateitems set template_id = 'T4L3B-2A107-6V7' where template_id = 'KX1Z9-2A215-1D9'
update templateitems set template_id = 'WTWLQ-H4A14-7H7' where template_id = 'X9FML-H4A05-2F3'
GO
