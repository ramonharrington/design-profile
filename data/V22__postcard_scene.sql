USE [design_profile]
GO

SET IDENTITY_INSERT Scene ON
GO

INSERT INTO Scene (Id, Filename) VALUES (3, '2C39C150-4F5B-4C8C-993B-C84F858CE8D3')
GO

SET IDENTITY_INSERT Scene OFF
GO

INSERT INTO SceneProduct (SceneId, ProductId) VALUES (3, '232')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (3, 191, 55, 743, 55, 745, 451, 179, 448, 0.95)
GO
