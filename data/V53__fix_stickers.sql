USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


WITH cteStickers AS (
	SELECT p1.* FROM ScenePlacement p1 LEFT JOIN SceneProduct p2 ON p1.Id = p2.ScenePlacementId WHERE p2.ProductId = 'AFB'
)
UPDATE cteStickers SET TopLeftX = 224, TopLeftY = 119, TopRightX = 349, TopRightY = 169, BottomRightX = 276, BottomRightY = 268, BottomLeftX = 151, BottomLeftY = 219
GO

UPDATE Template SET UseForMatching = 0, UseForLogoCenter = 0 WHERE TemplateId IN ('N07X9-2A530-5K4', 'H70N1-H4A96-6P0')
GO
