USE [design_profile]
GO

-- remove dupes
delete from TemplateItems where Id in (805, 802, 804)
GO

--deactivate 
Update Template set UseForLogoCenter = 0 where id in (82,84,86,100,101,102,103)
GO

-- add new ones
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('X1215-H4A68-1F9', 'EAE', 'Women''s T-Shirt', NULL, 'prod', 0, 1, 0) --prod w
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('8KBX9-2A871-7O6', 'EAE', 'Women''s T-Shirt', NULL, 'local', 0, 1, 0)  --dev w
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('8KBX9-2A871-7O6', 'EAE', 'Women''s T-Shirt', NULL, 'dev', 0, 1, 0)  --dev w
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('B4L15-H4A68-0T5', 'HJB', 'T-shirt', NULL, 'prod', 0, 1, 0) --prod m
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('GPBX9-2A221-6N6', 'HJB', 'T-shirt', NULL, 'local', 0, 1, 0)  --dev m
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('GPBX9-2A221-6N6', 'HJB', 'T-shirt', NULL, 'dev', 0, 1, 0)  --dev m
GO

-- update scenes
UPDATE SceneProduct SET ProductId = 'HJB' WHERE ProductId = 'B06'
UPDATE SceneProduct SET ProductId = 'EAE' WHERE ProductId = 'A8V'
GO