USE [design_profile]
GO

UPDATE Image SET ImageTypeId = NULL
DELETE FROM ImageType
SET IDENTITY_INSERT ImageType ON

INSERT INTO ImageType (Id, Name) VALUES (1, 'Logo')
INSERT INTO ImageType (Id, Name) VALUES (2, 'Picture')
INSERT INTO ImageType (Id, Name) VALUES (3, 'Accent Image')
SET IDENTITY_INSERT ImageType OFF

GO
