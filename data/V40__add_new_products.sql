USE [design_profile]
GO

INSERT [dbo].[Template] ([TemplateId], [ProductId], [Name], [Tags], [Environment], [UseForMatching]) VALUES (N'LDHL9-2A716-5K5', N'B06', N'T-shirt', NULL, N'local', 1)
INSERT [dbo].[Template] ([TemplateId], [ProductId], [Name], [Tags], [Environment], [UseForMatching]) VALUES (N'MDHL9-2A156-2G5', N'057', N'Hat', NULL, N'local', 1)
INSERT [dbo].[Template] ([TemplateId], [ProductId], [Name], [Tags], [Environment], [UseForMatching]) VALUES (N'LDHL9-2A716-5K5', N'B06', N'T-shirt', NULL, N'dev', 1)
INSERT [dbo].[Template] ([TemplateId], [ProductId], [Name], [Tags], [Environment], [UseForMatching]) VALUES (N'MDHL9-2A156-2G5', N'057', N'Hat', NULL, N'dev', 1)
INSERT [dbo].[Template] ([TemplateId], [ProductId], [Name], [Tags], [Environment], [UseForMatching]) VALUES (N'LDHL9-2A716-5K5', N'B06', N'T-shirt', NULL, N'prod', 1)
INSERT [dbo].[Template] ([TemplateId], [ProductId], [Name], [Tags], [Environment], [UseForMatching]) VALUES (N'MDHL9-2A156-2G5', N'057', N'Hat', NULL, N'prod', 1)
GO

INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'MDHL9-2A156-2G5', N'item_329', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'LDHL9-2A716-5K5', N'item_298', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'TVXQQ-G4A64-0P7', N'item_329', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'XVXQQ-G4A83-4M8', N'item_298', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
