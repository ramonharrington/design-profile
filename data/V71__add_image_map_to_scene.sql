USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE Scene ADD SceneName [nvarchar](80) NULL
GO

ALTER TABLE ScenePlacement ADD TooltipX [int] NULL, TooltipY [int] NULL
GO

UPDATE Scene SET SceneName = 'holiday' WHERE Filename = '08901329-E1B9-49FE-8FEB-EBCE797EC7CC'
GO

DECLARE @SceneId INT
SELECT @SceneId = Id FROM Scene WHERE Filename = '08901329-E1B9-49FE-8FEB-EBCE797EC7CC'

-- Mug
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = 'A0Y'
)
UPDATE cte SET TooltipX = 485, TooltipY = 391

-- Men's Shirt
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = 'HJA'
)
UPDATE cte SET TooltipX = 963, TooltipY = 723

-- Coaster
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = 'BQ8'
)
UPDATE cte SET TooltipX = 361, TooltipY = 643

-- Canvas
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = 'K6U'
)
UPDATE cte SET TooltipX = 587, TooltipY = 771

-- Puzzle
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = 'BR3'
)
UPDATE cte SET TooltipX = 403, TooltipY = 947

-- Poster calendar
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = '645'
)
UPDATE cte SET TooltipX = 775, TooltipY = 1085

-- Postcard magnet
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = '220'
)
UPDATE cte SET TooltipX = 1045, TooltipY = 1155

-- Women's shirt
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = 'EAD'
)
UPDATE cte SET TooltipX = 439, TooltipY = 1539

-- Mouse pad
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = '679'
)
UPDATE cte SET TooltipX = 845, TooltipY = 1401

-- Calendar magnet
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = '218'
)
UPDATE cte SET TooltipX = 1025, TooltipY = 1553


-- Fix coaster scene
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE SceneId = @SceneId AND ProductId = 'BQ8' AND MaskColor = '#FF0000'
)
UPDATE cte SET TopLeftY = 461
GO


-- Fix canvas bag dimensions
;WITH cte AS (
	SELECT sp1.*
	FROM ScenePlacement sp1 LEFT JOIN SceneProduct sp2 ON sp1.Id = sp2.ScenePlacementId
	WHERE ProductId = 'A0B'
)
UPDATE cte SET topleftx = 110, toplefty = 202, toprightx = 193, toprighty = 188, bottomrightx = 200, bottomrighty = 271, bottomleftx = 120, bottomlefty = 284, topCenterx = 154, topcentery = 193, bottomcenterx = 165, bottomcentery = 279
GO
