USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

UPDATE ScenePlacement SET TopLeftX = 274, TopLeftY = 67, TopRightX = 609, TopRightY = 67, TopCenterX = 449, TopCenterY = 69 WHERE SceneId = 7
GO
