USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- dev
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_img_3', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_text_4', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_text_5', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_text_6', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_text_7', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_text_8', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_text_9', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_text_10', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_text_11', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_text_12', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_text_13', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'vpls_rect_1', '{"Recolor":"complete","Color":"primary","Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('DXN5B-2A292-8W6',	'item_833', '{"Recolor":"complete","Color":"secondary","Font":null}')

--prod
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_img_3', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_text_4', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_text_5', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_text_6', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_text_7', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_text_8', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_text_9', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_text_10', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_text_11', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_text_12', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_text_13', '{"Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'vpls_rect_1', '{"Recolor":"complete","Color":"primary","Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('4ZN28-J4A94-5N9',	'item_833', '{"Recolor":"complete","Color":"secondary","Font":null}')
GO


INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter)
VALUES ('DXN5B-2A292-8W6', 'F2W', 'Business Cards', null, 'local', 0, 1, 0),
('DXN5B-2A292-8W6', 'F2W', 'Business Cards', null, 'dev', 0, 1, 0),
('4ZN28-J4A94-5N9', 'F2W', 'Business Cards', null, 'prod', 0, 1, 0),
('4ZN28-J4A94-5N9', 'F2W', 'Business Cards', null, 'test', 0, 1, 0)

GO