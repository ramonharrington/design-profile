USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER TABLE Template ADD Environment nvarchar(50) NULL
GO

DELETE FROM Template
GO

-- Insert templates for Local
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('M0GK9-2A003-1E2', '331', 'Sticky Note', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('N0GK9-2A238-1G0', '679', 'Mouse Pad', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('P0GK9-2A836-8K7', '016', 'Large Banner', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('Q0GK9-2A370-1N9', 'A14', 'Mug', 'force-background', 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('R0GK9-2A974-4F5', '043', 'Lawn Sign', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('V0GK9-2A312-2H0', '276', 'Note Pad', 'force-background', 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('W0GK9-2A766-3H3', 'A4E', 'Notebook', 'force-background', 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('Z0GK9-2A183-0G7', '236', 'Note Card', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('11GK9-2A770-8H0', '357', 'Rack Card', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('31GK9-2A003-8H9', '640', 'Poster', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('41GK9-2A549-1O2', '376', 'Pen', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('51GK9-2A263-5J4', 'CDK', 'Lawn Sign', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('61GK9-2A776-5N6', 'CCZ', 'Car Magnet', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('71GK9-2A932-7L2', '051', 'Car Door Magnet', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('91GK9-2A133-5K5', '084', 'Postcard', NULL, 'local')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('F1GK9-2A016-7J6', '083', 'Return Address Label', NULL, 'local')
GO

-- Insert templates for Dev
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('M0GK9-2A003-1E2', '331', 'Sticky Note', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('N0GK9-2A238-1G0', '679', 'Mouse Pad', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('P0GK9-2A836-8K7', '016', 'Large Banner', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('Q0GK9-2A370-1N9', 'A14', 'Mug', 'force-background', 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('R0GK9-2A974-4F5', '043', 'Lawn Sign', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('V0GK9-2A312-2H0', '276', 'Note Pad', 'force-background', 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('W0GK9-2A766-3H3', 'A4E', 'Notebook', 'force-background', 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('Z0GK9-2A183-0G7', '236', 'Note Card', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('11GK9-2A770-8H0', '357', 'Rack Card', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('31GK9-2A003-8H9', '640', 'Poster', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('41GK9-2A549-1O2', '376', 'Pen', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('51GK9-2A263-5J4', 'CDK', 'Lawn Sign', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('61GK9-2A776-5N6', 'CCZ', 'Car Magnet', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('71GK9-2A932-7L2', '051', 'Car Door Magnet', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('91GK9-2A133-5K5', '084', 'Postcard', NULL, 'dev')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('F1GK9-2A016-7J6', '083', 'Return Address Label', NULL, 'dev')
GO


-- Insert templates items
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'M0GK9-2A003-1E2', N'item_264', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'N0GK9-2A238-1G0', N'item_348', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'P0GK9-2A836-8K7', N'item_661', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'P0GK9-2A836-8K7', N'vpls_text_3', N'{"Recolor":"bw","Color":"","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'P0GK9-2A836-8K7', N'vpls_text_4', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'P0GK9-2A836-8K7', N'vpls_shape_5', N'{"Recolor":"bw","Color":"complete","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'P0GK9-2A836-8K7', N'vpls_line_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'Q0GK9-2A370-1N9', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'Q0GK9-2A370-1N9', N'item_296', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'R0GK9-2A974-4F5', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'R0GK9-2A974-4F5', N'item_395', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'R0GK9-2A974-4F5', N'vpls_text_3', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'V0GK9-2A312-2H0', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'V0GK9-2A312-2H0', N'item_342', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'V0GK9-2A312-2H0', N'vpls_line_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'V0GK9-2A312-2H0', N'vpls_imagearea_3', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'W0GK9-2A766-3H3', N'vpls_img_1', N'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'W0GK9-2A766-3H3', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'W0GK9-2A766-3H3', N'item_706', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'W0GK9-2A766-3H3', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'W0GK9-2A766-3H3', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'W0GK9-2A766-3H3', N'vpls_imagearea_4', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'Z0GK9-2A183-0G7', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'Z0GK9-2A183-0G7', N'vpls_oval_1', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'Z0GK9-2A183-0G7', N'item_470', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'11GK9-2A770-8H0', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'11GK9-2A770-8H0', N'item_607', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'11GK9-2A770-8H0', N'vpls_text_8', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'31GK9-2A003-8H9', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'31GK9-2A003-8H9', N'item_361', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'31GK9-2A003-8H9', N'item_650', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'41GK9-2A549-1O2', N'item_374', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'41GK9-2A549-1O2', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'41GK9-2A549-1O2', N'item_685', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'51GK9-2A263-5J4', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'51GK9-2A263-5J4', N'item_402', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'51GK9-2A263-5J4', N'vpls_text_2', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'51GK9-2A263-5J4', N'vpls_shape_3', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'61GK9-2A776-5N6', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'61GK9-2A776-5N6', N'item_366', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'61GK9-2A776-5N6', N'vpls_text_2', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'61GK9-2A776-5N6', N'vpls_shape_3', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'71GK9-2A932-7L2', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'71GK9-2A932-7L2', N'item_400', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'71GK9-2A932-7L2', N'vpls_text_4', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'91GK9-2A133-5K5', N'item_681', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'91GK9-2A133-5K5', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'91GK9-2A133-5K5', N'item_484', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'91GK9-2A133-5K5', N'vpls_text_4', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'91GK9-2A133-5K5', N'vpls_text_5', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'91GK9-2A133-5K5', N'vpls_line_1', N'{"Recolor":"complete","Color":"bw","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'F1GK9-2A016-7J6', N'item_422', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'F1GK9-2A016-7J6', N'vpls_text_2', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'F1GK9-2A016-7J6', N'vpls_text_3', N'{"Recolor":"complete","Color":"secondary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'F1GK9-2A016-7J6', N'vpls_text_4', N'{"Recolor":"complete","Color":"secondary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'F1GK9-2A016-7J6', N'vpls_text_5', N'{"Recolor":"complete","Color":"secondary","Font":"primary"}')
GO


UPDATE SceneProduct SET ProductId = 'A14' WHERE ProductId = 'A0Y'
UPDATE SceneProduct SET ProductId = '236' WHERE ProductId = '232'
UPDATE SceneProduct SET ProductId = '016' WHERE ProductId = '018'
GO
