USE [design_profile]
GO

SET IDENTITY_INSERT Scene ON
GO

INSERT INTO Scene (Id, Filename) VALUES (6, '3E9F049C-5F92-4E4D-9D03-E15C4125B639')
INSERT INTO Scene (Id, Filename) VALUES (7, 'CBF4CBF9-BADD-4E46-9047-63ABAD0F6426')
GO

SET IDENTITY_INSERT Scene OFF
GO

INSERT INTO SceneProduct (SceneId, ProductId) VALUES (6, 'B73')
INSERT INTO SceneProduct (SceneId, ProductId) VALUES (7, 'A0Y')

INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, TopCenterX, TopCenterY, RightMiddleX, RightMiddleY, BottomCenterX, BottomCenterY, LeftMiddleX, LeftMiddleY)
	VALUES (6, 164, 69, 664, 70, 680, 384, 169, 395, 0.95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, TopCenterX, TopCenterY, RightMiddleX, RightMiddleY, BottomCenterX, BottomCenterY, LeftMiddleX, LeftMiddleY)
	VALUES (7, 271, 38, 610, 38, 610, 380, 271, 380, 0.95, 440, 38, NULL, NULL, 440, 430, NULL, NULL)
GO
