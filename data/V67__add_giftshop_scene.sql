USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @SceneId INT
DECLARE @ScenePlacementId INT

INSERT INTO Scene (Filename, Height, Width) VALUES ('CC1EA599-65CB-46F0-8D21-5745E563D98B', 637, 1440)
SELECT @SceneId = @@IDENTITY

INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 434, 70, 522, 80, 511, 180, 422, 171, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId, TemplateLeftEdge, TemplateRightEdge) VALUES (@ScenePlacementId, 'A0Y', 0.33, 0.67)
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 855, 137, 996, 130, 1003, 271, 861, 278, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'B06')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 691, 361, 663, 594, 498, 576, 525, 342, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'K6U')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 306, 336, 411, 300, 446, 404, 342, 440, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 265, 286, 375, 275, 386, 385, 276, 396, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 286, 252, 393, 282, 363, 389, 256, 359, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')
GO


-- Templates
-- fake templates below
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('KX1Z9-2A215-1D9', 'K6U', 'Canvas', NULL, 'local', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('KX1Z9-2A215-1D9', 'K6U', 'Canvas', NULL, 'dev', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('KRMH8-H4A42-6L7', 'K6U', 'Canvas', NULL, 'test', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('KRMH8-H4A42-6L7', 'K6U', 'Canvas', NULL, 'prod', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('002Z9-2A326-8I2', 'BQ8', 'Photo Coaster', NULL, 'local', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('002Z9-2A326-8I2', 'BQ8', 'Photo Coaster', NULL, 'dev', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('2QRH8-H4A08-3D3', 'BQ8', 'Photo Coaster', NULL, 'test', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('2QRH8-H4A08-3D3', 'BQ8', 'Photo Coaster', NULL, 'prod', 0, 0, 1)
UPDATE Template SET UseForGiftCenter = 1 WHERE ProductId IN ('A0Y', 'B06')
GO

INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('KX1Z9-2A215-1D9', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('KRMH8-H4A42-6L7', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('002Z9-2A326-8I2', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('2QRH8-H4A08-3D3', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
