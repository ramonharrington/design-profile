USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

UPDATE Template SET ProductId = 'EAD' WHERE ProductId = 'EAE'
UPDATE Template SET UseForGiftCenter = 1 WHERE TemplateId IN ('X1215-H4A68-1F9', '4FKJL-G4A61-5F9') AND Environment = 'prod'
UPDATE Template SET UseForGiftCenter = 0 WHERE TemplateId = 'DFKJL-G4A74-3I7' AND Environment = 'prod'
GO
