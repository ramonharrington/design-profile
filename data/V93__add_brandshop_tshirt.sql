USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('R0Z4B-2A992-6S7', 'HTZ', 'Long-Sleeve T-Shirt', NULL, 'local', 0, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('R0Z4B-2A992-6S7', 'HTZ', 'Long-Sleeve T-Shirt', NULL, 'dev', 0, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('HT4Z2-J4A41-7H7', 'HTZ', 'Long-Sleeve T-Shirt', NULL, 'test', 0, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('HT4Z2-J4A41-7H7', 'HTZ', 'Long-Sleeve T-Shirt', NULL, 'prod', 0, 1, 0)
GO

UPDATE Template SET Tags = 'force-background' WHERE Name = 'Mouse Pad'
GO

INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('R0Z4B-2A992-6S7', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('HT4Z2-J4A41-7H7', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
