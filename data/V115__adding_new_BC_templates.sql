USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter, rank, Height, Width, UseForBCsLogoFlow, UseInCarousel)
VALUES
('34VFB-2A734-3L3', 'B73', 'Business Card 2', NULL, 'local', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('34VFB-2A734-3L3', 'B73', 'Business Card 2', NULL, 'dev', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('8VH6Q-L4A55-2Q6', 'B73', 'Business Card 2', NULL, 'prod', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('8VH6Q-L4A55-2Q6', 'B73', 'Business Card 2', NULL, 'test', 0, 0, 0, 0, NULL, NULL, 1, 1)


INSERT INTO TemplateItems  (template_id, item_id, configuration)
VALUES
 ('34VFB-2A734-3L3', 'buildPathBackgroundRectangle', '{"Recolor":null,"Color":null,"Font":null}')
,('34VFB-2A734-3L3', 'item_795'					,'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
,('34VFB-2A734-3L3', 'vpls_text_5'				,	'{"Recolor":"complete","Color":"primary","Font":null}')
,('34VFB-2A734-3L3', 'vpls_text_9'				,	'{"Recolor":null,"Color":null,"Font":null}')
,('34VFB-2A734-3L3', 'vpls_text_10'				,'{"Recolor":null,"Color":null,"Font":null}')
,('34VFB-2A734-3L3', 'vpls_text_11'				,'{"Recolor":null,"Color":null,"Font":null}')
,('34VFB-2A734-3L3', 'item_1005'					,'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
,('34VFB-2A734-3L3', 'item_1069'					,'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
,('34VFB-2A734-3L3', 'item_1153'					,'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
,('34VFB-2A734-3L3', 'vpls_rect_1'				,	'{"Recolor":"complete","Color":"primary","Font":null}')
,('8VH6Q-L4A55-2Q6', 'buildPathBackgroundRectangle', '{"Recolor":null,"Color":null,"Font":null}')
,('8VH6Q-L4A55-2Q6', 'item_795'					,'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
,('8VH6Q-L4A55-2Q6', 'vpls_text_5'				,	'{"Recolor":"complete","Color":"primary","Font":null}')
,('8VH6Q-L4A55-2Q6', 'vpls_text_9'				,	'{"Recolor":null,"Color":null,"Font":null}')
,('8VH6Q-L4A55-2Q6', 'vpls_text_10'				,'{"Recolor":null,"Color":null,"Font":null}')
,('8VH6Q-L4A55-2Q6', 'vpls_text_11'				,'{"Recolor":null,"Color":null,"Font":null}')
,('8VH6Q-L4A55-2Q6', 'item_1005'					,'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
,('8VH6Q-L4A55-2Q6', 'item_1069'					,'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
,('8VH6Q-L4A55-2Q6', 'item_1153'					,'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
,('8VH6Q-L4A55-2Q6', 'vpls_rect_1'				,	'{"Recolor":"complete","Color":"primary","Font":null}')
GO

