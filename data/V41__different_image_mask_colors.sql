USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE ScenePlacement ADD MaskColor VARCHAR(7)
GO

UPDATE ScenePlacement SET TopLeftX = 121, TopLeftY = 42, TopRightX = 267, TopRightY = 116, BottomRightX = 169, BottomRightY = 247, BottomLeftX = 18, BottomLeftY = 167, BottomCenterX = NULL, BottomCenterY = NULL, MaskColor = '#FF0000' where id = 17
UPDATE ScenePlacement SET TopLeftX = 121, TopLeftY = 42, TopRightX = 267, TopRightY = 119, BottomRightX = 174, BottomRightY = 234, BottomLeftX = 23, BottomLeftY = 144, BottomCenterX = 95, BottomCenterY = 194 where id = 18
GO
