USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Add scenes
DECLARE @SceneId INT
DECLARE @ScenePlacementId INT
INSERT INTO Scene (Filename, Width, Height) VALUES ('23F06339-E83C-44E1-9BAD-DB5F5C0115F5', 500, 355)
SELECT @SceneId = @@IDENTITY

INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel)
	VALUES (@SceneId, 179, 138, 321, 138, 321, 249, 179, 249, 0.95)
SELECT @ScenePlacementId = @@IDENTITY

INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'AFB')
GO

DECLARE @SceneId INT
DECLARE @ScenePlacementId INT
INSERT INTO Scene (Filename, Width, Height) VALUES ('30C7A67D-15B9-40EA-B1D3-7165998F5FD3', 500, 481)
SELECT @SceneId = @@IDENTITY

INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel)
	VALUES (@SceneId, 166, 226, 362, 222, 364, 326, 168, 331, 0.95)
SELECT @ScenePlacementId = @@IDENTITY

INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'HXP')
GO

DECLARE @SceneId INT
DECLARE @ScenePlacementId INT
INSERT INTO Scene (Filename, Width, Height) VALUES ('ED665435-8EA1-4191-9A72-2E9C7ADD571A', 500, 528)
SELECT @SceneId = @@IDENTITY

INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel)
	VALUES (@SceneId, 187, 219, 312, 219, 312, 344, 187, 344, 0.95)
SELECT @ScenePlacementId = @@IDENTITY

INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'EAD')
GO


DELETE FROM TemplateItems WHERE template_id IN ('JB5W9-2A206-4R5', 'N07X9-2A530-5K4', 'BFBM4-H4A41-8R0', 'H70N1-H4A96-6P0')
GO

-- Add templates
-- Stickers
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('JB5W9-2A206-4R5', 'AFB', 'Large Circle Sticker', NULL, 'local', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('JB5W9-2A206-4R5', 'AFB', 'Large Circle Sticker', NULL, 'dev', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('BFBM4-H4A41-8R0', 'AFB', 'Large Circle Sticker', NULL, 'test', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('BFBM4-H4A41-8R0', 'AFB', 'Large Circle Sticker', NULL, 'prod', 1, 1, 0)
GO

INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('JB5W9-2A206-4R5', 'item_412', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('BFBM4-H4A41-8R0', 'item_412', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO

-- Bags
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('N07X9-2A530-5K4', 'HXP', 'Paper Bag', NULL, 'local', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('N07X9-2A530-5K4', 'HXP', 'Paper Bag', NULL, 'dev', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('H70N1-H4A96-6P0', 'HXP', 'Paper Bag', NULL, 'test', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('H70N1-H4A96-6P0', 'HXP', 'Paper Bag', NULL, 'prod', 1, 1, 0)
GO

INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('N07X9-2A530-5K4', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('H70N1-H4A96-6P0', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO

-- Ladies T-Shirts
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('LDHL9-2A716-5K5', 'EAD', 'Women''s T-shirt', NULL, 'local', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('LDHL9-2A716-5K5', 'EAD', 'Women''s T-shirt', NULL, 'dev', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('J26XV-G4A18-3J4', 'EAD', 'Women''s T-shirt', NULL, 'test', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('J26XV-G4A18-3J4', 'EAD', 'Women''s T-shirt', NULL, 'prod', 1, 1, 0)
GO
