USE [design_profile]
GO

UPDATE Template set TemplateId = 'X1215-H4A68-1F9' where Id in (102,103) --prod w
UPDATE Template set TemplateId = '8KBX9-2A871-7O6' where Id in (100,101) --dev w
UPDATE Template set TemplateId = 'B4L15-H4A68-0T5' where Id in (86) --prod m
UPDATE Template set TemplateId = 'GPBX9-2A221-6N6' where Id in (82,84) --dev m
GO

INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('8KBX9-2A871-7O6', 'item_332', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('X1215-H4A68-1F9', 'item_332', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('B4L15-H4A68-0T5', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('GPBX9-2A221-6N6', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
