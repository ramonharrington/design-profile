USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('BXRRQ-J4A81-0G8', 'vpls_rect_1', '{"Recolor":"complete","Color":"primary","Font":null}')
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('WB87B-2A026-2W6', 'vpls_rect_1', '{"Recolor":"complete","Color":"primary","Font":null}')
GO

UPDATE Template SET Tags = 'force-background' WHERE TemplateId IN ('BXRRQ-J4A81-0G8', 'WB87B-2A026-2W6')
GO
