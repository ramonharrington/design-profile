USE [design_profile]
GO

ALTER TABLE ScenePlacement ADD TopLeftX INT NOT NULL, TopLeftY INT NOT NULL, TopRightX INT NOT NULL, TopRightY INT NOT NULL, BottomRightX INT NOT NULL, BottomRightY INT NOT NULL, BottomLeftX INT NOT NULL, BottomLeftY INT NOT NULL
GO

ALTER TABLE ScenePlacement DROP COLUMN TopLeft, TopRight, BottomRight, BottomLeft
GO

SET IDENTITY_INSERT Scene ON
GO

INSERT INTO Scene (Id, Filename) VALUES (1, 'B829D326-9038-4034-BBFC-D7F4E07A3D31')
INSERT INTO Scene (Id, Filename) VALUES (2, '5AF6AD29-84CC-46EF-A212-A7E48103CF4F')

SET IDENTITY_INSERT Scene OFF
GO

SET IDENTITY_INSERT SceneProduct ON
GO

INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (1, 1, 'B73')
INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (2, 2, 'B06')

SET IDENTITY_INSERT SceneProduct OFF
GO

SET IDENTITY_INSERT ScenePlacement ON
GO

INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (1, 1, 301, 194, 1004, 203, 997, 588, 284, 576, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (2, 2, 460, 259, 828, 259, 810, 523, 476, 523, 0.95)

SET IDENTITY_INSERT ScenePlacement OFF
GO

