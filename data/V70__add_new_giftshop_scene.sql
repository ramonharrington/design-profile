USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @SceneId INT
DECLARE @ScenePlacementId INT

INSERT INTO Scene (Filename, Height, Width) VALUES ('08901329-E1B9-49FE-8FEB-EBCE797EC7CC', 1788, 1440)
SELECT @SceneId = @@IDENTITY

INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 454, 229, 542, 239, 531, 338, 442, 329, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'A0Y')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 852, 313, 992, 299, 1006, 452, 866, 466, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'HJA')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 534, 511, 688, 529, 662, 745, 510, 727, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'K6U')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 319, 504, 417, 470, 450, 568, 352, 600, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 280, 258, 384, 448, 395, 550, 291, 560, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 301, 427, 400, 455, 373, 553, 273, 526, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')

-- Puzzle
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 344, 817, 493, 785, 516, 891, 367, 923, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BR3')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 344, 775, 488, 795, 472, 912, 329, 891, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BR3')

-- Calendar Poster
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 609, 874, 887, 836, 912, 1017, 632, 1055, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '645')

-- Photo Magnet
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 991, 988, 1080, 1004, 1058, 1120, 969, 1104, 0.95, '#00FF00')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 999, 982, 1089, 989, 1080, 1106, 990, 1099, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 1007, 991, 1097, 998, 1088, 1116, 997, 1109, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 1016, 1008, 1104, 991, 1125, 1106, 1036, 1123, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')

-- Women's T-shirt
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 383, 1197, 509, 1199, 506, 1337, 380, 1335, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'EAD')

-- Mouse pad
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 714, 1275, 866, 1222, 912, 1350, 757, 1403, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '679')

-- Calendar magnet
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 974, 1399, 1095, 1381, 1108, 1460, 986, 1478, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 975, 1410, 1099, 1400, 1106, 1482, 983, 1488, 0.95, '#00FF00')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 978, 1415, 1102, 1415, 1102, 1498, 978, 1498, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 974, 1427, 1095, 1445, 1084, 1525, 966, 1507, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')


-- Templates
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('LKDZ9-2A924-3F9', 'BR3', 'Puzzle', NULL, 'local', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('LKDZ9-2A924-3F9', 'BR3', 'Puzzle', NULL, 'dev', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('BB9WB-H4A94-4T8', 'BR3', 'Puzzle', NULL, 'test', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('BB9WB-H4A94-4T8', 'BR3', 'Puzzle', NULL, 'prod', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('PLHZ9-2A150-3K5', '645', 'Poster calendar', NULL, 'local', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('PLHZ9-2A150-3K5', '645', 'Poster calendar', NULL, 'dev', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('B5ZVB-H4A48-7U7', '645', 'Poster calendar', NULL, 'test', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('B5ZVB-H4A48-7U7', '645', 'Poster calendar', NULL, 'prod', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('QLHZ9-2A179-4O8', '218', 'Calendar magnet', NULL, 'local', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('QLHZ9-2A179-4O8', '218', 'Calendar magnet', NULL, 'dev', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('25ZVB-H4A55-2Y7', '218', 'Calendar magnet', NULL, 'test', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('25ZVB-H4A55-2Y7', '218', 'Calendar magnet', NULL, 'prod', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('NLHZ9-2A281-5L1', '220', 'Postcard magnet', NULL, 'local', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('NLHZ9-2A281-5L1', '220', 'Postcard magnet', NULL, 'dev', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('N5ZVB-H4A47-0P7', '220', 'Postcard magnet', NULL, 'test', 0, 0, 1)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('N5ZVB-H4A47-0P7', '220', 'Postcard magnet', NULL, 'prod', 0, 0, 1)
GO

INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('LKDZ9-2A924-3F9', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('BB9WB-H4A94-4T8', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('PLHZ9-2A150-3K5', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('B5ZVB-H4A48-7U7', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('QLHZ9-2A179-4O8', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('25ZVB-H4A55-2Y7', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('NLHZ9-2A281-5L1', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('N5ZVB-H4A47-0P7', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO

-- Change Product Id of Men's T-shirts
UPDATE SceneProduct SET ProductId = 'HJA' WHERE ProductId = 'B06'
UPDATE Template SET ProductId = 'HJA' WHERE ProductId = 'B06'
GO


-- Update retired mug document
UPDATE Template SET TemplateId = 'XBJZ9-2A038-7G3' WHERE TemplateId = '14QX9-2A492-6M2'
UPDATE TemplateItems SET Template_Id = 'XBJZ9-2A038-7G3' WHERE Template_Id = '14QX9-2A492-6M2'
GO

-- Update t-shirts product id
UPDATE Template SET ProductId = 'HJA' WHERE ProductId IN ('HJA','HJB','B06')
UPDATE SceneProduct SET ProductId = 'HJA' WHERE ProductId IN ('HJA','HJB','B06')
GO

-- Enable products for gift shop
UPDATE Template SET UseForGiftCenter = 1 WHERE TemplateId IN ('XRPX9-2A177-8L1', 'GWDZ6-H4A14-1D9', 'N0GK9-2A238-1G0')
GO
