USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER TABLE SceneProduct ADD TemplateTopEdge FLOAT NULL, TemplateRightEdge FLOAT NULL, TemplateBottomEdge FLOAT NULL, TemplateLeftEdge FLOAT NULL
GO

UPDATE Scene SET Filename = '2712A10E-E619-4B40-81D8-9826E86A63AF', Height = 319, Width = 500 WHERE Id = 5
GO
UPDATE SceneProduct SET ProductId = '679' WHERE SceneId = 5
GO
UPDATE ScenePlacement SET TopLeftX = 97, TopLeftY = 31, TopRightX = 406, TopRightY = 29, BottomRightX = 406, BottomRightY = 284, BottomLeftX = 97,
	BottomLeftY = 283, TopCenterX = NULL, TopCenterY = NULL, BottomCenterX = NULL, BottomCenterY = NULL WHERE SceneId = 5
GO

-- Fix mug rendering to only render left side of design
UPDATE SceneProduct SET TemplateRightEdge = 0.45 WHERE ProductId = 'A14'
GO

SET IDENTITY_INSERT Scene ON
GO

INSERT INTO Scene (Id, Filename) VALUES (8, '2712A10E-E619-4B40-81D8-9826E86A63AF')

SET IDENTITY_INSERT Scene OFF
GO

SET IDENTITY_INSERT SceneProduct ON
GO

INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (10, 8, '372')

SET IDENTITY_INSERT SceneProduct OFF
GO

SET IDENTITY_INSERT ScenePlacement ON
GO

INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (8, 8, 158, 166, 332, 166, 332, 340, 158, 340, 0.95)

SET IDENTITY_INSERT ScenePlacement OFF
GO

