USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


SET IDENTITY_INSERT Scene ON
GO

INSERT INTO Scene (Id, Filename, Height, Width) VALUES (14, '8E59D221-C533-4304-8D41-6C951414D25B', 380, 500)
INSERT INTO Scene (Id, Filename, Height, Width) VALUES (15, 'AEDDDC71-49CE-4E35-96BD-DFE9336945FD', 459, 500)
INSERT INTO Scene (Id, Filename, Height, Width) VALUES (16, '58EEAFDD-BED6-4F05-B823-32251804FC75', 380, 500)
INSERT INTO Scene (Id, Filename, Height, Width) VALUES (17, '48C93A4F-7B19-4A09-9F79-E5B3F521F4CB', 640, 500)
GO

SET IDENTITY_INSERT Scene OFF
GO

SET IDENTITY_INSERT SceneProduct ON
GO

INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (16, 14, 'A8V')
INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (17, 15, '236')
INSERT INTO SceneProduct (Id, SceneId, ProductId) VALUES (18, 16, '331')
INSERT INTO SceneProduct (Id, SceneId, ProductId, TemplateTopEdge, TemplateBottomEdge) VALUES (19, 17, '376', 0.33, 0.67)
GO

SET IDENTITY_INSERT SceneProduct OFF
GO

SET IDENTITY_INSERT ScenePlacement ON
GO

INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (14, 14, 194, 89, 310, 89, 310, 205, 194, 205, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (15, 15, 13, 153, 208, 12, 312, 147, 101, 302, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (16, 15, 229, 238, 472, 235, 480, 427, 231, 431, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (17, 16, 121, 42, 267, 116, 169, 247, 18, 167, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, BottomCenterX, BottomCenterY, ShadowLevel) VALUES (18, 16, 121, 42, 267, 116, 169, 231, 22, 147, 97, 190, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (19, 16, 309, 33, 481, 68, 440, 217, 262, 178, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (20, 17, 163, 486, 173, 327, 199, 328, 188, 487, 0.95)
INSERT INTO ScenePlacement (Id, SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (21, 17, 371, 207, 430, 60, 453, 70, 396, 217, 0.95)
GO

SET IDENTITY_INSERT ScenePlacement OFF
GO

UPDATE Scene set Height = 319 WHERE Id = 5
UPDATE Scene set Height = 528 WHERE Id = 11
UPDATE Scene set Height = 459 WHERE Id = 12
UPDATE Scene set Height = 432 WHERE Id = 13
GO
