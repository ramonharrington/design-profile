USE [design_profile]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[event_tracking](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[page_url] [varchar](150) NULL,
	[event_type] [varchar](150) NULL,
	[event_value] [varchar](150) NULL,
	[user_id_type] [varchar](100) NULL,
	[user_id] [varchar](100) NULL,
	[channel] [varchar](200) NULL,
	[ip_address_created] [varchar](20) NULL,
	[referrer] [varchar](max) NULL,
 CONSTRAINT [PK_event_tracking] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[event_tracking] ADD  DEFAULT (getdate()) FOR [timestamp]
GO

