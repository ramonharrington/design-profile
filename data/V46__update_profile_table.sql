USE [design_profile]
GO

CREATE TABLE [dbo].[ProfileType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ProfileType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE Profile ADD ProfileTypeId INT CONSTRAINT DF_Profile_ProfileType DEFAULT 1 NOT NULL
GO

ALTER TABLE Profile DROP CONSTRAINT DF_Profile_ProfileType
GO

SET IDENTITY_INSERT ProfileType ON
GO

INSERT INTO ProfileType (Id, Name) VALUES (1, 'Logo Center')
INSERT INTO ProfileType (Id, Name) VALUES (2, 'Gift Shop')
GO

ALTER TABLE [dbo].[Profile]  WITH CHECK ADD  CONSTRAINT [FK_Profile_ProfileType] FOREIGN KEY([ProfileTypeId])
REFERENCES [dbo].[ProfileType] ([Id])
GO
