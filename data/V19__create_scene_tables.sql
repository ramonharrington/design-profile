USE [design_profile]
GO
/****** Object:  Table [dbo].[Account] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Scene](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Filename] [nvarchar](255) NOT NULL
 CONSTRAINT [PK_Scene] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SceneProduct] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SceneProduct](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SceneId] [int] NOT NULL,
	[ProductId] [nvarchar](10) NOT NULL
 CONSTRAINT [PK_SceneProduct] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScenePlacement] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScenePlacement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SceneId] [int] NOT NULL,
	[TopLeft] [int] NOT NULL,
	[TopRight] [int] NOT NULL,
	[BottomRight] [int] NOT NULL,
	[BottomLeft] [int] NOT NULL,
	[ShadowLevel] [decimal] NULL
 CONSTRAINT [PK_ScenePlacement] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[SceneProduct]  WITH CHECK ADD  CONSTRAINT [FK_SceneProduct_Scene] FOREIGN KEY([SceneId])
REFERENCES [dbo].[Scene] ([Id])
GO
ALTER TABLE [dbo].[SceneProduct] CHECK CONSTRAINT [FK_SceneProduct_Scene]
GO
ALTER TABLE [dbo].[ScenePlacement]  WITH CHECK ADD  CONSTRAINT [FK_ScenePlacement_Scene] FOREIGN KEY([SceneId])
REFERENCES [dbo].[Scene] ([Id])
GO
ALTER TABLE [dbo].[ScenePlacement] CHECK CONSTRAINT [FK_ScenePlacement_Scene]
GO
