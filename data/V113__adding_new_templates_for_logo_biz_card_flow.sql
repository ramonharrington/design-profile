USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE Template
ADD UseForBCsLogoFlow BIT NOT NULL DEFAULT 0,
UseInCarousel BIT NOT NULL DEFAULT 0
GO

UPDATE Template
SET UseForBCsLogoFlow = 1,
UseInCarousel = 1
WHERE ProductId = 'B73'

UPDATE Template
SET UseForBCsLogoFlow = 1
WHERE ProductId in ('A0Y', '376', 'A0B')

GO