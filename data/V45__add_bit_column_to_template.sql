USE [design_profile]
GO

ALTER TABLE Template ADD UseForLogoCenter BIT NOT NULL DEFAULT 0
ALTER TABLE Template ADD UseForGiftCenter BIT NOT NULL DEFAULT 0
GO

UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 34
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 35
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 36
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 37
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 38
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 39
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 40
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 41
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 42
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 43
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 44
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 45
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 46
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 47
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 48
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 49
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 50
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 51
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 52
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 53
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 54
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 55
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 56
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 57
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 58
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 59
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 60
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 61
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 62
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 63
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 64
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 65
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 66
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 67
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 68
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 69
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 70
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 71
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 72
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 73
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 74
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 75
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 76
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 77
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 78
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 79
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 80
UPDATE Template SET UseForLogoCenter = 0 WHERE Id = 81
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 82
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 83
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 84
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 85
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 86
UPDATE Template SET UseForLogoCenter = 1 WHERE Id = 87
GO