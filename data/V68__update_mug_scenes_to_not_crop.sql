USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

UPDATE Template SET UseForGiftCenter = 0 WHERE TemplateId = 'Q0GK9-2A370-1N9'
UPDATE SceneProduct SET TemplateLeftEdge = NULL, TemplateRightEdge = NULL WHERE ProductId = 'A0Y'
GO
