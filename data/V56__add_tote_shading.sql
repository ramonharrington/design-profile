USE [design_profile]
GO

WITH cteScene AS (
	SELECT p1.* FROM ScenePlacement p1 LEFT JOIN SceneProduct p2 ON p1.Id = p2.ScenePlacementId WHERE p2.ProductId = 'A0B'
)
UPDATE cteScene SET ShadowLevel = 0.6
GO
