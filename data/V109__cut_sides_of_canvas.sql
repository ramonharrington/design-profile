USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

UPDATE SceneProduct SET TemplateTopEdge = '0.12', TemplateRightEdge = '0.84', TemplateBottomEdge = '0.88', TemplateLeftEdge = '0.16' WHERE ProductId = 'BPT'
GO
