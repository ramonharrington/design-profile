USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[event_tracking] ADD user_agent VARCHAR(255) NULL
GO
