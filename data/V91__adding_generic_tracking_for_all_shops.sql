SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ShopTracking](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ProfileId] [int] NOT NULL,
	[PfId] [varchar](40) NULL,
	[AltDocId] [varchar](20) NULL,
	[ComboId] [int] NULL,
	[DesignTemplateId] [varchar](20) NULL,
	[SceneId] [int] NULL,
	[ProfileSettings] [nvarchar](max) NULL,
	[PreviewEventType] [varchar](50) NOT NULL,
	[PreviewEventDate] [datetime] NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ShopperKey] [int] NULL,
	[Locale] [varchar](100) NULL,
	[UserAgent] [varchar](512) NULL,
	[Referer] [varchar](1000)  NULL,
	[Channel] [varchar](50) NULL,
	[PageUrl] [varchar](1000) NULL,
	[IpAddress] [varchar](20) NULL
 CONSTRAINT [PK_ShopTracking] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


