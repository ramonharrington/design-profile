USE [design_profile]
GO

ALTER TABLE Scene ADD Height INT NULL, Width INT NULL
GO

UPDATE Scene SET Height = 783, Width = 1290 WHERE ID = 1
UPDATE Scene SET Height = 783, Width = 1290 WHERE ID = 2
UPDATE Scene SET Height = 522, Width = 860 WHERE ID = 3
UPDATE Scene SET Height = 522, Width = 860 WHERE ID = 4
UPDATE Scene SET Height = 522, Width = 860 WHERE ID = 5
UPDATE Scene SET Height = 522, Width = 860 WHERE ID = 6
UPDATE Scene SET Height = 522, Width = 860 WHERE ID = 7
