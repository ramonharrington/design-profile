USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @SceneId INT
DECLARE @ScenePlacementId INT

-- Hat
INSERT INTO Scene (Filename, Height, Width) VALUES ('6819CFA7-BA1D-4DC4-B097-3AFF7FB2C077', 380, 500)
SELECT @SceneId = @@IDENTITY
INSERT INTO ScenePlacement (SceneId, ShadowLevel, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY) VALUES (@SceneId, 1, 180, 116, 307, 116, 307, 191, 180, 191)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '057')

-- Long-sleeve T-shirt
INSERT INTO Scene (Filename, Height, Width) VALUES ('B03ECA46-09B0-40D0-87A2-0E7CCDBB1190', 380, 500)
SELECT @SceneId = @@IDENTITY
INSERT INTO ScenePlacement (SceneId, ShadowLevel, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY) VALUES (@SceneId, 1, 190, 102, 295, 102, 295, 207, 190, 207)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'HTZ')
GO
