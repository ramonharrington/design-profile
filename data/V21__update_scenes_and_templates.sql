USE [design_profile]
GO

UPDATE ScenePlacement SET TopLeftX = 269, TopLeftY = 138, TopRightX = 976, TopRightY = 147, BottomRightX = 970, BottomRightY = 536, BottomLeftX = 253, BottomLeftY = 525 WHERE Id = 1
INSERT INTO SceneProduct (SceneId, ProductId) VALUES (2, '372')

TRUNCATE TABLE TemplateItems

SET IDENTITY_INSERT [dbo].[TemplateItems] ON 

GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'NLKB9-2A135-8M8', N'vpls_img_6', N'{"ImageUsage":"logo","Recolor":"complete","Color":"","Font":""}', 95)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'vpls_img_3', N'{"ImageUsage":"placeholder","Recolor":"","Color":"","Font":""}', 105)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'vpls_img_2', N'{"ImageUsage":"placeholder","Recolor":"","Color":"","Font":""}', 106)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'vpls_img_1', N'{"ImageUsage":"placeholder","Recolor":"","Color":"","Font":""}', 107)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'item_1042', N'{"ImageUsage":"","Recolor":"complete","Color":"primary","Font":""}', 108)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'item_659', N'{"ImageUsage":"logo","Recolor":"complete","Color":"","Font":""}', 109)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'NLKB9-2A135-8M8', N'vpls_img_1', N'{"ImageUsage":"logo","Recolor":"complete","Color":"","Font":""}', 110)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_img_2', N'{"ImageUsage":"picture","Recolor":"","Color":"","Font":""}', 111)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_img_1', N'{"ImageUsage":"picture","Recolor":"","Color":"","Font":""}', 112)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_img_3', N'{"ImageUsage":"placeholder","Recolor":"","Color":"","Font":""}', 113)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'item_694', N'{"ImageUsage":"logo","Recolor":"","Color":"","Font":""}', 114)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'TGNB9-2A083-6U3', N'vpls_img_3', N'{"ImageUsage":"placeholder","Recolor":"hue","Color":"primary","Font":""}', 115)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'TGNB9-2A083-6U3', N'vpls_shape_5', N'{"Recolor":"","Color":"","Font":"secondary"}', 116)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'TGNB9-2A083-6U3', N'vpls_shape_6', N'{"Recolor":"","Color":"","Font":"secondary"}', 117)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'TGNB9-2A083-6U3', N'vpls_img_1', N'{"ImageUsage":"placeholder","Recolor":"complete","Color":"","Font":""}', 118)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'TGNB9-2A083-6U3', N'vpls_img_2', N'{"ImageUsage":"placeholder","Recolor":"complete","Color":"","Font":""}', 119)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'item_653', N'{"ImageUsage":"logo","Recolor":"complete","Color":"","Font":""}', 120)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 121)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'QHNB9-2A258-6J8', N'item_593', N'{"ImageUsage":"logo","Recolor":"complete","Color":"","Font":""}', 122)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'VHNB9-2A780-2C4', N'vpls_img_3', N'{"ImageUsage":"placeholder","Recolor":"","Color":"","Font":""}', 123)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'VHNB9-2A780-2C4', N'item_508', N'{"ImageUsage":"logo","Recolor":"complete","Color":"","Font":""}', 124)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'VHNB9-2A780-2C4', N'vpls_img_1', N'{"ImageUsage":"placeholder","Recolor":"complete","Color":"primary","Font":""}', 125)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_text_4', N'{"Recolor":"complete","Color":"primary","Font":"primary"}', 126)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_text_5', N'{"Recolor":"","Color":"","Font":"primary"}', 127)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_text_6', N'{"Recolor":"","Color":"","Font":"primary"}', 128)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_text_8', N'{"Recolor":"complete","Color":"primary","Font":"primary"}', 129)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_text_9', N'{"Recolor":"","Color":"","Font":"secondary"}', 130)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_text_10', N'{"Recolor":"","Color":"","Font":"secondary"}', 131)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_text_11', N'{"Recolor":"","Color":"","Font":"secondary"}', 132)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_text_12', N'{"Recolor":"complete","Color":"primary","Font":"primary"}', 133)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_text_13', N'{"Recolor":"complete","Color":"primary","Font":"primary"}', 134)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'vpls_text_5', N'{"Recolor":"","Color":"","Font":"primary"}', 135)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'vpls_text_6', N'{"Recolor":"","Color":"","Font":"secondary"}', 136)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'vpls_text_7', N'{"Recolor":"","Color":"","Font":"primary"}', 137)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'vpls_text_8', N'{"Recolor":"","Color":"","Font":"primary"}', 138)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'vpls_shape_12', N'{"Recolor":"","Color":"","Font":"primary"}', 139)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'vpls_text_9', N'{"Recolor":"","Color":"","Font":"primary"}', 140)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'vpls_text_10', N'{"Recolor":"","Color":"","Font":"primary"}', 141)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'vpls_text_11', N'{"Recolor":"","Color":"","Font":"primary"}', 142)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8HNB9-2A186-7T8', N'vpls_line_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 143)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'vpls_text_6', N'{"Recolor":"","Color":"","Font":"secondary"}', 144)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'vpls_text_7', N'{"Recolor":"","Color":"","Font":"primary"}', 145)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'vpls_text_8', N'{"Recolor":"","Color":"","Font":"primary"}', 146)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'vpls_text_9', N'{"Recolor":"","Color":"","Font":"primary"}', 147)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'vpls_text_10', N'{"Recolor":"","Color":"","Font":"primary"}', 148)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'2FNB9-2A145-4B9', N'vpls_text_11', N'{"Recolor":"","Color":"","Font":"primary"}', 149)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'NLKB9-2A135-8M8', N'vpls_text_2', N'{"Recolor":"","Color":"","Font":"primary"}', 150)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'NLKB9-2A135-8M8', N'vpls_text_3', N'{"Recolor":"","Color":"","Font":"secondary"}', 151)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'NLKB9-2A135-8M8', N'vpls_text_4', N'{"Recolor":"","Color":"","Font":"primary"}', 152)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'NLKB9-2A135-8M8', N'vpls_text_5', N'{"Recolor":"","Color":"","Font":"secondary"}', 153)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'QHNB9-2A258-6J8', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 154)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'QHNB9-2A258-6J8', N'vpls_text_2', N'{"Recolor":"","Color":"","Font":"secondary"}', 155)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'QHNB9-2A258-6J8', N'vpls_text_5', N'{"Recolor":"","Color":"","Font":"primary"}', 156)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'QHNB9-2A258-6J8', N'vpls_text_6', N'{"Recolor":"","Color":"","Font":"primary"}', 157)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'QHNB9-2A258-6J8', N'vpls_text_7', N'{"Recolor":"","Color":"","Font":"primary"}', 158)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'QHNB9-2A258-6J8', N'item_2221', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"secondary"}', 159)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'TGNB9-2A083-6U3', N'vpls_shape_7', N'{"Recolor":"","Color":"","Font":"secondary"}', 160)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'TGNB9-2A083-6U3', N'vpls_shape_8', N'{"Recolor":"","Color":"","Font":"secondary"}', 161)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'TGNB9-2A083-6U3', N'vpls_shape_9', N'{"Recolor":"","Color":"","Font":"primary"}', 162)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'TGNB9-2A083-6U3', N'vpls_shape_10', N'{"Recolor":"","Color":"","Font":"primary"}', 163)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'TGNB9-2A083-6U3', N'vpls_text_4', N'{"Recolor":"","Color":"","Font":"secondary"}', 164)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'VHNB9-2A780-2C4', N'vpls_img_2', N'{"Recolor":"complete","Color":"secondary","Font":"","ImageUsage":""}', 165)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'VHNB9-2A780-2C4', N'vpls_text_1', N'{"Recolor":"secondary","Color":"","Font":"primary"}', 166)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'01229-2A107-5M1', N'item_407', N'{"Recolor":"complete","Color":"primary","Font":""}', 225)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'01229-2A107-5M1', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 226)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'04429-2A596-3E8', N'vpls_line_1', N'{"Recolor":"complete","Color":"secondary","Font":""}', 227)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'04429-2A596-3E8', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 228)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'04429-2A596-3E8', N'vpls_text_5', N'{"Recolor":"complete","Color":"secondary","Font":""}', 229)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8F329-2A572-9W9', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 230)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8F329-2A572-9W9', N'vpls_text_2', N'{"Recolor":"complete","Color":"","Font":"primary"}', 231)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8F329-2A572-9W9', N'vpls_text_3', N'{"Recolor":"complete","Color":"","Font":"primary"}', 232)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8F329-2A572-9W9', N'vpls_text_4', N'{"Recolor":"complete","Color":"","Font":"primary"}', 233)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8F329-2A572-9W9', N'vpls_text_5', N'{"Recolor":"complete","Color":"","Font":"primary"}', 234)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8F329-2A572-9W9', N'vpls_text_6', N'{"Recolor":"complete","Color":"","Font":"primary"}', 235)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8F329-2A572-9W9', N'vpls_text_8', N'{"Recolor":"complete","Color":"","Font":"secondary"}', 236)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'G2229-2A845-4J8', N'item_2221', N'{"ImageUsage":null,"Recolor":"complete","Color":"secondary","Font":""}', 237)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'G2229-2A845-4J8', N'item_2349', N'{"ImageUsage":null,"Recolor":"complete","Color":"secondary","Font":""}', 238)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'G2229-2A845-4J8', N'vpls_line_1', N'{"Recolor":"complete","Color":"secondary","Font":""}', 239)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'G2229-2A845-4J8', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 240)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'G2229-2A845-4J8', N'vpls_text_2', N'{"Recolor":"complete","Color":"secondary","Font":""}', 241)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'G2229-2A845-4J8', N'vpls_text_5', N'{"Recolor":"complete","Color":"secondary","Font":""}', 242)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'G2229-2A845-4J8', N'vpls_text_6', N'{"Recolor":"complete","Color":"secondary","Font":""}', 243)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'G2229-2A845-4J8', N'vpls_text_7', N'{"Recolor":"complete","Color":"secondary","Font":""}', 244)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_img_695', N'{"ImageUsage":"logo","Recolor":"","Color":"","Font":""}', 245)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_img_696', N'{"ImageUsage":"picture","Recolor":"","Color":"","Font":""}', 246)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'GFNB9-2A625-4X7', N'vpls_img_697', N'{"ImageUsage":"picture","Recolor":"","Color":"","Font":""}', 247)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_img_848', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"none"}', 248)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_img_849', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"picture"}', 249)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_img_850', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"logo"}', 250)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_text_10', N'{"Recolor":"complete","Color":"secondary","Font":"secondary"}', 251)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_text_11', N'{"Recolor":"complete","Color":"secondary","Font":"secondary"}', 252)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_text_3', N'{"Recolor":"complete","Color":"primary","Font":"primary"}', 253)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_text_4', N'{"Recolor":"complete","Color":"secondary","Font":"primary"}', 254)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_text_5', N'{"Recolor":"complete","Color":"primary","Font":"primary"}', 255)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_text_6', N'{"Recolor":"complete","Color":"secondary","Font":"primary"}', 256)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_text_7', N'{"Recolor":"complete","Color":"primary","Font":"secondary"}', 257)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_text_8', N'{"Recolor":"complete","Color":"primary","Font":"secondary"}', 258)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'HVC39-2A881-7K3', N'vpls_text_9', N'{"Recolor":"complete","Color":"secondary","Font":"secondary"}', 259)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'KLTV8-2A215-9K9', N'vpls_img_1', N'{"ImageUsage":null,"Recolor":"false","Color":"","Font":""}', 260)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'LPTV8-2A871-4N7', N'item_435', N'{"ImageUsage":null,"Recolor":"complete","Color":"primary","Font":""}', 261)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'LPTV8-2A871-4N7', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 262)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'NPTV8-2A267-0P5', N'vpls_oval_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 263)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'PT129-2A296-8L4', N'item_377', N'{"ImageUsage":null,"Recolor":"complete","Color":"primary","Font":""}', 264)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'PT129-2A296-8L4', N'vpls_rect_1', N'{"Recolor":"complete","Color":"tertiary","Font":""}', 265)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'RPTV8-2A955-0M6', N'item_524', N'{"ImageUsage":null,"Recolor":"complete","Color":"primary","Font":"primary"}', 266)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'RPTV8-2A955-0M6', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 267)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'4QTV8-2A837-5O8', N'item_1977', N'{"ImageUsage":null,"Recolor":"complete","Color":"secondary","Font":"primary"}', 268)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'4QTV8-2A837-5O8', N'item_2038', N'{"ImageUsage":null,"Recolor":"complete","Color":"secondary","Font":"primary"}', 269)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'4QTV8-2A837-5O8', N'item_2106', N'{"ImageUsage":null,"Recolor":"complete","Color":"secondary","Font":"primary"}', 270)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'4QTV8-2A837-5O8', N'item_2233', N'{"ImageUsage":null,"Recolor":"complete","Color":"secondary","Font":"primary"}', 271)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'4QTV8-2A837-5O8', N'item_2289', N'{"ImageUsage":null,"Recolor":"complete","Color":"secondary","Font":"primary"}', 272)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'4QTV8-2A837-5O8', N'item_524', N'{"ImageUsage":null,"Recolor":"complete","Color":"secondary","Font":"primary"}', 273)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'4QTV8-2A837-5O8', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":""}', 274)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'04429-2A596-3E8', N'item_653', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"logo"}', 275)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'4QTV8-2A837-5O8', N'item_477', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"logo"}', 276)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'8F329-2A572-9W9', N'item_560', N'{"ImageUsage":"logo","Recolor":"","Color":"","Font":""}', 277)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'G2229-2A845-4J8', N'item_593', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"logo"}', 278)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'KLTV8-2A215-9K9', N'item_355', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"logo"}', 279)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'LPTV8-2A871-4N7', N'item_355', N'{"Recolor":"","Color":"","Font":"logo","ImageUsage":""}', 280)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'NPTV8-2A267-0P5', N'item_355', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"logo"}', 281)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'PT129-2A296-8L4', N'item_414', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"logo"}', 282)
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration], [Id]) VALUES (N'RPTV8-2A955-0M6', N'item_477', N'{"Recolor":"","Color":"","Font":"","ImageUsage":"logo"}', 283)
GO
SET IDENTITY_INSERT [dbo].[TemplateItems] OFF
GO
