USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE ContinueFromAnotherDevice
ADD email_sent_at datetime NULL,
email_clicked_at datetime NULL,
user_agent_email_send varchar(1000) NULL,
user_agent_clickthrough varchar(1000) NULL,
edit_url varchar(1000) NULL,
preview_url varchar(1000) NULL

GO
