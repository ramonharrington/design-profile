USE [design_profile]
GO

DELETE FROM PreviewTracking WHERE SceneId = 14
DELETE FROM SceneProduct WHERE ScenePlacementId = 14
DELETE FROM ScenePlacement Where SceneId = 14
DELETE FROM Scene WHERE Id = 14
GO

UPDATE Template SET ProductId = 'A8V', TemplateId = '0PZ97-H4A04-7S9' WHERE ProductId IN ('A8V','EAD') AND Environment IN ('test', 'prod')
UPDATE Template SET ProductId = 'A8V', TemplateId = 'XRPX9-2A177-8L1' WHERE ProductId IN ('A8V','EAD') AND Environment IN ('local', 'dev')
UPDATE Template SET ProductId = 'B06', TemplateId = 'LDHL9-2A716-5K5' where ProductId = 'HJB' AND Environment IN ('local', 'dev')
UPDATE Template SET ProductId = 'B06', TemplateId = 'J26XV-G4A18-3J4' where ProductId = 'HJB' AND Environment IN ('test', 'prod')
GO

UPDATE TemplateItems SET Item_Id = 'vpls_img_1', Template_Id = '0PZ97-H4A04-7S9' WHERE Template_Id IN ('J26XV-G4A18-3J4', '2RG97-H4A31-3L3', '0PZ97-H4A04-7S9')
UPDATE TemplateItems SET Item_Id = 'vpls_img_1', Template_Id = 'J26XV-G4A18-3J4' WHERE Template_Id IN ('J26XV-G4A18-3J4')
GO

UPDATE SceneProduct SET ProductId = 'A8V' WHERE ProductId = 'EAD'
GO

DELETE FROM TemplateItems WHERE template_id = 'J26XV-G4A18-3J4'
GO
INSERT INTO TemplateItems (template_id, item_id, configuration) VALUES ('J26XV-G4A18-3J4', 'vpls_img_1', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
