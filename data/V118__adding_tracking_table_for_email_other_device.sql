USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE ContinueFromAnotherDevice (
	id INT IDENTITY(1,1) PRIMARY KEY,
	alt_doc_id varchar(255) NOT NULL,
	session_id bigint NULL,
	email varchar(500) NOT NULL
);
GO

CREATE NONCLUSTERED INDEX IX_email_CFAD ON ContinueFromAnotherDevice (email)
GO

CREATE NONCLUSTERED INDEX IX_alt_doc_id_CFAD ON ContinueFromAnotherDevice (alt_doc_id)
GO
