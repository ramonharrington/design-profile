USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE Template
ADD rank int NOT NULL 
CONSTRAINT rank_default DEFAULT 0
GO

UPDATE Template SET rank = 1 WHERE ProductId IN ('A0Y', '376')
UPDATE Template SET rank = -1 WHERE ProductId = 'B73'
GO