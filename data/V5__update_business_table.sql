USE [design_profile]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Business]
ADD [Url] [nvarchar](100), [JobTitle] [nvarchar](50), [EmployeeName] [nvarchar](100)
GO
