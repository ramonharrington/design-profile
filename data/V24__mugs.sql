USE [design_profile]
GO

DELETE FROM SceneProduct WHERE ProductId = 'BLN'
GO

SET IDENTITY_INSERT Scene ON
GO

INSERT INTO Scene (Id, Filename) VALUES (5, 'BC0A4A46-A6F2-4D53-9BFC-5DE80CD7A037')
GO

SET IDENTITY_INSERT Scene OFF
GO

ALTER TABLE ScenePlacement ADD TopCenterX INT NULL, TopCenterY INT NULL, RightMiddleX INT NULL, RightMiddleY INT NULL, BottomCenterX INT NULL, BottomCenterY INT NULL, LeftMiddleX INT NULL, LeftMiddleY INT NULL
GO

INSERT INTO SceneProduct (SceneId, ProductId) VALUES (5, 'A0Y')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, TopCenterX, TopCenterY, RightMiddleX, RightMiddleY, BottomCenterX, BottomCenterY, LeftMiddleX, LeftMiddleY)
	VALUES (5, 327, 176, 582, 176, 573, 354, 328, 354, 0.95, 459, 195, NULL, NULL, 458, 386, NULL, NULL)
GO
