USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @SceneId INT
DECLARE @ScenePlacementId INT

INSERT INTO Scene (Filename, Height, Width, SceneName) VALUES ('5D21B69C-CE1A-4126-A4AC-550EAEC12FEA', 5656, 828, 'holiday_mobile')
SELECT @SceneId = @@IDENTITY

-- Mug
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 120, 476, 276, 493, 256, 671, 99, 653, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'A0Y')

-- Coaster
-- blue
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 544, 697, 735, 632, 799, 823, 608, 885, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')
-- red
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 467, 609, 668, 588, 688, 786, 487, 806, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')
-- white
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 505, 545, 699, 599, 646, 792, 451, 738, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ8')

-- Men's shirt
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 254, 1264, 531, 1236, 560, 1538, 283, 1566, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'HJA')

-- Puzzle
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 84, 2180, 375, 2221, 341, 2451, 52, 2410, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BR3')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 89, 2255, 384, 2201, 432, 2426, 136, 2479, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BR3')


-- Canvas
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 487, 2340, 793, 2376, 739, 2806, 437, 2770, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BPT')

-- Photo Magnet
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 102, 2764, 267, 2795, 224, 3023, 58, 2989, 0.95, '#00FF00')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 119, 2752, 295, 2768, 276, 2996, 100, 2981, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 135, 2772, 311, 2786, 292, 3016, 116, 3001, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 152, 2805, 325, 2772, 368, 2997, 195, 3030, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '220')

-- Women's T-shirt
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 284, 3436, 528, 3390, 576, 3656, 332, 3703, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'EAD')

-- Calendar Poster
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 155, 4190, 754, 4295, 683, 4698, 85, 4594, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '645')

-- Mouse pad
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 41, 4863, 366, 4805, 417, 5081, 88, 5137, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '679')

-- Calendar magnet
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 477, 5103, 717, 5065, 740, 5227, 500, 5264, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 484, 5126, 723, 5106, 737, 5260, 497, 5280, 0.95, '#00FF00')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) VALUES (@SceneId, 488, 5136, 729, 5136, 729, 5294, 488, 5294, 0.95, '#FF0000')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel) VALUES (@SceneId, 478, 5158, 716, 5195, 693, 5351, 454, 5314, 0.95)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '218')
GO



-- Postcard magnet white
UPDATE ScenePlacement SET TopLeftX = 1016, TopLeftY = 1008, TopRightX = 1104, TopRightY = 991, BottomRightX = 1125, BottomRightY = 1106, BottomLeftX = 1036, BottomLeftY = 1123 WHERE TooltipX = 1045 AND TooltipY = 1155 AND MaskColor IS NULL

-- Postcard magnet red
UPDATE ScenePlacement SET TopLeftX = 1007, TopLeftY = 991, BottomLeftX = 997, BottomRightX = 1088, BottomRightY = 1116 WHERE TooltipX = 1045 AND TooltipY = 1155 AND MaskColor = '#FF0000'

-- Postcard magnet blue
UPDATE ScenePlacement SET TopLeftX = 999, TopLeftY = 982, TopRightX = 1089, TopRightY = 989, BottomRightX = 1080, BottomRightY = 1106, BottomLeftX = 990, BottomLeftY = 1099 WHERE TooltipX = 1045 AND TooltipY = 1155 AND MaskColor = '#0000FF'

-- Postcard magnet green
UPDATE ScenePlacement SET TopLeftX = 991, TopLeftY = 988, BottomRightX = 1058, BottomRightY = 1120, BottomLeftX = 969, BottomLeftY = 1104 WHERE TooltipX = 1045 AND TooltipY = 1155 AND MaskColor = '#00FF00'
GO
