USE [design_profile]
GO

/****** Object:  Table [dbo].[DocumentAsTemplate]    Script Date: 8/15/2017 2:48:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DocumentAsTemplate](
	[original_doc_id] [varchar](50) NOT NULL,
	[new_doc_id] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DocumentAsTemplate] PRIMARY KEY CLUSTERED 
(
	[original_doc_id] ASC,
	[new_doc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


