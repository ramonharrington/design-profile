USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE Template ADD UseForMatching BIT NULL
GO

UPDATE Template SET UseForMatching = 0
GO

UPDATE Template SET UseForMatching = 1 WHERE ProductId IN ('331', '679', 'A14', '376', '236', 'A4E', '276')
GO
