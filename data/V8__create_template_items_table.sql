USE [design_profile]
GO

/****** Object:  Table [dbo].[TemplateItems]    Script Date: 8/3/2017 4:39:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TemplateItems](
	[template_id] [varchar](50) NOT NULL,
	[item_id] [varchar](250) NOT NULL,
	[configuration] [nvarchar](max) NULL,
 CONSTRAINT [PK_TemplateItems] PRIMARY KEY CLUSTERED 
(
	[template_id] ASC,
	[item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


