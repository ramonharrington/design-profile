USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE Template ADD Height INT, Width INT
GO

-- Mug
UPDATE Template SET Height = 432, Width = 437 WHERE TemplateId IN ('XBJZ9-2A038-7G3', 'XKVJ4-H4A78-8N3')
UPDATE Template SET Height = 384, Width = 974 WHERE TemplateId IN ('Q0GK9-2A370-1N9', 'DFKJL-G4A74-3I7')
GO

-- Hat
UPDATE Template SET Height = 374, Width = 431 WHERE TemplateId IN ('MDHL9-2A156-2G5', 'M26XV-G4A11-9G4')
GO

-- Calendar magnet
UPDATE Template SET Height = 508, Width = 319 WHERE TemplateId IN ('QLHZ9-2A179-4O8', '25ZVB-H4A55-2Y7')
GO

-- Postcard magnet
UPDATE Template SET Height = 813, Width = 623 WHERE TemplateId IN ('NLHZ9-2A281-5L1', 'N5ZVB-H4A47-0P7')
GO

-- Note card
UPDATE Template SET Height = 628, Width = 306 WHERE TemplateId IN ('Z0GK9-2A183-0G7', '2GKJL-G4A24-1O0')
GO

-- Note pad
UPDATE Template SET Height = 156, Width = 633 WHERE TemplateId IN ('V0GK9-2A312-2H0', 'KFKJL-G4A21-5H3')
GO

-- Sticky notes
UPDATE Template SET Height = 61, Width = 159 WHERE TemplateId IN ('M0GK9-2A003-1E2', '2FKJL-G4A64-7G9')
GO

-- Pens
UPDATE Template SET Height = 125, Width = 42 WHERE TemplateId IN ('41GK9-2A549-1O2', 'FGKJL-G4A80-0N6')
GO

-- Poster calendar
UPDATE Template SET Height = 1274, Width = 1056 WHERE TemplateId IN ('PLHZ9-2A150-3K5', 'B5ZVB-H4A48-7U7')
GO

-- Mouse pad
UPDATE Template SET Height = 1126, Width = 1347 WHERE TemplateId IN ('N0GK9-2A238-1G0', '4FKJL-G4A61-5F9')
GO

-- Tote bag
UPDATE Template SET Height = 1152, Width = 1152 WHERE TemplateId IN ('R75W9-2A300-8G7', '182V4-H4A48-4U6')
GO

-- Notebook
UPDATE Template SET Height = 231, Width = 545 WHERE TemplateId IN ('W0GK9-2A766-3H3', 'PFKJL-G4A36-1I7')
GO

-- Circle stickers
UPDATE Template SET Height = 275, Width = 319 WHERE TemplateId IN ('JB5W9-2A206-4R5', 'BFBM4-H4A41-8R0')
GO

-- Business cards
UPDATE Template SET Height = 132, Width = 132 WHERE TemplateId IN ('DXN5B-2A292-8W6', '4ZN28-J4A94-5N9')
GO

-- Canvas
UPDATE Template SET Height = 2263, Width = 1686 WHERE TemplateId IN ('T4L3B-2A107-6V7', 'WTWLQ-H4A14-7H7')
GO

-- Photo Coaster
UPDATE Template SET Height = 590, Width = 664 WHERE TemplateId IN ('002Z9-2A326-8I2', '2QRH8-H4A08-3D3')
GO

-- Puzzle
UPDATE Template SET Height = 1186, Width = 1481 WHERE TemplateId IN ('LKDZ9-2A924-3F9', 'BB9WB-H4A94-4T8')
GO

-- Women's T-shirt
UPDATE Template SET Height = 649, Width = 746 WHERE TemplateId IN ('XRPX9-2A177-8L1', 'GWDZ6-H4A14-1D9')
UPDATE Template SET Height = 1151, Width = 1151 WHERE TemplateId IN ('8KBX9-2A871-7O6', 'X1215-H4A68-1F9')
GO

-- T-shirt
UPDATE Template SET Height = 1728, Width = 1728 WHERE TemplateId IN ('GPBX9-2A221-6N6', 'B4L15-H4A68-0T5')
GO

-- Long sleeve T-shirt
UPDATE Template SET Height = 1722, Width = 1727 WHERE TemplateId IN ('R0Z4B-2A992-6S7', 'HT4Z2-J4A41-7H7')
GO
