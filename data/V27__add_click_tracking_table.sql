USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [dbo].[PreviewEventType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_PreviewEventType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[PreviewTracking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[PfId] [varchar](40) NOT NULL,
	[AltDocId] [varchar](20) NOT NULL,
	[ComboId] [int] NULL,
	[DesignTemplateId] [varchar](20) NULL,
	[SceneId] [int] NULL,
	[ProfileSettings] [nvarchar](max) NULL,
	[PreviewEventTypeId] [int] NULL,
	[PreviewEventDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PreviewTracking] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PreviewTracking] ADD  CONSTRAINT [DF_PreviewTracking_PreviewEventDate]  DEFAULT (getdate()) FOR [PreviewEventDate]
GO

ALTER TABLE [dbo].[PreviewTracking]  WITH CHECK ADD  CONSTRAINT [FK_PreviewTracking_PreviewEventType] FOREIGN KEY([PreviewEventTypeId])
REFERENCES [dbo].[PreviewEventType] ([Id])
GO

ALTER TABLE [dbo].[PreviewTracking] CHECK CONSTRAINT [FK_PreviewTracking_PreviewEventType]
GO

ALTER TABLE [dbo].[PreviewTracking]  WITH CHECK ADD  CONSTRAINT [FK_PreviewTracking_Profile] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profile] ([Id])
GO

ALTER TABLE [dbo].[PreviewTracking] CHECK CONSTRAINT [FK_PreviewTracking_Profile]
GO

ALTER TABLE [dbo].[PreviewTracking]  WITH CHECK ADD  CONSTRAINT [FK_PreviewTracking_Scene] FOREIGN KEY([SceneId])
REFERENCES [dbo].[Scene] ([Id])
GO

ALTER TABLE [dbo].[PreviewTracking] CHECK CONSTRAINT [FK_PreviewTracking_Scene]
GO

CREATE NONCLUSTERED INDEX [NonClusteredIndex-PreviewTracking_ProfileId] ON [dbo].[PreviewTracking]
(
	[ProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


SET IDENTITY_INSERT PreviewEventType ON
GO

INSERT INTO PreviewEventType (Id, Name) VALUES (1, 'Studio')
INSERT INTO PreviewEventType (Id, Name) VALUES (2, 'Portfolio')
GO

SET IDENTITY_INSERT PreviewEventType OFF
GO
