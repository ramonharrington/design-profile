DECLARE @SceneId INT
DECLARE @ScenePlacementId INT


/************************
	gift certificates 
**************************/
INSERT INTO Scene (Filename, Height, Width, SceneName, MinViewportWidth) VALUES ('591267ce-23bb-458e-a1b8-c1d037f9d6a7', 500 ,380, null, null)
SELECT @SceneId = @@IDENTITY

--white placement
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor)
	VALUES (@SceneId, 67, 133, 314, 151, 307, 271, 56, 253, 0.95, NULL)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '622')

--blue placement
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) 
VALUES (@SceneId, 312, 221, 492, 255, 424, 279, 311, 254, 0.95, '#0000FF')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, '622')

--template entry
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForGiftCenter, UseForLogoCenter) VALUES 
('2XCRQ-J4A50-7Q1', '622', 'Gift Certificate', null, 'prod', 0, 0, 0),
('CB87B-2A666-4M6', '622', 'Gift Certificate', null, 'dev', 0, 0, 1),
('CB87B-2A666-4M6', '622', 'Gift Certificate', null, 'local', 0, 0, 1)

/************************
	paper coasters
**************************/
INSERT INTO Scene (Filename, Height, Width, SceneName, MinViewportWidth) VALUES ('0d4a9eba-73d9-4ba1-b915-4d4b9bc7ffdf', 500, 459, null, null)
SELECT @SceneId = @@IDENTITY
 
--white placement
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor)
	VALUES (@SceneId, 172, 218, 285, 302, 167, 393, 56, 301, 0.95, NULL)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ9')

--red placement
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) 
VALUES (@SceneId, 129, 226, 272, 276, 201, 392, 56, 334, 0.95, '#E00808')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ9')

--green placement
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor) 
VALUES (@SceneId, 260, 130, 397, 172, 344, 277, 199, 231, 0.95, '#05A34A')
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'BQ9')

--template entry
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForGiftCenter, UseForLogoCenter) VALUES 
('BXRRQ-J4A81-0G8', 'BQ9', 'Paper coaster', null, 'prod', 0, 0, 0),
('WB87B-2A026-2W6', 'BQ9', 'Paper coaster', null, 'dev', 0, 0, 1),
('WB87B-2A026-2W6', 'BQ9', 'Paper coaster', null, 'local', 0, 0, 1)

/************************
	table runners
**************************/
INSERT INTO Scene (Filename, Height, Width, SceneName, MinViewportWidth) VALUES ('ed1b9539-791e-4df1-9faa-0a156a58d599', 500, 295, null, null)
SELECT @SceneId = @@IDENTITY
 
--white placement
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor)
	VALUES (@SceneId, 244, 103, 350, 98, 349, 196, 244, 209, 0.95, NULL)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId, TemplateTopEdge) VALUES (@ScenePlacementId, 'FGZ',  0.70)

--template entry
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForGiftCenter, UseForLogoCenter) VALUES 
('4GVRQ-J4A11-8W7', 'FGZ', 'Table runner', null, 'prod', 0, 0, 0),
('0C87B-2A666-1Q3', 'FGZ', 'Table runner', null, 'dev', 0, 0, 1),
('0C87B-2A666-1Q3', 'FGZ', 'Table runner', null, 'local', 0, 0, 1)

/************************
	table cloths
**************************/
INSERT INTO Scene (Filename, Height, Width, SceneName, MinViewportWidth) VALUES ('8c801300-5334-4d97-b5cd-8f9166a385b7', 500, 380, null, null)
SELECT @SceneId = @@IDENTITY
 
--white placement
INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, MaskColor)
	VALUES (@SceneId, 124, 89, 297, 97, 298, 286, 123, 258, 0.95, NULL)
SELECT @ScenePlacementId = @@IDENTITY
INSERT INTO SceneProduct (ScenePlacementId, ProductId, TemplateTopEdge, TemplateLeftEdge) VALUES (@ScenePlacementId, 'FGU', 0.55, 0.2)

--template entry
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForGiftCenter, UseForLogoCenter) VALUES 
('LMNRQ-J4A33-9M4', 'FGU', 'Tablecloth 4 sides', null, 'prod', 0, 0, 0),
('LB87B-2A733-2U6', 'FGU', 'Tablecloth 4 sides', null, 'dev', 0, 0, 1),
('LB87B-2A733-2U6', 'FGU', 'Tablecloth 4 sides', null, 'local', 0, 0, 1)


/************************
	folders
**************************/
--template entry
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForGiftCenter, UseForLogoCenter) VALUES 
('01QRQ-J4A02-7N7', '156', 'Folders', null, 'prod', 0, 0, 0),
('VB87B-2A384-6R4', '156', 'Folders', null, 'dev', 0, 0, 0),
('VB87B-2A384-6R4', '156', 'Folders', null, 'local', 0, 0, 0)
