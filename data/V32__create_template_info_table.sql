USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Template](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateId] [varchar](50),
	[ProductId] [nvarchar](20) NULL,
	[Name] [nvarchar](50) NULL,
	[Tags] [nvarchar](MAX) NULL
 CONSTRAINT [PK_Template] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET IDENTITY_INSERT Template ON
GO

INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (1, '01229-2A107-5M1', '372', 'T-Shirt', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (2, '04429-2A596-3E8', '065', 'Letterhead', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (3, '2FNB9-2A145-4B9', 'B73', 'Business Card', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (4, 'H0MJ9-2A251-4S8', '016', 'Large Banner', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (5, '4QTV8-2A837-5O8', '018', 'Small Banner', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (6, 'J0MJ9-2A133-7U1', '043', 'Lawn Sign', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (7, 'K0MJ9-2A101-8Q8', '679', 'Mouse Pad', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (8, '8F329-2A572-9W9', 'B73', 'Business Card', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (9, 'L0MJ9-2A951-9T1', 'CDK', 'Lawn Sign', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (10, '8HNB9-2A186-7T8', '065', 'Letterhead', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (11, 'M0MJ9-2A391-6Y0', '276', 'Note Pad', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (12, 'N0MJ9-2A152-7R4', '640', 'Poster', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (13, 'P0MJ9-2A839-3T2', 'A4E', 'Notebook', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (14, 'Q0MJ9-2A316-2D0', '376', 'Pen', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (15, 'R0MJ9-2A956-1H6', '051', 'Car Door Magnet', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (16, 'G2229-2A845-4J8', 'B73', 'Business Card', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (17, 'GFNB9-2A625-4X7', 'DD0', 'Flyer', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (18, 'T0MJ9-2A354-6B7', '357', 'Rack Card', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (19, 'V0MJ9-2A057-9E9', '236', 'Note Card', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (20, 'W0MJ9-2A914-7J6', 'A14', 'Mug', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (21, 'X0MJ9-2A363-3C4', '084', 'Postcard', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (22, 'KLTV8-2A215-9K9', 'A0Y', 'Mug', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (23, 'Z0MJ9-2A036-5K8', 'CCZ', 'Car Magnet', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (24, 'LPTV8-2A871-4N7', 'A0Y', 'Mug', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (25, '01MJ9-2A634-2E6', '331', 'Sticky Note', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (26, '11MJ9-2A732-4G0', '083', 'Return Address Label', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (27, 'NLKB9-2A135-8M8', '018', 'Small Banner', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (28, 'NPTV8-2A267-0P5', 'A0Y', 'Mug', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (29, 'PT129-2A296-8L4', 'A0Y', 'Mug', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (30, 'QHNB9-2A258-6J8', 'B73', 'Business Card', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (31, 'RPTV8-2A955-0M6', '018', 'Small Banner', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (32, 'TGNB9-2A083-6U3', 'BLN', 'Vertical Banner', NULL)
INSERT INTO Template (Id, TemplateId, ProductId, Name, Tags) VALUES (33, 'VHNB9-2A780-2C4', '232', 'Note Card', NULL)
GO

SET IDENTITY_INSERT Template OFF
GO
