USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Add scenes
DECLARE @SceneId INT
DECLARE @ScenePlacementId INT
INSERT INTO Scene (Filename, Width, Height) VALUES ('BF355A92-3641-4439-9429-E98ECC1684D2', 500, 432)
SELECT @SceneId = @@IDENTITY

INSERT INTO ScenePlacement (SceneId, TopLeftX, TopLeftY, TopRightX, TopRightY, BottomRightX, BottomRightY, BottomLeftX, BottomLeftY, ShadowLevel, TopCenterX, TopCenterY, BottomCenterX, BottomCenterY)
	VALUES (@SceneId, 105, 197, 195, 183, 207, 276, 117, 289, 0.95, 150, 187, 164, 287)
SELECT @ScenePlacementId = @@IDENTITY

INSERT INTO SceneProduct (ScenePlacementId, ProductId) VALUES (@ScenePlacementId, 'A0B')
GO


INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('R75W9-2A300-8G7', 'A0B', 'Tote Bag', NULL, 'local', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('R75W9-2A300-8G7', 'A0B', 'Tote Bag', NULL, 'dev', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('182V4-H4A48-4U6', 'A0B', 'Tote Bag', NULL, 'test', 1, 1, 0)
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter) VALUES ('182V4-H4A48-4U6', 'A0B', 'Tote Bag', NULL, 'prod', 1, 1, 0)
GO

INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('R75W9-2A300-8G7', 'vpls_div_0', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('182V4-H4A48-4U6', 'vpls_div_0', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
