USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment, UseForMatching, UseForLogoCenter, UseForGiftCenter, rank, Height, Width, UseForBCsLogoFlow, UseInCarousel)
VALUES
('K0FKB-2A854-3G5', 'B73', 'Business Card 5', NULL, 'local', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('K0FKB-2A854-3G5', 'B73', 'Business Card 5', NULL, 'dev', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('WL87M-M4A01-4G6', 'B73', 'Business Card 5', NULL, 'prod', 0, 0, 0, 0, NULL, NULL, 1, 1)
,('WL87M-M4A01-4G6', 'B73', 'Business Card 5', NULL, 'test', 0, 0, 0, 0, NULL, NULL, 1, 1)
GO

INSERT INTO TemplateItems  (template_id, item_id, configuration)
VALUES
--dev/local
('K0FKB-2A854-3G5','vpls_rect_1','{"Recolor":"complete","Color":"primary","Font":null}')
,('K0FKB-2A854-3G5','item_1024','{"ImageUsage":"logo","Recolor":"false","Color":null,"Font":null}')
,('K0FKB-2A854-3G5','vpls_text_3','{"Recolor":"complete","Color":"bw","Font":null}')
,('K0FKB-2A854-3G5','vpls_text_4','{"Recolor":"complete","Color":"bw","Font":null}')
,('K0FKB-2A854-3G5','vpls_text_5','{"Recolor":"complete","Color":"primary","Font":null}')
,('K0FKB-2A854-3G5','vpls_text_6','{"Recolor":"complete","Color":"primary","Font":null}')
,('K0FKB-2A854-3G5','vpls_text_7','{"Recolor":"complete","Color":"bw","Font":null}')
,('K0FKB-2A854-3G5','vpls_text_8','{"Recolor":"complete","Color":"bw","Font":null}')
,('K0FKB-2A854-3G5','vpls_text_9','{"Recolor":"complete","Color":"primary","Font":null}')
,('K0FKB-2A854-3G5','vpls_text_10','{"Recolor":"complete","Color":"primary","Font":null}')

--prod
,('WL87M-M4A01-4G6','vpls_rect_1','{"Recolor":"complete","Color":"primary","Font":null}')
,('WL87M-M4A01-4G6','item_1024','{"ImageUsage":"logo","Recolor":"false","Color":null,"Font":null}')
,('WL87M-M4A01-4G6','vpls_text_3','{"Recolor":"complete","Color":"bw","Font":null}')
,('WL87M-M4A01-4G6','vpls_text_4','{"Recolor":"complete","Color":"bw","Font":null}')
,('WL87M-M4A01-4G6','vpls_text_5','{"Recolor":"complete","Color":"primary","Font":null}')
,('WL87M-M4A01-4G6','vpls_text_6','{"Recolor":"complete","Color":"primary","Font":null}')
,('WL87M-M4A01-4G6','vpls_text_7','{"Recolor":"complete","Color":"bw","Font":null}')
,('WL87M-M4A01-4G6','vpls_text_8','{"Recolor":"complete","Color":"bw","Font":null}')
,('WL87M-M4A01-4G6','vpls_text_9','{"Recolor":"complete","Color":"primary","Font":null}')
,('WL87M-M4A01-4G6','vpls_text_10','{"Recolor":"complete","Color":"primary","Font":null}')
GO

