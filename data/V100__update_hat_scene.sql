USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

WITH cte AS (
	SELECT pl.* FROM ScenePlacement pl LEFT JOIN SceneProduct pr ON pl.Id = pr.ScenePlacementId WHERE pr.ProductId = '057'
)
UPDATE cte SET TopLeftX = 163, TopLeftY = 160, TopRightX = 261, TopRightY = 163, BottomRightX = 217, BottomRightY = 224, BottomLeftX = 150, BottomLeftY = 210, TopCenterX = 194, TopCenterY = 161, BottomCenterX = 187, BottomCenterY = 213
GO
