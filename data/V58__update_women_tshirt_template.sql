USE [design_profile]
GO

UPDATE Template set TemplateId = 'X1215-H4A68-1F9' where Id in (100,101)
UPDATE Template set TemplateId = '8KBX9-2A871-7O6' where Id in (102,103)
GO

INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('8KBX9-2A871-7O6', 'item_332', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
INSERT INTO TemplateItems (Template_Id, Item_Id, Configuration) VALUES ('X1215-H4A68-1F9', 'item_332', '{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO