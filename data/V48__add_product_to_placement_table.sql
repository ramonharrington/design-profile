USE [design_profile]
GO

ALTER TABLE [dbo].[SceneProduct] ADD ScenePlacementId INT NULL
GO

WITH cteScenePlacement AS (
	SELECT product.*, placement.Id AS PlacementId
	FROM ScenePlacement placement LEFT JOIN SceneProduct product ON placement.SceneId = product.SceneId
)
UPDATE cteScenePlacement SET ScenePlacementId = PlacementId
GO

ALTER TABLE SceneProduct WITH CHECK ADD CONSTRAINT [FK_SceneProduct_ScenePlacement] FOREIGN KEY ([ScenePlacementId])
REFERENCES [dbo].[ScenePlacement] ([Id])
GO

ALTER TABLE SceneProduct DROP CONSTRAINT FK_SceneProduct_Scene
GO

ALTER TABLE SceneProduct DROP COLUMN SceneId
GO


INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 16 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 15

INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 18 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 17
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 19 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 17

INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 21 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 20

INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 23 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 22
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 24 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 22

INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 26 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 25
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 27 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 25

INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 29 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 30 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 31 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 32 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 33 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 34 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 35 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 36 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 37 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 38 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 39 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 40 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 41 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
INSERT INTO SceneProduct 
SELECT ProductId, TemplateTopEdge, TemplateRightEdge, TemplateBottomEdge, TemplateLeftEdge, 42 AS ScenePlacementId
FROM SceneProduct WHERE ScenePlacementId = 28
GO
