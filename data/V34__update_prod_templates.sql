USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Insert templates for Dev
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('2FKJL-G4A64-7G9', '331', 'Sticky Note', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('4FKJL-G4A61-5F9', '679', 'Mouse Pad', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('7FKJL-G4A34-9I7', '016', 'Large Banner', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('DFKJL-G4A74-3I7', 'A14', 'Mug', 'force-background', 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('GFKJL-G4A71-2I2', '043', 'Lawn Sign', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('KFKJL-G4A21-5H3', '276', 'Note Pad', 'force-background', 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('PFKJL-G4A36-1I7', 'A4E', 'Notebook', 'force-background', 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('2GKJL-G4A24-1O0', '236', 'Note Card', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('6GKJL-G4A33-1L3', '357', 'Rack Card', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('7GKJL-G4A30-9J3', '640', 'Poster', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('FGKJL-G4A80-0N6', '376', 'Pen', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('KGKJL-G4A29-2M4', 'CDK', 'Lawn Sign', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('MGKJL-G4A32-2O1', 'CCZ', 'Car Magnet', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('XGKJL-G4A16-4M8', '051', 'Car Door Magnet', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('0HKJL-G4A94-2K5', '084', 'Postcard', NULL, 'prod')
INSERT INTO Template (TemplateId, ProductId, Name, Tags, Environment) VALUES ('3HKJL-G4A83-1J7', '083', 'Return Address Label', NULL, 'prod')
GO


-- Insert templates items
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'2FKJL-G4A64-7G9', N'item_264', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'4FKJL-G4A61-5F9', N'item_348', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7FKJL-G4A34-9I7', N'item_661', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7FKJL-G4A34-9I7', N'vpls_text_3', N'{"Recolor":"bw","Color":"","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7FKJL-G4A34-9I7', N'vpls_text_4', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7FKJL-G4A34-9I7', N'vpls_shape_5', N'{"Recolor":"bw","Color":"complete","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7FKJL-G4A34-9I7', N'vpls_line_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'DFKJL-G4A74-3I7', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'DFKJL-G4A74-3I7', N'item_296', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GFKJL-G4A71-2I2', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GFKJL-G4A71-2I2', N'item_395', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'GFKJL-G4A71-2I2', N'vpls_text_3', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KFKJL-G4A21-5H3', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KFKJL-G4A21-5H3', N'item_342', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KFKJL-G4A21-5H3', N'vpls_line_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KFKJL-G4A21-5H3', N'vpls_imagearea_3', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'PFKJL-G4A36-1I7', N'vpls_img_1', N'{"ImageUsage":null,"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'PFKJL-G4A36-1I7', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'PFKJL-G4A36-1I7', N'item_706', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'PFKJL-G4A36-1I7', N'vpls_text_2', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'PFKJL-G4A36-1I7', N'vpls_text_3', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'PFKJL-G4A36-1I7', N'vpls_imagearea_4', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'2GKJL-G4A24-1O0', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'2GKJL-G4A24-1O0', N'vpls_oval_1', N'{"Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'2GKJL-G4A24-1O0', N'item_470', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'6GKJL-G4A33-1L3', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'6GKJL-G4A33-1L3', N'item_607', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'6GKJL-G4A33-1L3', N'vpls_text_8', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7GKJL-G4A30-9J3', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7GKJL-G4A30-9J3', N'item_361', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'7GKJL-G4A30-9J3', N'item_650', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'FGKJL-G4A80-0N6', N'item_374', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'FGKJL-G4A80-0N6', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'FGKJL-G4A80-0N6', N'item_685', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KGKJL-G4A29-2M4', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KGKJL-G4A29-2M4', N'item_402', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KGKJL-G4A29-2M4', N'vpls_text_2', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'KGKJL-G4A29-2M4', N'vpls_shape_3', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'MGKJL-G4A32-2O1', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'MGKJL-G4A32-2O1', N'item_366', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'MGKJL-G4A32-2O1', N'vpls_text_2', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'MGKJL-G4A32-2O1', N'vpls_shape_3', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'XGKJL-G4A16-4M8', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'XGKJL-G4A16-4M8', N'item_400', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'XGKJL-G4A16-4M8', N'vpls_text_4', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'0HKJL-G4A94-2K5', N'item_681', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'0HKJL-G4A94-2K5', N'vpls_rect_1', N'{"Recolor":"complete","Color":"primary","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'0HKJL-G4A94-2K5', N'item_484', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'0HKJL-G4A94-2K5', N'vpls_text_4', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'0HKJL-G4A94-2K5', N'vpls_text_5', N'{"Recolor":"complete","Color":"bw","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'0HKJL-G4A94-2K5', N'vpls_line_1', N'{"Recolor":"complete","Color":"bw","Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'3HKJL-G4A83-1J7', N'item_422', N'{"ImageUsage":"logo","Recolor":null,"Color":null,"Font":null}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'3HKJL-G4A83-1J7', N'vpls_text_2', N'{"Recolor":"complete","Color":"primary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'3HKJL-G4A83-1J7', N'vpls_text_3', N'{"Recolor":"complete","Color":"secondary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'3HKJL-G4A83-1J7', N'vpls_text_4', N'{"Recolor":"complete","Color":"secondary","Font":"primary"}')
GO
INSERT [dbo].[TemplateItems] ([template_id], [item_id], [configuration]) VALUES (N'3HKJL-G4A83-1J7', N'vpls_text_5', N'{"Recolor":"complete","Color":"secondary","Font":"primary"}')
GO
