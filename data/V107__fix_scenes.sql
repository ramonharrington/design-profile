USE [design_profile]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Update ScenePlacement set BottomLeftX = 280, BottomLeftY = 245 where TopLeftX = 312 and TopLeftY = 221
Update SceneProduct set TemplateTopEdge = 0.55, TemplateRightEdge = 0.8, TemplateLeftEdge = 0.2 where ProductId = 'FGU'
GO
