Design Profile
===================

This app will be used to host projects in AWS using ASP.NET MVC.

Deployment
-------------
#### Jenkins
The [Build Pipeline](https://vbujenkins.vistaprint.net/view/Hatchery.Pipeline/) is managed by VBUJenkins.

----------

Development Setup
-----------------
#### Create Database


#### Update project

If table "schema_version" exists in database, truncate that table
Replace old project GUID F4323E75-C014-4C75-8718-F80D3CC9F10F with new GUID
Replace all occurrences of "DesignProfile" in text files with new project name
Rename files and directories from "DesignProfile" to new project name


#### Setup IIS
1. Create website for Hatchery.
2. Set binding for http://loc.designprofile.vistaprint.io
3. Create a database on DBLOCAL for *design_profile*
  1. Create login *designprofile* with password *designprofile* and grant it access the design_profile database.

### Setup Webpack and friends
1. install node and npm
2. install webpack and dev server globally
  * npm install webpack -g
  * npm install webpack-dev-server -g
3. install rest of the node modules
  * npm install

### General Information
* Use something like VS code or atom for the front end Development, syntax highlighting and autocomplete in VS isn't working well
* All components should live on their own and have accompanying CSS
* The API and the front end should be completely isolated -- no more passing data via viewbags or models
* webpack will take care of adding vendor-specific CSS prefixes via the autoprefixer module
* webpack-dev-server handles hot reloading the app.  Both JSX and CSS changes will be made live without reloading the page.  
  * If this isn't working make sure we are launching with `webpack-dev-server --hot --inline`
* webpack will handle minifying our code when we are ready for production
* The project uses the webpack test runner for visual studio.
* The webpack dev server is automatically launched when you open the project on port 9000 (http://localhost:9000)
Webpack dev build is done after each build.  Once we are ready for production there is a minified version that can be built too.


AWS VM Credentials
-------------

Dev/Test:  fwhRK-iLqgg;eC4sv*=roP*rs8?YVoyf
Prod:  7MHZ;eHdhExWRQnTD96-*@8ecPb5x8?b


DB
-------------
Dev:  udesign_profilerm / 77353731DA2A4CB
Test: udesign_profilerm / 8E5FE157A4B949E
Prod: udesign_profilerm / 86845661C5DC487
