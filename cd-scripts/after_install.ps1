$ErrorActionPreference = "Stop"

# Repoint default site path
$xml = [xml](gc 'C:\windows\sysnative\inetsrv\config\applicationHost.config')
$target = $xml.configuration."system.applicationHost".sites.site | ?{ $_.name -eq "Default Web Site" } | %{ $_.application } | ?{ $_.path -eq "/" } | %{ $_.virtualDirectory } | ?{ $_.path -eq "/" }
$target.physicalPath = 'C:\inetpub\wwwroot'
$xml.Save('C:\windows\sysnative\inetsrv\config\applicationHost.config')

# Copy config file for the environment if we can find one
$instanceId = (New-Object System.Net.WebClient).DownloadString('http://169.254.169.254/latest/meta-data/instance-id')
$az = (New-Object System.Net.WebClient).DownloadString('http://169.254.169.254/latest/meta-data/placement/availability-zone')
$region = $az -replace '-([0-9])\w','-$1'
$environment = Get-EC2Tag -Filter @{Name='resource-id';Values=$instanceId} -Region $region | ?{ $_.Key -eq 'DesignProfileEnvironment' } | select -first 1 | %{ $_.Value }

$refpath = "C:\configs\$environment.config"
if (test-path $refpath) 
{
	copy-item $refpath 'C:\inetpub\wwwroot\bin\settings-config.xml' -force
}

set-content 'C:\inetpub\wwwroot\bin\environment.txt' $environment