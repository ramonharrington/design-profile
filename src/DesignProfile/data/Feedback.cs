//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DesignProfile.data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Feedback
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public int FeedbackTypeId { get; set; }
        public string Message { get; set; }
        public System.DateTime FeedbackDate { get; set; }
    
        public virtual FeedbackType FeedbackType { get; set; }
        public virtual Profile Profile { get; set; }
    }
}
