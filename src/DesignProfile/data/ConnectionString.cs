using System.Data.Entity.Core.EntityClient;

namespace DesignProfile.data
{
    public static class ConnectionString
    {
        public static string Current
        {
            get
            {
                var formattedCstr = string.Format(Properties.Settings.Default.SqlServerConnectionString,
                    Properties.Settings.Default.SqlServerUsername, Properties.Settings.Default.SqlServerPassword);
                return GetEntityConnectionString(formattedCstr);
            }
        }

        public static string Hangfire
        {
            get
            {
                return string.Format(Properties.Settings.Default.SqlServerConnectionString,
                    Properties.Settings.Default.SqlServerUsername, Properties.Settings.Default.SqlServerPassword);
            }
        }

        private static string GetEntityConnectionString(string formattedCstr)
        {
            var entityCStrObject = new EntityConnectionStringBuilder
            {
                Metadata = "res://*",
                Provider = "System.Data.SqlClient",
                ProviderConnectionString = formattedCstr
            };
            return entityCStrObject.ConnectionString;
        }
    }
}
