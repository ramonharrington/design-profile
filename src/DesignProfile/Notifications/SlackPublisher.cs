using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using DesignProfile.Properties;
using Newtonsoft.Json;

namespace DesignProfile.Notifications
{
    public class SlackPublisher
    {
        private readonly string _slackRoomDev;
        private readonly string _slackRoomProd;

        public SlackPublisher(string slackRoomDev, string slackRoomProd)
        {
            _slackRoomDev = slackRoomDev;
            _slackRoomProd = slackRoomProd;
        }


        public void PublishMessageAsync(string message, string author, string image_url)
        {
            var formatedMessage = new Dictionary<string, string>();
            formatedMessage.Add("pretext", message);
            formatedMessage.Add("author_name", author);
            formatedMessage.Add("image_url", image_url);
            PublishMessageAsync(formatedMessage);
        }

        public void PublishMessageAsync(object msg)
        {
            new Task(()=> DoPublishMessage(msg)).Start();
        }

        private void DoPublishMessage(object msg)
        {
            try
            {
                string environment = Settings.Default.Environment;
                string channel = null;
                string url = "https://hooks.slack.com/services/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";
                if (environment.Equals("prod"))
                {
                    channel = _slackRoomProd;
                }
                else if (environment.Equals("dev"))
                {
                    channel = _slackRoomDev;
                }

                if (channel == null)
                {
                    return;
                }

                var wr = (HttpWebRequest) WebRequest.Create(url);
                wr.Method = "POST";
                wr.ContentType = "application/json";

                using (var reqStream = wr.GetRequestStream())
                using (var sw = new StreamWriter(reqStream))
                using (var json = new JsonTextWriter(sw))
                {
                    JsonSerializer.Create().Serialize(json, DesignProfileMessageBuilder(msg, channel));
                }

                wr.GetResponse();
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Warn("Could not send message to slack", ex);
            }
        }

        private object DesignProfileMessageBuilder(object msg, string channel)
        {
            try
            {
                var response = new Dictionary<string, object>();
                response.Add("attachments", new List<object> {msg});
                response.Add("channel", "#" + channel);
                return response;
            }
            catch (Exception e)
            {
                return new Dictionary<string, string> {{"text", "Error parsing notification " +e.Message}};
            }
        }
    }
}
