using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesignProfile.Notifications
{
    /// <summary>
    /// Publish a notification to a chat room
    /// </summary>
    public interface IChatRoomPublisher
    {
        void PublishMessage(object msg);
    }
}
