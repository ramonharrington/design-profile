using System;
using System.IO;
using System.Net;
using DesignProfile.Properties;
using Newtonsoft.Json;

namespace DesignProfile.Notifications
{
    public class HipChatPublisher : IChatRoomPublisher
    {
        private readonly int _devRoomId;
        private readonly string _devRoomKey;
        private readonly int _prodRoomId;
        private readonly string _prodRoomKey;

        public HipChatPublisher(int devRoomId, string devRoomKey, int prodRoomId, string prodRoomKey)
        {
            _devRoomId = devRoomId;
            _devRoomKey = devRoomKey;
            _prodRoomId = prodRoomId;
            _prodRoomKey = prodRoomKey;
        }

        public void PublishMessage(object msg)
        {
            string url = null;
            if (Settings.Default.Environment == "prod")
            {
                url = $"https://api.hipchat.com/v2/room/{_devRoomId}/notification?auth_token={_devRoomKey}";
            }
            else if (Settings.Default.Environment == "test")
            {
                url = $"https://api.hipchat.com/v2/room/{_prodRoomId}/notification?auth_token={_prodRoomKey}";
            }

            if (url == null)
            {
                return;
            }

            var wr = (HttpWebRequest)WebRequest.Create(new Uri(url));
            wr.Method = "POST";
            wr.ContentType = "application/json";

            try
            {
                using (var reqStream = wr.GetRequestStream())
                using (var sw = new StreamWriter(reqStream))
                using (var json = new JsonTextWriter(sw))
                {
                    JsonSerializer.Create().Serialize(json, msg);
                }

                wr.GetResponse();
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Warn("Could not send message to hipchat", ex);
            }
        }
    }
}
