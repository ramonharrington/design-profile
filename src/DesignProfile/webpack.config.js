/// <binding />
var path = require('path');
var WebpackNotifierPlugin = require('webpack-notifier');
var webpack = require('webpack');
var precss = require('precss');
var autoprefixer = require('autoprefixer');

require('es6-promise').polyfill();

module.exports = {
  context: path.join(__dirname, 'Content'),
  entry: [

    './server'
  ],
  output: {
    path: path.join(__dirname, 'Content'),
    filename: '[name].bundle.js',
    publicPath: 'https://loc.designprofile.vistaprint.io:9000/Content'
  },
  module: {
    loaders: [
      // Transform JSX in .jsx files
      {
          test: /\.jsx$/,
          loader: 'babel',
          exclude: /node_modules/,
      },
      {
        test: /\.scss/,
        loaders: ['style', 'css', 'sass']
      },
      // CSS inlining and post-css filters
      {
          test: /\.css$/,
          loader: 'style-loader!css-loader?-minimize!postcss-loader',
      },
    ],
  },
  postcss: function() {
      return [precss, autoprefixer];
  },
  resolve: {
    // Allow require('./blah') to require blah.jsx
    extensions: ['', '.js', '.jsx']
  },
  externals: {
    // Use external version of React (from CDN for client-side, or
    // bundled with ReactJS.NET for server-side)
    //react: 'React'
  },
  devServer: {
      contentBase: ".",
      host: "loc.designprofile.vistaprint.io",
      port: 9000,
      publicPath: 'https://loc.designprofile.vistaprint.io:9000/Content'
  },
  plugins: [
    new WebpackNotifierPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
};
