﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using DesignProfile.Builders;
using DesignProfile.Properties;
using Newtonsoft.Json;

namespace DesignProfile.Adapters
{
    public class TextClassificationAdapter
    {
        private const string _textClassificationEndpoint = "text-analysis/tagging";

        /// <summary>
        /// Create and populate a business object with information.
        /// </summary>
        /// <param name="business">The business.</param>
        /// <param name="textFields">The text fields.</param>
        public void UpdateBusinessInformation(data.Business business, IList<Tuple<string, string>> textFields)
        {
            /*
            var classifiedFields = ClassifyText(textFields.Select(t => t.Item1).ToList()).ToList();

            business.Phone1 = GetField(classifiedFields, TextClassificationType.Phone, textFields);
            business.Phone2 = GetField(classifiedFields, TextClassificationType.Phone, textFields);
            business.Email = GetField(classifiedFields, TextClassificationType.Email, textFields);
            business.Url = GetField(classifiedFields, TextClassificationType.Website, textFields);
            business.EmployeeName = GetField(classifiedFields, TextClassificationType.Name, textFields);
            business.Name = GetField(classifiedFields, TextClassificationType.CompanyName, textFields);
            business.Address1 = GetField(classifiedFields, TextClassificationType.Address, textFields);
            business.Address2 = GetField(classifiedFields, TextClassificationType.Address, textFields);
            */

            // Fill in the blanks with the text purpose id
            business.Phone1 = business.Phone1 ?? GetPurposeField(textFields, ProfileBuilder.PurposeField.Phone);
            business.Email = business.Email ?? GetPurposeField(textFields, ProfileBuilder.PurposeField.Email);
            business.Url = business.Url ?? GetPurposeField(textFields, ProfileBuilder.PurposeField.Web);
            business.EmployeeName = business.EmployeeName ?? GetPurposeField(textFields, ProfileBuilder.PurposeField.FullName);
            business.Name = business.Name ?? GetPurposeField(textFields, ProfileBuilder.PurposeField.CompanyName);
            business.Address1 = business.Address1 ?? GetPurposeField(textFields, ProfileBuilder.PurposeField.Address1);
            business.Tagline = GetPurposeField(textFields, ProfileBuilder.PurposeField.CompanyMessage);
        }

        private IEnumerable<Tuple<string, TextClassificationType>> ClassifyText(IList<string> textFields)
        {
            var classifiedFields = new List<Tuple<string, TextClassificationType>>();

            if (textFields == null || !textFields.Any())
            {
                return classifiedFields;
            }

            try
            {
                var requestUrl = $"{Settings.Default.TextClassificationServer}/{_textClassificationEndpoint}?text=";
                foreach (var textField in textFields)
                {
                    requestUrl = $"{requestUrl}\"{textField}\",";
                }

                requestUrl = requestUrl.Substring(0, requestUrl.Length - 1);

                var request = (HttpWebRequest) WebRequest.Create(requestUrl);
                request.Method = "GET";

                using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    var classificationList = JsonConvert.DeserializeObject<IEnumerable<string>>(reader.ReadToEnd())
                        .ToList();

                    if (classificationList.Count() != textFields.Count)
                    {
                        return classifiedFields;
                    }

                    for (int i = 0; i < textFields.Count; i++)
                    {
                        classifiedFields.Add(new Tuple<string, TextClassificationType>(textFields.ElementAt(i),
                            (TextClassificationType) Enum.Parse(typeof(TextClassificationType),
                                classificationList.ElementAt(i))));
                    }
                }
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error calling text classification service", ex);
            }

            return classifiedFields;
        }

        /// <summary>
        /// Find the field to use from the classified list.  If one is found, remove it from the classified list and from the original list
        /// </summary>
        /// <param name="classifiedFields">The classified fields.</param>
        /// <param name="classificationType">Type of the classification.</param>
        /// <param name="originalFields">The original fields.</param>
        /// <returns></returns>
        private string GetField(IList<Tuple<string, TextClassificationType>> classifiedFields, TextClassificationType classificationType, IList<Tuple<string, string>> originalFields)
        {
            var element = classifiedFields.FirstOrDefault(t => t.Item2 == classificationType);
            if (element != null)
            {
                classifiedFields.Remove(element);
                originalFields.Remove(originalFields.FirstOrDefault(t => t.Item1 == element.Item1));
                return element.Item1;
            }

            return null;
        }

        private string GetPurposeField(IList<Tuple<string, string>> textFields,
            string purpose)
        {
            return textFields.FirstOrDefault(t => t.Item2.ToLower() == purpose.ToLower())?.Item1;
        }
    }

    public enum TextClassificationType
    {
        Name,
        CompanyName,
        Address,
        Phone,
        Email,
        Website
    }
}