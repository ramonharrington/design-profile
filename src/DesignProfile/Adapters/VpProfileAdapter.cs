﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DesignProfile.Models;
using DesignProfile.Properties;
using Newtonsoft.Json;

namespace DesignProfile.Adapters
{
    internal class VpProfileAdapter
    {
        internal ShopperProfileData GetShopperProfile(int shopperKey)
        {
            return new ShopperProfileData() {ShopperId = shopperKey.ToString()};
            var urlBase = Settings.Default.VpApiServer + "/hatchery/rpc/designprofileapi/getshopperprofiledata/" +
                          shopperKey;
            var request = (HttpWebRequest) WebRequest.Create(urlBase);
            request.Headers.Add("X-Hatch-Oath", "All your base info is belong to us");
            try
            {
                using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    var shopperProfileData = JsonConvert.DeserializeObject<ShopperProfileData>(reader.ReadToEnd());
                    foreach (var upload in shopperProfileData.ShopperUploads)
                    {
                        if (!upload.ImagePreview.Contains("http"))
                        {
                            upload.ImagePreview = Settings.Default.VpSecureBaseUrl + upload.ImagePreview;
                        }
                    }
                    foreach (var document in shopperProfileData.Documents)
                    {
                        foreach (var upload in document.DocumentToUploadIds)
                        {
                            if (!upload.ImagePreview.Contains("http"))
                            {
                                upload.ImagePreview = Settings.Default.VpSecureBaseUrl + upload.ImagePreview;
                            }
                        }
                    }

                    return shopperProfileData;
                }
            }
            catch (WebException e)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error getting vp monolith api response", e);
                throw;
            }
        }

        internal IEnumerable<ImageData> GetShopperUploads(int shopperKey, int? maxCount = null)
        {
            var shopperProfileData = GetShopperProfile(shopperKey);
            var recentUploads = shopperProfileData.ShopperUploads
                .Select(u => new ImageData
                {
                    ImageId = u.ImageId,
                    ImagePreview = u.ImagePreview.StartsWith("http") ? u.ImagePreview : Settings.Default.VpSecureBaseUrl + u.ImagePreview,
                    Type = u.Type,
                    UploadDate = u.UploadDate
                });

            if (maxCount != null)
            {
                recentUploads = recentUploads.Take(maxCount.Value);
            }

            return recentUploads;
        }

        internal ImageData GetShopperUpload(int shopperKey, int uploadId)
        {
            var shopperProfileData = GetShopperProfile(shopperKey);

            var upload = shopperProfileData.ShopperUploads.FirstOrDefault(u => u.ImageId == uploadId);
            return new ImageData
            {
                ImageId = upload.ImageId,
                ImagePreview = upload.ImagePreview.StartsWith("http") ? upload.ImagePreview  : Settings.Default.VpSecureBaseUrl + upload.ImagePreview,
                Type = upload.Type,
                UploadDate = upload.UploadDate
            };
        }
    }
}