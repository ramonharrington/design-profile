﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DesignProfile.Controllers;
using DesignProfile.data;

namespace DesignProfile.Adapters
{
    public static class DesignComboProvider
    {
        private static IEnumerable<GalleryProductData> _products = new List<GalleryProductData>
        {
            new GalleryProductData("B73", "B73", "Business Cards", "161"),
            new GalleryProductData("048", "HJA", "T-Shirts", "191"),
            new GalleryProductData("057", "057", "Hats", "15", "347"),
            new GalleryProductData("679", "679", "Mouse Pads", "50"),
            new GalleryProductData("JTG", "376", "Pens", "331", "4098"),
            new GalleryProductData("A0Y", "A0Y", "Mugs", "145", "904"),
            new GalleryProductData("043", "043", "Lawn Signs", "200"),
            new GalleryProductData("016", "016", "Banners", "58"),
            new GalleryProductData("051", "051", "Car door magnets", "193")
        };

        /// <summary>
        /// Gets a list of templates based on the profile settings
        /// </summary>
        /// <param name="profile">The profile.</param>
        /// <param name="categoryDesignCount">The category design count.</param>
        /// <returns></returns>
        internal static async Task<IDictionary<GalleryProductData, IEnumerable<ComboData>>> GetCombosForProfile(
            Profile profile, int categoryDesignCount = 5)
        {
            var companyInfo = new Dictionary<string, string>();

            companyInfo["companyname"] = profile.Business.Name;
            companyInfo["companymessage"] = profile.Business.Tagline;
            companyInfo["fullname"] = profile.Business.EmployeeName;
            companyInfo["jobtitle"] = profile.Business.JobTitle;
            companyInfo["email"] = profile.Business.Email;
            companyInfo["phone"] = profile.Business.Phone1;
            companyInfo["web"] = profile.Business.Url;
            companyInfo["address1"] = profile.Business.Address1;
            companyInfo["address2"] = profile.Business.Address2;
            companyInfo["address3"] = profile.Business.City;
            companyInfo["fax"] = profile.Business.Phone2;
            var keyword = profile.Business.Category ?? "business";

            var results = new Dictionary<GalleryProductData, IEnumerable<ComboData>>();

            using (var client = new HttpClient())
            using (var c =
                new SqlConnection("Server=dbcore.vpdev.com;Database=inspirational_content;UID=vistaread;PWD=darkmagic"))
            {
                c.Open();

                foreach (var product in _products)
                {
                    var url = "https://5.gallery.vpsvc.com/api/search/pg/" + product.ProductGroup +
                              "/culture/en-us?keyword=" + keyword;
                    var response = await client.GetAsync(url);
                    var keywordSearchResult = await response.Content.ReadAsAsync<KeywordSearchResult>();
                    int counter = 0;
                    var productResults = new List<ComboData>();

                    for (var i = 5; i >= 1; i--)
                    {
                        if (keywordSearchResult.UniqueEntityIds.ContainsKey(i))
                        {
                            var resultBracket = keywordSearchResult.UniqueEntityIds[i].ToList();

                            if (resultBracket.Any() && counter < categoryDesignCount)
                            {
                                var query = c.CreateCommand();
                                query.CommandType = CommandType.Text;
                                query.CommandText =
                                    "WITH cte AS ( " +
                                    $"SELECT DISTINCT e1.unique_entity_id, e2.entity_id FROM sdd_ag_entity_attribute_link e1 LEFT JOIN sdd_ag_entity e2 ON e1.unique_entity_id = e2.unique_entity_id WHERE attribute_id = 11 AND attribute_alt_value = '{product.ProductId}' AND e1.unique_entity_id IN ({string.Join(", ", resultBracket)}) " +
                                    "), " +
                                    "cteRanked AS( " +
                                    "SELECT entity_id, COALESCE(abs_rank, 0) AS Ranking, l.layout_id, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"companyname\"%' THEN 1 ELSE 0 END AS CompanyName, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"companymessage\"%' THEN 1 ELSE 0 END AS CompanyMessage, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"fullname\"%' THEN 1 ELSE 0 END AS FullName, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"jobtitle\"%' THEN 1 ELSE 0 END AS JobTitle, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"email\"%' THEN 1 ELSE 0 END AS Email, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"phone\"%' THEN 1 ELSE 0 END AS Phone, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"web\"%' THEN 1 ELSE 0 END AS Web, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"address1\"%' THEN 1 ELSE 0 END AS Address1, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"address2\"%' THEN 1 ELSE 0 END AS Address2, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"address3\"%' THEN 1 ELSE 0 END AS Address3, " +
                                    "CASE WHEN l.xml LIKE '%vppname=\"fax\"%' THEN 1 ELSE 0 END AS Fax " +
                                    "FROM cte " +
                                    "LEFT JOIN sdd_ag_entity_ranking r ON cte.unique_entity_id = r.unique_entity_id " +
                                    "LEFT JOIN sdd_ic_combo c ON cte.entity_id = c.combo_id " +
                                    "LEFT JOIN sdd_ic_layout l ON c.layout_id = l.layout_id " +
                                    ") " +
                                    "SELECT entity_id, CompanyName, CompanyMessage, FullName, JobTitle, Email, Phone, Web, Address1, Address2, Address3, Fax, MAX(Ranking) AS Ranking FROM cteRanked GROUP BY entity_id, CompanyName, CompanyMessage, FullName, JobTitle, Email, Phone, Web, Address1, Address2, Address3, Fax " +
                                    "ORDER BY MAX(Ranking) DESC";

                                try
                                {
                                    var reader = query.ExecuteReader();

                                    while (reader.Read() && counter < categoryDesignCount)
                                    {
                                        productResults.Add(new ComboData(reader["entity_id"].ToString(),
                                            reader["companyname"].ToString() == "1", reader["companymessage"].ToString() == "1",
                                            reader["fullname"].ToString() == "1", reader["jobtitle"].ToString() == "1",
                                            reader["email"].ToString() == "1", reader["phone"].ToString() == "1",
                                            reader["web"].ToString() == "1", reader["address1"].ToString() == "1",
                                            reader["address2"].ToString() == "1", reader["address3"].ToString() == "1",
                                            reader["fax"].ToString() == "1", int.Parse(reader["Ranking"].ToString())));
                                        counter++;
                                    }
                                    reader.Close();
                                }
                                catch (Exception ex)
                                {
                                    return new Dictionary<GalleryProductData, IEnumerable<ComboData>>();
                                }
                            }
                        }
                    }

                    results.Add(product, productResults);
                }

                c.Close();
            }

            return results;
        }

        private class KeywordSearchResult
        {
            public string Keyword { get; set; }
            public IList<int> AttributeValueLinkIds { get; set; }
            public IDictionary<int, IEnumerable<long>> UniqueEntityIds { get; set; }
        }
    }

    internal class GalleryProductData
    {
        public string ProductId { get; set; }
        public string BrandshopProductId { get; set; }
        public string Name { get; set; }
        public string ProductGroup { get; set; }
        public string SceneId { get; set; }

        public GalleryProductData(string productId, string brandshopProductId, string name, string productGroup, string sceneId = null)
        {
            ProductId = productId;
            BrandshopProductId = brandshopProductId;
            Name = name;
            ProductGroup = productGroup;
            SceneId = sceneId;
        }
    }

    internal class ComboData
    {
        public string ComboId { get; set; }
        public bool CompanyName { get; set; }
        public bool CompanyMessage { get; set; }
        public bool FullName { get; set; }
        public bool JobTitle { get; set; }
        public bool Email { get; set; }
        public bool Phone { get; set; }
        public bool Web { get; set; }
        public bool Address1 { get; set; }
        public bool Address2 { get; set; }
        public bool Address3 { get; set; }
        public bool Fax { get; set; }
        public int Ranking { get; set; }

        public ComboData(string comboId, bool companyName, bool companyMessage, bool fullName, bool jobTitle, bool email, bool phone, bool web, bool address1, bool address2, bool address3, bool fax, int ranking)
        {
            ComboId = comboId;
            CompanyName = companyName;
            CompanyMessage = companyMessage;
            FullName = fullName;
            JobTitle = jobTitle;
            Email = email;
            Phone = phone;
            Web = web;
            Address1 = address1;
            Address2 = address2;
            Address3 = address3;
            Fax = fax;
            Ranking = ranking;
        }
    }
}