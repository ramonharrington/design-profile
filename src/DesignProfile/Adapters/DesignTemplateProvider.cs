﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DesignProfile.data;

namespace DesignProfile.Adapters
{
    public static class DesignTemplateProvider
    {
        /// <summary>
        /// Gets a list of templates based on the profile settings
        /// </summary>
        /// <param name="items">The items.</param>
        /// <param name="doctemplates">The doctemplates.</param>
        /// <returns></returns>
        public static IDictionary<string, int> GetTemplatesWithColorCount(this IQueryable<data.TemplateItem> items)
        {
            var templates = items.GroupBy(x => x.template_id).Select(grp => grp.Key);
            var templateColorLookup = new Dictionary<string, int>();
            foreach (var template in templates)
            {
                int secondaryColorCount = items.Any(i => i.template_id == template &&
                                                        i.configuration.ToLower().Contains("\"color\":\"secondary\"")) ? 1 : 0;
                var tertiaryColorCount = items.Any(i => i.template_id == template &&
                                                        i.configuration.ToLower().Contains("\"color\":\"tertiary\"")) ? 1 : 0;
                templateColorLookup.Add(template, 1 + secondaryColorCount + tertiaryColorCount);
            }

            return templateColorLookup;
        }
    }
}