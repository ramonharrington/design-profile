﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using DesignProfile.data;
using Hatchery.Imaging;
using Hatchery.Imaging.Data;
using Color = System.Drawing.Color;
using Font = System.Drawing.Font;

namespace DesignProfile.Adapters
{
    public class ImagingAdapter
    {
        public Bitmap GetBitmapFromBytes(byte[] imageData)
        {
            MemoryStream ms = new MemoryStream(imageData);
            return new Bitmap(ms);
        }

        public byte[] GetBytesFromBitmap(Bitmap image)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[]) converter.ConvertTo(image, typeof(byte[]));
        }

        public bool IsLogo(Bitmap image)
        {
            return Analysis.IsLogo(image);
        }

        public bool ContainsTransparentPixels(Bitmap image)
        {
            return Analysis.ContainsTransparentPixels(image);
        }

        public Color? GetBackgroundColor(Bitmap image)
        {
            return ColorUtils.GetBackgroundColor(image);
        }

        public IEnumerable<KeyValuePair<Color, int>> GetImageColors(Bitmap image, int tolerance = 70,
            int? maxNumberOfColors = default(int?))
        {
            return ColorUtils.GetImageColors(image, tolerance, maxNumberOfColors);
        }

        public IEnumerable<string> DeprioritizeGreyColors(IEnumerable<string> colors)
        {
            if (colors == null || !colors.Any())
            {
                return colors;
            }

            var colorsToUse = new List<string>();
            foreach (var color in colors.Where(c => !IsGrayShade(c)))
            {
                colorsToUse.Add(color);
            }
            foreach (var color in colors.Where(c => IsGrayShade(c)))
            {
                colorsToUse.Add(color);
            }

            return colorsToUse;
        }

        public IEnumerable<KeyValuePair<Color, int>> GetImageColors(byte[] image, int tolerance = 70,
            int? maxNumberOfColors = default(int?))
        {
            return ColorUtils.GetImageColors(image, tolerance, maxNumberOfColors);
        }

        public int ColorDifference(Color color1, Color color2)
        {
            return ColorUtils.ColorDifference(color1, color2);
        }

        public bool AreSameColor(Color color1, Color color2, int tolerance)
        {
            var difference = ColorDifference(color1, color2);
            return difference < (tolerance * tolerance);
        }

        public IEnumerable<string> GetTagsFromImages(Bitmap image)
        {
            return null;
        }

        public byte[] GetImageWithoutBackground(Bitmap image)
        {
            return ColorUtils.KnockoutColor(image);
        }

        public byte[] CropImage(byte[] imageData, float cropTop, float cropRight, float cropBottom, float cropLeft)
        {
            return GetBytesFromBitmap(ImageUtils.CropImage(GetBitmapFromBytes(imageData), cropTop, 1.0f - cropRight,
                1.0f - cropBottom, cropLeft));
        }

        public Bitmap PadBitmap(Bitmap image, int padding)
        {
            if (padding <= 0)
            {
                return image;
            }

            return ImageUtils.PadImage(image, padding);
        }

        /// <summary>
        /// Gets the scene.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <param name="mask">The mask.</param>
        /// <param name="placementImages">The placement images.</param>
        /// <param name="placementProducts">The placement products.</param>
        /// <param name="outputFormat">The output format.</param>
        /// <param name="width">The width.</param>
        /// <returns></returns>
        public byte[] GetScene(Bitmap scene, Bitmap mask, IDictionary<string, Bitmap> placementImages,
            IEnumerable<PlacementProduct> placementProducts, int? width, string outputFormat)
        {
            var placements = new List<PlacementData>();

            foreach (var placementProduct in placementProducts)
            {
                var placementData = new PlacementData
                {
                    TopLeft = new Point(placementProduct.ScenePlacement.TopLeftX,
                        placementProduct.ScenePlacement.TopLeftY),
                    TopRight = new Point(placementProduct.ScenePlacement.TopRightX,
                        placementProduct.ScenePlacement.TopRightY),
                    BottomRight =
                        new Point(placementProduct.ScenePlacement.BottomRightX,
                            placementProduct.ScenePlacement.BottomRightY),
                    BottomLeft =
                        new Point(placementProduct.ScenePlacement.BottomLeftX,
                            placementProduct.ScenePlacement.BottomLeftY),
                    ShadowLevel = (float) placementProduct.ScenePlacement.ShadowLevel.Value,
                    MaskColor = ColorTranslator.FromHtml(placementProduct.ScenePlacement.MaskColor ?? "#FFFFFF"),
                    PlacementImageKey = placementProduct.TemplateId
                };

                if (placementProduct.ScenePlacement.TopCenterX.HasValue &&
                    placementProduct.ScenePlacement.TopCenterY.HasValue)
                {
                    placementData.TopCenter = new Point(placementProduct.ScenePlacement.TopCenterX.Value,
                        placementProduct.ScenePlacement.TopCenterY.Value);
                }
                if (placementProduct.ScenePlacement.RightMiddleX.HasValue &&
                    placementProduct.ScenePlacement.RightMiddleY.HasValue)
                {
                    placementData.RightMiddle = new Point(placementProduct.ScenePlacement.RightMiddleX.Value,
                        placementProduct.ScenePlacement.RightMiddleY.Value);
                }
                if (placementProduct.ScenePlacement.BottomCenterX.HasValue &&
                    placementProduct.ScenePlacement.BottomCenterY.HasValue)
                {
                    placementData.BottomCenter = new Point(placementProduct.ScenePlacement.BottomCenterX.Value,
                        placementProduct.ScenePlacement.BottomCenterY.Value);
                }
                if (placementProduct.ScenePlacement.LeftMiddleX.HasValue &&
                    placementProduct.ScenePlacement.LeftMiddleY.HasValue)
                {
                    placementData.LeftMiddle = new Point(placementProduct.ScenePlacement.LeftMiddleX.Value,
                        placementProduct.ScenePlacement.LeftMiddleY.Value);
                }

                if (placementProduct.SceneProduct != null && (placementProduct.SceneProduct.TemplateTopEdge != null ||
                                                              placementProduct.SceneProduct.TemplateRightEdge != null ||
                                                              placementProduct.SceneProduct.TemplateBottomEdge !=
                                                              null ||
                                                              placementProduct.SceneProduct.TemplateLeftEdge != null))
                {
                    placementData.ImageView = new ProductView
                    {
                        TopEdge = (float?) placementProduct.SceneProduct.TemplateTopEdge,
                        RightEdge = (float?) placementProduct.SceneProduct.TemplateRightEdge,
                        BottomEdge = (float?) placementProduct.SceneProduct.TemplateBottomEdge,
                        LeftEdge = (float?) placementProduct.SceneProduct.TemplateLeftEdge
                    };
                }

                placements.Add(placementData);
            }

            return Hatchery.Imaging.SceneUtils.SuperImposeWithMask(scene, placementImages, mask, placements, width,
                true, false, outputFormat);
        }

        public static bool IsGrayShade(string colorString)
        {
            try
            {
                var color = ColorTranslator.FromHtml(colorString);
                return (Math.Abs(color.R - color.G) <= 5 && Math.Abs(color.G - color.B) <= 5 &&
                        Math.Abs(color.R - color.B) <= 5);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public string GetBase64TextImage(string text)
        {
            using (var bitmap = new Bitmap(600, 50))
            using (var graphicImage = Graphics.FromImage(bitmap))
            {
                graphicImage.SmoothingMode = SmoothingMode.AntiAlias;
                graphicImage.DrawString(text, new Font("Arial", 18, FontStyle.Regular), SystemBrushes.WindowText, new Point(10, 10));

                var ms = new MemoryStream();
                bitmap.Save(ms, ImageFormat.Png);
                ms.Position = 0;
                var byteBuffer = ms.ToArray();
                ms.Close();
                var base64String = Convert.ToBase64String(byteBuffer);
                byteBuffer = null;
                return base64String;
            }
        }
    }

    public class PlacementProduct
    {
        public SceneProduct SceneProduct { get; set; }
        public ScenePlacement ScenePlacement { get; set; }
        public string TemplateId { get; set; }
    }
}