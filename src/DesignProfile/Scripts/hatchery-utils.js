/**
    Hatchery util library for common tasks
**/

var HATCHERY = HATCHERY || (function () {
    var _args = {}; // private
    var start_time = new Date().getTime();

    return {

        init : function(args) {
            _args = args;

            //initialize jquery if not already defined
            if (typeof ($) === 'undefined') {
                var jq = document.createElement('script');
                jq.src = "https://code.jquery.com/jquery-2.2.4.min.js";

                jq.onload = function() {
                    this.initializeClickTracker();

                    // page load
                    var end_time = new Date().getTime();
                    var load_time = end_time - start_time;

                    this.tracker("page_load", (load_time / 1000));

                    // setup error handling / capturing
                    window.onerror = function (message, source, lineno, colno, error) {
                        // if not production then do not capture
                        if (/localhost|vpdev|vptest/.test(window.location.hostname)) {
                            return;
                        }

                        // hipchat vars
                        var key = 'nXnlLckIgfVrrvssleM2vblt4wFpfYcitWW9gwM3';
                        var room = 2900248;
                        var url = "https://api.hipchat.com/v2/room/" + room + "/notification?auth_token=" + key;

                        // browser detection
                        navigator.sayswho = (function () {
                            var N = navigator.appName, ua = navigator.userAgent, tem;
                            var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
                            if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
                            M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
                            return M;
                        })();

                        var msg = {
                            color: "yellow",
                            message_format: "html",
                            message: "<br/>js error<br/>"
                                + "url: " + window.location.href + "<br/"
                                + "message:" + message + "<br/>"
                                + "source: " + source + "<br/>"
                                + "line: " + lineno + "<br/>"
                                + "col: " + colno + "<br/>"
                                + "error: " + error + "<br/>"
                                + "browser: " + navigator.sayswho,
                            notify: false,
                        };

                        $.ajax({
                            url: url,
                            method: "post",
                            data: JSON.stringify(msg),
                            contentType: 'application/json',
                        }).success(function (resp) {
                        }).error(function (resp) {
                            console.log("not posted");
                        });
                    }
                }.bind(this);

                document.getElementsByTagName('head')[0].appendChild(jq);
            }
        },

        alertSaleError: function (message, name, address) {
            // if not production then do not capture
            if (/localhost|vpdev|vptest/.test(window.location.hostname)) {
                return;
            }

            // hipchat vars
            var key = 'xNxBZXRwSgP3b6R5h3A9p1VeeWGALIRRz5JT66ee';
            var room = 2772849;
            var url = "https://api.hipchat.com/v2/room/" + room + "/notification?auth_token=" + key;

            // browser detection
            navigator.sayswho = (function () {
                var N = navigator.appName, ua = navigator.userAgent, tem;
                var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
                if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
                M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
                return M;
            })();

            var msg = {
                color: "red",
                message_format: "html",
                message: "<br/>new order error<br/>"
                    + "url: " + window.location.href + "<br/"
                    + "message:" + message + "<br/>"
                    + "name: " + name + "<br/>"
                    + "address: " + address + "<br/>"
                    + "browser: " + navigator.sayswho,
                notify: false,
            };

            $.ajax({
                url: url,
                method: "post",
                data: JSON.stringify(msg),
                contentType: 'application/json',
            }).success(function (resp) {
                console.log("posted");
            }).error(function (resp) {
                console.log("not posted");
            });
        },

        tracker: function (type, value, channel) {

          if(typeof HATCHERY.ip_address === 'undefined'){
            $.get('https://api.ipify.org?format=json&callback=?', function(response){
              HATCHERY.ip_address = response.ip;
              HATCHERY.tracker(type,value,channel);
            });

            return;
          }

          if(typeof HATCHERY_FINGERPRINT === 'undefined'){
            setTimeout(function(){
              HATCHERY.tracker(type,value,channel);
            }, 1000);

            return;
          }

          var data = {
              event_type: type,
              event_value: value,
              page_url: window.location.toString(),
              channel: channel,
              user_id_type: 'browser_fingerprint',
              user_id: HATCHERY_FINGERPRINT,
              ip_address_created: HATCHERY.ip_address,
              referrer: document.referrer,
              session_id: window.sessionId,
              user_agent: navigator.userAgent
          };

          $.ajax({
              url: '/api/tracker',
              accepts: 'application/json',
              data: JSON.stringify(data),
              dataType: 'json',
              contentType: 'application/json',
              method: 'POST',
              error: function (){}
          });
        },

        initializeClickTracker: function () {
            $("a").click(function (e) {
                var value = getNodeIdent(e.target);
                HATCHERY.tracker("click", value);
            });
            $('.tp-track-clicks').click(function(e) {
                var value = getNodeIdent(e.target);
                HATCHERY.tracker("click", value);
            });

            if (typeof _args.track !== 'undefined') {
                for(var i =0; i < _args.track.length; i++) {
                    $(_args.track[i]).click(function (e) {
                        var value = getNodeIdent(e.target);

                        HATCHERY.tracker("click", value);
                    });
                }
            }
        },
    };

    function getNodeIdent(node) {

        var value = node.getAttribute('data-href');
        if (value === null) {
            value = node.getAttribute('href');
        }
        if (value === null || /javascript/.test(value) === true) {
            value = node.id;
        }
        if (value === '') {
            value = node.getAttribute("class");
        }

        if (value === null) {
            if (node.parentNode === null) {
                return null;
            } else {
                return getNodeIdent(node.parentNode);
            }
        } else {
            return value;
        }
    }
}());
