document.addEventListener("DOMContentLoaded", function(event) {
    //get rid of vp layout
    var boundingBox = document.getElementsByClassName("content-bounding-box")[0];
    if(boundingBox){
        boundingBox.style.maxWidth = "100%";
    }
    
    var header = document.getElementsByTagName("nav");
    for(var i = header.length -1; i >= 0; i--){
        header[i].remove();
    }
    var footer = document.getElementsByTagName("footer");
    for(var i = footer.length -1; i >= 0; i--){
        footer[i].remove();
    }
    var mobileHeader = document.getElementsByClassName("header-and-nav");
    for(var i = mobileHeader.length -1; i >= 0; i--){
        mobileHeader[i].remove();
    }

    var sitesBar = document.getElementsByClassName("sites-bar");
    for(var i = sitesBar.length -1; i >= 0; i--){
        sitesBar[i].remove();
    }

    //load brandshop react
    if(window.location.href.includes('startLogo')){
      window.hatchery_shopType = 'startLogo';
    }else{
      window.hatchery_shopType = "brandshop";
    }

    var iframe = document.getElementById("designProfileFrame");
	if (typeof iframe !== 'undefined' && iframe !== null) {
		var app = document.createElement("div");
		app.id = "app";
		iframe.parentElement.appendChild(app);
		iframe.parentElement.replaceChild(app, iframe);
	}
    var scr = document.createElement("script");
    scr.onload = function(){
        var shopScr = document.createElement("script");
        if(window.location.host.includes("vpdev")){
            shopScr.src = "https://dev.designprofile.vistaprint.io/giftshop/app.min.js";
        }
        else if(window.location.host.includes("vptest")){
            shopScr.src = "https://test.designprofile.vistaprint.io/giftshop/app.min.js";
        }
        else if(window.location.host.includes("fermonolith")){
            shopScr.src = "https://loc.designprofile.vistaprint.io:9003/app.js";
        }
        else if(window.location.host.includes("loc.")){
            shopScr.src = "https://loc.designprofile.vistaprint.io:9003/app.js";
        }
        else if(window.location.host.includes("vistaprint")){
            shopScr.src = "https://designprofile.vistaprint.io/giftshop/app.min.js";
        }
        else{
            shopScr.src = "https://loc.designprofile.vistaprint.io:9003/app.js";
        }
        document.getElementsByTagName("head")[0].appendChild(shopScr);
    };
    scr.src = "https://cdnjs.cloudflare.com/ajax/libs/react/16.0.0/umd/react.production.min.js";
    document.getElementsByTagName("head")[0].appendChild(scr);
});





// Google Tag Manager
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NR2MHFP');


function addGTM(gtmId){
  let headScript = document.createElement('script');
  headScript.innerHTML = "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','"+gtmId+"');";

  var nos = document.createElement("noscript");
  var iframe = document.createElement("iframe");
  iframe.src = "https://www.googletagmanager.com/ns.html?id=GTM-NR2MHFP";
  iframe.height="0";
  iframe.width="0";
  iframe.style="display:none;visibility:hidden";

  nos.appendChild(iframe);
  document.body.insertBefore(nos, document.body.firstChild);
  document.head.insertBefore(headScript, document.head.firstChild);
}

if(window.location.href.includes('startLogo')){
  addGTM('GTM-TPD624C');
}else{
  addGTM('GTM-NR2MHFP');
}

