// Google Tag Manager
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NB8MBKK');


function addGTM(){
  var body = document.getElementsByTagName('body')[0];
  var iframe = document.createElement("iframe");
  var nos = document.createElement("noscript");

  iframe.src = "https://www.googletagmanager.com/ns.html?id=GTM-NB8MBKK";
  iframe.height="0";
  iframe.width="0";
  iframe.style="display:none;visibility:hidden";

  nos.appendChild(iframe);
  body.appendChild(nos);
}


addGTM();
