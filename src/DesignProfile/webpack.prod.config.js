/// <binding />
var path = require('path');
var webpack = require('webpack');
var precss = require('precss');
var autoprefixer = require('autoprefixer');

process.env.NODE_ENV = 'production';

require('es6-promise').polyfill();

module.exports = {
  context: path.join(__dirname, 'Content'),
  entry: [
    './server'
  ],
  output: {
    path: path.join(__dirname, 'Content'),
    filename: '[name].bundle.min.js',
  },
  devtool: 'cheap-module-source-map',
  module: {
    loaders: [
      // Transform JSX in .jsx files
      {
          test: /\.jsx$/,
          loader: 'babel',
          exclude: /node_modules/,
      },
      {
        test: /\.scss/,
        loaders: ['style', 'css', 'sass']
      },
      // CSS inlining and post-css filters
      {
          test: /\.css$/,
          loader: 'style-loader!css-loader?-minimize!postcss-loader',
      },
    ],
  },
  postcss: function() {
      return [precss, autoprefixer];
  },
  resolve: {
    // Allow require('./blah') to require blah.jsx
    extensions: ['', '.js', '.jsx']
  },
  externals: {
    // Use external version of React (from CDN for client-side, or
    // bundled with ReactJS.NET for server-side)
    //react: 'React'
  },
  plugins:[
    new webpack.DefinePlugin({
      'process.env':{
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress:{
        warnings: false
      }
    })
  ]
};
