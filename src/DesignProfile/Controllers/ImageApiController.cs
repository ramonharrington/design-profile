﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using DesignProfile.Adapters;
using DesignProfile.Controllers.Extensions;
using DesignProfile.data;
using DesignProfile.Models;
using DesignProfile.Properties;
using DesignProfile.Tasks;
using DesignProfile.Utils;
using DesignProfile.Validation;
using Hangfire;
using Newtonsoft.Json;
using Image = DesignProfile.data.Image;

namespace DesignProfile.Controllers
{
    /// <summary>
    /// Used to manage images
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ImageApiController : ApiController
    {
        [Route("api/Image/{id}/isInPortfolio")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> GetImage(int id)
        {
            int? shopperKey = null;
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var image = await ctx.Images.FindAsync(id);
                shopperKey = image?.Business?.Account?.ShopperKey;
                if (shopperKey == null)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.NotFound
                    };
                }

                if (Request.GetShopperKey() != shopperKey)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.Unauthorized
                    };
                }

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(image.UploadId != null), Encoding.UTF8,
                        "application/json")
                };
            }
        }

        [Route("api/Image/HasTransparentPixels")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> HasTransparentPixels(string imageUrl)
        {
            var url = System.Web.HttpUtility.UrlDecode(imageUrl);
            if (!url.StartsWith("http"))
            {
                url = $"{Settings.Default.VpBaseUrl}{url}";
            }

            try
            {
                using (var client = new WebClient())
                {
                    var data = await client.DownloadDataTaskAsync(url);
                    var image = (Bitmap) System.Drawing.Image.FromStream(new MemoryStream(data));

                    return new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(new ImagingAdapter().ContainsTransparentPixels(image).ToString(),
                            Encoding.UTF8, "application/text")
                    };
                }
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [Route("api/Image/GetBackgroundColor")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> GetBackgroundColor(string imageUrl)
        {
            var url = System.Web.HttpUtility.UrlDecode(imageUrl);
            if (!url.StartsWith("http"))
            {
                url = $"{Settings.Default.VpBaseUrl}{url}";
            }

            try
            {
                using (var client = new WebClient())
                {
                    var data = await client.DownloadDataTaskAsync(url);
                    var image = (Bitmap) System.Drawing.Image.FromStream(new MemoryStream(data));
                    var color = new ImagingAdapter().GetBackgroundColor(image);

                    return new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(color == null ? "" : ColorTranslator.ToHtml(color.Value),
                            Encoding.UTF8, "application/text")
                    };
                }
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Upload image(s) for the specified business.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <param name="cropTop">The crop top.</param>
        /// <param name="cropRight">The crop right.</param>
        /// <param name="cropBottom">The crop bottom.</param>
        /// <param name="cropLeft">The crop left.</param>
        /// <param name="synchronous">if set to <c>true</c> wait on upload completion.</param>
        /// <returns></returns>
        /// <exception cref="HttpResponseException"></exception>
        [Route("api/Image")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> Post(int profileId, float cropTop = 0, float cropRight = 0,
            float cropBottom = 0, float cropLeft = 0, bool synchronous = false)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var files = new Dictionary<string, byte[]>();
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);

            foreach (var file in provider.Contents)
            {
                try
                {
                    var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
                    var buffer = await file.ReadAsByteArrayAsync();

                    files.Add(filename, buffer);
                }
                catch (Exception ex)
                {
                    // unable to get file
                }
            }

            var settings = await AddImagesAndUpdateProfiles(profileId, files, cropTop, cropRight, cropBottom, cropLeft, synchronous, "giftshop");

            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(settings), Encoding.UTF8,
                    "application/json")
            };
        }

        [Route("api/Image/{id}/Crop")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> UpdateImageCrop(int id, int profileId, float cropTop = 0, float cropRight = 0,
            float cropBottom = 0, float cropLeft = 0, bool synchronous = false)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FindAsync(profileId);
                if (profile == null)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.NotFound
                    };
                }

                var image = await ctx.Images.FindAsync(id);
                if (image == null)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.NotFound
                    };
                }

                if (!profile.Business.Images.Any(i => i.Id == id))
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.NotFound
                    };
                }

                try
                {
                    image.CropTop = cropTop;
                    image.CropRight = cropRight;
                    image.CropBottom = cropBottom;
                    image.CropLeft = cropLeft;

                    await ctx.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error updating crop size of image", ex);

                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.InternalServerError
                    };
                }

                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK
                };
            }
        }

        /// <summary>
        /// Upload logo for the specified business.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <param name="cropTop">The crop top.</param>
        /// <param name="cropRight">The crop right.</param>
        /// <param name="cropBottom">The crop bottom.</param>
        /// <param name="cropLeft">The crop left.</param>
        /// <param name="synchronous">if set to <c>true</c> [synchronous].</param>
        /// <returns></returns>
        /// <exception cref="HttpResponseException"></exception>
        [Route("api/Logo")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> UploadLogo(int profileId, float cropTop = 0, float cropRight = 0, float cropBottom = 0, float cropLeft = 0, bool synchronous = false, string shopname = "undefined")
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);

            if (!provider.Contents.Any())
            {
                throw new HttpResponseException(HttpStatusCode.ExpectationFailed);
            }

            string fileName;
            byte[] imageData;
            try
            {
                var file = provider.Contents.First();
                fileName = file.Headers.ContentDisposition.FileName.Trim('\"');
                imageData = await file.ReadAsByteArrayAsync();
            }
            catch (Exception ex)
            {
                // unable to get file
                throw new HttpResponseException(HttpStatusCode.ExpectationFailed);
            }

            var settings = await AddLogoToProfile(Request.GetShopperKey() ?? 0, profileId, fileName, imageData, cropTop, cropRight, cropBottom, cropLeft, synchronous, shopname);

            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(settings), Encoding.UTF8,
                    "application/json")
            };
        }

        /// <summary>
        /// Select an existing image as the profile logo
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <param name="imageId">The image identifier.</param>
        /// <returns></returns>
        [Route("api/Profile/{profileId}/Logo/{imageId}")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> SelectLogoFromPortfolio(int profileId, int imageId)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FindAsync(profileId);

                if (profile == null)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.NotFound
                    };
                }

                if (!profile.Business.Account.CanView(Request))
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.Unauthorized
                    };
                }

                Image image = null;
                var shopperKey = Request.GetShopperKey().Value;
                if (ctx.Images.Any(i => i.UploadId == imageId && i.Business.Account.ShopperKey == shopperKey))
                {
                    image = await ctx.Images.FirstAsync(i => i.UploadId == imageId);
                }
                else
                {
                    var vpProfileAdapter = new VpProfileAdapter();
                    var upload = vpProfileAdapter.GetShopperUpload(shopperKey, imageId);

                    ctx.Images.Add(new Image
                    {
                        ImageTypeId = 1,
                        BusinessId = profile.BusinessId,
                        Title = "Portfolio image " + upload.ImageId,
                        Uploaded = DateTime.Now,
                        Url = upload.ImagePreview,
                        UploadId = upload.ImageId
                    });

                    await ctx.SaveChangesAsync();

                    image = await ctx.Images.FirstAsync(i => i.UploadId == imageId && i.Business.Account.ShopperKey == shopperKey);
                }

                if (image == null)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.NotFound
                    };
                }

                if (!image.Business.Account.CanView(Request))
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.Unauthorized
                    };
                }

                // Mark the image as a logo, if it's not already the case
                if (image.ImageTypeId != 1)
                {
                    image.ImageTypeId = 1;
                }

                profile = await ctx.Profiles.FindAsync(profileId);
                var settings = JsonConvert.DeserializeObject<ProfileSettings>(profile.Settings);

                if (settings.images.logo == null)
                {
                    settings.images.logo = new imageData();
                }

                settings.images.logo.id = image.Id;
                settings.images.logo.url = image.Url;

                var newPictures = new List<imageData>();
                foreach (var profileImage in profile.Business.Images.Where(i => i.ImageTypeId == 2))
                {
                    newPictures.Add(new imageData
                    {
                        id = profileImage.Id,
                        url = profileImage.Url
                    });

                }
                settings.images.pictures = newPictures;

                var imagingAdapter = new ImagingAdapter();
                var newColors = imagingAdapter.GetImageColors(ImageRetrievalUtils.GetImageBytes(image.Url))?.Take(3)
                    ?.Select(c => "#" + c.Key.R.ToString("X2") + c.Key.G.ToString("X2") + c.Key.B.ToString("X2"));
                var colorsToUse = imagingAdapter.DeprioritizeGreyColors(newColors);
                settings.colors.primary = colorsToUse?.FirstOrDefault() ?? "";
                settings.colors.secondary = colorsToUse?.ElementAtOrDefault(1) ?? "";
                settings.colors.tertiary = colorsToUse?.ElementAtOrDefault(2) ?? "";

                profile.Settings = JsonConvert.SerializeObject(settings);
                await ctx.SaveChangesAsync();

                if (image.UploadId == null)
                {
                    try
                    {
                        BackgroundJob.Enqueue(() => ImageTransfer.AddImageToPortfolio(image.Id));
                    }
                    catch (Exception e)
                    {
                        var asdf = e.Message;
                    }
                }

                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(JsonConvert.SerializeObject(settings), Encoding.UTF8,
                        "application/json")
                };
            }
        }

        [Route("api/Image/GetColors")]
        [HttpGet]
        public HttpResponseMessage GetImageColors(string imageUrl)
        {
            try
            {
                var imagingAdapter = new ImagingAdapter();
                var profileColors = new ProfileColors();
                if (!imageUrl.StartsWith("http"))
                {
                    imageUrl = $"{Settings.Default.VpBaseUrl}{imageUrl}";
                }

                var detectedColors = imagingAdapter.GetImageColors(ImageRetrievalUtils.GetImageBytes(imageUrl))?.Take(5)
                    ?.Select(c => "#" + c.Key.R.ToString("X2") + c.Key.G.ToString("X2") + c.Key.B.ToString("X2"));

                var colorsToUse = imagingAdapter.DeprioritizeGreyColors(detectedColors);

                profileColors.Primary = colorsToUse?.FirstOrDefault() ?? "";
                profileColors.Secondary = colorsToUse?.ElementAtOrDefault(1) ?? "";
                profileColors.Tertiary = colorsToUse?.ElementAtOrDefault(2) ?? "";
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(JsonConvert.SerializeObject(profileColors), Encoding.UTF8,
                        "application/json")
                };
            }
            catch (Exception e)
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(e.Message, Encoding.UTF8, "application/text")
                };
            }
        }

        [Route("api/Image/{id}")]
        [HttpDelete]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> Delete(int id, int? profileId)
        {
            // TODO: After we add in the ability to get shopper key, remove the fallback to 0
            var shopperKey = Request.GetShopperKey() ?? 0;

            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var image = ctx.Images.FirstOrDefault(i => i.Id == id);

                if (image == null)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.NotFound
                    };
                }

                var businessId = image.BusinessId;

                if (!image.Business.Account.CanView(Request))
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.Unauthorized
                    };
                }

                ctx.Images.Remove(image);
                await ctx.SaveChangesAsync();

                S3Utils.RemoveImageFromAws(image.Url, shopperKey);

                // Update the profiles of the business with the new set of images
                var business = await ctx.Businesses.FindAsync(businessId);
                var pictures = business.Images.GetPictures();

                ProfileSettings profileSettings = null;
                foreach (var profile in business.Profiles)
                {
                    var settings = JsonConvert.DeserializeObject<ProfileSettings>(profile.Settings);
                    settings.images.pictures = pictures;
                    profile.Settings = JsonConvert.SerializeObject(settings);

                    if (profileId == null || profile.Id == profileId)
                    {
                        profileSettings = settings;
                    }
                }
                await ctx.SaveChangesAsync();

                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(JsonConvert.SerializeObject(profileSettings), Encoding.UTF8,
                        "application/json")
                };
            }
        }

        /// <summary>
        /// Gets the portfolio images from the shopper's vistaprint account.
        /// </summary>
        /// <returns></returns>
        [Route("api/Profile/{profileId}/ProfileImages")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> GetProfileImages(int profileId)
        {
            int? shopperKey = null;
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FindAsync(profileId);
                shopperKey = profile?.Business?.Account?.ShopperKey;
                if (shopperKey == null)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.NotFound
                    };
                }

                if (Request.GetShopperKey() != shopperKey)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.Unauthorized
                    };
                }

                var images = profile.Business.Images
                    .Select(i => new ImageData {ImageId = i.Id, ImagePreview = i.Url, Type = "ProfileImage", UploadDate = i.Uploaded})
                    .ToList();
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(images), Encoding.UTF8,
                        "application/json")
                };
            }
        }

        /// <summary>
        /// Gets the portfolio images from the shopper's vistaprint account.
        /// </summary>
        /// <returns></returns>
        [Route("api/Profile/{profileId}/PortfolioImages")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> GetPortfolioImages(int profileId)
        {
            int? shopperKey = null;
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                shopperKey = (await ctx.Profiles.FindAsync(profileId))?.Business?.Account?.ShopperKey;
                if (shopperKey == null)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.NotFound
                    };
                }

                if (Request.GetShopperKey() != shopperKey)
                {
                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.Unauthorized
                    };
                }
            }

            var vpProfileAdapter = new VpProfileAdapter();
            try
            {
                var recentUploads = vpProfileAdapter.GetShopperUploads(shopperKey.Value, 20);
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(recentUploads), Encoding.UTF8,
                        "application/json")
                };
            }
            catch (Exception e)
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError
                };
            }
        }

        /// <summary>
        /// Adds the images for the business and update related profiles.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <param name="files">The files.</param>
        /// <param name="cropTop">The crop top.</param>
        /// <param name="cropRight">The crop right.</param>
        /// <param name="cropBottom">The crop bottom.</param>
        /// <param name="cropLeft">The crop left.</param>
        /// <param name="synchronous">if set to <c>true</c> [synchronous].</param>
        /// <returns></returns>
        /// <exception cref="Exception">Business not found</exception>
        /// <exception cref="System.Exception">Business not found</exception>
        private async Task<ProfileSettings> AddImagesAndUpdateProfiles(int profileId,
            IDictionary<string, byte[]> files, float cropTop, float cropRight, float cropBottom, float cropLeft, bool synchronous, string shopName)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var business = ctx.Profiles.Find(profileId).Business;
                if (business == null)
                {
                    throw new Exception("Business not found");
                }

                if (!business.Account.CanView(Request))
                {
                    throw new Exception("Unauthorized");
                }

                foreach (var file in files)
                {
                    try
                    {
                        // Upload file to S3
                        Regex rgx = new Regex("[^a-zA-Z0-9.-]");
                        var fileKey = rgx.Replace(file.Key, "");
                        if (fileKey.Length > 50)
                        {
                            fileKey = fileKey.Substring(0, 50);
                        }
                        var s3Filename = Guid.NewGuid() + "_" + fileKey;
                        var url = S3Utils.AddDesignProfileImage(s3Filename, business.Account.ShopperKey?.ToString() ?? "null", file.Value, shopName);

                        var image = (Bitmap)System.Drawing.Image.FromStream(new MemoryStream(file.Value));

                        var newImage = new Image()
                        {
                            Title = fileKey,
                            BusinessId = business.Id,
                            Url = url,
                            Uploaded = DateTime.Now,
                            ImageTypeId = 2, // photo
                            CropTop = cropTop,
                            CropRight = cropRight,
                            CropBottom = cropBottom,
                            CropLeft = cropLeft,
                            Width = image.Width,
                            Height = image.Height
                        };
                        ctx.Images.Add(newImage);
                        ctx.SaveChanges();

                        if (synchronous)
                        {
                            await ImageTransfer.AddImageToPortfolio(newImage.Id);
                        }
                        else
                        {
                            BackgroundJob.Enqueue(() => ImageTransfer.AddImageToPortfolio(newImage.Id));
                        }
                    }
                    catch (Exception ex)
                    {
                        // unable to upload file
                    }
                }

                business = ctx.Businesses.First(b => b.Id == business.Id);

                // Update the profiles of the business with the new set of images
                var pictures = business.Images.GetPictures().OrderByDescending(i => i.id).Take(1);
                ProfileSettings profileSettings = null;

                foreach (var profile in business.Profiles)
                {
                    var settings = JsonConvert.DeserializeObject<ProfileSettings>(profile.Settings);
                    settings.images.pictures = pictures;
                    profile.Settings = JsonConvert.SerializeObject(settings);

                    if (profile.Id == profileId)
                    {
                        profileSettings = settings;
                    }
                }
                ctx.SaveChanges();

                return profileSettings;
            }
        }

        /// <summary>
        /// Upload the logo and assign it to the profile.
        /// </summary>
        /// <param name="shopperKey">The shopper key.</param>
        /// <param name="profileId">The profile identifier.</param>
        /// <param name="logoName">Name of the logo.</param>
        /// <param name="logoData">The logo data.</param>
        /// <param name="cropTop">The crop top.</param>
        /// <param name="cropRight">The crop right.</param>
        /// <param name="cropBottom">The crop bottom.</param>
        /// <param name="cropLeft">The crop left.</param>
        /// <param name="synchronous">if set to <c>true</c> [synchronous].</param>
        /// <returns></returns>
        /// <exception cref="Exception">Business not found</exception>
        private async Task<ProfileSettings> AddLogoToProfile(int shopperKey, int profileId, string logoName, byte[] logoData, float cropTop, float cropRight, float cropBottom, float cropLeft, bool synchronous, string shopName)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = ctx.Profiles.Find(profileId);

                if (profile == null)
                {
                    throw new Exception("Unable to find profile");
                }

                if (!profile.Business.Account.CanView(Request))
                {
                    throw new Exception("Unauthorized");
                }

                Regex rgx = new Regex("[^a-zA-Z0-9.-]");
                logoName = rgx.Replace(logoName, "");
                if (logoName.Length > 50)
                {
                    logoName = logoName.Substring(0, 50);
                }

                var newImage = new Image
                {
                    Title = logoName,
                    BusinessId = profile.BusinessId,
                    ImageTypeId = 1, // Logo
                    Uploaded = DateTime.Now
                };

                try
                {
                    logoData = new ImagingAdapter().CropImage(logoData, cropTop, cropRight, cropBottom, cropLeft);
                }
                catch (Exception ex)
                {
                    var asdf = ex.Message;
                }

                try
                {
                    // Upload file to S3
                    var s3Filename = Guid.NewGuid() + "_" + logoName;
                    newImage.Url = S3Utils.AddDesignProfileImage(s3Filename, shopperKey.ToString(), logoData, shopName);

                    ctx.Images.Add(newImage);
                    ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    // unable to upload file
                }

                if (synchronous)
                {
                    await ImageTransfer.AddImageToPortfolio(newImage.Id);
                }
                else
                {
                    BackgroundJob.Enqueue(() => ImageTransfer.AddImageToPortfolio(newImage.Id));
                }

                var logo = new imageData
                {
                    id = newImage.Id,
                    url = newImage.Url
                };

                var settings = JsonConvert.DeserializeObject<ProfileSettings>(profile.Settings);
                settings.images.pictures = ctx.Businesses.Find(profile.BusinessId).Images.GetPictures();
                settings.images.logo = logo;

                var imagingAdapter = new ImagingAdapter();
                var newColors = imagingAdapter.GetImageColors(logoData).Take(5).Select(c => "#" + c.Key.R.ToString("X2") + c.Key.G.ToString("X2") + c.Key.B.ToString("X2"));

                var colorsToUse = imagingAdapter.DeprioritizeGreyColors(newColors);

                settings.colors.primary = colorsToUse.FirstOrDefault() ?? "";
                settings.colors.secondary = colorsToUse.ElementAtOrDefault(1) ?? "";
                settings.colors.tertiary = colorsToUse.ElementAtOrDefault(2) ?? "";

                profile.Settings = JsonConvert.SerializeObject(settings);
                ctx.SaveChanges();

                return settings;
            }
        }
    }
}
