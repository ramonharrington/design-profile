﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DesignProfile.Adapters;
using DesignProfile.Builders;
using DesignProfile.Controllers.Extensions;
using DesignProfile.data;
using DesignProfile.Models;
using DesignProfile.Validation;
using Newtonsoft.Json;
using Hatchery.Imaging;
using Profile = DesignProfile.data.Profile;

namespace DesignProfile.Controllers
{
    public class ProfileApiController : ApiController
    {
        [Route("api/Profile/{id}")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> Get(int id)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FindAsync(id);
                if (profile == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                if (!profile.Business.Account.CanView(Request))
                {
                    return new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(profile.ToModel()), Encoding.UTF8, "application/json")
                };
            }
        }

        /// <summary>
        /// Gets the profile by shopper key.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="initialize">if there is no profile found, whether to initialize one.</param>
        /// <returns></returns>
        [Route("api/Shopper/{id}/Profile")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> GetProfileByShopperKey(int id, bool initialize = false)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FirstOrDefaultAsync(p => p.Business.Account.ShopperKey == id);
                if (profile == null)
                {
                    if (initialize)
                    {
                        return await InitializeProfile(id);
                    }
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                if (!profile.Business.Account.CanView(Request))
                {
                    return new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(profile.ToModel()), Encoding.UTF8, "application/json")
                };
            }
        }

        /// <summary>
        /// Gets the profile by shopper key.  Called by Vistaprint Gallery
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="initialize">if there is no profile found, whether to initialize one.</param>
        /// <returns></returns>
        [Route("api/Shopper/{id}/GalleryProfile")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetGalleryProfile(int id, bool initialize = false)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FirstOrDefaultAsync(p => p.Business.Account.ShopperKey == id);
                if (profile == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(new GalleryProfile(profile)), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("api/Shopper/{id}/Initialize")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> InitializeProfile(int id)
        {
            var profileAdapter = new ProfileBuilder(new VpProfileAdapter());

            if (id == 0)
            {
                id = Request.GetShopperKey() ?? 0;
            }

            Models.Profile profile = null;
            if (id == 0)
            {
                profile = await Task.FromResult(profileAdapter.IntializeNullShopper());
            }
            else
            {
                profile = await Task.FromResult(profileAdapter.Initialize(id));
            }

            if (profile == null)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(JsonConvert.SerializeObject(profile), Encoding.UTF8,
                    "application/json")
            };
        }

        /// <summary>
        /// Updates the profile with the provided settings
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="settings">The settings.</param>
        /// <returns></returns>
        [Route("api/Profile/{id}/Settings")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> Post(int id, [FromBody] ProfileSettings settings)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FirstOrDefaultAsync(p => p.Id == id);
                if (profile == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                if (!profile.Business.Account.CanView(Request))
                {
                    return new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }

                if (settings == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(profile.ToModel()), Encoding.UTF8, "application/json")
                    };
                }
                profile.Settings = JsonConvert.SerializeObject(settings);

                try
                {
                    ctx.SaveChanges();
                    return new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(profile.ToModel()), Encoding.UTF8, "application/json")
                    };
                }
                catch (Exception e)
                {
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(e), Encoding.UTF8, "application/json")
                    };
                }
            }
        }

        /// <summary>
        /// Logic to update a profile to a given shopper
        /// 
        /// This will either, merge or "give" the profile to the given shopper
        /// </summary>
        [Route("api/Profile/{profileIdToUpdate}/Apply/{shopperKey}")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> UpdateShopperToProfile(int shopperKey, int profileIdToUpdate)
        {
            if (shopperKey <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                Profile newProfile = null;
                var profileToUpdate = await ctx.Profiles.FirstOrDefaultAsync(p => p.Id == profileIdToUpdate);
                if (profileToUpdate == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }

                var shopperProfile = await ctx.Profiles.FirstOrDefaultAsync(p => p.Business.Account.ShopperKey == shopperKey);
                if (shopperProfile == null) //easiest case, we just update the given profile to the new shopper 
                {
                    profileToUpdate.Business.Account.ShopperKey = shopperKey;
                    newProfile = profileToUpdate;
                }
                else if (shopperProfile.Id == profileToUpdate.Id)
                {
                    newProfile = profileToUpdate;
                }
                else
                {
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
                try
                {
                    ctx.SaveChanges();
                    return new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(newProfile.ToModel()), Encoding.UTF8, "application/json")
                    };
                }
                catch (Exception e)
                {
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(e), Encoding.UTF8, "application/json")
                    };
                }
            }
        }

        [Route("api/Profile/{id}/GenerateColors")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> GenerateColors(int id, bool force = false)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FirstOrDefaultAsync(p => p.Id == id);
                if (profile == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                if (!profile.Business.Account.CanView(Request))
                {
                    return new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }

                var settings = JsonConvert.DeserializeObject<ProfileSettings>(profile.Settings);

                if (settings.images?.logo == null)
                {
                    // No logo to extract colors from
                    return new HttpResponseMessage(HttpStatusCode.NoContent) { };
                }

                if (settings.colors != null && settings.colors.primary != null && force == false)
                {
                    // If colors exist and we're not forcing the override
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }

                byte[] imageData;
                // Get the image and parse through the colors
                using (var client = new WebClient())
                {
                    imageData = await client.DownloadDataTaskAsync(new Uri(settings.images.logo.url));
                }

                var colors = ColorUtils.GetImageColors(imageData).ToList();

                settings.colors = new color();
                if (colors != null && colors.Any())
                {
                    settings.colors.primary = "#" + colors.First().Key.Name.Substring(2);

                    if (colors.Count() > 1)
                    {
                        settings.colors.secondary = "#" + colors.ElementAt(1).Key.Name.Substring(2);
                    }

                    if (colors.Count() > 2)
                    {
                        settings.colors.tertiary = "#" + colors.ElementAt(2).Key.Name.Substring(2);
                    }

                    profile.Settings = JsonConvert.SerializeObject(settings);

                    try
                    {
                        ctx.SaveChanges();
                        return new HttpResponseMessage(HttpStatusCode.OK)
                        {
                            Content = new StringContent(JsonConvert.SerializeObject(profile.ToModel()), Encoding.UTF8, "application/json")
                        };
                    }
                    catch (Exception e)
                    {
                        return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                        {
                            Content = new StringContent(JsonConvert.SerializeObject(e), Encoding.UTF8, "application/json")
                        };
                    }
                }

                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
    }
}
