﻿using DesignProfile.Notifications;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesignProfile.Controllers
{
    public class EditDocumentController : Controller
    {
        public ActionResult Index(int id, string studioUrl)
        {
            try
            {
                using (data.DesignProfileEntities db = new data.DesignProfileEntities(data.ConnectionString.Current))
                {
                    var entry = db.ContinueFromAnotherDevices.FirstOrDefault(r => r.id == id);
                    entry.email_clicked_at = DateTime.Now;
                    entry.user_agent_clickthrough = HttpContext.Request.UserAgent;
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                // Oh well
            }

            BackgroundJob.Enqueue(() => NotifySlack(id));

            // Default behavior
            return Redirect(studioUrl);
        }

        public static void NotifySlack(int id)
        {
            using (data.DesignProfileEntities db = new data.DesignProfileEntities(data.ConnectionString.Current))
            {
                var entry = db.ContinueFromAnotherDevices.FirstOrDefault(r => r.id == id);

                var slackPublisher = new SlackPublisher("#continueanotherdevice", "#continueanotherdevice");
                slackPublisher.PublishMessageAsync("Return from email: " + entry.email + " User Agent: " + entry.user_agent_clickthrough, "bot", entry.preview_url);
            }
        }
    }
}