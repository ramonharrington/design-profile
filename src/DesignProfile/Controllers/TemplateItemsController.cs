﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using DesignProfile.Adapters;
using DesignProfile.data;
using DesignProfile.Properties;
using DevDefined.OAuth.Utility;
using Newtonsoft.Json;

namespace DesignProfile.Controllers
{
    public class TemplateItemsController : ApiController
    {
        [Route("api/Template")]
        [HttpGet]
        public HttpResponseMessage GetTemplates()
        {
            var environment = Settings.Default.Environment;

            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var templates = ctx.Templates.Where(t => t.UseForMatching == true && t.Environment == environment).Select(t => t.TemplateId).ToList();
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(
                        JsonConvert.SerializeObject(ctx.TemplateItems.Where(t => templates.Contains(t.template_id))
                            .Select(d => d.template_id).Distinct()), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("api/GetItemMetadata/{designTemplate}")]
        [HttpGet]
        public HttpResponseMessage Get(string designTemplate)
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(GetItemMetadata(designTemplate)), Encoding.UTF8, "application/json")
            };
        }

        internal Dictionary<string, ItemConfiguration> GetItemMetadata(string designTemplate)
        {
            var result = new Dictionary<string, ItemConfiguration>();
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                IQueryable<TemplateItem> itemData = ctx.TemplateItems.Where(p => p.template_id == designTemplate);
                foreach (TemplateItem item in itemData)
                {
                    if (!result.ContainsKey(item.item_id))
                    {
                        if (item.configuration == null)
                        {
                            result.Add(item.item_id,
                                item.item_id.Contains("_img_") || item.item_id.Contains("item_")
                                    ? new ImageItemConfiguration()
                                    : new ItemConfiguration());
                        }
                        else
                        {
                            result.Add(item.item_id,
                                item.item_id.Contains("_img_") || item.item_id.Contains("item_")
                                    ? JsonConvert.DeserializeObject<ImageItemConfiguration>(item.configuration)
                                    : JsonConvert.DeserializeObject<ItemConfiguration>(item.configuration));
                        }
                    }
                }
            }
            return result;
        }

        /*obsolete*/
        [Route("api/UpsertItemMetadata/{designTemplate}")]
        [HttpPost]
        public HttpResponseMessage Post(string designTemplate, Dictionary<string, object> templateItemsDataModel)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                IQueryable<TemplateItem> itemData = ctx.TemplateItems.Where(p => p.template_id == designTemplate);
                foreach (KeyValuePair<string, object> pair in templateItemsDataModel)
                {
                    string configurationValue = null;
                    if (pair.Value != null)
                    {
                        configurationValue = JsonConvert.SerializeObject(pair.Value);
                    } //default to null if empty or whitespace
                    //this is a design template
                    TemplateItem item = itemData.FirstOrDefault(x => x.item_id == pair.Key);
                    if (item == null)
                    {
                        item = new TemplateItem
                        {
                            configuration = configurationValue,
                            item_id = pair.Key,
                            template_id = designTemplate
                        };
                        ctx.TemplateItems.Add(item);
                    }
                    else
                    {
                        item.configuration = configurationValue;
                    }

                }
                try
                {
                    ctx.SaveChanges();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                catch (Exception e)
                {
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(e), Encoding.UTF8, "application/json")
                    };
                }
            }
        }

        [Route("api/UpsertItemMetadataV2/{documentTemplate}")]
        [HttpPost]
        public HttpResponseMessage PostV2(string documentTemplate, [FromBody] IEnumerable<ItemMetadataAndXmlResponse> templateItemsDataModel)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                IQueryable<TemplateItem> itemData = ctx.TemplateItems.Where(p => p.template_id == documentTemplate);
                foreach (ItemMetadataAndXmlResponse itemMetadataAndXmlResponse in templateItemsDataModel)
                {
                    string configurationValue = null;
                    if (itemMetadataAndXmlResponse.Metadata != null)
                    {
                        configurationValue = JsonConvert.SerializeObject(itemMetadataAndXmlResponse.Metadata);
                    } //default to null if empty or whitespace
                    //this is a design template
                    TemplateItem item = itemData.FirstOrDefault(x => x.item_id == itemMetadataAndXmlResponse.ItemId);
                    if (item == null)
                    {
                        item = new TemplateItem
                        {
                            configuration = configurationValue,
                            item_id = itemMetadataAndXmlResponse.ItemId,
                            template_id = documentTemplate
                        };

                        if (item.item_id != null)
                        {
                            ctx.TemplateItems.Add(item);
                        }
                    }
                    else
                    {
                        item.configuration = configurationValue;
                    }

                }
                try
                {
                    ctx.SaveChanges();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                catch (Exception e)
                {
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(e), Encoding.UTF8, "application/json")
                    };
                }
            }
        }

        [Route("api/GetItemMetadataAndXml/{documentTemplate}")]
        [HttpGet]
        public HttpResponseMessage GetMetadataAndXml(string documentTemplate)
        {
            var getItemXmlAndTypeUrl = Settings.Default.VpApiServer +
                                       "/hatchery/rpc/DesignProfileDocumentItemApi/GetItemXmlAndType/" +
                                       documentTemplate;
            var getItemPreviewUrl = Settings.Default.VpBaseUrl +
                                    "/studio4/ComponentPreview/Render?contentserver=1&devcompservice=1";
            var templateItems = new List<ItemMetadataAndXmlResponse>();

            //get item xml
            var request = (HttpWebRequest) WebRequest.Create(getItemXmlAndTypeUrl);
            request.ContentType = "application/json";
            request.Method = "GET";
            try
            {
                using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    templateItems =
                        JsonConvert.DeserializeObject<List<ItemMetadataAndXmlResponse>>(reader.ReadToEnd());
                }
            }
            catch (WebException e)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error getting vp doc items", e);
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                resp.Content =
                    new StringContent(getItemXmlAndTypeUrl + " : " + e.Response.GetResponseStream().ReadToEnd());
                return resp;
            }
            //get previews 
            foreach (ItemMetadataAndXmlResponse docItemXml in templateItems)
            {
                try
                {
                    using (var wc = new WebClient())
                    {
                        byte[] response = wc.UploadData(getItemPreviewUrl, Encoding.ASCII.GetBytes(docItemXml.Xml));
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(docItemXml.Xml);
                        var elemId = xmlDoc.DocumentElement.Attributes["id"].Value;
                        docItemXml.ItemId = elemId;

                        if (docItemXml.Type == "TextField")
                        {
                            docItemXml.ItemPreviewUrl = "data:image/png;base64," +
                                                        (new ImagingAdapter()).GetBase64TextImage(xmlDoc.InnerText);
                        }
                        else
                        {
                            docItemXml.ItemPreviewUrl = "data:image/png;base64," + Convert.ToBase64String(response);
                        }
                    }
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error getting metadata for document template " + documentTemplate, ex);
                }
            }
            //get/init metadata
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                IQueryable<TemplateItem> itemData = ctx.TemplateItems.Where(p => p.template_id == documentTemplate);
                foreach (ItemMetadataAndXmlResponse templateItem in templateItems)
                {
                    var record = itemData.Where(x=>x.item_id == templateItem.ItemId).FirstOrDefault();
                    if (record != null)
                    {
                        templateItem.Metadata = templateItem.Type.Equals("Image", StringComparison.CurrentCultureIgnoreCase) 
                            ? JsonConvert.DeserializeObject<ImageItemConfiguration>(record.configuration)
                            : JsonConvert.DeserializeObject<ItemConfiguration>(record.configuration);
                    }
                    else
                    {
                        templateItem.Metadata = templateItem.Type.Equals("Image", StringComparison.CurrentCultureIgnoreCase) ?
                            new ImageItemConfiguration() : new ItemConfiguration();
                    }
                }
            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(templateItems), Encoding.UTF8, "application/json")
            };
        }

        public class ItemConfiguration
        {
            public string Recolor { get; set; }
            public string Color { get; set; }
            public string Font { get; set; }
        }

        public class ImageItemConfiguration : ItemConfiguration
        {
            public string ImageUsage { get; set; }
        }

        public class ItemMetadataAndXmlResponse
        {
            public string Type { get; set; }
            public string Xml { get; set; }
            public string ItemId { get; set; }
            public string ItemPreviewUrl { get; set; }
            public object Metadata { get; set; }
        }
    }
}
