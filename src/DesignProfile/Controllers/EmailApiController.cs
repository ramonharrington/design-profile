﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DesignProfile.Validation;
using Newtonsoft.Json;
using DesignProfile.data;
using Hangfire;
using DesignProfile.Notifications;

namespace DesignProfile.Controllers
{
    public class EmailApiController : ApiController
    {
        [Route("api/Email/DeviceChange")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> DeviceChange([FromBody] DeviceChangeRequest requestModel)
        {
            try
            {
                if (requestModel == null || string.IsNullOrEmpty(requestModel.Email))
                {
                    NLog.LogManager.GetCurrentClassLogger()
                        .Error("Empty email address ");
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }

                var request = (HttpWebRequest) WebRequest.Create("https://api.sparkpost.com/api/v1/transmissions");
                request.ContentType = "application/json";
                request.Headers["authorization"] = "2628a2837c2a9ba8fce2de3674be1d710fe9df7a";
                request.Method = "POST";

                var model = new SparkPostModel
                {
                    campaign_id = "brandshop_device_change",
                    content = new SparkPostContent {template_id = "brandshop-device-change"},
                    recipients = new List<SparkPostRecipient>
                    {
                        new SparkPostRecipient
                        {
                            address = requestModel.Email
                        }
                    }
                };

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));
                request.ContentLength = body.Length;

                // Make the request to the monolith
                var bodyStream = request.GetRequestStream();
                bodyStream.Write(body, 0, body.Length);
                bodyStream.Close();

                using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                }
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                };
            }
            catch (Exception e)
            {
                NLog.LogManager.GetCurrentClassLogger()
                    .Error("Error posting device change email " + e);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [Route("api/Email/ContinueOnAnotherDevice")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> ContinueOnAnotherDevice(
            [FromBody] ContinueOnAnotherDeviceRequest requestModel)
        {
            try
            {
                if (requestModel == null || string.IsNullOrEmpty(requestModel.Email) 
                    || string.IsNullOrEmpty(requestModel.EditUrl) || string.IsNullOrEmpty(requestModel.PreviewUrl))
                {
                    NLog.LogManager.GetCurrentClassLogger()
                        .Error("Invalid model");
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }

                var request = (HttpWebRequest)WebRequest.Create("https://api.sparkpost.com/api/v1/transmissions");
                request.ContentType = "application/json";
                request.Headers["authorization"] = "2628a2837c2a9ba8fce2de3674be1d710fe9df7a";
                request.Method = "POST";

                int id = SaveToDbAndSlack(requestModel.Email, 0, requestModel.EditUrl, requestModel.PreviewUrl, HttpContext.Current.Request.UserAgent);
                var editUrl = $"http://{Properties.Settings.Default.DesignProfileHost}/EditDocument/{id}?studioUrl={HttpContext.Current.Server.UrlEncode(requestModel.EditUrl)}";

                var model = new SparkPostModel
                {
                    campaign_id = "continue_from_another_device",
                    content = new SparkPostContent { template_id = "continue-from-another-device" },
                    recipients = new List<SparkPostRecipient>
                    {
                        new SparkPostRecipient
                        {
                            address = requestModel.Email
                        }
                    },
                    substitution_data = new SparkPostSubstitutionData
                    {
                        edit_url = editUrl,
                        preview_url = requestModel.PreviewUrl,
                        studioEmailBookmarkEmailHeading = requestModel.StudioEmailBookmarkEmailHeading,
                        studioEmailBookmarkEmailBody = requestModel.StudioEmailBookmarkEmailBody,
                        studioEmailBookmarkEmailSubject = requestModel.StudioEmailBookmarkEmailSubject,
                        studioEmailBookmarkEmailCta = requestModel.StudioEmailBookmarkEmailCta
                    }
                };
                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));
                request.ContentLength = body.Length;
                // Make the request to the monolith
                var bodyStream = request.GetRequestStream();
                bodyStream.Write(body, 0, body.Length);
                bodyStream.Close();

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                };
                
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                BackgroundJob.Enqueue(() => PublishErrorToSlack(e));

                NLog.LogManager.GetCurrentClassLogger()
                    .Error("Error posting continue on antoher device email " + e);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        public static void PublishErrorToSlack(Exception e)
        {
            var slackPublisher = new SlackPublisher("#continueanotherdevice", "#continueanotherdevice");
            slackPublisher.PublishMessageAsync("Error posting continue on antoher device email " + e);
        }

        /// <summary>
        /// Used by Hangfire - do not change
        /// 
        /// Doesnt save to slack anymore
        /// </summary>
        public void SaveToDbAndSlack(string email, long session, string studioUrl, string previewUrl)
        {
            SaveToDbAndSlack(email, session, studioUrl, previewUrl, string.Empty);
        }

        public int SaveToDbAndSlack(string email, long session, string studioUrl, string previewUrl, string userAgent)
        {
            try
            {
                var uri = new Uri(studioUrl);
                var query = uri.Query.Replace("?", "");
                var queryValues = query.Split('&').Select(q => q.Split('=')).ToDictionary(k => k[0], v => v[1]);
                //all this is just optional, sending the email is the most important thing
                using (var ctx = new DesignProfileEntities(ConnectionString.Current))
                {
                    var entry = new ContinueFromAnotherDevice
                    {
                        email = email,
                        alt_doc_id = queryValues["alt_doc_id"],
                        session_id = session,
                        edit_url = studioUrl,
                        preview_url = previewUrl,
                        user_agent_email_send = userAgent,
                        email_sent_at = DateTime.Now
                    };
                    ctx.ContinueFromAnotherDevices.Add(entry);
                    ctx.SaveChanges();

                    return entry.id;
                }
            }
            catch (Exception e)
            {
                NLog.LogManager.GetCurrentClassLogger()
                .Error("Error saving reporting data or sending to slack" + e);
                return 0;
            }
        }

        public static void PublishToSlack(int id)
        {
            using (var db = new DesignProfileEntities(ConnectionString.Current))
            {
                var entry = db.ContinueFromAnotherDevices.FirstOrDefault(e => e.id == id);
                var slackPublisher = new SlackPublisher("#continueanotherdevice", "#continueanotherdevice");
                slackPublisher.PublishMessageAsync("Email sent to: " + entry.email, "bot", entry.preview_url);
            }
        }

        public class ContinueOnAnotherDeviceRequest
        {
            public ContinueOnAnotherDeviceRequest()
            {
                StudioEmailBookmarkEmailHeading = "CONTINUE DESIGNING";
                StudioEmailBookmarkEmailBody = "Pick up where you left off";
                StudioEmailBookmarkEmailSubject = "Your saved design";
                StudioEmailBookmarkEmailCta = "Continue";
            }

            public string Email { get; set; }
            public string EditUrl { get; set; }
            public string PreviewUrl { get; set; }
            public string StudioEmailBookmarkEmailHeading { get; set; }
            public string StudioEmailBookmarkEmailBody { get; set; }
            public string StudioEmailBookmarkEmailSubject { get; set; }
            public string StudioEmailBookmarkEmailCta { get; set; }
        }

        public class DeviceChangeRequest
        {
            public string Email { get; set; }
        }

        public class SparkPostModel
        {
            public string campaign_id { get; set; }
            public List<SparkPostRecipient> recipients { get; set; }
            public SparkPostContent content { get; set; }
            public SparkPostSubstitutionData substitution_data { get; set; }
        }

        public class SparkPostRecipient
        {
            public string address { get; set; }
        }

        public class SparkPostContent
        {
            public string template_id { get; set; }
        }

        public class SparkPostSubstitutionData
        {
            public string preview_url { get; set; }
            public string edit_url { get; set; }
            public string studioEmailBookmarkEmailHeading { get; set; }
            public string studioEmailBookmarkEmailBody { get; set; }
            public string studioEmailBookmarkEmailSubject { get; set; }
            public string studioEmailBookmarkEmailCta { get; set; }
        }
    }
}