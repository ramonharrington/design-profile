using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DesignProfile.Controllers
{
    public class AdminApiController : ApiController
    {
        /// <summary>
        /// Test endpoint
        /// </summary>
        /// <returns></returns>
        [Route("api/admin/test")]
        [HttpGet]
        public async Task<HttpResponseMessage> Index()
        {
            var msg = new HttpResponseMessage(HttpStatusCode.Accepted);
            await Task.Run(() => { msg.Content = new StringContent("admin api test"); });
            return msg;
        }
    }
}
