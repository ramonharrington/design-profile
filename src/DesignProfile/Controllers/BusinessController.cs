﻿using System;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using DesignProfile.Controllers.Extensions;
using DesignProfile.data;
using DesignProfile.Models;
using DesignProfile.Validation;
using Newtonsoft.Json;

namespace DesignProfile.Controllers
{
    public class BusinessController : ApiController
    {
        [Route("api/Business/{id}")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> Get(int id)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var business = await ctx.Businesses.FirstOrDefaultAsync(p => p.Id == id);
                if (business == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                if (!business.Account.CanView(Request))
                {
                    return new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(business.ToModel()), Encoding.UTF8,
                        "application/json")
                };
            }
        }

        [Route("api/Business")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> Post(Models.Business business)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var entity = await ctx.Businesses.FirstOrDefaultAsync(b => b.Id == business.Id);
                if (entity != null)
                {
                    if (!entity.Account.CanView(Request))
                    {
                        return new HttpResponseMessage(HttpStatusCode.Unauthorized);
                    }

                    business.ToEntity().CopyTo(entity, false);
                }
                else
                {
                    ctx.Businesses.Add(business.ToEntity());
                }

                int businessId = 0;
                try
                {
                    businessId = await ctx.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    var asdf = ex;
                }

                return new HttpResponseMessage(HttpStatusCode.Accepted)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(new {businessId}),
                        Encoding.UTF8,
                        "application/json")
                };
            }
        }
    }
}
