using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DesignProfile.data;
using Newtonsoft.Json;

namespace DesignProfile.Controllers
{
    public class EventTrackingApiController : ApiController
    {
        [HttpPost]
        [Route("api/tracker")]
        public bool EventTrack([FromBody] event_tracking dto)
        {
            var content = new StringContent(JsonConvert.SerializeObject(dto));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            if (dto == null || dto.page_url == null || dto.event_value == null || dto.event_type == null)
            {
                return false;
            }

            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                try
                {
                    dto.timestamp = DateTime.Now;
                    if (dto.page_url.Length > 150)
                    {
                        dto.page_url = dto.page_url.Substring(0, 150);
                    }

                    if (dto.event_type.Length > 150)
                    {
                        dto.event_type = dto.event_type.Substring(0, 150);
                    }

                    if (dto.event_value.Length > 150)
                    {
                        dto.event_value = dto.event_value.Substring(0, 150);
                    }

                    dto.user_agent = dto.user_agent;

                    ctx.event_tracking.Add(dto);
                    ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Unable to log event", ex);
                }
            }

            return false;
        }

        [HttpPost]
        [Route("api/previeweventtracker")]
        public async Task<HttpResponseMessage> TrackPreviewEvent([FromBody] PreviewTrackingRequest request)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                try
                {
                    ctx.PreviewTrackings.Add(new PreviewTracking
                    {
                        ProfileId = request.ProfileId,
                        PfId = request.PfId,
                        AltDocId = request.AltDocId,
                        ComboId = request.ComboId,
                        DesignTemplateId = request.Template,
                        SceneId = string.IsNullOrEmpty(request.Scene) ? null : ctx.Scenes.FirstOrDefault(s => s.Filename == request.Scene)?.Id,
                        ProfileSettings = request.Settings,
                        PreviewEventTypeId = (int) Enum.Parse(typeof(PreviewEventType), request.Action),
                        PreviewEventDate = DateTime.Now
                    });

                    await ctx.SaveChangesAsync();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error tracking preview event", ex);
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                }
            }
        }

        [HttpPost]
        [Route("api/shoptracker")]
        public async Task<HttpResponseMessage> TrackShopEvent([FromBody] ShopTrackignRequest request)
        {
            PreviewEventType eventType;
            if (!Enum.TryParse(request.Action, true, out eventType))
            {
                NLog.LogManager.GetCurrentClassLogger().Error($"Event type {request.Action} doesn't exists");
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                try
                {
                    ctx.ShopTrackings.Add(new ShopTracking()
                    {
                        ProfileId = request.ProfileId,
                        PfId = request.PfId,
                        AltDocId = request.AltDocId,
                        ComboId = request.ComboId,
                        DesignTemplateId = request.Template,
                        SceneId = string.IsNullOrEmpty(request.Scene) ? null : ctx.Scenes.FirstOrDefault(s => s.Filename == request.Scene)?.Id,
                        ProfileSettings = request.Settings,
                        PreviewEventType = eventType.ToString(),
                        PreviewEventDate = DateTime.Now,
                        Value = request.Value,
                        ShopperKey = request.ShopperKey,
                        Locale = request.Locale,
                        UserAgent = request.UserAgent,
                        Referer = request.Referrer,
                        Channel = request.Channel,
                        PageUrl =  request.PageUrl,
                        IpAddress = request.IpAddress
                    });

                    await ctx.SaveChangesAsync();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error tracking preview event", ex);
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                }
            }
        }
    }

    public class ShopTrackignRequest
    {
        //generic data
        public string Action { get; set; }
        public string Value { get; set; }
        public int ShopperKey { get; set; }
        public string Locale { get; set; }
        public string UserAgent { get; set; }
        public string Referrer { get; set; }
        public string Channel { get; set; }
        public string PageUrl { get; set; }
        public string IpAddress { get; set; }
        //profile data
        public int ProfileId { get; set; }
        public string Settings { get; set; }
        //preview item data
        public string PfId { get; set; }
        public string AltDocId { get; set; }
        public int? ComboId { get; set; }
        public string Template { get; set; }
        public string Scene { get; set; }
    }

    public class PreviewTrackingRequest
    {
        public int ProfileId { get; set; }
        public string PfId { get; set; }
        public string AltDocId { get; set; }
        public int? ComboId { get; set; }
        public string Template { get; set; }
        public string Scene { get; set; }
        public string Settings { get; set; }
        public string Action { get; set; }
    }

    internal enum PreviewEventType
    {
        GoToStudio,
        AddToPortfolio,
        ViewPortfolio,
        AddToCart,
        ViewProductDetail,
        PageView,
        ActionEvent,
        ModalOpen,
        ItemLoaded,
        SceneLoaded,
        ModalClosed,
        EmailSent,
        PreviewUpdated
    }
}
