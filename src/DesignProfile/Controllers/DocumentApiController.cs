﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using DesignProfile.Models;
using DesignProfile.Properties;
using DevDefined.OAuth.Utility;
using Newtonsoft.Json;

namespace DesignProfile.Controllers
{
    public class DocumentApiController : ApiController
    {
        [Route("api/Document/{shopperKey}/ProfileData")]
        [HttpGet]
        public HttpResponseMessage GetForShopper(int shopperKey)
        {
            var urlBase = Settings.Default.VpApiServer + "/hatchery/rpc/DesignProfileApi/GetShopperProfileData/" + shopperKey;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(urlBase);
                request.Headers.Add("X-Hatch-Oath", "All your base info is belong to us");
                request.ContentType = "application/json";
                request.Method = "GET";

                ShopperProfileData shopperData = new ShopperProfileData();
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    shopperData = JsonConvert.DeserializeObject<ShopperProfileData>(reader.ReadToEnd());
                }
                
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ObjectContent<AssetTransferModel>(Transform(shopperData), new JsonMediaTypeFormatter(),
                "application/json")
                };
            }
            catch (WebException e)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error getting vp monolith api response", e);
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                resp.Content = new StringContent(urlBase + " : " + e.Response.GetResponseStream().ReadToEnd());
                return resp;
            }
        }

        private AssetTransferModel Transform(ShopperProfileData profileData)
        {
            var assetModel =  new AssetTransferModel();
            assetModel.PreviewHeight = 400;
            assetModel.PreviewWidth = 600;
            assetModel.TextFieldPurposeNameToValue =
                profileData.Documents.FirstOrDefault()
                    .DocumentToTextFieldData
                    .GroupBy(x=>x.PurposeTextId)
                    .ToDictionary(x => x.Key, x => x.First().Value);
            assetModel.Images = profileData.Documents.FirstOrDefault().DocumentToUploadIds.Select(x => x.ImageId).ToList();
            assetModel.Fonts = DetermineFonts(profileData.Documents.FirstOrDefault().DocumentToTextFieldData);
            return assetModel;
        }

        private ProfileFonts DetermineFonts(List<TextFieldData> collectionOfTextData)
        {
            var fonts = new ProfileFonts();
            var grouping =
                collectionOfTextData.GroupBy(x => x.FontFamily)
                    .Select(x => new {Family = x.Key, Count = x.Count()})
                    .OrderByDescending(x=>x.Count);
            fonts.Main = grouping.ElementAt(0).Family;
            fonts.Secondary = grouping.ElementAt(1).Family;
            return fonts;
        }
    }
}
