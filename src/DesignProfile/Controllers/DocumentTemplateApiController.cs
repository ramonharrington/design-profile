﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using DesignProfile.data;
using DesignProfile.Properties;

namespace DesignProfile.Controllers
{
    public class DocumentTemplateApiController : ApiController
    {
        [Route("api/Pfid/{pfid}/DocumentTemplate")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetPfidDocumentTemplate(string pfid)
        {
            var environment = Settings.Default.Environment;
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var documentTemplate = await ctx.Templates.FirstOrDefaultAsync(t => t.ProductId == pfid && t.Environment == environment && t.UseForMatching == true);
                if (documentTemplate == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ObjectContent<DocumentTemplate>(
                        new DocumentTemplate
                        {
                            TemplateId = documentTemplate.TemplateId,
                            Tags = documentTemplate.Tags?.Split(',')
                        }, new JsonMediaTypeFormatter(), "application/json")
                };
            }
        }

        private class DocumentTemplate
        {
            public string TemplateId { get; set; }
            public IEnumerable<string> Tags { get; set; }
        }
    }
}