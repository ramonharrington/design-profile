﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DesignProfile.Adapters;
using DesignProfile.Builders;
using DesignProfile.Controllers.Extensions;
using DesignProfile.data;
using DesignProfile.Models;
using DesignProfile.Properties;
using DesignProfile.Tasks;
using DesignProfile.Utils;
using DesignProfile.Validation;
using Hangfire;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Image = DesignProfile.data.Image;

namespace DesignProfile.Controllers
{
    public class FacebookApiController : ApiController
    {
        private static string CLIENT_ID = "225002344297686";
        private static string CLIENT_SECRET = "3b0048e014fbab5ce4cc310221b3a935";

        [Route("api/facebook/search")]
        [HttpGet]
        public async Task<HttpResponseMessage> Search(string query, string type, string lat = "", string lon = "", string after = "", string before = "", string limit = "5")
        {
            var accessToken = GeApplicationAccessToken();

            if (!string.IsNullOrEmpty(accessToken))
            {
                var graphSearchUrl = $"https://graph.facebook.com/v2.11/search?type={type}&access_token={accessToken}&q={HttpUtility.UrlEncode(query)}&center={lat},{lon}&fields=single_line_address,name,picture,category,fan_count&limit=20&before={before}&after={after}&distance=50000&limit={limit}";
                if (string.IsNullOrEmpty(lon) && string.IsNullOrEmpty(lat))
                {
                    graphSearchUrl = $"https://graph.facebook.com/v2.11/search?type={type}&access_token={accessToken}&q={HttpUtility.UrlEncode(query)}&fields=single_line_address,name,picture,category,fan_count&limit=20&before={before}&after={after}&limit={limit}";
                }
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(graphSearchUrl);
                    request.Method = "GET";

                    JObject graphSearchResponse;
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        graphSearchResponse = JsonConvert.DeserializeObject<JObject>(reader.ReadToEnd());
                    }
                    if (graphSearchResponse["paging"] != null)
                    {
                        graphSearchResponse["paging"]["next"]?.Parent.Remove();
                        graphSearchResponse["paging"]["previous"]?.Parent.Remove();
                    }
                    if (Settings.Default.Environment == "local")
                    {
                        graphSearchResponse["url"] = request.Address.ToString();
                    }
                    var msg = new HttpResponseMessage(HttpStatusCode.Accepted);
                    msg.Content = new StringContent(graphSearchResponse.ToString(), Encoding.UTF8, "application/json");
                    return msg;
                }
                catch (Exception e)
                {
                    NLog.LogManager.GetCurrentClassLogger()
                        .Error("Error while querying Facebook's graph api");
                }
            }

            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [Route("api/facebook/profile/{profileId}/selectAsLogo/{imageId}")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> PromoteImageToLogo(int profileId, int imageId)
        {
            if (profileId == 0 || imageId == 0)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            try

            {
                using (var ctx = new DesignProfileEntities(ConnectionString.Current))
                {
                    var profile = await ctx.Profiles.FirstOrDefaultAsync(p => p.Id == profileId);
                    if (profile == null)
                    {
                        var profileAdapter = new ProfileBuilder(new VpProfileAdapter());

                        if (profileId == 0)
                        {
                            profileId = Request.GetShopperKey() ?? 0;
                        }

                        if (profileId == 0)
                        {
                            return new HttpResponseMessage(HttpStatusCode.BadRequest);
                        }

                        var profileModel = profileAdapter.Initialize(profileId);

                        if (profileModel == null)
                        {
                            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                        }

                        profile = await ctx.Profiles.FindAsync(profileId);

                        if (profile == null)
                        {
                            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                        }
                    }
                    var image = profile.Business.Images.FirstOrDefault(i => i.Id == imageId);
                    if (image == null)
                    {
                        return new HttpResponseMessage(HttpStatusCode.NotFound);
                    }
                    image.ImageTypeId = 1;
                    await ctx.SaveChangesAsync();
                    return new HttpResponseMessage(HttpStatusCode.Accepted);
                }
            }
            catch (Exception e)
            {
                NLog.LogManager.GetCurrentClassLogger()
                    .Error("Error while promoting image to logo");
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }


        [Route("api/facebook/place/{pageId}")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> GetFacebookPlaceAsJson(string pageId, bool enhance = false)
        {
            FacebookProfile facebookProfile = await GetFacebookProfileFromPlaceId(pageId);
            if (enhance)
            {
                EnhanceFacebookModel(facebookProfile);
            }
            return new HttpResponseMessage(HttpStatusCode.Accepted)
            {
                Content = new StringContent(JsonConvert.SerializeObject(facebookProfile), Encoding.UTF8, "application/json")
            };
        }

        [Route("api/facebook/profile/{profileId}/place/{pageId}")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> PopulateProfileFromPlace(int profileId, string pageId)
        {
            FacebookProfile facebookProfile = await GetFacebookProfileFromPlaceId(pageId);
            // First, make sure a design profile record exists for this profile
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FirstOrDefaultAsync(p => p.Id == profileId);
                if (profile == null)
                {
                    var profileAdapter = new ProfileBuilder(new VpProfileAdapter());

                    if (profileId == 0)
                    {
                        profileId = Request.GetShopperKey() ?? 0;
                    }

                    if (profileId == 0)
                    {
                        return new HttpResponseMessage(HttpStatusCode.BadRequest);
                    }

                    var profileModel = profileAdapter.Initialize(profileId);

                    if (profileModel == null)
                    {
                        return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                    }

                    profile = await ctx.Profiles.FindAsync(profileId);

                    if (profile == null)
                    {
                        return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                    }
                }

                try
                {
                    EnhanceFacebookModel(facebookProfile);
                    // Update profile with data
                    var business = profile.Business;
                    business.FbPlaceId = facebookProfile.Id;
                    business.Name = facebookProfile.Name?.Trim()?.PadRight(255)?.Substring(0, 255).Trim();
                    business.Tagline = facebookProfile.Description?.Trim()?.PadRight(255)?.Substring(0, 255).Trim();
                    business.Address1 = facebookProfile.Location?.Street?.Trim()?.PadRight(255)?.Substring(0, 255)
                        .Trim();
                    business.Category = facebookProfile.Category?.Trim()?.PadRight(255)?.Substring(0, 255).Trim();
                    business.City = facebookProfile.Location?.City?.Trim()?.PadRight(255)?.Substring(0, 255).Trim();
                    business.State = facebookProfile.Location?.State?.Trim()?.PadRight(255)?.Substring(0, 255).Trim();
                    business.Postal = facebookProfile.Location?.Zip?.Trim()?.PadRight(255)?.Substring(0, 255).Trim();
                    business.Phone1 = facebookProfile.Phone?.Trim()?.PadRight(255)?.Substring(0, 255).Trim();
                    business.Email = facebookProfile.Emails?.FirstOrDefault()?.Trim()?.PadRight(255)?.Substring(0, 255)
                        .Trim();
                    business.Url = facebookProfile.Website?.Trim()?.PadRight(255)?.Substring(0, 255).Trim() ??
                                   facebookProfile.Link?.Trim()?.PadRight(255)?.Substring(0, 255).Trim();
                    ctx.Images.RemoveRange(ctx.Images.Where(i => i.BusinessId == business.Id));

                    if (facebookProfile.Pictures != null && facebookProfile.Pictures.Any())
                    {
                        foreach (var picture in facebookProfile.Pictures)
                        {
                            WebClient client = new WebClient();
                            byte[] data = client.DownloadData(picture);

                            //upload to S3
                            string s3Filename = Guid.NewGuid().ToString();
                            var url = S3Utils.AddDesignProfileImage(s3Filename, business.Account.ShopperKey?.ToString() ?? "null", data,
                                "facebook");

                            var image = (Bitmap) System.Drawing.Image.FromStream(new MemoryStream(data));

                            var newImage = new Image()
                            {
                                Title = "Facebook Image",
                                BusinessId = business.Id,
                                Url = url,
                                Uploaded = DateTime.Now,
                                ImageTypeId = 2, // photo
                                Width = image.Width,
                                Height = image.Height
                            };
                            ctx.Images.Add(newImage);
                            ctx.SaveChanges();

                            //upload to cimpress
                            BackgroundJob.Enqueue(() => ImageTransfer.AddImageToPortfolio(newImage.Id));
                        };
                    }

                    // Update the profiles of the business with the new set of images
                    var pictures = business.Images.GetPictures().OrderByDescending(i => i.id).Take(1);

                    foreach (var bizProfile in business.Profiles)
                    {
                        var settings = JsonConvert.DeserializeObject<ProfileSettings>(bizProfile.Settings);
                        settings.images.pictures = pictures;
                        bizProfile.Settings = JsonConvert.SerializeObject(settings);
                    }

                    await ctx.SaveChangesAsync();
                    return new HttpResponseMessage(HttpStatusCode.Accepted);
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error importing facebook profile", ex);
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                }
            }
        }

        private async Task<FacebookProfile> GetFacebookProfileFromPlaceId(string placeId)
        {
            var accessToken = GeApplicationAccessToken();
            if (!string.IsNullOrEmpty(accessToken))
            {
                var images = new List<string>();

                var pageInformation = GetPageInformationAsync(accessToken, placeId);
                var profilePicture = GetLargeResolutionProfilePicturesFromPage(accessToken, placeId);
                await Task.WhenAll(pageInformation, profilePicture);

                var facebookObject = JsonConvert.DeserializeObject<JObject>(pageInformation.Result);
                var graphSearchResponse = facebookObject.ToObject<FacebookProfile>();

                if (!string.IsNullOrEmpty(profilePicture.Result))
                {
                    var pictureObjectArray = JsonConvert.DeserializeObject<JObject>(profilePicture.Result);
                    if (pictureObjectArray["data"] != null && pictureObjectArray["data"].HasValues)
                    {
                        var profileImages = pictureObjectArray["data"].Value<JArray>();
                        foreach (JToken profileImage in profileImages)
                        {
                            images.Add(profileImage["source"].Value<string>());
                        }
                    }
                }
                if (facebookObject["cover"] != null && facebookObject["cover"].HasValues)
                {
                    images.Add(facebookObject["cover"]["source"].Value<string>());
                }
                graphSearchResponse.Pictures = images;
                return graphSearchResponse;
            }
            return null;
        }

        private async Task<string> GetPageInformationAsync(string accessToken, string pageId)
        {
            var graphSearchUrl = $"https://graph.facebook.com/v2.11/{pageId}?access_token={accessToken}&fields=category,category_list,company_overview,contact_address,single_line_address,cover,current_location,description,description_html,emails,general_info,hours,is_always_open,link,location,name,phone,place_type,website,picture,about";
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(graphSearchUrl);
                request.Method = "GET";

                string jsonresponse;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    jsonresponse = await reader.ReadToEndAsync();
                }
                
                return jsonresponse;
            }
            catch (Exception e)
            {
                NLog.LogManager.GetCurrentClassLogger()
                    .Error(e, $"Error while querying Facebook's graph api {pageId}");
                return null;
            }
        }

        private async Task<string> GetLargeResolutionProfilePicturesFromPage(string accessToken, string pageId)
        {
            var graphSearchUrl = $"https://graph.facebook.com/v2.11/{pageId}/photos?access_token={accessToken}&fields=source.width(9999)";
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(graphSearchUrl);
                request.Method = "GET";

                string jsonresponse;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    jsonresponse = await reader.ReadToEndAsync();
                }

                return jsonresponse;
            }
            catch (Exception e)
            {
                NLog.LogManager.GetCurrentClassLogger()
                    .Error(e, $"Error while getting large resolution image from Facebook's graph api {pageId}");
                return null;
            }
        }

        private string GeApplicationAccessToken()
        {
            var accessTokenRequestUrl =
                "https://graph.facebook.com/v2.11/oauth/access_token?client_id={0}&client_secret={1}&grant_type=client_credentials";
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(string.Format(accessTokenRequestUrl, CLIENT_ID, CLIENT_SECRET));
                request.Method = "GET";

                JObject oauthResponse;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    oauthResponse = JsonConvert.DeserializeObject<JObject>(reader.ReadToEnd());
                }

                return oauthResponse["access_token"].ToString();
            }
            catch (Exception e)
            {
                NLog.LogManager.GetCurrentClassLogger()
                    .Error(e, "Error getting facebook's access token");
                return null;
            }
        }

        /// <summary>
        /// we should be doing this in the client or something closer to the text transfer but this is convenient for now
        /// </summary>
        /// <param name="profile"></param>
        private void EnhanceFacebookModel(FacebookProfile profile)
        {
            //select the right company message
            if (profile.About?.Length < profile.Description?.Length)
            {
                profile.Description = profile.About;
            }
            if (!string.IsNullOrEmpty(profile.Website))
            {
                var websites =
                    profile.Website.Split(' ').ToList(); //sometimes there are multiple websites in one string
                var webToType = new Dictionary<string, string>();
                foreach (string website in websites)
                {
                    if (website.Contains("facebook.com"))
                    {
                        webToType.Add(website, "facebook");
                    }
                    else if (website.Contains("twitter.com"))
                    {
                        webToType.Add(website, "twitter");
                    }
                    else
                    {
                        webToType.Add(website, "company");
                    }
                }
                var websiteToUse = webToType.FirstOrDefault(x => x.Value == "company");
                if (string.IsNullOrEmpty(websiteToUse.Key))
                {
                    websiteToUse = webToType.First();
                }
                var uri = new Uri(websiteToUse.Key);
                profile.Website = uri.Authority + (uri.AbsolutePath.Length > 1 ? uri.AbsolutePath : "");
            }

        }
    }
}