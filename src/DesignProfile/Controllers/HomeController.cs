using System.Linq;
using System.Web.Mvc;

namespace DesignProfile.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.IsLocal = Properties.Settings.Default.Environment.Equals("local");
            ViewBag.GoogleTagManagerKey = Properties.Settings.Default.GoogleTagManagerKey;
            ViewBag.AuthServiceKey = Properties.Settings.Default.VistaprintAuthKey;
            ViewBag.AuthServiceUrl = Properties.Settings.Default.VistaprintAuthUrl;
            ViewBag.VpSecureBaseUrl = Properties.Settings.Default.VpSecureBaseUrl;
            ViewBag.Enviroment = Properties.Settings.Default.Environment;
            return View();
        }
    }
}
