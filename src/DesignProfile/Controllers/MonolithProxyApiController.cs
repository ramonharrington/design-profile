﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using DesignProfile.data;
using DesignProfile.Properties;
using DevDefined.OAuth.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DesignProfile.Controllers
{
    public class MonolithProxyApiController : ApiController
    {
        [Route("api/GetFonts")]
        [HttpGet]
        public HttpResponseMessage GetFonts()
        {
            var urlBase = Settings.Default.VpApiServer + "/hatchery/rpc/assettransferapi/getfonts";
            var request = (HttpWebRequest)WebRequest.Create(urlBase);
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    var monolithResponse = JsonConvert.DeserializeObject(reader.ReadToEnd());
                    var result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new ObjectContent<object>(monolithResponse, new JsonMediaTypeFormatter(),
                        "application/json");
                    return result;
                }
            }
            catch (WebException e)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error getting vp monolith api response", e);
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                resp.Content = new StringContent(urlBase + " : " + e.Response.GetResponseStream().ReadToEnd());
                return resp;
            }
        }

        [Route("api/ProxyPost")]
        [HttpPost]
        public HttpResponseMessage ProxyPost([FromBody] MonolithRequestWithBody monolithRequestWithBody)
        {
            var urlBase = Settings.Default.VpBaseUrl + monolithRequestWithBody.MonolithEndpoint;
            var request = (HttpWebRequest)WebRequest.Create(urlBase);
            request.ContentType = "application/json";
            request.Method = "POST";

            var body = Encoding.UTF8.GetBytes(monolithRequestWithBody.Payload.ToString());
            request.ContentLength = body.Length;

            var bodyStream = request.GetRequestStream();
            bodyStream.Write(body, 0, body.Length);
            bodyStream.Close();

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    var monolithResponse = JsonConvert.DeserializeObject(reader.ReadToEnd());
                    var result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new ObjectContent<object>(monolithResponse, new JsonMediaTypeFormatter(),
                        "application/json");
                    return result;
                }
            }
            catch (WebException e)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error getting vp monolith api response", e);
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                resp.Content = new StringContent(urlBase + " : " + e.Response.GetResponseStream().ReadToEnd());
                return resp;
            }
        }

        public class MonolithRequestWithBody
        {
            public string MonolithEndpoint { get; set; }
            public JObject Payload { get; set; }
        }
    }
}
