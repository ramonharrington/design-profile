﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using DesignProfile.data;
using Microsoft.Owin;

namespace DesignProfile.Controllers.Extensions
{
    /// <summary>
    /// Extension methods to help with session operations
    /// </summary>
    public static class AuthorizationHelpers
    {
        /// <summary>
        /// Gets the shopper key from the cookie
        /// </summary>
        /// <param name="httpRequestMessage">The HTTP request message.</param>
        /// <returns></returns>
        public static int? GetShopperKey(this HttpRequestMessage httpRequestMessage)
        {
            var cookieValue = httpRequestMessage.Headers.GetCookies("shopperKey").FirstOrDefault()?["shopperKey"].Value;
            if (cookieValue == null)
            {
                return null;
            }

            int shopperKey;
            if (int.TryParse(cookieValue, out shopperKey))
            {
                return shopperKey;
            }

            return null;
        }

        public static bool CanView(this Account account, HttpRequestMessage httpRequestMessage)
        {
            return (account.ShopperKey == httpRequestMessage.GetShopperKey()) || IsDevMode(httpRequestMessage);
        }


        internal static string GetIpAddress(HttpRequestMessage httpRequestMessage)
        {
            if (httpRequestMessage.Properties.ContainsKey("MS_HttpContext"))
            {
                return IPAddress.Parse(((HttpContextBase)httpRequestMessage.Properties["MS_HttpContext"]).Request
                    .UserHostAddress).ToString();
            }
            if (httpRequestMessage.Properties.ContainsKey("MS_OwinContext"))
            {
                return IPAddress.Parse(((OwinContext)httpRequestMessage.Properties["MS_OwinContext"]).Request
                    .RemoteIpAddress).ToString();
            }
            return null;
        }

        internal static string GetIpAddress(HttpActionContext actionContext)
        {
            if (actionContext.Request.Properties.ContainsKey("MS_HttpContext"))
            {
                return IPAddress.Parse(((HttpContextBase)actionContext.Request.Properties["MS_HttpContext"]).Request
                    .UserHostAddress).ToString();
            }
            if (actionContext.Request.Properties.ContainsKey("MS_OwinContext"))
            {
                return IPAddress.Parse(((OwinContext)actionContext.Request.Properties["MS_OwinContext"]).Request
                    .RemoteIpAddress).ToString();
            }
            return null;
        }

        private static bool IsDevMode(HttpRequestMessage httpRequestMessage)
        {
            ///TODO: remove escape clause
            return true;
            var ipAddress = GetIpAddress(httpRequestMessage);
            return ipAddress == "127.0.0.1" || ipAddress == "::1";
        }
    }
}