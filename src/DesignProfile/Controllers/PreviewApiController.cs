﻿using DesignProfile.Adapters;
using DesignProfile.Controllers.Extensions;
using DesignProfile.data;
using DesignProfile.Models;
using DesignProfile.Notifications;
using DesignProfile.Properties;
using DesignProfile.Utils;
using DevDefined.OAuth.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using Newtonsoft.Json.Linq;using Hangfire;using WebGrease.Css.Extensions;

namespace DesignProfile.Controllers
{
    /// <summary>
    /// Api controller handling product-scene preview generation
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class PreviewApiController : ApiController
    {
        private const string AssetTransferUrl = "/hatchery/rpc/assettransferapi/AssetTransfer";
        private const string GetDocItemXml = "/hatchery/rpc/DesignProfileDocumentItemApi/GetItemXmlAndType/";
        private const string AddToPortfolioUrl = "/hatchery/rpc/DesignProfileApi/SaveDocumentToPortfolio";
        private const string AddToCartUrl = "/hatchery/rpc/BasketApi/addtocart";

        private SlackPublisher GeneratedDesignSlackPublisher =
            new SlackPublisher(slackRoomDev: "designpro-all-dev", slackRoomProd: "designpro-all-prod");

        /// <summary>
        /// Return the set of possible preview requests based on the profile
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <param name="numPreviews">The number previews.</param>
        /// <returns></returns>
        [Route("api/Profile/{profileId}/Previews/{numPreviews}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetPreviews(int profileId, int numPreviews,
            bool allowMixedScenes = false)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FindAsync(profileId);
                var securityIssue = InvalidSecurity(profile);
                if (securityIssue != null)
                {
                    return securityIssue;
                }

                var environment = Settings.Default.Environment;
                var previewKeys = new List<ProductPreview>();
                foreach (var template in ctx.Templates.Where(t => t.Environment == environment && t.UseForLogoCenter)
                    .ToList())
                {
                    foreach (var scene in ctx.SceneProducts.Where(sp => sp.ProductId == template.ProductId)
                        .Select(s => s.ScenePlacement.Scene).Distinct())
                    {
                        if (!allowMixedScenes)
                        {
                            bool allowScene = true;
                            foreach (var placement in scene.ScenePlacements)
                            {
                                if (!placement.SceneProducts.Any(p => p.ProductId == template.ProductId))
                                {
                                    allowScene = false;
                                    break;
                                }
                            }

                            if (!allowScene)
                            {
                                continue;
                            }
                        }
                        var previewKey = new ProductPreview {Template = template.TemplateId};

                        previewKey.Scene = scene.Filename;
                        previewKey.PlaceholderUrl = "/content/images/" + scene.Filename + ".png";
                        previewKey.Height = scene.Height ?? 0;
                        previewKey.Width = scene.Width ?? 0;
                        previewKey.ProductName = template.Name;
                        previewKey.Seed = 0;
                        previewKey.ProductId = template.ProductId;
                        previewKey.Key = scene.Filename + "_" + template.TemplateId + "_" + previewKey.Seed;
                        previewKey.AvailableQuantities =
                            QuantityAndSizesUtils.GetAvailableQuantities(previewKey.ProductId);
                        previewKey.AvailableSizes = QuantityAndSizesUtils.GetAvailableSizes(previewKey.ProductId);
                        if (!previewKeys.Any(p => p.Key == previewKey.Key))
                        {
                            previewKeys.Add(previewKey);
                        }
                    }
                }

                var result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ObjectContent<object>(previewKeys.Take(numPreviews), new JsonMediaTypeFormatter(),
                    "application/json");
                return result;
            }
        }

        [Route("api/Scene")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetSceneWithCropDimensions([FromUri] int profileId, [FromUri] int width,
            [FromUri] string eventName, [FromBody] IEnumerable<ProductDimensions> productDimensions, bool hideDefaultText = false)
        {
            return await GetSceneInternal(profileId, width, eventName, hideDefaultText, productDimensions);
        }

        [Route("api/Scene")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetScene(int profileId, int width, string eventName, bool hideDefaultText = false)
        {
            return await GetSceneInternal(profileId, width, eventName, hideDefaultText, null);
        }

        private async Task<HttpResponseMessage> GetSceneInternal(int profileId, int width, string eventName, bool hideDefaultText, IEnumerable<ProductDimensions> productDimensions)
        {
            data.Profile profile;
            var sceneId = string.Empty;
            var placementImages = new ConcurrentDictionary<string, Bitmap>();
            var placementProducts = new ConcurrentBag<PlacementProduct>();
            var sceneProductPreviews = new ConcurrentBag<SceneProductPreview>();
            int requestedHeight;
            var environment = Settings.Default.Environment;

            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                profile = await ctx.Profiles.FindAsync(profileId);
                var securityIssue = InvalidSecurity(profile);
                if (securityIssue != null)
                {
                    return securityIssue;
                }

                // For now, just get the last image
                // images = profile.Business.Images.ToList();
                var image = profile.Business.Images.Where(i => i.UploadId.HasValue).OrderByDescending(i => i.Id).FirstOrDefault();

                var scenes = ctx.Scenes.Include(s => s.ScenePlacements.Select(p => p.SceneProducts))
                    .Where(s => s.SceneName == eventName);

                var scene = scenes.Where(s => s.MinViewportWidth <= width).OrderByDescending(s => s.Width).FirstOrDefault();
                if (scene == null)
                {
                    scene = scenes.OrderByDescending(s => s.Width).FirstOrDefault();
                }

                if (scene == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                sceneId = scene.Filename;

                requestedHeight = scene.Height.Value * (width / scene.Width.Value);
                width = scene.Width.Value;

                // Get the list of placements in the scene
                var scenePlacements = scene.ScenePlacements.ToList();

                var templates = await ctx.Templates.Where(t => t.UseForGiftCenter && t.Environment == environment).ToListAsync();
                var smartcrop = new SmartCrop(image.Url);

                // Get the template/product to use for each placement
                //foreach (var scenePlacement in scenePlacements)
                Parallel.ForEach(scenePlacements, async (scenePlacement) =>
                {
                    var sceneProduct = scenePlacement.SceneProducts.FirstOrDefault();
                    var selectedTemplate = templates.FirstOrDefault(t => t.ProductId == sceneProduct.ProductId);
                    if (selectedTemplate == null)
                    {
                        return;
                    }

                    placementProducts.Add(new PlacementProduct
                    {
                        TemplateId = selectedTemplate.TemplateId,
                        ScenePlacement = scenePlacement,
                        SceneProduct = sceneProduct
                    });

                    if (!placementImages.ContainsKey(selectedTemplate.TemplateId))
                    {
                        SceneProductPreview sceneProductPreview = null;
                        var imageCrop = new CropDimensions();

                        // Legacy cropping -- overwrite with smartcrop coordinates below
                        // if available
                        var imageRatio = 1.0 * (image.Width ?? 1) / (image.Height ?? 1);
                        var placeholderRatio = 1.0 * (selectedTemplate.Width ?? 1) / (selectedTemplate.Height ?? 1);
                        

                        if (placeholderRatio > imageRatio)
                        {
                            imageCrop.CropLeft = imageCrop.CropRight = 0;
                            imageCrop.CropTop = imageCrop.CropBottom = imageRatio / placeholderRatio / 6;
                        }
                        else
                        {
                            imageCrop.CropTop = imageCrop.CropBottom = 0;
                            imageCrop.CropLeft = imageCrop.CropRight = placeholderRatio / imageRatio / 6;
                        }

                        if (productDimensions != null)
                        {
                            var dimensions =
                                productDimensions.FirstOrDefault(p => p.ProductId == sceneProduct.ProductId);
                            if (dimensions != null)
                            {
                                imageCrop = dimensions;
                            }
                        }

                        // Try to get the smartcrop coordinates -- if fails just continue with
                        // legacy cropping
                        try
                        {
                            var coords = smartcrop.GetResponse(selectedTemplate.Width.Value, selectedTemplate.Height.Value);
                            var multiplier = 8;

                            if (image.Height.Value > selectedTemplate.Height.Value ||
                                image.Width.Value > selectedTemplate.Width.Value)
                            {
                                if (smartcrop.GetResponse() != null && smartcrop.GetResponse().results.Length > 0)
                                {
                                    imageCrop.CropTop = (double)coords.y1 / image.Height.Value / multiplier;
                                    imageCrop.CropLeft = (double)coords.x1 / image.Width.Value / multiplier;
                                    imageCrop.CropRight = (double)(selectedTemplate.Width.Value - coords.x2) /
                                                          image.Width.Value * -1 / multiplier;
                                    imageCrop.CropBottom = (double)(selectedTemplate.Height.Value - coords.y2) /
                                                           image.Height.Value * -1 / multiplier;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            NLog.LogManager.GetCurrentClassLogger().Error(ex, "Smart cropping error occured");
                        }
                        
                        int maxAttempts = 3;
                        // try a few times :(
                        for (int i = 0; i < maxAttempts; i++)
                        {
                            if (sceneProductPreview != null)
                            {
                                break;
                            }
                            
                            try
                            {
                                sceneProductPreview = await GetPreview<SceneProductPreview>(profile, scene.Filename,
                                    scenePlacement, selectedTemplate, 400, 400, 0, image, "stretch", hideDefaultText, imageCrop);
                            }
                            catch (Exception ex)
                            {
                                NLog.LogManager.GetCurrentClassLogger()
                                    .Error("Error getting scene product preview, attempt " + i, ex);
                            }
                        }

                        if (sceneProductPreview == null)
                        {
                            return;
                        }

                        sceneProductPreview.ProductId = sceneProduct.ProductId;
                        sceneProductPreview.AvailableQuantities =
                            QuantityAndSizesUtils.GetAvailableQuantities(sceneProduct.ProductId);
                        sceneProductPreview.AvailableSizes =
                            QuantityAndSizesUtils.GetAvailableSizes(sceneProduct.ProductId);
                        sceneProductPreview.CropDimensions = imageCrop;

                        sceneProductPreviews.Add(sceneProductPreview);
                        var bitmap = GetBitmapFromUrl(sceneProductPreview.PreviewUrl);
                        placementImages.AddOrUpdate(selectedTemplate.TemplateId, bitmap, (key, oldValue) => bitmap);
                    }
                });
            }

            var scenePreview = new ScenePreview
            {
                Key = sceneId,
                Height = requestedHeight,
                Width = width,
                Products = sceneProductPreviews
            };

            var base64scene = await GetPreviewImage(sceneId, placementImages,
                placementProducts, width, "png");

            // Get the preview image

            scenePreview.PreviewData = "data:image/png;base64," + Convert.ToBase64String(base64scene);

            BackgroundJob.Enqueue(() => S3Utils.SaveSceneImage(profile.Id + "_" + DateTime.Now.ToString("yyMMdd_hhss") + ".png", base64scene));
            

            // Add the imagemap data
            foreach (var placementImage in placementImages)
            {
                placementImage.Value.Dispose();
            }

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ObjectContent<ScenePreview>(scenePreview, new JsonMediaTypeFormatter(),
                "application/json");
            return result;
        }

        [Route("api/Preview")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetPreview(int profileId, string scene, int height, int width, int seed,
            [FromUri] IEnumerable<string> template, bool hideDefaultText = false)
        {
            var environment = Settings.Default.Environment;
            var templates = template;
            data.Profile profile;
            data.Scene sceneToUse;
            ProductPreview productPreview = null;
            var placementImages = new Dictionary<string, Bitmap>();
            var placementProducts = new List<PlacementProduct>();

            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                profile = await ctx.Profiles.FindAsync(profileId);
                var securityIssue = InvalidSecurity(profile);
                if (securityIssue != null)
                {
                    return securityIssue;
                }

                // Get the list of requested templates
                IList<Template> templatesToUse = null;
                if (templates != null && templates.Any())
                {
                    templatesToUse = await ctx.Templates.Where(t => templates.Contains(t.TemplateId) && t.Environment == environment).Distinct()
                        .ToListAsync();
                }

                var templateProductIds = new Dictionary<string, string>();
                foreach (var templateToUse in templatesToUse)
                {
                    if (!templateProductIds.ContainsKey(templateToUse.TemplateId))
                    {
                        templateProductIds.Add(templateToUse.TemplateId, templateToUse.ProductId);
                    }
                }

                // Get the scene to render
                sceneToUse = await ctx.Scenes.Include(s => s.ScenePlacements.Select(p => p.SceneProducts))
                    .FirstOrDefaultAsync(s => s.Filename == scene);

                // Get the list of placements in the scene
                var scenePlacements = sceneToUse.ScenePlacements.ToList();

                // Get the template/product to use for each placement
                foreach (var scenePlacement in scenePlacements)
                {
                    SceneProduct sceneProduct = null;
                    data.Template selectedTemplate = null;

                    if (templateProductIds != null && templateProductIds.Any())
                    {
                        sceneProduct =
                            scenePlacement.SceneProducts.FirstOrDefault(
                                s => templateProductIds.Values.Contains(s.ProductId));
                        if (sceneProduct != null)
                        {
                            var selectedTemplateId = templateProductIds.FirstOrDefault(t => t.Value == sceneProduct.ProductId)
                                .Key;
                            selectedTemplate =
                                await ctx.Templates.FirstOrDefaultAsync(t => t.TemplateId == selectedTemplateId);
                        }
                    }

                    if (sceneProduct == null)
                    {
                        sceneProduct = scenePlacement.SceneProducts.FirstOrDefault();
                        var tmp = ctx.Templates.FirstOrDefault(t => t.ProductId == sceneProduct.ProductId && t.Environment == environment);
                        selectedTemplate = tmp;
                    }

                    placementProducts.Add(new PlacementProduct
                    {
                        TemplateId = selectedTemplate.TemplateId,
                        ScenePlacement = scenePlacement,
                        SceneProduct = sceneProduct
                    });

                    var image = profile.Business.Images.Where(i => i.ImageTypeId == 1 && i.UploadId.HasValue).OrderByDescending(i => i.Id).FirstOrDefault();

                    if (!placementImages.ContainsKey(selectedTemplate.TemplateId))
                    {
                        productPreview = await GetPreview<ProductPreview>(profile, scene, scenePlacement,
                            selectedTemplate, width, height, seed, image, "crop", hideDefaultText);
                        if (productPreview == null)
                        {
                            return new HttpResponseMessage(HttpStatusCode.NotFound);
                        }
                        productPreview.ProductId = sceneProduct.ProductId;
                        productPreview.AvailableQuantities =
                            QuantityAndSizesUtils.GetAvailableQuantities(sceneProduct.ProductId);
                        productPreview.AvailableSizes = QuantityAndSizesUtils.GetAvailableSizes(sceneProduct.ProductId);
                        placementImages.Add(selectedTemplate.TemplateId, GetBitmapFromUrl(productPreview.PreviewUrl));
                    }
                }
            }

            productPreview.PreviewData = "data:image/png;base64," +
                                         Convert.ToBase64String(await GetPreviewImage(sceneToUse.Filename,
                                             placementImages,
                                             placementProducts, width, "jpg"));

            foreach (var placementImage in placementImages)
            {
                placementImage.Value.Dispose();
            }

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ObjectContent<ProductPreview>(productPreview, new JsonMediaTypeFormatter(),
                "application/json");
            return result;
        }

        [Route("api/ComboPreview")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetComboPreview(int profileId, string scene, int height, int width, int seed,
            [FromUri] string combo, string product, bool hideDefaultText = false)
        {
            data.Profile profile;
            data.Scene sceneToUse;
            ProductPreview productPreview = null;
            var placementImages = new Dictionary<string, Bitmap>();
            var placementProducts = new List<PlacementProduct>();

            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                profile = await ctx.Profiles.FindAsync(profileId);
                var securityIssue = InvalidSecurity(profile);
                if (securityIssue != null)
                {
                    return securityIssue;
                }

                // Get the scene to render
                sceneToUse = await ctx.Scenes.Include(s => s.ScenePlacements.Select(p => p.SceneProducts))
                    .FirstOrDefaultAsync(s => s.Filename == scene);

                // Get the list of placements in the scene
                var scenePlacements = sceneToUse.ScenePlacements.ToList();

                // Get the template/product to use for each placement
                foreach (var scenePlacement in scenePlacements)
                {
                    SceneProduct sceneProduct = null;

                    if (sceneProduct == null)
                    {
                        sceneProduct = scenePlacement.SceneProducts.FirstOrDefault();
                    }

                    placementProducts.Add(new PlacementProduct
                    {
                        TemplateId = combo,
                        ScenePlacement = scenePlacement,
                        SceneProduct = sceneProduct
                    });

                    if (!placementImages.ContainsKey(combo))
                    {
                        productPreview = new ProductPreview();
                        productPreview.ProductId = sceneProduct.ProductId;
                        productPreview.AvailableQuantities =
                            QuantityAndSizesUtils.GetAvailableQuantities(sceneProduct.ProductId);
                        productPreview.AvailableSizes = QuantityAndSizesUtils.GetAvailableSizes(sceneProduct.ProductId);

                        placementImages.Add(combo, GetBitmapFromUrl(GetComboPreviewUrl(profile, combo, product)));
                    }
                }
            }

            productPreview.PreviewData = "data:image/png;base64," +
                                         Convert.ToBase64String(await GetPreviewImage(sceneToUse.Filename,
                                             placementImages,
                                             placementProducts, width, "jpg"));

            foreach (var placementImage in placementImages)
            {
                placementImage.Value.Dispose();
            }

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ObjectContent<ProductPreview>(productPreview, new JsonMediaTypeFormatter(),
                "application/json");
            return result;
        }

        private string GetComboPreviewUrl(data.Profile profile, string comboId, string productId)
        {
            var genPreviewUrl = "http://fermonolith.vistaprint.net/hatchery/rpc/assettransferapi/GenerateDesignPreview";

            var companyInfo = new Dictionary<string, string>();
            companyInfo["companyname"] = profile.Business.Name;
            companyInfo["companymessage"] = profile.Business.Tagline;
            companyInfo["fullname"] = profile.Business.EmployeeName;
            companyInfo["jobtitle"] = profile.Business.JobTitle;
            companyInfo["email"] = profile.Business.Email;
            companyInfo["phone"] = profile.Business.Phone1;
            companyInfo["web"] = profile.Business.Url;
            companyInfo["address1"] = profile.Business.Address1;
            companyInfo["address2"] = profile.Business.Address2;
            companyInfo["city"] = profile.Business.City;
            companyInfo["state"] = profile.Business.State;
            companyInfo["postal"] = profile.Business.Postal;
            companyInfo["fax"] = profile.Business.Phone2;

            if (companyInfo.ContainsKey("address2") && string.IsNullOrEmpty(companyInfo["address2"]))
            {
                companyInfo["address2"] = $"{companyInfo["city"]}, {companyInfo["state"]} {companyInfo["postal"]}";
            }

            var assetTransModel = new AssetTransferModel
            {
                PreviewHeight = 600,
                PreviewWidth = 600,
                DesignTemplate = comboId + "_" + productId,
                TextFieldPurposeNameToValue = companyInfo
            };

            var httpRequest = WebRequest.Create(genPreviewUrl);
            httpRequest.Timeout = 10000;
            httpRequest.Method = "POST";
            httpRequest.ContentType = "application/json";

            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(assetTransModel));
            httpRequest.ContentLength = body.Length;

            // Make the request to the monolith
            var bodyStream = httpRequest.GetRequestStream();
            bodyStream.Write(body, 0, body.Length);
            bodyStream.Close();


            using (HttpWebResponse response = (HttpWebResponse)httpRequest.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                var jobject = JsonConvert.DeserializeObject<JObject>(reader.ReadToEnd());
                return jobject["PreviewUrl"].Value<string>();
            }
        }

        [Route("api/Profile/{profileId}/RefreshPreview/{templateIdToUse}")]
        [HttpPost]
        public async Task<HttpResponseMessage> RefreshPreview(int profileId, string templateIdToUse, [FromBody] Dictionary<string, string> purposeText, int previewWidth = 400, 
            int previewHeight = 400)
        {
            List<ProductPreview> productPreviews;
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FindAsync(profileId);
                var securityIssue = InvalidSecurity(profile);
                if (securityIssue != null)
                {
                    return securityIssue;
                }
                if (purposeText != null)
                {
                    foreach (string purposeKey in purposeText.Keys){
                        if (purposeKey.ToLower().Equals("companyname"))
                        {
                            profile.Business.Name = purposeText[purposeKey];
                        }
                        if (purposeKey.ToLower().Equals("companymessage"))
                        {
                            profile.Business.Tagline = purposeText[purposeKey];
                        }
                        if (purposeKey.ToLower().Equals("address1"))
                        {
                            profile.Business.Address1 = purposeText[purposeKey];
                        }
                        if (purposeKey.ToLower().Equals("address2"))
                        {
                            profile.Business.Address2 = purposeText[purposeKey];
                        }
                        if (purposeKey.ToLower().Equals("address3"))
                        {
                            profile.Business.City = purposeText[purposeKey];
                        }
                        if (purposeKey.ToLower().Equals("email"))
                        {
                            profile.Business.Email = purposeText[purposeKey];
                        }
                        if (purposeKey.ToLower().Equals("web"))
                        {
                            profile.Business.Url = purposeText[purposeKey];
                        }
                        if (purposeKey.ToLower().Equals("fullname"))
                        {
                            profile.Business.EmployeeName = purposeText[purposeKey];
                        }
                        if (purposeKey.ToLower().Equals("fax"))
                        {
                            profile.Business.Phone2 = purposeText[purposeKey];
                        }
                        if (purposeKey.ToLower().Equals("phone"))
                        {
                            profile.Business.Phone1 = purposeText[purposeKey];
                        }
                        if (purposeKey.ToLower().Equals("jobtitle"))
                        {
                            profile.Business.JobTitle = purposeText[purposeKey];
                        }
                    }
                    ctx.SaveChanges();
                }
                var environment = Settings.Default.Environment;

                IEnumerable<Template> templates;
                if (templateIdToUse != null)
                {
                    templates = ctx.Templates
                        .Where(t => t.Environment == environment && t.UseForLogoCenter &&
                                    t.TemplateId == templateIdToUse).OrderByDescending(x => x.rank).ToList();
                }
                else
                {
                    templates = ctx.Templates.Where(t => t.Environment == environment && t.UseForLogoCenter)
                        .OrderByDescending(x => x.rank).ToList();
                }
                productPreviews = await GetPreviewSetInternal(profileId, 1, previewWidth, previewHeight,
                    false, ctx, true, templates);
            }

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ObjectContent<object>(productPreviews.FirstOrDefault(), new JsonMediaTypeFormatter(),
                "application/json");
            return result;
        }

        /// <summary>
        /// Gets the live preview.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <param name="imageId">The image identifier.</param>
        /// <param name="height">The height.</param>
        /// <param name="width">The width.</param>
        /// <param name="template">The template.</param>
        /// <param name="cropTop">The crop top.</param>
        /// <param name="cropRight">The crop right.</param>
        /// <param name="cropBottom">The crop bottom.</param>
        /// <param name="cropLeft">The crop left.</param>
        /// <returns></returns>
        [Route("api/LivePreview")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetLivePreview(int profileId, int imageId, int height, int width, string template, double? cropTop = null, double? cropRight = null, double? cropBottom = null, double? cropLeft = null)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FindAsync(profileId);
                var templateToUse = await ctx.Templates.Where(t => t.TemplateId == template).FirstOrDefaultAsync();
                var image = await ctx.Images.FindAsync(imageId);

                var excludedUrls = new List<string> { "http://localhost.us", "http://fermonolith.vistaprint.net" };
                var request =
                    (HttpWebRequest)WebRequest.Create((excludedUrls.Contains(Settings.Default.VpBaseUrl)
                                                          ? "https://www.vpdev.com"
                                                          : Settings.Default.VpBaseUrl) + AssetTransferUrl);
                request.ContentType = "application/json";
                request.Method = "POST";

                var settings = JsonConvert.DeserializeObject<ProfileSettings>(profile.Settings);

                var textFields = new Dictionary<string, string>
                {
                    {"companyname", profile.Business.Name ?? ""},
                    {"companymessage", profile.Business.Tagline ?? ""},
                    {"fullname", profile.Business.EmployeeName ?? ""},
                    {"jobtitle", profile.Business.JobTitle ?? ""},
                    {"email", profile.Business.Email ?? ""},
                    {"phone", profile.Business.Phone1 ?? ""},
                    {"web", profile.Business.Url ?? ""}
                };

                var model = new AssetTransferModel
                {
                    Colors = settings.colors,
                    Fonts = settings.fonts,
                    DesignTemplate = template,
                    PreviewWidth = width / 2,
                    PreviewHeight = height,
                    SaveDocument = true,
                    Images = new List<ImageAsset> { new ImageAsset { Id = image.UploadId.Value, ImageType = "logo" } },
                    TextFieldPurposeNameToValue = textFields,
                    CropTop = cropTop ?? image.CropTop ?? 0,
                    CropRight = cropRight ?? image.CropRight ?? 0,
                    CropBottom = cropBottom ?? image.CropBottom ?? 0,
                    CropLeft = cropLeft ?? image.CropLeft ?? 0,
                    CropMode = "stretch",
                    ImageFormat = "png"
                };

                try
                {
                    // attempt to pre-load the metadata
                    model.ItemMetaData = new TemplateItemsController().GetItemMetadata(template);
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error trying to get item metadata", ex);
                }

                if (templateToUse.Tags != null && templateToUse.Tags.Contains("force-background") && model.Images.Any())
                {
                    // get image background
                    var data = ImageRetrievalUtils.GetImageBytes(image.Url);

                    var bitmap = (Bitmap)System.Drawing.Image.FromStream(new MemoryStream(data));

                    model.Colors.tertiary = model.Colors.secondary;
                    model.Colors.secondary = model.Colors.primary;

                    var backgroundColor = new ImagingAdapter().GetBackgroundColor(bitmap);
                    if (backgroundColor.HasValue)
                    {
                        model.Colors.primary = ColorTranslator.ToHtml(backgroundColor.Value);
                    }
                    else
                    {
                        model.Colors.primary = "#FFFFFF";
                    }
                }

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));
                request.ContentLength = body.Length;

                // Make the request to the monolith
                var bodyStream = request.GetRequestStream();
                bodyStream.Write(body, 0, body.Length);
                bodyStream.Close();

                try
                {
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        var preview = JsonConvert.DeserializeObject<Preview>(reader.ReadToEnd());
                        var imagingAdapter = new ImagingAdapter();
                        var altDocId = preview.AltDocId;
                        var previewData = "data:image/png;base64," +
                                          Convert.ToBase64String(imagingAdapter.GetBytesFromBitmap(GetBitmapFromUrl(
                                              Settings.Default.VpSecureBaseUrl + "/vp/ns/livepreview.aspx?alt_doc_id=" +
                                              preview.AltDocId + "&height=" + height + "&width=" + width)));

                        return new HttpResponseMessage
                        {
                            StatusCode = HttpStatusCode.OK,
                            Content = new ObjectContent<ProductPreview>(
                                new ProductPreview {AltDocId = altDocId, PreviewData = previewData},
                                new JsonMediaTypeFormatter(), "application/json")
                        };
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    NLog.LogManager.GetCurrentClassLogger().Error("Error trying to get preview", ex);
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                }
            }
        }

        [Route("api/Profile/{profileId}/ComboPreviews/InstructionSet")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetPreviewInstructionSet(int profileId, int numPreviews = 5,
            int previewHeight = 400, int previewWidth = 400)
        {
            var previews = new List<ProductPreview>();

            try
            {
                using (var ctx = new DesignProfileEntities(ConnectionString.Current))
                {
                    var profile = await ctx.Profiles.FindAsync(profileId);
                    var securityIssue = InvalidSecurity(profile);
                    if (securityIssue != null)
                    {
                        return securityIssue;
                    }

                    var combos = await DesignComboProvider.GetCombosForProfile(profile, numPreviews);

                    foreach (var productCombos in combos)
                    {
                        try
                        {
                            var productId = productCombos.Key.BrandshopProductId;

                            var scene =
                                await ctx.Scenes.Where(s => s.ScenePlacements.Count == 1 &&
                                                      s.ScenePlacements.Any(
                                                          sp => sp.SceneProducts.FirstOrDefault().ProductId ==
                                                                productId)).FirstOrDefaultAsync();

                            foreach (var combo in productCombos.Value)
                            {
                                var preview = new ComboPreview()
                                {
                                    ProductId = productId,
                                    ComboId = int.Parse(combo.ComboId),
                                    Scene = scene?.Filename,
                                    Width = previewWidth,
                                    Height = previewHeight,
                                    Seed = 0,

                                    // Combo layout fields
                                    HasCompanyNameField = combo.CompanyName,
                                    HasCompanyMessageField = combo.CompanyMessage,
                                    HasFullNameField = combo.FullName,
                                    HasJobTitleField = combo.JobTitle,
                                    HasEmailField = combo.Email,
                                    HasPhoneField = combo.Phone,
                                    HasWebField = combo.Web,
                                    HasAddress1Field = combo.Address1,
                                    HasAddress2Field = combo.Address2,
                                    HasAddress3Field = combo.Address3,
                                    HasFaxField = combo.Fax,
                                    Ranking = combo.Ranking
                                };
                                preview.Key = scene.Filename + "_" + combo.ComboId + "_" + preview.Seed;

                                if (!previews.Any(p => p.Key == preview.Key))
                                {
                                    previews.Add(preview);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            NLog.LogManager.GetCurrentClassLogger().Error("Error getting combo preview", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error building combo preview instruction set", ex);
            }

            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new ObjectContent<object>(previews, new JsonMediaTypeFormatter(), "application/json")
            };
        }

        /// <summary>
        /// Gets a set of previews.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <param name="numPreviews">The number previews.</param>
        /// <param name="previewWidth">Width of the preview.</param>
        /// <param name="previewHeight">Height of the preview.</param>
        /// <param name="templateIdToUse">The template identifier to use.</param>
        /// <param name="allowMixedScenes">if set to <c>true</c> [allow mixed scenes].</param>
        /// <returns></returns>
        [Route("api/Profile/{profileId}/CompletePreviews/{numPreviews}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetPreviewSet(int profileId, int numPreviews,
            int previewWidth = 400, int previewHeight = 400, string templateIdToUse = null, bool allowMixedScenes = false, bool hideDefaultText = false)
        {
            List<ProductPreview> productPreviews;
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FindAsync(profileId);
                var securityIssue = InvalidSecurity(profile);
                if (securityIssue != null)
                {
                    return securityIssue;
                }
                var environment = Settings.Default.Environment;

                IEnumerable<Template> templates;
                if (templateIdToUse != null)
                {
                    templates = ctx.Templates
                        .Where(t => t.Environment == environment && t.UseForLogoCenter &&
                                    t.TemplateId == templateIdToUse).OrderByDescending(x => x.rank).ToList();
                }
                else
                {
                    templates = ctx.Templates.Where(t => t.Environment == environment && t.UseForLogoCenter)
                        .OrderByDescending(x => x.rank).ToList();
                }
                productPreviews = await GetPreviewSetInternal(profileId, numPreviews, previewWidth, previewHeight,
                    allowMixedScenes, ctx, hideDefaultText, templates);
            }

                var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ObjectContent<object>(productPreviews, new JsonMediaTypeFormatter(),
                "application/json");
            return result;
        }

        [Route("api/Profile/{profileId}/LogoOnlyFlow")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetBusinessCardLogoPreviews(int profileId, int numPreviews = 10,
            int previewWidth = 400, int previewHeight = 400, bool hideDefaultText = false)
        {
            Task<List<ProductPreview>> carouselPreviews;
            Task<List<ProductPreview>> otherPreviews;

            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FindAsync(profileId);
                var securityIssue = InvalidSecurity(profile);
                if (securityIssue != null)
                {
                    return securityIssue;
                }
                var environment = Settings.Default.Environment;

                IEnumerable<Template> carouselProductTemplates = ctx.Templates
                    .Where(t => t.Environment == environment && t.UseForBCsLogoFlow && t.UseInCarousel)
                    .OrderBy(x => x.rank).ToList();
                IEnumerable<Template> otherProductTemplates = ctx.Templates
                    .Where(t => t.Environment == environment && t.UseForBCsLogoFlow && !t.UseInCarousel)
                    .OrderByDescending(x => x.rank).ToList();

                carouselPreviews =  GetPreviewSetInternal(profileId, numPreviews, previewWidth, previewHeight,
                    false, ctx, hideDefaultText, carouselProductTemplates);
                otherPreviews =  GetPreviewSetInternal(profileId, numPreviews, previewWidth, previewHeight,
                    false, ctx, hideDefaultText, otherProductTemplates);
                await Task.WhenAll(new List<Task> {carouselPreviews, otherPreviews});
            }

            var previews = new Dictionary<string, List<ProductPreview>>{{"carousel", carouselPreviews.Result}, {"other", otherPreviews.Result}};
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ObjectContent<object>(previews, new JsonMediaTypeFormatter(),
                "application/json");
            return result;
        }

        private async Task<List<ProductPreview>> GetPreviewSetInternal(int profileId, int numPreviews,
            int previewWidth, int previewHeight, bool allowMixedScenes,
            DesignProfileEntities ctx, bool hideDefaultText, IEnumerable<Template> templates)
        {
            var productPreviews = new List<ProductPreview>();

            foreach (var template in templates)
            {
                foreach (var scene in ctx.SceneProducts.Where(sp => sp.ProductId == template.ProductId)
                    .Select(s => s.ScenePlacement.Scene).Distinct())
                {
                    if (!allowMixedScenes)
                    {
                        bool allowScene = true;
                        foreach (var placement in scene.ScenePlacements)
                        {
                            if (!placement.SceneProducts.Any(p => p.ProductId == template.ProductId))
                            {
                                allowScene = false;
                                break;
                            }
                        }

                        if (!allowScene)
                        {
                            continue;
                        }
                    }
                    var productPreview = new ProductPreview {Template = template.TemplateId};

                    productPreview.Scene = scene.Filename;
                    productPreview.PlaceholderUrl = "/content/images/" + scene.Filename + ".png";
                    productPreview.Height = scene.Height ?? 0;
                    productPreview.Width = scene.Width ?? 0;
                    productPreview.ProductName = template.Name;
                    productPreview.Seed = 0;
                    productPreview.ProductId = template.ProductId;
                    productPreview.Key = scene.Filename + "_" + template.TemplateId + "_" + productPreview.Seed;
                    productPreview.AvailableQuantities =
                        QuantityAndSizesUtils.GetAvailableQuantities(productPreview.ProductId);
                    productPreview.AvailableSizes = QuantityAndSizesUtils.GetAvailableSizes(productPreview.ProductId);
                    if (!productPreviews.Any(p => p.Key == productPreview.Key))
                    {
                        productPreviews.Add(productPreview);
                    }
                }
            }

            var sslFailureCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            ServicePointManager.ServerCertificateValidationCallback += sslFailureCallback;

            productPreviews = productPreviews.Take(numPreviews).ToList();

            Parallel.ForEach(productPreviews, (preview) =>
            {
                try
                {
                    var host = Request.RequestUri.Host;
                    if (string.Equals(host, "designprofile_dev_webservers",
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        host = "dev.designprofile.vistaprint.io";
                    }
                    else if (string.Equals(host, "designprofile_test_webservers",
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        host = "test.designprofile.vistaprint.io";
                    }
                    else if (string.Equals(host, "designprofile_webservers",
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        host = "designprofile.vistaprint.com";
                    }

                    var url =
                        $"{Request.RequestUri.Scheme}://{host}/api/Preview?profileId={profileId}&scene={preview.Scene}&height={previewHeight}&width={previewWidth}&seed=0&template={preview.Template}&hideDefaultText={hideDefaultText}";
                    var request =
                        (HttpWebRequest) WebRequest.Create(url);
                    request.ContentType = "application/json";
                    request.Method = "GET";

                    using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        var productPreview = JsonConvert.DeserializeObject<ProductPreview>(reader.ReadToEnd());
                        preview.ProductId = productPreview.ProductId;
                        preview.AvailableQuantities = productPreview.AvailableQuantities;
                        preview.AvailableSizes = productPreview.AvailableSizes;
                        preview.PreviewData = productPreview.PreviewData;
                        preview.PreviewUrl = preview.PreviewUrl;
                        preview.EditUrl = productPreview.EditUrl;
                        preview.ComboId = productPreview.ComboId;
                        preview.AltDocId = productPreview.AltDocId;
                        preview.ProductName = productPreview.ProductName;
                        preview.FormattedPrice = productPreview.FormattedPrice;
                    }
                }
                catch (WebException e)
                {
                    var error = e.Message;
                    System.Diagnostics.Debugger.Log(1, "error", error);
                    NLog.LogManager.GetCurrentClassLogger().Error("Error getting brandshop preview", e);
                }
            });

            return productPreviews;
        }

        /// <summary>
        /// Gets the set of previews used by Vistaprint Gallery
        /// </summary>
        /// <param name="shopperKey">The shopper key.</param>
        /// <param name="numPreviews">The number previews.</param>
        /// <param name="pfIdsToUse">The pf ids to use.</param>
        /// <param name="previewWidth">Width of the preview.</param>
        /// <param name="previewHeight">Height of the preview.</param>
        /// <returns></returns>
        [Route("api/Shopper/{shopperKey}/GalleryPreviews/{numPreviews}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetGalleryPreviews(int shopperKey, int numPreviews,
            string pfIdsToUse = null, int previewWidth = 400, int previewHeight = 400)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FirstOrDefaultAsync(p => p.Business.Account.ShopperKey == shopperKey);
                var securityIssue = InvalidSecurity(profile);
                if (securityIssue != null)
                {
                    return securityIssue;
                }

                var pfIds = new List<string>();
                if (!string.IsNullOrEmpty(pfIdsToUse))
                {
                    pfIds = pfIdsToUse.Split(',').ToList();
                }

                var previews =
                    await GetPreviews(ctx, profile, null, numPreviews, previewWidth, previewHeight, pfIds);
                var galleryPreviews = new List<GalleryPreview>();
                previews.ForEach(p => galleryPreviews.Add(new GalleryPreview
                {
                    EditUrl = p.EditUrl,
                    AltDocId = p.AltDocId,
                    ComboId = p.ComboId
                }));

                var result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ObjectContent<GalleryPreviewSet>(new GalleryPreviewSet(galleryPreviews),
                    new JsonMediaTypeFormatter(),
                    "application/json");
                return result;
            }
        }

        [Route("api/MatchingTestPreviews/{numPreviews}")]
        [HttpPost]
        public HttpResponseMessage GetPreviews([FromBody] AssetTransferModel model, int numPreviews)
        {
            var listOfTemplates = new List<string> {"3876106_B73", "1377355_B73", "62531_B06", "2806325_A0Y"};
            if (!string.IsNullOrEmpty(model.DesignTemplate))
            {
                listOfTemplates = new List<string> {model.DesignTemplate};
            }
            var urlBase = Settings.Default.VpBaseUrl + AssetTransferUrl;
            var listOfPreviews = new List<ProductPreview>();

            foreach (string template in listOfTemplates)
            {
                try
                {
                    var request = (HttpWebRequest) WebRequest.Create(urlBase);
                    request.ContentType = "application/json";
                    request.Method = "POST";

                    model.DesignTemplate = template;
                    var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));
                    request.ContentLength = body.Length;

                    var bodyStream = request.GetRequestStream();
                    bodyStream.Write(body, 0, body.Length);
                    bodyStream.Close();

                    using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        ProductPreview monolithResponse =
                            JsonConvert.DeserializeObject<ProductPreview>(reader.ReadToEnd());
                        listOfPreviews.Add(monolithResponse);
                    }
                }
                catch (WebException e)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error getting vp monolith api response", e);
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                    resp.Content = new StringContent(urlBase + " : " + e.Response.GetResponseStream().ReadToEnd());
                    return resp;
                }
            }

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ObjectContent<object>(listOfPreviews, new JsonMediaTypeFormatter(),
                "application/json");
            return result;
        }

        /// <summary>
        /// Adds the specified document to the portfolio.
        /// </summary>
        /// <param name="profileId">The profile identifier.</param>
        /// <param name="altDocId">The alt document identifier.</param>
        /// <returns></returns>
        [Route("api/Profile/{profileId}/Portfolio/Document/{altDocId}")]
        [HttpPost]
        public async Task<HttpResponseMessage> AddToPortfolio(int profileId, string altDocId)
        {
            var urlBase = Settings.Default.VpApiServer + AddToPortfolioUrl;
            int? shopperKey;

            try
            {
                using (var ctx = new DesignProfileEntities(ConnectionString.Current))
                {
                    var profile = await ctx.Profiles.FindAsync(profileId);
                    var securityIssue = InvalidSecurity(profile);
                    if (securityIssue != null)
                    {
                        return securityIssue;
                    }

                    shopperKey = profile.Business.Account.ShopperKey;
                }

                if (shopperKey == null)
                {
                    NLog.LogManager.GetCurrentClassLogger()
                        .Error("Unable to save document to portfolio - unknown user");
                    return new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }

                var requestData = new SaveDocumentToPortfolioRequest
                {
                    ShopperKey = shopperKey.Value,
                    AltDocId = altDocId
                };

                using (var http = new HttpClient())
                {
                    http.DefaultRequestHeaders.Add("X-Hatch-Oath", "All your base info is belong to us");
                    var response =
                        await
                            http.PostAsync(urlBase, new StringContent(JsonConvert.SerializeObject(requestData),
                                Encoding.UTF8,
                                "application/json"));

                    await response.Content.ReadAsStringAsync();
                    if (!response.IsSuccessStatusCode)
                    {
                        NLog.LogManager.GetCurrentClassLogger().Error("Error saving to portfolio");
                        return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                    }

                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error saving to portfolio", ex);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Adds the products in the request to the session's cart. Relies on a shopper being logged in to the session.
        /// Documents are saved to that shopper as well.
        /// </summary>
        /// <param name="addToCartRequests"></param>
        /// <returns></returns>
        [Route("api/AddToCart")]
        [HttpPost]
        public async Task<HttpResponseMessage> AddToCart([FromBody] List<CartItem> cartItems, int? shopperKey = null)
        {
            var environment = Settings.Default.Environment;

            if (cartItems == null || !cartItems.Any())
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Unable to add to cart - request was empty");
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            foreach (CartItem itemToAdd in cartItems)
            {
                if (string.IsNullOrEmpty(itemToAdd.Product) || string.IsNullOrEmpty(itemToAdd.AltDocID))
                {
                    NLog.LogManager.GetCurrentClassLogger()
                        .Error("Unable to add to cart - product or document was empty");
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
            }
            try
            {
                var urlBase = Settings.Default.VpApiServer + AddToCartUrl;
                using (var http = new HttpClient())
                {
                    http.DefaultRequestHeaders.Add("X-Hatch-Oath", "All your base info is belong to us");
                    var response =
                        await
                            http.PostAsync(urlBase,
                                new StringContent(
                                    JsonConvert.SerializeObject(new AddToCartRequest
                                    {
                                        CartItems = cartItems,
                                        ShopperKey = shopperKey ?? Request.GetShopperKey() ?? 0
                                    }),
                                    Encoding.UTF8,
                                    "application/json"));
                    var responseBody = await response.Content.ReadAsStringAsync();
                    if (!response.IsSuccessStatusCode)
                    {
                        NLog.LogManager.GetCurrentClassLogger().Error("Error saving to portfolio");
                        return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                    }

                    //returns back products that had errors when adding to cart
                    return new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(responseBody, Encoding.UTF8)
                    };
                }
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error adding to cart", ex);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        public class AddToCartRequest
        {
            public IEnumerable<CartItem> CartItems { get; set; }
            public int ShopperKey { get; set; }
        }

        public class CartItem
        {
            public string AltDocID { get; set; }
            public string Product { get; set; }
            public int Quantity { get; set; }
        }

        [Route("api/Previews/DocItems/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDocumentItemPreviews(string id)
        {
            var previewUrls = new List<object>();
            List<ItemXmlAndTypeResponse> xmlList = null;
            var getXmlUrl = Settings.Default.VpBaseUrl + GetDocItemXml + id;
            var getItemPreviewUrl = Settings.Default.VpBaseUrl +
                                    "/studio4/ComponentPreview/Render?contentserver=1&devcompservice=1";

            var request = (HttpWebRequest) WebRequest.Create(getXmlUrl);
            request.ContentType = "application/json";
            request.Method = "GET";
            try
            {
                using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    xmlList =
                        JsonConvert.DeserializeObject<List<ItemXmlAndTypeResponse>>(reader.ReadToEnd());
                }
            }
            catch (WebException e)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error getting vp doc items", e);
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                resp.Content = new StringContent(getXmlUrl + " : " + e.Response.GetResponseStream().ReadToEnd());
                return resp;
            }

            foreach (ItemXmlAndTypeResponse docItemXml in xmlList)
            {
                using (var wc = new WebClient())
                {
                    byte[] response = wc.UploadData(getItemPreviewUrl, Encoding.ASCII.GetBytes(docItemXml.Xml));
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(docItemXml.Xml);
                    var elemId = xmlDoc.DocumentElement.Attributes["id"].Value;
                    if (docItemXml.Type.Equals("image", StringComparison.CurrentCultureIgnoreCase) ||
                        elemId.Contains("imagearea"))
                    {
                        previewUrls.Add(new ImageItemPreviewModel
                        {
                            ItemPreviewUrl = "data:image/png;base64," + Convert.ToBase64String(response),
                            Key = elemId,
                            ImageUsage = "",
                            Type = docItemXml.Type
                        });
                    }
                    else
                    {
                        previewUrls.Add(new ItemPreviewModel
                        {
                            ItemPreviewUrl = "data:image/png;base64," + Convert.ToBase64String(response),
                            Key = elemId,
                            Type = docItemXml.Type
                        });
                    }
                }
            }

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content =
                new ObjectContent<List<object>>(previewUrls, new JsonMediaTypeFormatter(), "application/json");
            return result;
        }

        private string DefaultTextUtility(string originalText, bool hideDefaultText)
        {
            var defaultText = hideDefaultText ? " " : "";
            if (string.IsNullOrEmpty(originalText))
            {
                return defaultText;
            }
            return originalText;
        }

        private async Task<T> GetPreview<T>(data.Profile profile, string scene, ScenePlacement scenePlacement,
            data.Template template, int width, int height, int seed, data.Image imageRecord, string cropMode,
            bool hideDefaultText, CropDimensions cropDimensions = null) where T : Preview
        {
            /// TODO: We do this because the size of the logo is a lot smaller than the document and we need a larger
            /// resolution so the image isn't grainy.  Remove this hack and check this programmatically based on scene product
            if (template.ProductId == "FGU")
            {
                width = width * 2;
            }

            var excludedUrls = new List<string> {"http://localhost.us", "http://fermonolith.vistaprint.net"};
            var request =
                (HttpWebRequest) WebRequest.Create((excludedUrls.Contains(Settings.Default.VpBaseUrl)
                                                       ? "https://www.vpdev.com"
                                                       : Settings.Default.VpBaseUrl) + AssetTransferUrl);
            request.ContentType = "application/json";
            request.Method = "POST";

            var settings = JsonConvert.DeserializeObject<ProfileSettings>(profile.Settings);
            settings = settings.RemixSettings(seed);

            var textFields = new Dictionary<string, string>
            {
                {"companyname", DefaultTextUtility(profile.Business.Name, hideDefaultText)},
                {"companymessage", DefaultTextUtility(profile.Business.Tagline, hideDefaultText)},
                {"fullname", DefaultTextUtility(profile.Business.EmployeeName, hideDefaultText)},
                {"jobtitle", DefaultTextUtility(profile.Business.JobTitle, hideDefaultText)},
                {"email", DefaultTextUtility(profile.Business.Email, hideDefaultText)},
                {"phone", DefaultTextUtility(profile.Business.Phone1, hideDefaultText)},
                {"web", DefaultTextUtility(profile.Business.Url, hideDefaultText)},
                {"address1", DefaultTextUtility(profile.Business.Address1, hideDefaultText)},
                {"address2", DefaultTextUtility(profile.Business.Address2, hideDefaultText)},
                {"address3", DefaultTextUtility(profile.Business.City, hideDefaultText)},
                {"fax", DefaultTextUtility(profile.Business.Phone2, hideDefaultText)},
            };

            var model = new AssetTransferModel
            {
                Colors = settings.colors,
                Fonts = settings.fonts,
                DesignTemplate = template.TemplateId,
                PreviewWidth = width / 2,
                PreviewHeight = height,
                SaveDocument = true,
                Images = new List<ImageAsset> {new ImageAsset {Id = imageRecord.UploadId.Value, ImageType = "logo"}},
                TextFieldPurposeNameToValue = textFields,
                CropTop = cropDimensions?.CropTop ?? imageRecord.CropTop ?? 0,
                CropRight = cropDimensions?.CropRight ?? imageRecord.CropRight ?? 0,
                CropBottom = cropDimensions?.CropBottom ?? imageRecord.CropBottom ?? 0,
                CropLeft = cropDimensions?.CropLeft ?? imageRecord.CropLeft ?? 0,
                CropMode = cropMode,
                ImageFormat = "jpg"
            };

            try
            {
                // attempt to pre-load the metadata
                model.ItemMetaData = new TemplateItemsController().GetItemMetadata(template.TemplateId);
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error trying to get item metadata", ex);
            }

            if (template.Tags != null && template.Tags.Contains("force-background") && model.Images.Any())
            {
                // get image background
                var data = ImageRetrievalUtils.GetImageBytes(imageRecord.Url);

                var image = (Bitmap) System.Drawing.Image.FromStream(new MemoryStream(data));

                model.Colors.tertiary = model.Colors.secondary;
                model.Colors.secondary = model.Colors.primary;

                var backgroundColor = new ImagingAdapter().GetBackgroundColor(image);
                if (backgroundColor.HasValue)
                {
                    model.Colors.primary = ColorTranslator.ToHtml(backgroundColor.Value);
                }
                else
                {
                    model.Colors.primary = "#FFFFFF";
                }
            }

            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));
            request.ContentLength = body.Length;

            // Make the request to the monolith
            var bodyStream = request.GetRequestStream();
            bodyStream.Write(body, 0, body.Length);
            bodyStream.Close();

            T preview;

            try
            {
                using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    preview = JsonConvert.DeserializeObject<T>(reader.ReadToEnd());
                    preview.ScenePlacementId = scenePlacement.Id;
                    preview.Template = template.TemplateId;
                    preview.AvailableQuantities =
                        QuantityAndSizesUtils.GetAvailableQuantities(preview.ProductId);
                    preview.AvailableSizes = QuantityAndSizesUtils.GetAvailableSizes(preview.ProductId);
                    preview.OriginalSource = imageRecord.Url;
                }

                if (preview is ProductPreview)
                {
                    ProductPreview derivedObj = preview as ProductPreview;
                    derivedObj.EditUrl = derivedObj.EditUrl.StartsWith("http")
                        ? $"{derivedObj.EditUrl}"
                        : $"{Settings.Default.VpSecureBaseUrl}{derivedObj.EditUrl}";
                    derivedObj.Settings = JsonConvert.SerializeObject(settings);
                    derivedObj.Key = scene + "_" + template + "_" + seed;
                }
                else if (preview is SceneProductPreview)
                {
                    SceneProductPreview derivedObj = preview as SceneProductPreview;
                    if (scenePlacement.TooltipX.HasValue && scenePlacement.TooltipY.HasValue)
                    {
                        derivedObj.TooltipLocation =
                            new ElementCoordinates
                            {
                                X = scenePlacement.TooltipX.Value,
                                Y = scenePlacement.TooltipY.Value
                            };

                        derivedObj.CropDimensions = new CropDimensions
                        {
                            CropTop = imageRecord.CropTop ?? 0,
                            CropRight = imageRecord.CropRight ?? 0,
                            CropBottom = imageRecord.CropBottom ?? 0,
                            CropLeft = imageRecord.CropLeft ?? 0
                        };

                        derivedObj.PlacementHeight = template.Height;
                        derivedObj.PlacementWidth = template.Width;
                    }
                }

                return preview;
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                NLog.LogManager.GetCurrentClassLogger().Error("Error trying to get preview", ex);
                return null;
            }
        }

        private Bitmap GetBitmapFromUrl(string url)
        {
            try
            {
                using (var client = new WebClient())
                {
                    var data = client.DownloadData(new Uri(url));
                    return (Bitmap) System.Drawing.Image.FromStream(new MemoryStream(data));
                }
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error trying to image from url", ex);
                return null;
            }
        }

        /// <summary>
        /// Returns a set of ProductPreview objects, which consist of urls and other preview details.  Used for
        /// later lazy-loading the previews for each tile
        /// </summary>
        /// <param name="ctx">The CTX.</param>
        /// <param name="profile">The profile.</param>
        /// <param name="templateIdToUse">The template identifier to use.</param>
        /// <param name="numPreviews">The number previews.</param>
        /// <param name="previewWidth">Width of the preview.</param>
        /// <param name="pfIdsToUse">The pf ids to use.</param>
        /// <returns></returns>
        private async Task<IEnumerable<ProductPreview>> GetPreviews(DesignProfileEntities ctx, data.Profile profile,
            string templateIdToUse, int numPreviews, int previewWidth, int previewHeight, IEnumerable<string> pfIdsToUse)
        {
            ProfileSettings settings = JsonConvert.DeserializeObject<ProfileSettings>(profile.Settings);

            if (settings.images?.logo == null)
            {
                return new List<ProductPreview>();
            }

            IList<KeyValuePair<string, int>> templateIds;
            Dictionary<string, string> textFields;

            if (string.IsNullOrEmpty(templateIdToUse))
            {
                templateIds = ctx.TemplateItems.GetTemplatesWithColorCount().Take(numPreviews).ToList();
            }
            else
            {
                templateIds = ctx.TemplateItems.Where(i => i.template_id == templateIdToUse)
                    .GetTemplatesWithColorCount().Take(numPreviews).ToList();
            }
            if (!templateIds.Any())
            {
                // Couldn't find any suitable templates for this profile
                return new List<ProductPreview>();
            }

            textFields = new Dictionary<string, string>
            {
                {"companyname", profile.Business.Name ?? ""},
                {"companymessage", profile.Business.Tagline ?? ""},
                {"fullname", profile.Business.EmployeeName ?? ""},
                {"jobtitle", profile.Business.JobTitle ?? ""},
                {"email", profile.Business.Email ?? ""},
                {"phone", profile.Business.Phone1 ?? ""},
                {"web", profile.Business.Url ?? ""}
            };

            // Get the list of images
            var businessImages = GetImages(settings, profile.Business.Images);

            var urlBase = Settings.Default.VpBaseUrl + AssetTransferUrl;
            var listOfPreviews = new ConcurrentBag<ProductPreview>();

            var modelsToRequest = new List<AssetTransferModel>();

            foreach (var fontVariations in GetFontVariations(settings?.fonts ?? new font()))
            {
                foreach (var colorVariations in GetColorVariations(settings?.colors ?? new color()))
                {
                    var tmpColorList = new color();

                    tmpColorList.primary = colorVariations.Item1;
                    tmpColorList.secondary = colorVariations.Item2;
                    tmpColorList.tertiary = colorVariations.Item3;

                    foreach (var templateId in templateIds)
                    {
                        if (modelsToRequest.Count >= numPreviews)
                        {
                            break;
                        }

                        if (modelsToRequest.Count(m => m.DesignTemplate == templateId.Key) < templateId.Value)
                        {
                            var tmpFontList = new font();
                            tmpFontList.main = fontVariations.Item1;
                            tmpFontList.secondary = fontVariations.Item2;

                            var model = new AssetTransferModel
                            {
                                PreviewWidth = previewWidth,
                                PreviewHeight = previewHeight,
                                TextFieldPurposeNameToValue = textFields,
                                Images = businessImages
                                    .Select(i => new ImageAsset {Id = i.Key, ImageType = i.Value})
                                    .ToList(),
                                Colors = tmpColorList,
                                Fonts = tmpFontList,
                                SaveDocument = true,
                                DesignTemplate = templateId.Key
                            };

                            modelsToRequest.Add(model);
                        }
                    }
                }
            }

            var productScenes = ctx.SceneProducts.ToList();

            Parallel.ForEach(modelsToRequest, (model) =>
            {
                try
                {
                    listOfPreviews.Add(GetPreviewWithAssets(model, urlBase, settings));
                }
                catch (WebException e)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error getting vp monolith api response", e);
                }
            });

            foreach (var preview in listOfPreviews)
            {
                var product = productScenes.FirstOrDefault(s => s.ProductId == preview.ProductId);
                if (product != null)
                {
                    preview.Scene = product.ScenePlacement.Scene.Filename;
                    preview.Height = product.ScenePlacement.Scene.Height.Value;
                    preview.Width = product.ScenePlacement.Scene.Width.Value;
                }
                else
                {
                    preview.Height = 0;
                    preview.Width = 0;
                }
            }

            if (pfIdsToUse != null && pfIdsToUse.Any())
            {
                return listOfPreviews.Where(l => pfIdsToUse.Contains(l.ProductId));
            }

            foreach (ProductPreview preview in listOfPreviews)
            {
                var messageData = new Dictionary<string, string>();
                messageData.Add("previewUrl", preview.PreviewUrl);
                messageData.Add("productName", preview.ProductName);
                messageData.Add("profileId", profile.Id.ToString());
                GeneratedDesignSlackPublisher.PublishMessageAsync(message: preview.ProductName, author:
                    "Profile ID: " + profile.Id.ToString(), image_url: preview.PreviewUrl);
            }

            return listOfPreviews;
        }

        /// <summary>
        /// Gets an image rendering based on the inputs.
        /// </summary>
        /// <param name="sceneId">The scene identifier.</param>
        /// <param name="placementImages">The placement images.</param>
        /// <param name="placementProducts">The placement products.</param>
        /// <param name="width">The width.</param>
        /// <param name="outputFormat">The output format.</param>
        /// <returns></returns>
        private async Task<byte[]> GetPreviewImage(string sceneId, IDictionary<string, Bitmap> placementImages,
            IEnumerable<PlacementProduct> placementProducts, int width, string outputFormat)
        {
            // Place the image on a scene
            using (Bitmap sceneImage =
                (Bitmap) System.Drawing.Image.FromFile(
                    System.Web.HttpContext.Current.Request.MapPath("~\\Content\\images\\") + sceneId + ".png"))
            using (Bitmap maskImage =
                (Bitmap) System.Drawing.Image.FromFile(
                    System.Web.HttpContext.Current.Request.MapPath("~\\Content\\images\\") + sceneId + "_mask.png"))
            {
                var imagingAdapter = new ImagingAdapter();
                var imageData =
                    await Task.FromResult(imagingAdapter.GetScene(sceneImage, maskImage, placementImages,
                        placementProducts, width, outputFormat));

                return imageData;
            }
        }

        private ProductPreview GetPreviewWithAssets(AssetTransferModel model, string previewProviderUrl,
            ProfileSettings settings)
        {
            var templateId = model.DesignTemplate;
            model.DesignTemplate = model.DesignTemplate + TemplateMapper.GetProduct(templateId);
            var request = (HttpWebRequest) WebRequest.Create(previewProviderUrl);
            request.ContentType = "application/json";
            request.Method = "POST";

            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));
            request.ContentLength = body.Length;

            var bodyStream = request.GetRequestStream();
            bodyStream.Write(body, 0, body.Length);
            bodyStream.Close();

            using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                ProductPreview productPreview =
                    JsonConvert.DeserializeObject<ProductPreview>(reader.ReadToEnd());
                productPreview.Template = templateId;
                productPreview.Key = model.GetHashCode().ToString();
                productPreview.EditUrl = productPreview.EditUrl.StartsWith("http")
                    ? $"{productPreview.EditUrl}"
                    : $"{Settings.Default.VpSecureBaseUrl}{productPreview.EditUrl}";
                productPreview.AvailableQuantities =
                    QuantityAndSizesUtils.GetAvailableQuantities(productPreview.ProductId);
                productPreview.AvailableSizes = QuantityAndSizesUtils.GetAvailableSizes(productPreview.ProductId);
                var tmpSettings = new ProfileSettings
                {
                    colors = model.Colors,
                    fonts = model.Fonts,
                    images = settings.images
                };
                //productPreview.Settings = JsonConvert.SerializeObject(tmpSettings);

                return productPreview;
            }
        }

        private IEnumerable<Tuple<string, string, string>> GetColorVariations(color color)
        {
            var colors = new List<string>
            {
                color.primary,
                color.secondary,
                color.tertiary
            };

            var variations = new List<Tuple<string, string, string>>();

            for (var x = 0; x < 3; x++)
            {
                for (var y = 0; y < 3; y++)
                {
                    for (var z = 0; z < 3; z++)
                    {
                        if (x != y && x != z && y != z)
                        {
                            var color1 = colors.ElementAt(x);
                            var color2 = colors.ElementAt(y);
                            var color3 = colors.ElementAt(z);

                            if (!string.IsNullOrEmpty(color1) && !ImagingAdapter.IsGrayShade(color1))
                            {
                                if (string.IsNullOrEmpty(color2))
                                {
                                    color2 = color3;
                                    color3 = "";
                                }

                                variations.Add(new Tuple<string, string, string>(color1, color2, color3));
                            }
                        }
                    }
                }
            }

            if (!variations.Any())
            {
                variations.Add(new Tuple<string, string, string>(colors.ElementAt(0), colors.ElementAt(1),
                    colors.ElementAt(2)));
            }

            return variations;
        }

        private IEnumerable<Tuple<string, string>> GetFontVariations(font font)
        {
            var variations = new List<Tuple<string, string>>();
            variations.Add(new Tuple<string, string>(font.main, font.secondary));

            if (!string.IsNullOrEmpty(font.secondary))
            {
                variations.Add(new Tuple<string, string>(font.secondary, font.main));
            }

            return variations;
        }

        private IDictionary<int, string> GetImages(ProfileSettings settings, IEnumerable<data.Image> images)
        {
            var businessImages = new Dictionary<int, string>();

            // Get the list of images
            var imageIds = new List<int>();
            int? primaryImageId = null;
            if (settings?.images?.logo != null)
            {
                imageIds.Add(settings.images.logo.id);
                primaryImageId = settings.images.logo.id;
            }

            if (settings?.images?.pictures != null && settings.images.pictures.Any())
            {
                foreach (var picture in settings.images.pictures)
                {
                    imageIds.Add(picture.id);
                }
            }

            if (imageIds.Any())
            {
                var uploads = images.Where(i => imageIds.Contains(i.Id) && i.UploadId != null)
                    .ToList();

                if (uploads.Any(u => u.Id == primaryImageId))
                {
                    var image = uploads.Single(u => u.Id == primaryImageId);
                    businessImages.Add(image.UploadId.Value,
                        image.ImageTypeId == 3 ? "libraryimage" : (image.ImageTypeId == 2 ? "picture" : "logo"));
                }

                foreach (var upload in uploads.Where(u => u.Id != primaryImageId))
                {
                    businessImages.Add(upload.UploadId.Value,
                        upload.ImageTypeId == 3 ? "libraryimage" : (upload.ImageTypeId == 2 ? "picture" : "logo"));
                }
            }

            return businessImages;
        }

        private HttpResponseMessage InvalidSecurity(data.Profile profile)
        {
            if (profile == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            if (Request.Headers.Contains("Origin"))
            {
                var origins = Request.Headers.GetValues("Origin");
                if (origins.Any(o => o.Contains("designprofile.")))
                {
                    return null;
                }
            }

            if (!profile.Business.Account.CanView(Request))
            {
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

            return null;
        }

        public class ItemPreviewModel
        {
            public string Key { get; set; }
            public string ItemPreviewUrl { get; set; }
            public string Type { get; set; }
        }

        public class ImageItemPreviewModel : ItemPreviewModel
        {
            public string ImageUsage { get; set; }
        }

        public class ScenePreview
        {
            public string Key { get; set; }
            public string PreviewData { get; set; }
            public string Scene { get; set; }
            public int Height { get; set; }
            public int Width { get; set; }
            public IEnumerable<SceneProductPreview> Products { get; set; }
        }

        public class SceneProductPreview : Preview
        {
            public ElementCoordinates TooltipLocation { get; set; }
            public CropDimensions CropDimensions { get; set; }
            public int? PlacementHeight { get; set; }
            public int? PlacementWidth { get; set; }

        }

        public class CropDimensions
        {
            public double CropTop { get; set; }
            public double CropRight { get; set; }
            public double CropBottom { get; set; }
            public double CropLeft { get; set; }
        }

        public class ProductDimensions : CropDimensions
        {
            public string ProductId { get; set; }
            public bool UserCropped { get; set; }
        }

        public class ElementCoordinates
        {
            public int X { get; set; }

            public int Y { get; set; }

            //public Point TopLeft { get; set; }
            //public Point TopRight { get; set; }
            //public Point BottomRight { get; set; }
            //public Point BottomLeft { get; set; }
        }

        public class ComboPreview : ProductPreview
        {
            public bool HasCompanyNameField { get; set; }
            public bool HasCompanyMessageField { get; set; }
            public bool HasFullNameField { get; set; }
            public bool HasJobTitleField { get; set; }
            public bool HasEmailField { get; set; }
            public bool HasPhoneField { get; set; }
            public bool HasWebField { get; set; }
            public bool HasAddress1Field { get; set; }
            public bool HasAddress2Field { get; set; }
            public bool HasAddress3Field { get; set; }
            public bool HasFaxField { get; set; }
            public int Ranking { get; set; }
        }

        public class ProductPreview : Preview
        {
            public string Key { get; set; }
            public string PreviewData { get; set; }
            public string Scene { get; set; }
            public string EditUrl { get; set; }
            public string PlaceholderUrl { get; set; }
            public int Seed { get; set; }
            public int Height { get; set; }
            public int Width { get; set; }
            public string Settings { get; set; }
            public bool IsInPortfolio { get; set; }
        }

        public class Preview
        {
            public int ScenePlacementId { get; set; }
            public string PreviewUrl { get; set; }
            public string ProductId { get; set; }
            public string ProductName { get; set; }
            public int? ComboId { get; set; }
            public string AltDocId { get; set; }
            public string FormattedPrice { get; set; }
            public string Template { get; set; }
            public List<int> AvailableQuantities { get; set; }
            public Dictionary<string, string> AvailableSizes { get; set; }
            public string OriginalSource { get; set; }
        }

        public class ItemXmlAndTypeResponse
        {
            public string Type { get; set; }
            public string Xml { get; set; }
        }

        public class AssetTransferModel
        {
            //preview properties
            public int PreviewHeight { get; set; }

            public int PreviewWidth { get; set; }
            public string DesignTemplate { get; set; }
            public string CropMode { get; set; }
            public double CropTop { get; set; }
            public double CropRight { get; set; }
            public double CropBottom { get; set; }
            public double CropLeft { get; set; }
            public string ImageFormat { get; set; }

            //assets
            public IDictionary<string, string> TextFieldPurposeNameToValue { get; set; }

            public List<ImageAsset> Images { get; set; }
            public color Colors { get; set; }
            public font Fonts { get; set; }
            public bool SaveDocument { get; set; }
            public IDictionary<string, TemplateItemsController.ItemConfiguration> ItemMetaData { get; set; }
        }

        public class ImageAsset
        {
            public int Id { get; set; }
            public string ImageType { get; set; }
        }

        public class SaveDocumentToPortfolioRequest
        {
            public int ShopperKey { get; set; }
            public string AltDocId { get; set; }
        }
    }
}