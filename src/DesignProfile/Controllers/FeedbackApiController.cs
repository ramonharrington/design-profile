﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using DesignProfile.data;
using DesignProfile.Notifications;
using DesignProfile.Validation;
using Newtonsoft.Json;

namespace DesignProfile.Controllers
{
    public class FeedbackApiController : ApiController
    {
        private SlackPublisher _slackPublisher =
            new SlackPublisher(slackRoomDev: "brandshop-dev", slackRoomProd: "brandshop-feedback");

        [Route("api/Feedback")]
        [HttpPost]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> Post(FeedbackSubmission submission)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var profile = await ctx.Profiles.FindAsync(submission.ProfileId);

                if (profile == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                int feedbackTypeId = 3;
                if (string.Equals(submission.FeedbackType, "products", StringComparison.CurrentCultureIgnoreCase))
                {
                    feedbackTypeId = 1;
                }
                else if (string.Equals(submission.FeedbackType, "designs", StringComparison.CurrentCultureIgnoreCase))
                {
                    feedbackTypeId = 2;
                }

                try
                {
                    ctx.Feedbacks.Add(new Feedback
                    {
                        ProfileId = submission.ProfileId,
                        FeedbackTypeId = feedbackTypeId,
                        Message = submission.Message,
                        FeedbackDate = DateTime.Now
                    });

                    await ctx.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error adding feedback", ex);
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                }

                try
                {
                    var slackMsg = new Dictionary<string, string>
                    {
                        {"pretext", "New " + submission.FeedbackType + " feedback: " + submission.Message},
                        {"author_name", "Profile ID: " + submission.ProfileId}
                    };

                    // Notify slack room
                    _slackPublisher.PublishMessageAsync(slackMsg);
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error notifying Slack of new feedback", ex);
                }

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }
    }

    public class FeedbackSubmission
    {
        public int ProfileId { get; set; }
        public string FeedbackType { get; set; }
        public string Message { get; set; }
    }
}
