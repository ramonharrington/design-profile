﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using DesignProfile.data;
using DesignProfile.Validation;
using Newtonsoft.Json;

namespace DesignProfile.Controllers
{
    public class FontApiController : ApiController
    {
        [Route("api/Font")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> Get()
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(JsonConvert.SerializeObject(await ctx.Fonts.ToListAsync()), Encoding.UTF8,
                        "application/json")
                };
            }
        }
    }
}
