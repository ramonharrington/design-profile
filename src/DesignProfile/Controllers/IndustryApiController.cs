﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using DesignProfile.data;
using DesignProfile.Validation;
using Newtonsoft.Json;

namespace DesignProfile.Controllers
{
    public class IndustryApiController : ApiController
    {
        [Route("api/Industry")]
        [HttpGet]
        [VistaprintAuthorized]
        public async Task<HttpResponseMessage> Get()
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                return new HttpResponseMessage
                {
                    Content = new StringContent(
                        JsonConvert.SerializeObject(await ctx.Industries.Select(i => new {i.Id, i.Name}).ToListAsync()),
                        Encoding.UTF8, "application/json")
                };
            }
        }
    }
}
