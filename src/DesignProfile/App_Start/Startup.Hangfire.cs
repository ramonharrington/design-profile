using System;
using System.Net;
using Hangfire;
using Hangfire.Dashboard;
using DesignProfile.data;
using DesignProfile.Tasks;
using Owin;

namespace DesignProfile
{
    public partial class Startup
    {
        public void ConfigureHangfire(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .UseSqlServerStorage(ConnectionString.Hangfire);

            app.UseHangfireServer(new BackgroundJobServerOptions() { ServerName = System.Environment.MachineName });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions()
            {
                Authorization = new []
                {
                    new VistaprintIpFilter()
                } 
            });

            //RecurringJob.AddOrUpdate("DesignProfile-SendOrderedTemplatesToSlack", 
            //    ()=> OrderDataTaskManager.ReportOrderedTemplates(), "*/5 * * * *");
        }

        private class VistaprintIpFilter : IDashboardAuthorizationFilter
        {
            public bool Authorize(DashboardContext context)
           {
                if (context.Request.LocalIpAddress == "127.0.0.1" || context.Request.LocalIpAddress == "::1")
                {
                    return true;
                }

                return IsInRange(context.Request.LocalIpAddress, "10.0.0.0/8");
            }

            private bool IsInRange(string ipAddress, string CIDRmask)
            {
                string[] parts = CIDRmask.Split('/');

                int IP_addr = BitConverter.ToInt32(IPAddress.Parse(parts[0]).GetAddressBytes(), 0);
                int CIDR_addr = BitConverter.ToInt32(IPAddress.Parse(ipAddress).GetAddressBytes(), 0);
                int CIDR_mask = IPAddress.HostToNetworkOrder(-1 << (32 - int.Parse(parts[1])));

                return ((IP_addr & CIDR_mask) == (CIDR_addr & CIDR_mask));
            }
        }
    }
}
