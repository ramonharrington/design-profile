using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(DesignProfile.Startup))]

namespace DesignProfile
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureHangfire(app);
        }
    }
}
