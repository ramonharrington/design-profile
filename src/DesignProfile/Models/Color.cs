﻿namespace DesignProfile.Models
{
    public class Color
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HexCode { get; set; }
    }

    public static class ColorHelper
    {
        /// <summary>
        /// Converts a Color entity to a model
        /// </summary>
        /// <param name="color">The color.</param>
        /// <returns></returns>
        public static Color ToModel(this data.Color color)
        {
            var model = new Color()
            {
                Id = color.Id,
                Name = color.Name,
                HexCode = color.HexCode
            };

            return model;
        }

        /// <summary>
        /// Converts a Color model to an entity
        /// </summary>
        /// <param name="color">The color.</param>
        /// <returns></returns>
        public static data.Color ToEntity(this Color color)
        {
            var entity = new data.Color()
            {
                Id = color.Id,
                Name = color.Name,
                HexCode = color.HexCode
            };

            return entity;
        }
    }
}