﻿namespace DesignProfile.Models
{
    public class ProductTile
    {
        public int Id { get; set; }
        public string Pfid { get; set; }
        public string Markup { get; set; }
        public string Tags { get; set; }
        public bool HasLogoPlaceholder { get; set; }
        public bool HasImagePlaceholder { get; set; }
        public Color Color { get; set; }
    }

    public static class ProductTileHelper
    {
        /// <summary>
        /// Converts a ProductTile entity to a model.
        /// </summary>
        /// <param name="productTile">The productTile.</param>
        /// <param name="linkDepth">The link depth.</param>
        /// <returns></returns>
        public static ProductTile ToModel(this data.ProductTile productTile, int linkDepth = 0)
        {
            var model = new ProductTile()
            {
                Id = productTile.Id,
                Pfid = productTile.Pfid,
                Markup = productTile.Markup,
                Tags = productTile.Tags,
                HasLogoPlaceholder = productTile.HasLogoPlaceholder,
                HasImagePlaceholder = productTile.HasImagePlaceholder,
                Color = productTile.Color.ToModel()
            };

            return model;
        }

        /// <summary>
        /// Converts a ProductTile model to an entity.
        /// </summary>
        /// <param name="productTile">The productTile.</param>
        /// <returns></returns>
        public static data.ProductTile ToEntity(this ProductTile productTile)
        {
            var entity = new data.ProductTile()
            {
                Id = productTile.Id,
                Pfid = productTile.Pfid,
                Markup = productTile.Markup,
                Tags = productTile.Tags,
                HasLogoPlaceholder = productTile.HasLogoPlaceholder,
                HasImagePlaceholder = productTile.HasImagePlaceholder,
                ColorId = productTile.Color.Id
            };

            return entity;
        }

        /// <summary>
        /// Copies data from one ProductTile entity to another.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="updateRelationships">if set to <c>true</c> [update relationships].</param>
        public static void CopyTo(this data.ProductTile source, data.ProductTile destination, bool updateRelationships = true)
        {
            destination.Id = source.Id;
            destination.Pfid = source.Pfid;
            destination.Markup = source.Markup;
            destination.Tags = source.Tags;
            destination.HasLogoPlaceholder = source.HasLogoPlaceholder;
            destination.HasImagePlaceholder = source.HasImagePlaceholder;
            destination.Color = source.Color;
        }
    }
}