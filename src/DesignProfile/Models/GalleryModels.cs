﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace DesignProfile.Models
{
    /// <summary>
    /// Data object customized for Vistaprint Gallery
    /// </summary>
    public class GalleryProfile
    {
        public GalleryUserInfo UserInfo { get; set; }
        public int? LogoUploadId { get; set; }
        public IEnumerable<string> Colors { get; set; }

        public GalleryProfile()
        {
        }

        public GalleryProfile(data.Profile profile)
        {
            // Add user info
            UserInfo = new GalleryUserInfo
            {
                ShopperKey = profile.Business.Account.ShopperKey.Value,
                Company = profile.Business.Name,
                Tagline = profile.Business.Tagline,
                Address1 = profile.Business.Address1,
                Address2 = profile.Business.Address2,
                City = profile.Business.City,
                State = profile.Business.State,
                Postal = profile.Business.Postal,
                Phone1 = profile.Business.Phone1,
                Phone2 = profile.Business.Phone2,
                Email = profile.Business.Email,
                Url = profile.Business.Url,
                JobTitle = profile.Business.JobTitle,
                EmployeeName = profile.Business.EmployeeName
            };

            var profileSettings = JsonConvert.DeserializeObject<ProfileSettings>(profile.Settings);

            // Add logo
            if (profileSettings.images != null && profileSettings.images.logo != null)
            {
                var logoId = profileSettings.images.logo.id;
                var logo = profile.Business.Images.FirstOrDefault(i => i.Id == logoId);
                LogoUploadId = logo?.UploadId;
            }

            // Add colors
            if (profileSettings.colors != null)
            {
                var colors = new List<string>();
                if (profileSettings.colors.primary != null)
                {
                    colors.Add(profileSettings.colors.primary);
                    if (profileSettings.colors.secondary != null)
                    {
                        colors.Add(profileSettings.colors.secondary);
                        if (profileSettings.colors.tertiary != null)
                        {
                            colors.Add(profileSettings.colors.tertiary);
                        }
                    }
                }
                Colors = colors;
            }
        }
    }

    /// <summary>
    /// User info object, used in the Gallery Profile object
    /// </summary>
    public class GalleryUserInfo
    {
        public int ShopperKey { get; set; }
        public string Company { get; set; }
        public string Tagline { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postal { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string JobTitle { get; set; }
        public string EmployeeName { get; set; }
    }

    /// <summary>
    /// Contains a set of previews, called by Vistaprint Gallery
    /// </summary>
    public class GalleryPreviewSet
    {
        public IEnumerable<GalleryPreview> Previews { get; set; }

        public GalleryPreviewSet()
        {
        }

        public GalleryPreviewSet(IEnumerable<GalleryPreview> previews)
        {
            Previews = previews;
        }
    }

    /// <summary>
    /// A single preview, called by Vistaprint Gallery
    /// </summary>
    public class GalleryPreview
    {
        public string AltDocId { get; set; }
        public string EditUrl { get; set; }
        public int? ComboId { get; set; }
    }
}