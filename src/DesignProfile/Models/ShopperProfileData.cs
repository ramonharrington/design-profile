﻿using System;
using System.Collections.Generic;
using System.Linq;
using Amazon.DynamoDBv2.DocumentModel;

namespace DesignProfile.Models
{
    public class ShopperProfileData
    {
        public string ShopperId { get; set; }
        public List<DocumentData> Documents { get; set; }
        public List<ImageData> ShopperUploads { get; set; }
        public ImageData LastImage { get; set; }

        public ShopperProfileData()
        {
            Documents = new List<DocumentData>();
            ShopperUploads = new List<ImageData>();
        }

    }

    public class TextFieldData
    {
        public string PurposeTextId { get; set; }
        public string Value { get; set; }
        public string FontFamily { get; set; }
        public string HexColor { get; set; }
    }

    public class ImageData
    {
        public int ImageId { get; set; }
        public string Type { get; set; }
        public string ImagePreview { get; set; }
        public DateTime? UploadDate { get; set; }
    }

    public class DocumentData
    {
        public string Id { get; set; }
        public int? ComboId { get; set; }
        public int? ComboFamilyId { get; set; }
        public int? ComboFamilyImageId { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Ordered { get; set; }
        public string Preview { get; set; }
        public List<TextFieldData> DocumentToTextFieldData { get; set; }
        public List<ImageData> DocumentToUploadIds { get; set; }//maybe if they are logos? recolor data?
    }

    internal static class ShopperProfileHelper
    {
        private static int GetFieldWeight(string fieldName)
        {
            switch (fieldName)
            {
                case "companyname":
                    return 50;
                case "fullname":
                    return 30;
                case "companymessage":
                    return 25;
                case "web":
                    return 20;
                case "email":
                    return 20;
                default:
                    return 10;
            }
        }

        internal static DocumentData GetDocument(this ShopperProfileData shopperProfile)
        {
            if (shopperProfile?.Documents == null || !shopperProfile.Documents.Any())
            {
                return null;
            }

            var document = shopperProfile.Documents.Where(d => d.Ordered != null)
                .OrderByDescending(d => d.Ordered).FirstOrDefault();
            if (document != null)
            {
                return document;
            }

            return shopperProfile.Documents.OrderByDescending(d => d.Ordered).FirstOrDefault();
        }

        internal static IDictionary<string, int> GetFonts(this DocumentData document)
        {
            var fonts = new Dictionary<string, int>();
            if (document?.DocumentToTextFieldData == null || !document.DocumentToTextFieldData.Any())
            {
                return new Dictionary<string, int>();
            }

            foreach (var field in document.DocumentToTextFieldData)
            {
                if (!string.IsNullOrEmpty(field.PurposeTextId) && !string.IsNullOrEmpty(field.FontFamily))
                {
                    var weight = GetFieldWeight(field.PurposeTextId);
                    if (fonts.ContainsKey(field.FontFamily))
                    {
                        fonts[field.FontFamily] += weight;
                    }
                    else
                    {
                        fonts.Add(field.FontFamily, weight);
                    }
                }
            }

            return fonts;
        }

        internal static IDictionary<string, int> GetFontColors(this DocumentData document)
        {
            var colors = new Dictionary<string, int>();
            if (document?.DocumentToTextFieldData == null || !document.DocumentToTextFieldData.Any())
            {
                return new Dictionary<string, int>();
            }

            foreach (var field in document.DocumentToTextFieldData)
            {
                if (!string.IsNullOrEmpty(field.PurposeTextId) && !string.IsNullOrEmpty(field.HexColor))
                {
                    var weight = GetFieldWeight(field.PurposeTextId);
                    if (colors.ContainsKey(field.HexColor))
                    {
                        colors[field.HexColor] += weight;
                    }
                    else
                    {
                        colors.Add(field.HexColor, weight);
                    }
                }
            }

            return colors;
        }
    }
}
