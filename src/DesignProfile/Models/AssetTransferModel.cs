﻿using System.Collections.Generic;
using DesignProfile.Controllers;

namespace DesignProfile.Models
{
    public class AssetTransferModel
    {
        //preview properties
        public int PreviewHeight { get; set; }
        public int PreviewWidth { get; set; }
        public string DesignTemplate { get; set; }

        //assets
        public IDictionary<string, string> TextFieldPurposeNameToValue { get; set; }
        public List<int> Images { get; set; }
        public ProfileColors Colors { get; set; }
        public ProfileFonts Fonts { get; set; }
        public bool SaveDocument { get; set; }
        public string ImageFormat { get; set; }
    }

    public class ProfileColors
    {
        public string Primary { get; set; }
        public string Secondary { get; set; }
        public string Tertiary { get; set; }
    }

    public class ProfileFonts
    {
        public string Main { get; set; }
        public string Secondary { get; set; }
    }
}