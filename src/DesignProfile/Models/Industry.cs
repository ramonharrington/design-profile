﻿using System.Collections.Generic;

namespace DesignProfile.Models
{
    public class Industry
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Industry ParentIndustry { get; set; }
        public IEnumerable<Industry> ChildIndustries { get; set; }
    }

    public static class IndustryHelper
    {
        /// <summary>
        /// Converts an Industry entity to a model.
        /// </summary>
        /// <param name="industry">The industry.</param>
        /// <param name="linkDepth">The link depth.</param>
        /// <returns></returns>
        public static Industry ToModel(this data.Industry industry, int linkDepth = 0)
        {
            var model = new Industry()
            {
                Id = industry.Id,
                Name = industry.Name
            };

            if (linkDepth > 0)
            {
                model.ParentIndustry = industry.Industry2.ToModel(linkDepth - 1);

                var childIndustries = new List<Industry>();
                foreach (var child in industry.Industry1)
                {
                    childIndustries.Add(child.ToModel());
                }
                model.ChildIndustries = childIndustries;
            }

            return model;
        }

        /// <summary>
        /// Converts an Industry model to an entity.
        /// </summary>
        /// <param name="industry">The industry.</param>
        /// <returns></returns>
        public static data.Industry ToEntity(this Industry industry)
        {
            var entity = new data.Industry()
            {
                Id = industry.Id,
                Name = industry.Name,
                ParentIndustryId = industry.ParentIndustry.Id
            };

            return entity;
        }
    }
}