﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesignProfile.Models
{
    public class FacebookProfile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public FacebookLocation Location { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public string Link { get; set; }
        public IEnumerable<string> Emails { get; set; }
        public IEnumerable<string> Pictures { get; set; }
        public string Category { get; set; }
        public string About { get; set; }
    }

    public class FacebookLocation
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
    }
}