﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignProfile.Models
{
    public class Account
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public int? ShopperKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime AccountCreated { get; set; }

        public IEnumerable<Business> Businesses { get; set; }
    }

    public static class AccountHelper
    {
        /// <summary>
        /// Converts an Account entity to a model.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="linkDepth">The link depth.</param>
        /// <returns></returns>
        public static Account ToModel(this data.Account account, int linkDepth = 0)
        {
            var model = new Account()
            {
                Id = account.Id,
                Email = account.Email,
                ShopperKey = account.ShopperKey,
                FirstName = account.FirstName,
                LastName = account.LastName,
                AccountCreated = account.Created
            };

            if (linkDepth > 0)
            {
                var businesses = new List<Business>();

                foreach (var business in account.Businesses)
                {
                    businesses.Add(business.ToModel(linkDepth - 1));
                }

                model.Businesses = businesses;
            }

            return model;
        }

        /// <summary>
        /// Converts an Account model to an entity.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <returns></returns>
        public static data.Account ToEntity(this Account account)
        {
            var entity = new data.Account()
            {
                Id = account.Id,
                Email = account.Email,
                ShopperKey = account.ShopperKey,
                FirstName = account.FirstName,
                LastName = account.LastName,
                Created = account.AccountCreated
            };

            var businesses = new List<data.Business>();
            foreach (var business in account.Businesses)
            {
                businesses.Add(business.ToEntity());
            }
            entity.Businesses = businesses;

            return entity;
        }

        /// <summary>
        /// Copies data from one Account entity to another
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="updateRelationships">if set to <c>true</c> [update relationships].</param>
        public static void CopyTo(this data.Account source, data.Account destination, bool updateRelationships = true)
        {
            destination.Email = source.Email;
            destination.ShopperKey = source.ShopperKey;
            destination.FirstName = source.FirstName;
            destination.LastName = source.LastName;
            destination.Created = source.Created;

            if (updateRelationships)
            {
                foreach (var business in destination.Businesses)
                {
                    if (!source.Businesses.Any(p => p.Id == business.Id))
                    {
                        destination.Businesses.Remove(business);
                    }
                }

                foreach (var business in source.Businesses)
                {
                    if (!destination.Businesses.Any(p => p.Id == business.Id))
                    {
                        destination.Businesses.Add(business);
                    }
                }
            }
        }
    }
}
