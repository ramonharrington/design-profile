﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignProfile.Models
{
    /// <summary>
    /// Business object
    /// </summary>
    public class Business
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int? IndustryId1 { get; set; }
        public int? IndustryId2 { get; set; }
        public int? IndustryId3 { get; set; }
        public string Name { get; set; }
        public string Tagline { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postal { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string JobTitle { get; set; }
        public string EmployeeName { get; set; }
        public DateTime? Created { get; set; }
        public string Category { get; set; }
        public string FacebookPageId { get; set; }

        public Account Account { get; set; }
        public IEnumerable<Image> Images { get; set; }
        public virtual IEnumerable<Profile> Profiles { get; set; }
    }

    internal static class BusinessHelper
    {
        /// <summary>
        /// Converts a Business entity to a model
        /// </summary>
        /// <param name="business">The business.</param>
        /// <param name="linkDepth">The link depth.</param>
        /// <returns></returns>
        public static Business ToModel(this data.Business business, int linkDepth  = 0)
        {
            var model = new Business()
            {
                AccountId = business.AccountId,
                Id = business.Id,
                Name = business.Name,
                Tagline = business.Tagline,
                Address1 = business.Address1,
                Address2 = business.Address2,
                City = business.City,
                State = business.State,
                Postal = business.Postal,
                Phone1 = business.Phone1,
                Phone2 = business.Phone2,
                Email = business.Email,
                Url = business.Url,
                JobTitle = business.JobTitle,
                EmployeeName = business.EmployeeName,
                Created = business.Created,
                Category = business.Category,
                FacebookPageId = business.FbPlaceId,

                Account = business.Account.ToModel()
            };

            model.IndustryId1 = business.Industry?.Id;
            model.IndustryId2 = business.Industry1?.Id;
            model.IndustryId3 = business.Industry2?.Id;

            var images = new List<Image>();
            if (business.Images != null)
            {
                foreach (var image in business.Images)
                {
                    images.Add(image.ToModel());
                }
            }
            model.Images = images;

            if (linkDepth > 0)
            {
                // Profiles
                var profiles = new List<Profile>();
                foreach (var profile in business.Profiles)
                {
                    profiles.Add(profile.ToModel(linkDepth - 1));
                }
                model.Profiles = profiles;
            }

            return model;
        }

        /// <summary>
        /// Converts a Business model to an entity
        /// </summary>
        /// <param name="business">The business.</param>
        /// <returns></returns>
        public static data.Business ToEntity(this Business business)
        {
            var entity = new data.Business()
            {
                AccountId = business.AccountId,
                Name = business.Name,
                Tagline = business.Tagline,
                Address1 = business.Address1,
                Address2 = business.Address2,
                City = business.City,
                State = business.State,
                Postal = business.Postal,
                Phone1 = business.Phone1,
                Phone2 = business.Phone2,
                Email = business.Email,
                Url = business.Url,
                JobTitle = business.JobTitle,
                EmployeeName = business.EmployeeName
            };

            entity.IndustryId1 = business.IndustryId1;
            entity.IndustryId2 = business.IndustryId2;
            entity.IndustryId3 = business.IndustryId3;

            if (business.Profiles != null)
            {
                var profiles = new List<data.Profile>();
                foreach (var profile in business.Profiles)
                {
                    profiles.Add(profile.ToEntity());
                }
                entity.Profiles = profiles;
            }

            if (business.Images != null)
            {
                var images = new List<data.Image>();
                foreach (var image in business.Images)
                {
                    images.Add(image.ToEntity());
                }
                entity.Images = images;
            }

            return entity;
        }

        /// <summary>
        /// Copies data from one Business entity to another
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="updateRelationships">if set to <c>true</c> [update relationships].</param>
        public static void CopyTo(this data.Business source, data.Business destination, bool updateRelationships = true)
        {
            destination.Name = source.Name;
            destination.Tagline = source.Tagline;
            destination.Address1 = source.Address1;
            destination.Address2 = source.Address2;
            destination.City = source.City;
            destination.State = source.State;
            destination.Postal = source.Postal;
            destination.Phone1 = source.Phone1;
            destination.Phone2 = source.Phone2;
            destination.Email = source.Email;
            destination.Url = source.Url;
            destination.JobTitle = source.JobTitle;
            destination.EmployeeName = source.EmployeeName;
            destination.IndustryId1 = source.IndustryId1;
            destination.IndustryId2 = source.IndustryId2;
            destination.IndustryId3 = source.IndustryId3;

            if (updateRelationships)
            {
                foreach (var profile in destination.Profiles)
                {
                    if (!source.Profiles.Any(p => p.Id == profile.Id))
                    {
                        destination.Profiles.Remove(profile);
                    }
                }

                foreach (var profile in source.Profiles)
                {
                    if (!destination.Profiles.Any(p => p.Id == profile.Id))
                    {
                        destination.Profiles.Add(profile);
                    }
                }
            }
        }
    }
}