﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesignProfile.Models
{
    public class Profile
    {
        public int Id { get; set; }
        public Business Business { get; set; }
        public string Name { get; set; }
        public string Settings { get; set; }
        public DateTime Created { get; set; }
    }

    public static class ProfileHelper
    {
        /// <summary>
        /// Converts a Profile entity to a model.
        /// </summary>
        /// <param name="profile">The profile.</param>
        /// <param name="linkDepth">The link depth.</param>
        /// <returns></returns>
        public static Profile ToModel(this data.Profile profile, int linkDepth = 0)
        {
            var model = new Profile()
            {
                Id = profile.Id,
                Business = profile.Business.ToModel(linkDepth - 1),
                Name = profile.Name,
                Settings = profile.Settings,
                Created = profile.Created
            };

            return model;
        }

        /// <summary>
        /// Converts a Profile model to an entity.
        /// </summary>
        /// <param name="profile">The profile.</param>
        /// <returns></returns>
        public static data.Profile ToEntity(this Profile profile)
        {
            var entity = new data.Profile()
            {
                Id = profile.Id,
                BusinessId = profile.Business.Id,
                Name = profile.Name,
                Settings = profile.Settings,
                Created = profile.Created
            };

            return entity;
        }

        /// <summary>
        /// Copies data from one Profile entity to another.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        /// <param name="updateRelationships">if set to <c>true</c> [update relationships].</param>
        public static void CopyTo(this data.Profile source, data.Profile destination, bool updateRelationships = true)
        {
            destination.Id = source.Id;
            destination.BusinessId = source.BusinessId;
            destination.Name = source.Name;
            destination.Settings = source.Settings;
            destination.Created = source.Created;
        }
    }
}