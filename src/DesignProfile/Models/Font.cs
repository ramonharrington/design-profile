﻿namespace DesignProfile.Models
{
    public class Font
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string FontTags { get; set; }
    }

    public static class FontHelper
    {
        /// <summary>
        /// Converts a Font entity to a model
        /// </summary>
        /// <param name="font">The font.</param>
        /// <returns></returns>
        public static Font ToModel(this data.Font font)
        {
            var model = new Font()
            {
                Id = font.Id,
                Name = font.Name,
                Url = font.Url,
                FontTags = font.Tags
            };

            return model;
        }

        /// <summary>
        /// Converts a Font model to an entity
        /// </summary>
        /// <param name="font">The font.</param>
        /// <returns></returns>
        public static data.Font ToEntity(this Font font)
        {
            var entity = new data.Font()
            {
                Id = font.Id,
                Name = font.Name,
                Url = font.Url,
                Tags = font.FontTags
            };

            return entity;
        }
    }
}