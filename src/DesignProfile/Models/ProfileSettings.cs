﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DesignProfile.Adapters;
using NLog.Targets.Wrappers;

namespace DesignProfile.Models
{
    public class ProfileSettings
    {
        public font fonts { get; set; }
        public color colors { get; set; }
        public image images { get; set; }
    }

    public class font
    {
        public string main { get; set; }
        public string secondary { get; set; }
    }

    public class color
    {
        public string primary { get; set; }
        public string secondary { get; set; }
        public string tertiary { get; set; }
    }

    public class image
    {
        public imageData logo { get; set; }
        public IEnumerable<imageData> pictures { get; set; }
    }

    public class imageData
    {
        public int id { get; set; }
        public string url { get; set; }
    }

    public static class ProfileSettingsHelper
    {
        public static IEnumerable<imageData> GetPictures(this IEnumerable<data.Image> businessImages)
        {
            return businessImages.Where(i => i.ImageTypeId == 2).Select(b => new imageData
            {
                id = b.Id,
                url = b.Url
            }).ToList();
        }

        public static ProfileSettings RemixSettings(this ProfileSettings settings, int seed)
        {
            if (seed < 0)
            {
                return null;
            }

            var counter = 0;
            var tmpSettings = settings;

            while (true)
            {
                foreach (var colorVariations in GetColorVariations(settings?.colors ?? new color()))
                {
                    if (counter == seed)
                    {
                        return tmpSettings;
                    }
                    var tmpColorList = new color();
                    tmpColorList.primary = colorVariations.Item1;
                    tmpColorList.secondary = colorVariations.Item2;
                    tmpColorList.tertiary = colorVariations.Item3;
                    tmpSettings.colors = tmpColorList;
                    counter++;

                    foreach (var fontVariations in GetFontVariations(settings?.fonts ?? new font()))
                    {
                        if (counter == seed)
                        {
                            return tmpSettings;
                        }
                        var tmpFontList = new font();
                        tmpFontList.main = fontVariations.Item1;
                        tmpFontList.secondary = fontVariations.Item2;
                        tmpSettings.fonts = tmpFontList;
                        counter++;
                    }
                }
            }
        }

        private static IEnumerable<Tuple<string, string, string>> GetColorVariations(color color)
        {
            var colors = new List<string>
            {
                color.primary,
                color.secondary,
                color.tertiary
            };

            var variations = new List<Tuple<string, string, string>>();

            for (var x = 0; x < 3; x++)
            {
                for (var y = 0; y < 3; y++)
                {
                    for (var z = 0; z < 3; z++)
                    {
                        if (x != y && x != z && y != z)
                        {
                            var color1 = colors.ElementAt(x);
                            var color2 = colors.ElementAt(y);
                            var color3 = colors.ElementAt(z);

                            if (!string.IsNullOrEmpty(color1))
                            {
                                if (string.IsNullOrEmpty(color2))
                                {
                                    color2 = color3;
                                    color3 = "";
                                }

                                variations.Add(new Tuple<string, string, string>(color1, color2, color3));
                            }
                        }
                    }
                }
            }

            if (!variations.Any())
            {
                variations.Add(new Tuple<string, string, string>(colors.ElementAt(0), colors.ElementAt(1),
                    colors.ElementAt(2)));
            }

            return variations;
        }

        private static IEnumerable<Tuple<string, string>> GetFontVariations(font font)
        {
            var variations = new List<Tuple<string, string>>();
            variations.Add(new Tuple<string, string>(font.main, font.secondary));

            if (!string.IsNullOrEmpty(font.secondary))
            {
                variations.Add(new Tuple<string, string>(font.secondary, font.main));
            }

            return variations;
        }

    }
}