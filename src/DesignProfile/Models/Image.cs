﻿using System;
using System.Collections.Generic;

namespace DesignProfile.Models
{
    public class Image
    {
        public int Id { get; set; }
        public Business Business { get; set; }
        public ImageType? ImageType { get; set; }
        public string Title { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public string Url { get; set; }
        public DateTime Uploaded  { get; set; }
    }

    public static class ImageHelper
    {
        /// <summary>
        /// Converts an Image entity to a model
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="linkDepth">The link depth.</param>
        /// <returns></returns>
        public static Image ToModel(this data.Image image, int linkDepth = 0)
        {
            var model = new Image()
            {
                Id = image.Id,
                ImageType = (ImageType?)image.ImageType?.Id,
                Title = image.Title,
                Height = image.Height,
                Width = image.Width,
                Url = image.Url,
                Uploaded = image.Uploaded
            };

            if (linkDepth > 0)
            {
                model.Business = image.Business.ToModel(linkDepth - 1);
            }

            return model;
        }

        /// <summary>
        /// Converts an Image model to an entity
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns></returns>
        public static data.Image ToEntity(this Image image)
        {
            var entity = new data.Image()
            {
                Id = image.Id,
                Business = image.Business.ToEntity(),
                ImageTypeId = (int?)image.ImageType,
                Title = image.Title,
                Height = image.Height,
                Width = image.Width,
                Url = image.Url,
                Uploaded = image.Uploaded
            };

            return entity;
        }

        /// <summary>
        /// Copies data from one Image entity to another
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        public static void CopyTo(this data.Image source, data.Image destination)
        {
            destination.Id = source.Id;
            destination.BusinessId = destination.BusinessId;
            destination.ImageTypeId = source.ImageTypeId;
            destination.Title = source.Title;
            destination.Height = source.Height;
            destination.Width = source.Width;
            destination.Url = source.Url;
            destination.Uploaded = source.Uploaded;
        }
    }
}