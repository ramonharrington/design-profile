﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesignProfile.Models
{
    public enum ImageType
    {
        Logo = 1,
        Picture = 2
    }
}