using System.Collections.Generic;
using System.Collections.ObjectModel;
using Mandrill.Models;
using Mandrill.Requests.Messages;
using Newtonsoft.Json;
using WebGrease.Css.Extensions;

namespace DesignProfile
{
    public static class Email
    {
        public static void Send(string emailAddress)
        {
            var mandrill = new Mandrill.MandrillApi(Properties.Settings.Default.MandrillApiKey);

            var email = new EmailMessage
            {
                To = new []{ new EmailAddress(emailAddress) }
            };

            //email.AddGlobalVariable("VARIABLE_NAME", variable);
            email.MergeLanguage = "handlebars";

            mandrill.SendMessageTemplate(
                new SendMessageTemplateRequest(email, "template_name"));
        }
    }
}
