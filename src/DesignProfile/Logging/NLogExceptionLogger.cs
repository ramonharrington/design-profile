using System.Globalization;
using System.Web.Http.ExceptionHandling;
using NLog;

namespace DesignProfile.Logging
{
    public class NLogExceptionLogger : ExceptionLogger
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public override void Log(ExceptionLoggerContext context)
        {
            base.Log(context);
            try
            {
                var lei = new LogEventInfo(LogLevel.Error, typeof(NLogExceptionLogger).FullName, CultureInfo.CurrentCulture, context.Exception.Message, new object[0], context.Exception);
                lei.Properties["URL"] = context.Request.RequestUri.ToString();
                lei.Properties["Method"] = context.Request.Method;
                _logger.Log(lei);
            }
            catch
            { }
        }
    }
}
