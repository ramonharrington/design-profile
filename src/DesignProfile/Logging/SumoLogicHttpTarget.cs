using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Web;
using DesignProfile.Properties;
using NLog;
using NLog.Common;
using NLog.Layouts;

namespace DesignProfile.Logging
{
    public class SumoLogicHttpTarget : NLog.Targets.TargetWithLayout
    {
        private string URL = Settings.Default.SumoLogicUrl;

        private List<LogEventInfo> _backlog = new List<LogEventInfo>();
        private readonly object _backlogLock = new object();

        protected override void Write(AsyncLogEventInfo[] logEvents)
        {
            var allMessages = new List<LogEventInfo>(logEvents.Select(alei => alei.LogEvent));

            // If we failed to log anything previously, bring it back along
            if (_backlog.Count > 0)
            {
                List<LogEventInfo> toAppend;
                lock (_backlogLock)
                {
                    toAppend = _backlog;
                    _backlog = new List<LogEventInfo>();
                }
                allMessages.AddRange(toAppend);
            }

            // Nothing to log
            if (allMessages.Count == 0)
            {
                return;
            }

            var layout = Layout;

            using (var ms = new MemoryStream())
            {
                SerializeLogEvents(ms, allMessages, layout);

                // Post to sumo
                var req = (HttpWebRequest)WebRequest.Create(URL);
                req.Method = "POST";
                req.Headers["Content-Encoding"] = "gzip";
                req.ContentLength = ms.Length;

                try
                {
                    using (var reqStream = req.GetRequestStream())
                    {
                        ms.CopyTo(reqStream, 32 * 1024);
                        reqStream.Flush();
                    }

                    req.GetResponse();
                }
                catch
                {
                    // If any failures occur, preserve some of the messages for later retry
                    lock (_backlogLock)
                    {
                        const int maxBacklog = 10000;
                        int canAppend = Math.Min(maxBacklog - _backlog.Count, allMessages.Count);
                        if (canAppend > 0)
                        {
                            _backlog.AddRange(allMessages.Take(canAppend));
                        }
                    }
                }
            }
        }

        private static void SerializeLogEvents(MemoryStream ms, List<LogEventInfo> allMessages, Layout layout)
        {
            // Serialize each event to json, one per line, and gzip
            using (var gz = new GZipStream(ms, CompressionMode.Compress, true))
            using (var tw = new StreamWriter(gz))
            {
                foreach (var evt in allMessages)
                {
                    // There can't be any newlines or sumo will interpret it as one event.
                    string rendered = layout.Render(evt).Replace('\n', ' ');
                    tw.WriteLine(rendered);
                }
                gz.Flush();
            }

            ms.Seek(0, SeekOrigin.Begin);
        }

        protected override void Write(LogEventInfo logEvent)
        {
            throw new NotImplementedException();
        }

        protected override void Write(AsyncLogEventInfo logEvent)
        {
            throw new NotImplementedException();
        }
    }
}
