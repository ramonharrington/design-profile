using System;
using System.Linq;
using System.Web.Http.ExceptionHandling;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DesignProfile.Logging;
using Newtonsoft.Json;
using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using VP.SD.Runtime.Logging;
using GlobalConfiguration = System.Web.Http.GlobalConfiguration;

namespace DesignProfile
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.Services.Add(typeof(IExceptionLogger), new NLogExceptionLogger());
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None
            };
            System.Net.ServicePointManager.Expect100Continue = false;

            RegisterRoutes(RouteTable.Routes);

            NLog.LogManager.GlobalThreshold = Properties.Settings.Default.DebugLogs ? LogLevel.Trace : LogLevel.Warn;

            ConfigureSumo();

            NLog.LogManager.GetCurrentClassLogger().Info("Application starting");
        }

        protected void Application_BeginRequest()
        {
            if (Request.HttpMethod == "OPTIONS")
            {
                Response.StatusCode = 200;
                Response.Flush();
                Response.End();
            }
        }

        private static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "API",
                url: "api/{controller}/{id}",
                defaults: new {controller = "SurveyApi", action = "Index"}
            );

            routes.MapRoute(
                name: "Document Edit",
                url: "EditDocument/{id}",
                defaults: new { controller = "EditDocument", action = "Index" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{*url}",
                defaults: new { controller = "Home", action = "Index" }
            );
        }

        private void ConfigureSumo()
        {
            var lc = NLog.LogManager.Configuration;
            // Internal json formatting type, need to new up and configure
            var t = typeof(DefaultLogging).Assembly.GetType("VP.SD.Runtime.Logging.ExceptionsJsonlayout");
            var ci = t.GetConstructor(new Type[0]);
            var layout = (Layout)ci.Invoke(new object[0]);
            t.GetProperty("ApplicationName").SetValue(layout, new DefaultLoggingConfig().ApplicationName);

            // Create sumo target, wrap in buffering target
            Target target = new SumoLogicHttpTarget { Layout = layout };
            target = new NLog.Targets.Wrappers.BufferingTargetWrapper(target, 10000, 1000) { SlidingTimeout = false };
            lc.AddTarget("SumoLogicBuffer", target);
            lc.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, target));

            // Reset configuration property so changes are backed up
            NLog.LogManager.Configuration = lc;
        }
    }
}
