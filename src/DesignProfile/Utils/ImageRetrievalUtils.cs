﻿using System;
using System.Net;
using System.Net.Security;
using DesignProfile.Properties;

namespace DesignProfile.Utils
{
    public static class ImageRetrievalUtils
    {
        internal static byte[] GetImageBytes(string url)
        {
            var sslFailureCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            ServicePointManager.ServerCertificateValidationCallback += sslFailureCallback;
            using (var client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.Referer, "https://designprofile.vistaprint.com/");
                if (!url.StartsWith("http"))
                {
                    url = Settings.Default.VpBaseUrl + url;
                }
                return client.DownloadData(new Uri(url));
            }
        }

    }
}