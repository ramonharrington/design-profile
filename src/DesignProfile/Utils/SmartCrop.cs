﻿using System.IO;
using System.Linq;
using System.Net;
using DesignProfile.data;
using DesignProfile.Properties;
using Newtonsoft.Json;

namespace DesignProfile.Utils
{
    public class SmartCrop
    {
        private SmartCropResponse crops = null;

        public SmartCrop(string url)
        {
            var resolutions = GetResolutions();
            crops = GetCropResponse("?url=" + url + resolutions);
        }

        public SmartCrop(string url, int width, int height)
        {
            crops = GetCropResponse("?url=" + url + "&resolution=" + width + "x" + height);
        }

        public SmartCropResponse GetResponse()
        {
            return crops;
        }

        public SmartCropCoordinates GetResponse(int width, int height)
        {
            return crops.results.First().croppings.FirstOrDefault(t => t.target_width == width && t.target_height == height);
        }

        private static string GetResolutions()
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var resolutions = ctx.Templates.Where(b => b.Width != null && b.Height != null).Select(t => "" + t.Width + "x" + t.Height).Distinct().ToList();

                var resolutionString = "";
                foreach (var resolution in resolutions)
                {
                    resolutionString += "&resolution=" + resolution;
                }

                return resolutionString;
            }
        }

        private static SmartCropResponse GetCropResponse(string cropParams)
        {

            var request = (HttpWebRequest)WebRequest.Create("https://api.imagga.com/v1/croppings" + cropParams);
            request.Method = "GET";

            request.Headers.Add(HttpRequestHeader.Authorization, Settings.Default.ImaggaAuthorization);

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return JsonConvert.DeserializeObject<SmartCropResponse>(reader.ReadToEnd());
            }
        }
    }

    public class SmartCropResponse
    {
        public SmartCropResult[] results { get; set; }
        public SmartCropError[] unsuccessful { get; set; }
    }

    public class SmartCropResult
    {
        public string image { get; set; }
        public SmartCropCoordinates[] croppings { get; set; }
    }

    public class SmartCropError
    {
        public string image { get; set; }
        public string info { get; set; }
    }
    public class SmartCropCoordinates
    {
        public int x1 { get; set; }
        public int x2 { get; set; }
        public int y1 { get; set; }
        public int y2 { get; set; }
        public int target_width { get; set; }
        public int target_height { get; set; }
    }
}