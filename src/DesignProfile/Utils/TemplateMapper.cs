﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.EC2.Model;
using DesignProfile.Properties;

namespace DesignProfile.Utils
{
    public static class TemplateMapper
    {
        private static readonly Dictionary<string, string> PRODUCT_MAP =
            new Dictionary<string, string>
            {
                { "1377355", "B73"},
                { "3876104", "B73"},
                { "3876106", "B73"},
                { "3880572", "B73"},
                { "3880574", "B73"},
                { "2831164", "A14"},
                { "62531", "B06"},
            };

        public static string GetProduct(string design)
        {
            if (design.Contains('_'))
            {
                return design;
            }
            string result = PRODUCT_MAP.Where(e => e.Key == design).Select(e => e.Value).FirstOrDefault();
            return !string.IsNullOrWhiteSpace(result) ? "_" + result : string.Empty;
        }

        private static readonly int[] BIZ_CARDS_SCENES = { 1320, 287, 288, 548, 1589, 1595 };
        private static readonly int[] TSHIRT_SCENES = { 1356 };
        private static readonly int[] BANNER_SCENES = { 1727 };
        private static readonly int[] WRAPAROUND_MUG_SCENES = { 1631 };
        private static readonly int[] LOGO_MUG = { 1323 };

        private static readonly Dictionary<string, int[]> SCENE_MAP =
            new Dictionary<string, int[]>
            {
                //for document as templates they need to be the NEW doc id
                { "1377355", BIZ_CARDS_SCENES},
                { "3876104", BIZ_CARDS_SCENES},
                { "3876106", BIZ_CARDS_SCENES},
                { "3880572", BIZ_CARDS_SCENES},
                { "3880574", BIZ_CARDS_SCENES},
                { "X2229-2A937-6Q1", BIZ_CARDS_SCENES},
                { "8F329-2A572-9W9", BIZ_CARDS_SCENES},
                { "2831164", WRAPAROUND_MUG_SCENES},
                { "62531", TSHIRT_SCENES},
                { "F5229-2A186-5J2", TSHIRT_SCENES},
                { "J8229-2A634-0R3", TSHIRT_SCENES},
                { "HH229-2A696-7N5", TSHIRT_SCENES},
                { "KJ229-2A705-1K0", TSHIRT_SCENES},
                { "7TTV8-2A528-1T8", BANNER_SCENES },
                { "MPTV8-2A277-0I9", LOGO_MUG},
                { "PPTV8-2A509-1I3", LOGO_MUG},
                { "TV229-2A404-0M8", LOGO_MUG},
                { "TPTV8-2A582-9T9", BANNER_SCENES},
                { "HPTV8-2A394-0L5", LOGO_MUG},
            };

        private const string SCENE_URL = "&scene=http%3a%2f%2fwww.vistaprint.com%2fdocuments%2fapi%2fscene%2f";
        private static Random rnd = new Random();

        public static string GetSceneQueryString(string design)
        {
            int[] scenes = SCENE_MAP.Where(e => e.Key == design).Select(e => e.Value).FirstOrDefault();
            if (scenes == null || scenes.Length == 0)
            {
                return string.Empty;
            }
            int r = rnd.Next(0, scenes.Length);
            return SCENE_URL + scenes[r];
        }

        
    }
}