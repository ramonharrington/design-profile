using System.IO;
using System.Linq;
using System.Web;
using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Transfer;
using DesignProfile.Notifications;
using DesignProfile.Properties;

namespace DesignProfile.Utils
{
    public class S3Utils
    {
        private static string _serviceURL = "https://s3-eu-west-1.amazonaws.com";

        public static string SaveSceneImage(string fileName, byte[] blob)
        {
            fileName = HttpUtility.UrlEncode(fileName);
            string clientAccessKey = Settings.Default.S3AccessKey;
            string clientSecretKey = Settings.Default.S3SecretKey;
            string bucketName = Settings.Default.S3Bucket + "/scenes";

            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = _serviceURL;

            using (var client = AWSClientFactory.CreateAmazonS3Client(clientAccessKey, clientSecretKey, config))
            using (var transferUtility = new TransferUtility(client))
            {
                var ms = new System.IO.MemoryStream();
                ms.Write(blob, 0, blob.Length);
                ms.Position = 0;
                var request = new TransferUtilityUploadRequest
                {
                    BucketName = bucketName,
                    Key = fileName,
                    InputStream = ms
                };

                transferUtility.Upload(request);
            }

            var s3url = _serviceURL + "/" + bucketName + "/" + fileName;

            // send the scene via slack
            var slackPublisher = new SlackPublisher(slackRoomDev: "brandshop-dev", slackRoomProd: "brandshop-logos");
            slackPublisher.PublishMessageAsync("New scene created", "giftshop", s3url);
            return (s3url);
        }

        /// <summary>
        /// Adds an image to a design profile.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="shopperKey">The shopper key.</param>
        /// <param name="blob">The image data.</param>
        /// <returns></returns>
        public static string AddDesignProfileImage(string fileName, string shopperKey, byte[] blob, string shopName)
        {
            fileName = HttpUtility.UrlEncode(fileName);
            string clientAccessKey = Settings.Default.S3AccessKey;
            string clientSecretKey = Settings.Default.S3SecretKey;
            string bucketName = Settings.Default.S3Bucket + "/images/" + shopperKey;

            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = _serviceURL;

            using (var client = AWSClientFactory.CreateAmazonS3Client(clientAccessKey, clientSecretKey, config))
            using (var transferUtility = new TransferUtility(client))
            {
                var ms = new System.IO.MemoryStream();
                ms.Write(blob, 0, blob.Length);
                ms.Position = 0;
                var request = new TransferUtilityUploadRequest
                {
                    BucketName = bucketName,
                    Key = fileName,
                    InputStream = ms
                };

                transferUtility.Upload(request);
            }

            var s3url = _serviceURL + "/" + bucketName + "/" + fileName;

            // send the logo via slack
            var slackPublisher = new SlackPublisher(slackRoomDev: "brandshop-dev", slackRoomProd: "brandshop-logos");
            slackPublisher.PublishMessageAsync("New logo uploaded for shopper: " + shopperKey, shopName, s3url);
            return (s3url);
        }

        /// <summary>
        /// Removes the item from the AWS bucket
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="shopperKey">The shopper key.</param>
        public static void RemoveImageFromAws(string fileName, int shopperKey)
        {
            string clientAccessKey = Settings.Default.S3AccessKey;
            string clientSecretKey = Settings.Default.S3SecretKey;

            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = _serviceURL;
            var bucketName = Settings.Default.S3Bucket + "/images/" + shopperKey;

            using (var client = AWSClientFactory.CreateAmazonS3Client(clientAccessKey, clientSecretKey, config))
            {
                client.DeleteObject(bucketName, fileName.Replace(_serviceURL + "/" + bucketName + "/", ""));
            }
        }
    }
}
