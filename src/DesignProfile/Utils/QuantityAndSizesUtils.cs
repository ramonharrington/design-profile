﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesignProfile.Utils
{
    public static class QuantityAndSizesUtils
    { 
        static readonly List<string> QUANTITY_SET_1 = new List<string>{"HJB", "EAE", "HJA", "EAD", "057", "HTZ"};
        static readonly List<string> QUANTITY_SET_2 = new List<string>{"016", "376" };
        static readonly List<string> QUANTITY_SET_3 = new List<string>{ "679", "A14", "A4E", "A0B", "A0Y", "640", "645" };
        static readonly List<string> QUANTITY_SET_4 = new List<string>{ "276", "331" };
        static readonly List<string> QUANTITY_SET_5 = new List<string>{ "236", "622" };
        static readonly List<string> QUANTITY_SET_6 = new List<string>{ "ABF" };
        static readonly List<string> QUANTITY_SET_7 = new List<string>{ "HXP" };
        static readonly List<string> QUANTITY_SET_8 = new List<string> { "042", "043" };
        static readonly List<string> QUANTITY_SET_9 = new List<string> { "083" };
        static readonly List<string> QUANTITY_SET_10 = new List<string> { "084" };
        static readonly List<string> QUANTITY_SET_11 = new List<string> { "218", "220" };
        static readonly List<string> QUANTITY_SET_12 = new List<string> { "357" };
        static readonly List<string> QUANTITY_SET_13 = new List<string> { "BQ8" };
        static readonly List<string> QUANTITY_SET_14 = new List<string> { "BR3", "K6U", "BPT" };
        static readonly List<string> QUANTITY_SET_15 = new List<string> { "B73" };
        static readonly List<string> QUANTITY_SET_16 = new List<string> { "FGU", "FGZ" };
        static readonly List<string> QUANTITY_SET_17 = new List<string> { "BQ9" };

        public static List<int> GetAvailableQuantities(string productId)
        {
            if (QUANTITY_SET_1.Contains(productId))
            {
                return new List<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
            }
            if (QUANTITY_SET_2.Contains(productId))
            {
                return new List<int> {1, 2, 3, 4, 5, 10, 25, 50, 100, 250};
            }
            if (QUANTITY_SET_3.Contains(productId))
            {
                return new List<int> {1, 2, 3, 4, 5, 10, 20, 50, 100, 200};
            }
            if (QUANTITY_SET_4.Contains(productId))
            {
                return new List<int> {1, 2, 3, 4, 5, 10, 20, 30, 40, 50, 100, 200};
            }
            if (QUANTITY_SET_5.Contains(productId))
            {
                return new List<int> { 10, 20, 30, 40, 50, 100, 150, 200, 250 };
            }
            if (QUANTITY_SET_6.Contains(productId))
            {
                return new List<int> {6, 12, 18, 24, 30, 60, 120, 180, 240, 300, 600, 1200};
            }
            if (QUANTITY_SET_7.Contains(productId))
            {
                return new List<int> { 25, 50, 100, 150, 200, 250 };
            }
            if (QUANTITY_SET_8.Contains(productId))
            {
                return new List<int> { 1,2,3,4,6,8,10,20,30,40,50,60,70,80,90,100,200,500,1000 };
            }
            if (QUANTITY_SET_9.Contains(productId))
            {
                return new List<int> { 140, 280, 420, 560, 700, 840, 980, 1120, 1260, 1400 };
            }
            if (QUANTITY_SET_10.Contains(productId))
            {
                return new List<int> { 50, 100, 250, 500, 1000, 1500, 2000, 2500, 5000, 10000, 20000 };
            }
            if (QUANTITY_SET_11.Contains(productId))
            {
                return new List<int> { 10, 20, 30, 40, 50, 100, 200, 500, 1000 };
            }
            if (QUANTITY_SET_12.Contains(productId))
            {
                return new List<int> { 50, 100, 250, 500, 750, 1000, 1500, 2000, 2500, 5000, 10000 };
            }
            if (QUANTITY_SET_13.Contains(productId))
            {
                return new List<int> { 4, 8, 12, 16, 20, 40, 80, 200, 400 };
            }
            if (QUANTITY_SET_14.Contains(productId))
            {
                return new List<int> { 1,2,3,4,5,6,7,8,9,10,25,50 };
            }
            if (QUANTITY_SET_15.Contains(productId))
            {
                return new List<int> { 100, 250, 500, 1000, 1500, 2000, 2500, 5000, 10000 };
            }
            if (QUANTITY_SET_16.Contains(productId))
            {
                return new List<int> { 1, 2, 3 };
            }
            if (QUANTITY_SET_17.Contains(productId))
            {
                return new List<int> { 20, 40, 100, 200, 500, 1000 };
            }

            return new List<int>();
        }

        static readonly Dictionary<string, string> HANES_MEN_PFID_MAP = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            {"S", "HJB" },
            {"M", "HJA" },
            {"L", "HJ9" },
            {"XL", "HJ8" },
            {"2XL", "HJ7" },
            //{"3XL", "HJ6" },
            //{"4XL", "HJ5" },
        };

        static readonly Dictionary<string, string> HANES_WOMEN_PFID_MAP = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            {"S", "EAE" },
            {"M", "EAD" },
            {"L", "EB2" },
            {"XL", "EAC" },
            {"2XL", "EAB" },
            //{"3XL", "HJ6" },
            //{"4XL", "HJ5" },
        };

        static readonly Dictionary<string, string> LONG_SLEEVE_TSHIRT_PFID_MAP = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            //{"S", "HU0" },
            {"M", "HTZ" },
            {"L", "HTY" },
            {"XL", "HTX" },
            //{"2XL", "HTW" },
            //{"3XL", "HTV" },
        };

        static readonly Dictionary<string, Dictionary<string,string>> PRODUCT_TO_SIZES_MAP = new Dictionary<string, Dictionary<string, string>>(StringComparer.OrdinalIgnoreCase)
        {
            {"HJB", HANES_MEN_PFID_MAP },
            {"EAE", HANES_WOMEN_PFID_MAP },
            {"HJA", HANES_MEN_PFID_MAP },
            {"EAD", HANES_WOMEN_PFID_MAP },
            {"HTZ", LONG_SLEEVE_TSHIRT_PFID_MAP }
        };

        public static Dictionary<string, string> GetAvailableSizes(string productId)
        {
            if (PRODUCT_TO_SIZES_MAP.ContainsKey(productId))
            {
                return PRODUCT_TO_SIZES_MAP[productId];
            }
            return new Dictionary<string, string>();
        }
    }
}