import request from 'superagent';
import _ from 'lodash';
import $ from 'jquery'

import { trackPreviewEvent, trackGenericEvent } from './logging';
import { displayLogoSelectionDialog } from './navigation';

export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const UPDATE_PROFILE_TMP = 'UPDATE_PROFILE_TMP';
export const RESET_PROFILE = 'RESET_PROFILE';
export const PERSIST_BUSINESS = 'PERSIST_BUSINESS';
export const PERSIST_PROFILE_SETTINGS = 'PERSIST_PROFILE_SETTINGS';
export const GET_PREVIEWS = 'GET_PREVIEWS';
export const UPDATE_PREVIEW = 'UPDATE_PREVIEW';
export const GET_PORTFOLIO_IMAGES = 'GET_PORTFOLIO_IMAGES';
export const POLL_LOGO_UPLOAD = 'POLL_LOGO_UPLOAD';
export const DOCUMENT_ADDED_TO_PORTFOLIO = 'DOCUMENT_ADDED_TO_PORTFOLIO';
export const GET_INSTAGRAM_IMAGES = 'GET_INSTAGRAM_IMAGES';
export const ADD_TO_CART = 'ADD_TO_CART';

// Gift shop
export const GET_SCENE = 'GET_SCENE';

const API_URL = '/api/';
// Profile exports

export function getProfile(profileId) {
    if (typeof profileId === 'undefined' || profileId === null) {
        return;
    }

    return function(dispatch) {
        request.get(`${API_URL}Profile/${profileId}`).then(response => {
            let profile = profileToReactModel(response.body);
            dispatch({
                type: UPDATE_PROFILE,
                payload: profile
            });
        });
    }
}

export function getProfileByShopperKey(shopperKey) {
    if (typeof shopperKey === 'undefined' || shopperKey === null) {
        return;
    }

    return function(dispatch) {
        request.get(`${API_URL}Shopper/${shopperKey}/Profile?initialize=true`).then(response => {
            if (response.body !== null) {
                let profile = profileToReactModel(response.body);
                dispatch({
                    type: UPDATE_PROFILE,
                    payload: profile
                });

                if (_.get(profile, 'settings.images.logo', null) === null) {
                    displayLogoSelectionDialog()(dispatch);
                } else {
                    getPreviews(profile)(dispatch);
                }
            }
        });
    }
}

export function initializeProfileForShopper(shopperKey) {
    if (typeof shopperKey === 'undefined' || shopperKey === null) {
        return;
    }

    return function (dispatch) {
        request
            .get(`${API_URL}Shopper/${shopperKey}/Initialize`)
            .then(response => {
                let profile = profileToReactModel(response.body);
                dispatch({type: UPDATE_PROFILE, payload: profile});
            });
    }
}

export function updateProfile(profile) {
    return (dispatch) => {
        dispatch({type: UPDATE_PROFILE_TMP, payload: profile});
    }
}

export function uploadPictures(profile, images) {
    return function (dispatch) {
        const req = request.post(`${API_URL}Image?profileId=${profile.id}`);
        images.forEach(image => {
            req.attach(image.name, image);
        });
        req.then(response => {
            profile.settings = response.body;
            dispatch({type: UPDATE_PROFILE, payload: profile})
        });
    }
}

export function deleteImage(profile, imageId) {
    return function (dispatch) {
        request
            .delete(`${API_URL}Image/${imageId}?profileId=${profile.id}`)
            .then(response => {
                profile.settings = response.body;
                dispatch({type: UPDATE_PROFILE, payload: profile})
            });
    }
}

export function uploadLogo(profile, logos) {
    return function (dispatch) {
        const req = request.post(`${API_URL}Logo?profileId=${profile.id}`);
        logos.forEach(logo => {
            req.attach(logo.name, logo);
        });
        req.then(response => {
            trackGenericEvent(profile.id, 'upload_logo')(dispatch);
            profile.settings = response.body;
            dispatch({type: UPDATE_PROFILE, payload: profile});

            pollLogoUpload(_.get(response.body, 'images.logo.id', null))(dispatch);
            $("#preview-panel").focus();
        })
        .catch( (err) => {
          alert("Error occured uploading logo: " + err);
        });
    }
}

export function selectLogo(profile, imageId) {
    return function (dispatch) {
        request
            .post(`${API_URL}Profile/${profile.id}/Logo/${imageId}?source=portfolio`)
            .then(response => {
                profile.settings = response.body;
                dispatch({type: UPDATE_PROFILE, payload: profile});
                getPreviews(profile)(dispatch);
            });
    }
}

export function pollLogoUpload(id) {
    return (dispatch) => {
        dispatch({type: POLL_LOGO_UPLOAD, payload: id});
    }
}

export function updatePreviewsIfLogoUploadIsComplete(profile, imageId) {
    return function(dispatch) {
        if (typeof imageId !== 'undefined' && imageId !== null && imageId !== 'null') {
            request.get(`${API_URL}Image/${imageId}/isInPortfolio`).then(response => {
                var isInPortfolio = response.body;

                if (isInPortfolio === true) {
                    getPreviews(profile)(dispatch);
                    pollLogoUpload(null)(dispatch);
                } else {
                    pollLogoUpload(imageId)(dispatch);
                }
                window.parent.postMessage("dropSuccessful", "*");
            });
        }
    }
}

export function getProfileImages(profileId) {
    return function (dispatch) {
        request
            .get(`${API_URL}Profile/${profileId}/ProfileImages`)
            .then(response => {
                dispatch({type: GET_PORTFOLIO_IMAGES, payload: response.body})
            });
    }
}

export function getPortfolioImages(profileId) {
    return function (dispatch) {
        request
            .get(`${API_URL}Profile/${profileId}/PortfolioImages`)
            .then(response => {
                dispatch({type: GET_PORTFOLIO_IMAGES, payload: response.body})
            });
    }
}

export function getInstagramImages(instagramToken) {
    return function (dispatch) {
        if (typeof instagramToken === 'undefined' || 0 === instagramToken.length) {
            launchInstagramAuth().then((token) => {
                instagramToken = token;
                retrieveInstagramImages(token).then((instaImages) => {
                    dispatch({
                        type: GET_INSTAGRAM_IMAGES,
                        payload: {
                            "images": instaImages,
                            "token": instagramToken
                        }
                    });
                }).catch(()=>{
                    dispatch({
                        type: GET_INSTAGRAM_IMAGES,
                        payload: {
                            "images": [],
                            "token": instagramToken
                        }
                    });
                })
            });
        } else {
            retrieveInstagramImages(instagramToken).then((instaImages) => {
                dispatch({
                    type: GET_INSTAGRAM_IMAGES,
                    payload: {
                        "images": instaImages,
                        "token": instagramToken
                    }
                });
            }).catch((error) => { //retry auth, maybe the token is bad
                launchInstagramAuth().then((token) => {
                    instagramToken = token;
                    retrieveInstagramImages(token).then((instaImages) => {
                        dispatch({
                            type: GET_INSTAGRAM_IMAGES,
                            payload: {
                                "images": instaImages,
                                "token": instagramToken
                            }
                        });
                    })
                    .catch(()=>{
                        dispatch({
                            type: GET_INSTAGRAM_IMAGES,
                            payload: {
                                "images": [],
                                "token": instagramToken
                            }
                        });
                    })
                })
            })
        }
    };
}

// Matching Panel exports
export function getPreviews(profile) {
    if (typeof profile === 'undefined' || profile === null) {
        return () => {};
    }

    return function (dispatch) {
        request
            .get(`${API_URL}Profile/${profile.id}/CompletePreviews/20?previewWidth=344`)
            .then(response => {
                dispatch({type: GET_PREVIEWS, payload: response});
            });
    }
}

export function getScenes(profile) {
    if (typeof profile === 'undefined' || profile === null) {
        return () => {};
    }

    return function (dispatch) {
        request
            .get(`${API_URL}Scene?profileId=${profile.id}&width=1440&eventName=holiday`)
            .then(response => {
                dispatch({type: GET_SCENE, payload: response});
            });
    }
}

export function updatePreview(profileId, preview) {
    if (typeof profileId === 'undefined' || profileId === null || typeof preview === 'undefined' || preview === null) {
        return;
    }

    let previewUrl = API_URL + "Preview?profileId=" + profileId + "&template=" + preview.Template + "&scene=" + preview.Scene + "&height=" + preview.Height + "&width=" + preview.Width + "&seed=" + preview.Seed;

    return function (dispatch) {
        setTimeout(() => {
            request
                .get(previewUrl)
                .then(response => {
                    let previewItem = response.body;
                    previewItem.Key = preview.Key;
                    dispatch({type: UPDATE_PREVIEW, payload: previewItem});
                }, error => {
                    dispatch({
                        type: UPDATE_PREVIEW,
                        payload: {
                            "Key": preview.Key,
                            "Error": true
                        }
                    });
                });
        }, 0);
    }
}

export function viewPortfolio(profileId, preview) {
    return function (dispatch) {
        trackPreviewEvent(profileId, preview, "ViewPortfolio")(dispatch);
        setTimeout(() => {
            window.top.location = window.vpBaseUrl + "/vp/ns/my_account/my_portfolio.aspx"
        }, 100);
    };
}

export function editDocument(profileId, preview) {
    return function (dispatch) {
        trackPreviewEvent(profileId, preview, "GoToStudio")(dispatch);
        setTimeout(() => {
            window.top.location = preview.EditUrl
        }, 100);
    };
}

export function addToPortfolio(profileId, preview) {
    if (typeof profileId === 'undefined' || profileId === null) {
        return;
    }

    return function (dispatch) {
        dispatch({type: DOCUMENT_ADDED_TO_PORTFOLIO, payload: preview});
        request
            .post(`${API_URL}Profile/${profileId}/Portfolio/Document/${preview.AltDocId}`)
            .then(response => {
                trackPreviewEvent(profileId, preview, "AddToPortfolio")(dispatch);
            });
    }
}

export function addToCart(profileId, cartItems, preview) {
    if (typeof cartItems === 'undefined'
        || _.isEmpty(cartItems)) {
        return;
    }

    return function (dispatch) {
        dispatch({type: ADD_TO_CART, payload: cartItems});
        request
            .post(`${API_URL}AddToCart`)
            .send(cartItems)
            .then(response => {
                trackPreviewEvent(profileId, preview, "AddToCart")(dispatch);
            });
    }
}

// Model conversions
function profileToReactModel(profile) {
    let settings;
    let settingsString = _.get(profile, 'Settings', '');
    if (settingsString === null || settingsString === '') {
        settings = {
            "fonts": {},
            "colors": {},
            "images": {}
        };
    } else {
        settings = JSON.parse(settingsString);
    }

    return {
        "id": profile.Id,
        "business": {
            "id": profile.Business.Id,
            "name": profile.Business.Name,
            "tagline": profile.Business.Tagline,
            "industryId": profile.Business.IndustryId1,
            "employeeName": profile.Business.EmployeeName,
            "jobTitle": profile.Business.JobTitle,
            "email": profile.Business.Email,
            "phone": profile.Business.Phone1,
            "url": profile.Business.Url
        },
        "settings": settings
        // "images": profile.Business.Images.map((image) => {     return { "id":
        // image.Id, "title": image.Title, "height": image.Height, "width": image.Width,
        // "url": image.Url, "uploaded": image.Uploaded } })
    };
}
function retrieveInstagramImages(instagramToken) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' + instagramToken + '&callback=?',

            error: function () {
                reject("error");
            },
            success: function (response) {
                if(response.meta.code !== 200){
                    reject(response);
                    return;
                }
                var instaImages = [];
                response
                    .data
                    .map((items) => instaImages.push({"ImageId": items.id, "ImagePreview": items.images.standard_resolution.url, "Type": "InstagramImage", UploadDate: null}));
                resolve(instaImages);
            },
            type: 'GET',
            dataType: "jsonp"
        })
    });
}

function launchInstagramAuth() {
    return new Promise((resolve, reject) => {
        var token = null;
        //the pop-up window size
        var popupWidth = 700,
            popupHeight = 500,
            popupLeft = (window.screen.width - popupWidth) / 2,
            popupTop = (window.screen.height - popupHeight) / 2;

        var redirect_url = window.location.origin + '/insta';
        var popup = window.open(redirect_url, '', 'width=' + popupWidth + ',height=' + popupHeight + ',left=' + popupLeft + ',top=' + popupTop + '');

        popup.onload = function () {
            //open authorize url in pop-up
            if (window.location.hash.length === 0) {
                popup.open('https://instagram.com/oauth/authorize/?client_id=b02562cf70e74eb6b4efb25ef2d7877a' +
                        '&redirect_uri=' + redirect_url + '&response_type=token', '_self');
            }
            var interval = setInterval(function () {
                try {
                    //check if hash exists
                    if (popup.location.hash.length) {
                        //hash found, that includes the access token
                        clearInterval(interval);
                        token = popup
                            .location
                            .hash
                            .slice(14); //slice #access_token= from string
                        resolve(token)
                        popup.close();
                    }
                } catch (evt) {
                    //permission denied
                    popup.close();
                    reject();
                }
            }, 100);
        }
    });
}
