import request from 'superagent';

export const AUTH_ERROR = 'AUTH_ERROR';
export const AUTH_USER = 'AUTH_USER';
export const SIGN_OUT_USER = 'SIGN_OUT_USER';
export const GET_ACCOUNT = 'GET_ACCOUNT';

const API_URL = '/api/';


// Auth exports
export function verifyAuth() {
    return () => { return true; }
}

export function authUser() {
    return {
        type: AUTH_USER
    }
}

export function signOutUser() {
    return () => { return true; }
}


// Account exports
export function getAccount(accountId) {
    return function(dispatch) {
        request.get(`${API_URL}Account/${accountId}`).then(response => {
            dispatch({
                type: GET_ACCOUNT,
                payload: response
            });
        });
    }
}
