import request from 'superagent';
import { tracker } from '../utils/event_tracking.js';

const API_URL = '/api/';


// Event tracking
export function trackPreviewEvent(profileId, preview, action) {
    let previewTrackingEvent = {
        "ProfileId": profileId,
        "PfId": preview.ProductId,
        "AltDocId": preview.AltDocId,
        "ComboId": preview.ComboId,
        "Template": preview.Template,
        "Scene": preview.Scene,
        "Settings": preview.Settings,
        "Action": action
    };

    return function(dispatch) {
        request.post(`${API_URL}PreviewEventTracker`).send(previewTrackingEvent).then(response => {
        });
    }
}

export function trackGenericEvent(profileId, eventType) {
    return function(dispatch) {
        tracker(eventType, profileId, null);
    }
}

export function publishToSlack(profileId, previewUrl, type, env) {
    /*
    var messageText;
    var channel;
    if(type === "portfolio"){
        messageText = "Saved to portfolio";
    }else{
        messageText = "Edit in Studio"
    }
    switch(env){
        case "dev":
            channel = "#designpro-saved-dev"
            break;
        case "prod":
            channel = "#designpro-saved-prod"
            break;
        default:
            return;
    }
    var data = {
        "channel" : channel,
        "attachments": [
            {
                "pretext" : messageText,
                "author_name": "Profile ID: " + profileId,
                "image_url" : previewUrl
            }
        ]
    };

    return function(dispatch) {
        request.post("https://hooks.slack.com/services/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").set('Content-Type', 'application/x-www-form-urlencoded').send(data)
        .then(response => {
        });
    }
    */
}
