import $ from 'jquery';

import { RESET_PROFILE } from './profile';

export const MINIMIZE_ALL_PROFILE_DETAILS = 'MINIMIZE_ALL_PROFILE_DETAILS';
export const EXPAND_BUSINESS_PROFILE_DETAILS = 'EXPAND_BUSINESS_PROFILE_DETAILS';
export const MINIMIZE_BUSINESS_PROFILE_DETAILS = 'MINIMIZE_BUSINESS_PROFILE_DETAILS';
export const EXPAND_COLOR_PROFILE_DETAILS = 'EXPAND_COLOR_PROFILE_DETAILS';
export const MINIMIZE_COLOR_PROFILE_DETAILS = 'MINIMIZE_COLOR_PROFILE_DETAILS';
export const EXPAND_FONT_PROFILE_DETAILS = 'EXPAND_FONT_PROFILE_DETAILS';
export const MINIMIZE_FONT_PROFILE_DETAILS = 'MINIMIZE_FONT_PROFILE_DETAILS';
export const EXPAND_IMAGE_PROFILE_DETAILS = 'EXPAND_IMAGE_PROFILE_DETAILS';
export const MINIMIZE_IMAGE_PROFILE_DETAILS = 'MINIMIZE_IMAGE_PROFILE_DETAILS';
export const DISPLAY_COLOR_PICKER = 'DISPLAY_COLOR_PICKER';
export const HIDE_COLOR_PICKER = 'HIDE_COLOR_PICKER';
export const DISPLAY_LOGO_SELECTION_DIALOG = 'DISPLAY_LOGO_SELECTION_DIALOG';
export const HIDE_LOGO_SELECTION_DIALOG = 'HIDE_LOGO_SELECTION_DIALOG';
export const SHOW_FONT_SELECTION_AREA = 'SHOW_FONT_SELECTION_AREA';


// Browser functions
export function resizeFrame() {
    try {
        let height = $(".profile").height();
        window.parent.postMessage('{ "contentHeight": ' + height + ' }', window.vpBaseUrl);
    } catch(e) {}
    return function(dispatch) {};
}


// App Navigation
export function minimizeAllProfileDetails() {
    return (dispatch) => {
        dispatch({ type: MINIMIZE_ALL_PROFILE_DETAILS });
        dispatch({ type: RESET_PROFILE });
    }
}

export function expandBusinessProfileDetails() {
    return (dispatch) => {
        dispatch({ type: EXPAND_BUSINESS_PROFILE_DETAILS });
        dispatch({ type: RESET_PROFILE });
    }
}

export function minimizeBusinessProfileDetails() {
    return (dispatch) => {
        dispatch({ type: MINIMIZE_BUSINESS_PROFILE_DETAILS });
        dispatch({ type: RESET_PROFILE });
    }
}

export function expandColorProfileDetails() {
    return (dispatch) => {
        dispatch({ type: EXPAND_COLOR_PROFILE_DETAILS });
        dispatch({ type: RESET_PROFILE });
    }
}

export function minimizeColorProfileDetails() {
    return (dispatch) => {
        dispatch({ type: MINIMIZE_COLOR_PROFILE_DETAILS });
        dispatch({ type: RESET_PROFILE });
    }
}

export function expandFontProfileDetails() {
    return (dispatch) => {
        dispatch({ type: EXPAND_FONT_PROFILE_DETAILS });
        dispatch({ type: RESET_PROFILE });
    }
}

export function minimizeFontProfileDetails() {
    return (dispatch) => {
        dispatch({ type: MINIMIZE_FONT_PROFILE_DETAILS });
        dispatch({ type: RESET_PROFILE });
    }
}

export function expandImageProfileDetails() {
    return (dispatch) => {
        dispatch({ type: EXPAND_IMAGE_PROFILE_DETAILS });
        dispatch({ type: RESET_PROFILE });
    }
}

export function minimizeImageProfileDetails() {
    return (dispatch) => {
        dispatch({ type: MINIMIZE_IMAGE_PROFILE_DETAILS });
        dispatch({ type: RESET_PROFILE });
    }
}

export function displayColorPicker(colorId) {
    return (dispatch) => {
        dispatch({ type: DISPLAY_COLOR_PICKER, payload: colorId });
    }
}

export function hideColorPicker() {
    return (dispatch) => {
        dispatch({ type: HIDE_COLOR_PICKER });
    }
}

export function displayLogoSelectionDialog() {
    return (dispatch) => {
        dispatch({ type: DISPLAY_LOGO_SELECTION_DIALOG });
    }
}

export function hideLogoSelectionDialog() {
    return (dispatch) => {
        dispatch({ type: HIDE_LOGO_SELECTION_DIALOG });
        dispatch({ type: RESET_PROFILE });
    }
}

export function showFontSelectionArea(fontType) {
    return (dispatch) => {
        dispatch({ type: SHOW_FONT_SELECTION_AREA, payload: fontType });
    }
}

export function hideFontSelectionArea() {
    return (dispatch) => {
        dispatch({ type: SHOW_FONT_SELECTION_AREA, payload: null });
    }
}
