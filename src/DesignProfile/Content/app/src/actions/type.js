import request from 'superagent';

export const GET_INDUSTRIES = 'GET_INDUSTRIES';
export const GET_FONTS = 'GET_FONTS';
export const GET_COLORS = 'GET_COLORS';

const API_URL = '/api/';


// Type exports
export function getIndustries() {
    return function(dispatch) {
        request.get(`${API_URL}Industry`).then(response => {
            dispatch({
                type: GET_INDUSTRIES,
                payload: response
            });
        });
    }
}

export function getFonts(card) {
    return function(dispatch) {
        request.get(`${API_URL}Font`).then(response => {
            dispatch({
                type: GET_FONTS,
                payload: response
            });
        });
    }
}
