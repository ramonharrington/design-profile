import { combineReducers } from 'redux';
import TypeReducer from './type';
import AuthReducer from './auth';
import AccountReducer from './account';
import ProfileReducer from './profile';
import NavigationReducer from './navigation';

const rootReducer = combineReducers({
  type: TypeReducer,
  auth: AuthReducer,
  account: AccountReducer,
  profile: ProfileReducer,
  navigation: NavigationReducer
});

export default rootReducer;