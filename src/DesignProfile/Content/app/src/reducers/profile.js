import { UPDATE_PROFILE, UPDATE_PROFILE_TMP, RESET_PROFILE, GET_PREVIEWS, UPDATE_PREVIEW, GET_PORTFOLIO_IMAGES, POLL_LOGO_UPLOAD, DOCUMENT_ADDED_TO_PORTFOLIO, GET_INSTAGRAM_IMAGES, GET_SCENE } from '../actions/profile';
import $ from 'jquery';

const initialState =  {
    previewsGenerated: null,
    lastUploadCompletionCheck: null,
    loadingLogo: null
}

export default function profiles(state = initialState, action) {
    let previews = state.previews;

    switch (action.type) {
        case UPDATE_PROFILE:
            var originalProfile = $.extend(true, {}, action.payload);
            return {
                ...state, profile: action.payload, originalProfile: originalProfile, lastUpdate: new Date().getTime() / 1000, profileModified: false
            }
        case UPDATE_PROFILE_TMP:
            return {
                ...state, profile: action.payload, lastUpdate: new Date().getTime() / 1000, profileModified: true
            }
        case GET_PREVIEWS:
            return {
                ...state, previews: action.payload.body, previewsGenerated: new Date().getTime() / 1000
            }
        case UPDATE_PREVIEW:
            if (previews !== null) {
                let preview = previews.find(p => { return p.Key === action.payload.Key });
                if (typeof preview !== 'undefined' && preview !== null) {
                    if (action.payload.Error === true) {
                        preview.PreviewData = null;
                        preview.PlaceholderUrl = null;
                    } else {
                        preview.PreviewData = action.payload.PreviewData;
                        preview.EditUrl = action.payload.EditUrl;
                        preview.AltDocId = action.payload.AltDocId;
                        preview.ComboId = action.payload.ComboId;
                        preview.ProductId = action.payload.ProductId;
                        preview.Settings = action.payload.Settings;
                        preview.FormattedPrice = action.payload.FormattedPrice;
                        preview.AvailableQuantities = action.payload.AvailableQuantities;
                        preview.AvailableSizes = action.payload.AvailableSizes;
                    }
                }
            }

            return {
                ...state, previews: previews, previewsGenerated: new Date().getTime() / 1000
            }
        case GET_PORTFOLIO_IMAGES:
            return {
                ...state, portfolioImages: action.payload
            }
        case GET_INSTAGRAM_IMAGES:
        return {
            ...state, portfolioImages: action.payload.images, instagramToken: action.payload.token
        }
        case RESET_PROFILE:
            var profile = $.extend(true, {}, state.originalProfile);
            return {
                ...state, profile: profile, profileModified: false
            }
        case POLL_LOGO_UPLOAD:
            return {
                ...state, loadingLogo: action.payload, lastUploadCompletionCheck: new Date().getTime() / 1000
            }
        case DOCUMENT_ADDED_TO_PORTFOLIO:
            if (previews !== null) {
                let preview = previews.find(p => { return p.AltDocId === action.payload.AltDocId });
                if (preview !== null) {
                    preview.IsInPortfolio = true;
                }
            }

            return {
                ...state, previews: previews, previewsGenerated: new Date().getTime() / 1000
            }
        case GET_SCENE:
            return {
                ...state, scenes: action.payload.body
            }
    default:
            return state;
    }
}
