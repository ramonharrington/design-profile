import { GET_ACCOUNT } from '../actions/auth';

const initialState =  {
    businesses: [],
    profiles: []
};

export default function profiles(state = initialState, action) {
    switch (action.type) {
        case GET_ACCOUNT:
            return {
                ...state, account: action.payload.body
            };
        default:
            return state;
    }
}