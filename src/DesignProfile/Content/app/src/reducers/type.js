import { GET_INDUSTRIES, GET_FONTS, GET_COLORS } from '../actions/type';

export default function types(state = {}, action) {
    switch (action.type) {
        case GET_INDUSTRIES:
            let industries = [];
            for( let i in action.payload.body ) {
                industries.push({ Id: action.payload.body[i].Id, Name: action.payload.body[i].Name });
            }
            return {
                ...state, industries: industries
            };
        case GET_FONTS:
            let fonts = [];
            for( let i in action.payload.body ) {
                fonts.push({ Id: action.payload.body[i].Id, Name: action.payload.body[i].Name, SpritePosition: action.payload.body[i].SpritePosition });
            }
            return {
                ...state, fonts: fonts
            };
        case GET_COLORS:
            let colors = [];
            for( let i in action.payload.body ) {
                colors.push(action.payload.body[i]);
            }
            return {
                ...state, fonts: colors
            };
        default:
            return state;
    }
}