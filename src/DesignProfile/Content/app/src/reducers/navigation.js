import {  MINIMIZE_ALL_PROFILE_DETAILS, EXPAND_BUSINESS_PROFILE_DETAILS, MINIMIZE_BUSINESS_PROFILE_DETAILS, EXPAND_COLOR_PROFILE_DETAILS, MINIMIZE_COLOR_PROFILE_DETAILS, EXPAND_FONT_PROFILE_DETAILS, MINIMIZE_FONT_PROFILE_DETAILS, EXPAND_IMAGE_PROFILE_DETAILS, MINIMIZE_IMAGE_PROFILE_DETAILS, DISPLAY_COLOR_PICKER, HIDE_COLOR_PICKER, DISPLAY_LOGO_SELECTION_DIALOG, HIDE_LOGO_SELECTION_DIALOG, SHOW_FONT_SELECTION_AREA } from '../actions/navigation';

const initialState =  {
    expandBusinessProfileDetails: false,
    expandColorProfileDetails: false,
    expandFontProfileDetails: false,
    expandImageProfileDetails: false,
    logoSelectionDialogVisible: false
};

export default function profiles(state = initialState, action) {
    switch (action.type) {
        case MINIMIZE_ALL_PROFILE_DETAILS:
            return {
                ...state, expandBusinessProfileDetails: false, expandColorProfileDetails: false, expandFontProfileDetails: false, expandImageProfileDetails: false, logoSelectionDialogVisible: false
            }
        case EXPAND_BUSINESS_PROFILE_DETAILS:
            return {
                ...state, expandBusinessProfileDetails: true, expandColorProfileDetails: false, expandFontProfileDetails: false, expandImageProfileDetails: false, logoSelectionDialogVisible: false
            }
        case MINIMIZE_BUSINESS_PROFILE_DETAILS:
            return {
                ...state, expandBusinessProfileDetails: false, expandColorProfileDetails: false, expandFontProfileDetails: false, expandImageProfileDetails: false, logoSelectionDialogVisible: false
            }
        case EXPAND_COLOR_PROFILE_DETAILS:
            return {
                ...state, expandBusinessProfileDetails: false, expandColorProfileDetails: true, expandFontProfileDetails: false, expandImageProfileDetails: false, logoSelectionDialogVisible: false
            }
        case MINIMIZE_COLOR_PROFILE_DETAILS:
            return {
                ...state, expandBusinessProfileDetails: false, expandColorProfileDetails: false, expandFontProfileDetails: false, expandImageProfileDetails: false, logoSelectionDialogVisible: false
            }
        case EXPAND_FONT_PROFILE_DETAILS:
            return {
                ...state, expandBusinessProfileDetails: false, expandColorProfileDetails: false, expandFontProfileDetails: true, expandImageProfileDetails: false, logoSelectionDialogVisible: false
            }
        case MINIMIZE_FONT_PROFILE_DETAILS:
            return {
                ...state, expandBusinessProfileDetails: false, expandColorProfileDetails: false, expandFontProfileDetails: false, expandImageProfileDetails: false, logoSelectionDialogVisible: false
            }
        case EXPAND_IMAGE_PROFILE_DETAILS:
            return {
                ...state, expandBusinessProfileDetails: false, expandColorProfileDetails: false, expandFontProfileDetails: false, expandImageProfileDetails: true, logoSelectionDialogVisible: false
            }
        case MINIMIZE_IMAGE_PROFILE_DETAILS:
            return {
                ...state, expandBusinessProfileDetails: false, expandColorProfileDetails: false, expandFontProfileDetails: false, expandImageProfileDetails: false, logoSelectionDialogVisible: false
            }
        case DISPLAY_COLOR_PICKER:
            return {
                ...state, colorPickerAttachedElement: action.payload
            }
        case HIDE_COLOR_PICKER:
            return {
                ...state, colorPickerAttachedElement: null
            }
        case DISPLAY_LOGO_SELECTION_DIALOG:
            return {
                ...state, logoSelectionDialogVisible: true
            }
        case HIDE_LOGO_SELECTION_DIALOG:
            return {
                ...state, logoSelectionDialogVisible: false
            }
        case SHOW_FONT_SELECTION_AREA:
            return {
                ...state, fontSelectionArea: action.payload
            }
        default:
            return state;
    }
}