import $ from 'jquery';

var ip_address = null;

function tracker(type, value, channel) {
    if(ip_address === null){
    $.get('https://api.ipify.org?format=json&callback=?', function(response){
        ip_address = response.ip;

        tracker(type,value,channel);
    });

    return;
    }

    if(typeof window.HATCHERY_FINGERPRINT === 'undefined'){
    setTimeout(function(){
        tracker(type,value,channel);
    }, 1000);

    return;
    }

    var data = {
        event_type: type,
        event_value: value,
        page_url: window.location.toString(),
        channel: channel,
        user_id_type: 'browser_fingerprint',
        user_id: window.HATCHERY_FINGERPRINT,
        ip_address_created: ip_address,
        referrer: document.referrer,
        session_id: window.sessionId,
        user_agent: navigator.userAgent
    };

    $.ajax({
        url: '/api/tracker',
        accepts: 'application/json',
        data: JSON.stringify(data),
        dataType: 'json',
        contentType: 'application/json',
        method: 'POST',
        error: function (){}
    });
}

export { tracker };