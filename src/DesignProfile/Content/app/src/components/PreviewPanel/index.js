import React from 'react';
import _ from 'lodash';

import PreviewItem from '../PreviewItem';
import './PreviewPanel.sass';

var Masonry = require('react-masonry-component');

let masonryOptions = {
    transitionDuration: 0,
    columnWidth: '.preview-item',
    itemSelector: '.preview-item',
    gutter: 10,
    percentPosition: true,
    fitWidth: true
}

class PreviewPanel extends React.Component {
    render() {
        let previews = this.props.previews;
        let previewItems = (
        <div>
        </div>);

        if (typeof previews !== 'undefined' && previews !== null) {
            previewItems = previews.map((preview) => {
                return <PreviewItem key={preview.Key} profile={this.props.profile} preview={_.clone(preview)} actions={this.props.actions} handlePreviewClick={this.props.handlePreviewClick}/>;
            });
        }

        return (
            <div id="preview-panel" className="preview-panel">
                <Masonry className="preview-gallery" options={masonryOptions}>
                    {previewItems}
                </Masonry>
            </div>
        );
    }

}

export default PreviewPanel;
