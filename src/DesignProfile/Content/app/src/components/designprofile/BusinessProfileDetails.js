import React from 'react';
import _ from 'lodash';

import IndustrySelect from './IndustrySelect';

import '../styles/app.css';

class BusinessProfileDetails extends React.Component {
    render() {
        let business = _.get(this.props.profile, 'business', null);
        if (business === null) {
            return <div></div>;
        }

        if (business.industryId !== null) {
            business.IndustryId1 = parseInt(business.industryId, 10);
        }

        if (this.props.expandBusinessProfileDetails === true) {
            return this.renderEdit(business);
        } else {
            return this.renderView(business);
        }
    }

    renderView(business) {
        let employeeName = _.get(business, 'EmployeeName', null);
        if (employeeName) {
            let jobTitle = _.get(business, 'JobTitle', '');
            if (jobTitle) {
                employeeName += ', ' + jobTitle;
            }
        }

        return (
            <div>
                <div className="row">
                    <div className="ten columns panel-section-title">Company Information</div>
                    <div className="two columns panel-edit-link">
                        <div onClick={this.props.actions.expandBusinessProfileDetails}>EDIT</div>
                    </div>
                </div>
                <div className="row">
                    <div className="six columns">
                        <div className="business-details">
                            <div className="title">{business.name}</div>
                            <div className="tagline">{business.tagline}</div>
                            <div>{business.employeeName}</div>
                            <div></div>
                            <div>{business.email}</div>
                            <div>{business.phone}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderEdit(business) {
        let buttonClass = 'button-primary';
        if (!this.props.profileModified) {
            buttonClass = 'button-primary-disabled';
        }

        return (
            <div>
                <form>
                    <div className="row">
                        <div className="ten columns panel-section-title">Company Information</div>
                        <div className="two columns panel-edit-link" onClick={this.props.actions.minimizeBusinessProfileDetails}>CANCEL</div>
                    </div>
                    <div className="row">
                        <input id="businessName" ref="businessName" className="u-full-width" type="text" placeholder="Company Name" defaultValue={business.name} onChange={this.updateBusiness.bind(this)} /><br />
                        <input id="tagline" ref="tagline" className="u-full-width" type="text" placeholder="Tagline" defaultValue={business.tagline} onChange={this.updateBusiness.bind(this)} /><br />
                        <IndustrySelect ref="industry" industries={this.props.industries} selectedIndustry={_.get(business, 'industryId', '-1')} updateIndustry={this.updateIndustry.bind(this)} />
                        <input id="employeeName" ref="employeeName" className="u-full-width" type="text" placeholder="Your Name" defaultValue={business.employeeName} onChange={this.updateBusiness.bind(this)} /><br />
                        <input id="jobTitle" ref="jobTitle" className="u-full-width" type="text" placeholder="Job Title" defaultValue={business.jobTitle} onChange={this.updateBusiness.bind(this)} /><br />
                        <input id="email" ref="email" className="u-full-width" type="text" placeholder="Email Address" defaultValue={business.email} onChange={this.updateBusiness.bind(this)} /><br />
                        <input id="phone" ref="phone" className="u-full-width" type="text" placeholder="Phone #" defaultValue={business.phone} onChange={this.updateBusiness.bind(this)} /><br />
                    </div>
                    <input id="url" ref="url" className="u-full-width" type="text" placeholder="Web Address" defaultValue={business.url} onChange={this.updateBusiness.bind(this)} /><br />
                    <input type="button" className={buttonClass} value="Save Info" onClick={this.persistBusiness.bind(this)} />
                </form>
        </div>
        );
    }

    updateIndustry(industryId) {
        let profile = this.props.profile;
        profile.business.industryId = industryId;
        this.props.actions.updateProfile(profile);
    }

    updateBusiness() {
        let profile = this.props.profile;
        profile.business.name = this.refs.businessName.value;
        profile.business.tagline = this.refs.tagline.value;
        profile.business.employeeName = this.refs.employeeName.value;
        profile.business.jobTitle = this.refs.jobTitle.value;
        profile.business.email = this.refs.email.value;
        profile.business.phone = this.refs.phone.value;
        profile.business.url = this.refs.url.value;

        this.props.actions.updateProfile(profile);
    }

    persistBusiness() {
        if (this.props.profileModified) {
            this.props.actions.persistBusiness(this.props.profile);
            this.props.actions.minimizeBusinessProfileDetails();
        }
    }
}

export default BusinessProfileDetails;
