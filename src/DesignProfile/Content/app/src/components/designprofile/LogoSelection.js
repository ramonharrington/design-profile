import React from 'react';
import Dropzone from 'react-dropzone';
import _ from 'lodash';

class LogoSelection extends React.Component {
    componentWillMount() {
        this.props.actions.getPortfolioImages(this.props.profile.id);
        //this.props.actions.getInstagramImages(this.props.instagramToken);
    }

    render() {
        let portfolioGallery = null;
        if (_.get(this.props, 'portfolioImages', null) !== null) {
            portfolioGallery = (
                <div>
                    { this.props.portfolioImages.map((i) => {
                        return (
                            <div className='image-box-outer' key={i.ImageId}>
                                <div className='portfolio-image-box' onClick={this.selectPortfolioImage.bind(this)}>
                                    <img className='portfolio-image' id={i.ImageId} src={i.ImagePreview} alt='Logo' />
                                </div>
                            </div>
                        )})
                    }
                </div>
            );
        }

        return (
            <div className='modal-contents'>
                <div className="panel-title">
                    Select your logo
                </div>
                <div className='logo-modal-contents-action-area'>
                    <Dropzone onDrop={this.onDrop.bind(this)} accept=".jpeg,.jpg,.png" className='add-logo-drop-zone'>
                        <div className='add-image-box-text'>Drag your file here or click to upload</div>
                    </Dropzone>
                </div>
                <div>
                    or choose an image from your account<br /><br />
                </div>
                <div>
                    { portfolioGallery }
                </div>
                <div>
                    <input type='button' value='Cancel' onClick={this.closeModal.bind(this)} />
                </div>
            </div>
        );
    }

    onDrop(acceptedFiles, rejectedFiles) {
        if (this.props != null && this.props.actions != null) {
            this.props.actions.uploadLogo(this.props.profile, acceptedFiles);
        }
        this.props.actions.hideLogoSelectionDialog();
    }

    selectPortfolioImage(event) {
        this.props.actions.selectLogo(this.props.profile, event.target.id);
        this.props.actions.hideLogoSelectionDialog();
    }

    closeModal() {
        this.props.actions.hideLogoSelectionDialog();
    }
}

export default LogoSelection;
