import React from 'react';
import '../styles/app.css';

class IndustrySelect extends React.Component {
    render() {
        let industries;
        if (typeof this.props.industries !== 'undefined' && this.props.industries !== null) {
            industries = this.props.industries.map((industry) => {
                return <option key={industry.Id} value={industry.Id}>{industry.Name}</option>;
            });
        }

        return (
            <div>
                <select className="u-full-width" id="industry" defaultValue={this.props.selectedIndustry} onChange={this.updateIndustry.bind(this)}>
                    {industries}
                </select>
            </div>
        );
    }

    updateIndustry(event) {
        this.props.updateIndustry(event.target.value);
    }
}

export default IndustrySelect;
