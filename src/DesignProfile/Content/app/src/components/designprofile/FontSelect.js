import React from 'react';
import _ from 'lodash';
import FontAwesome from 'react-fontawesome';

import '../styles/app.css';

class FontSelect extends React.Component {
    render() {
        let selectedFont = _.get(this.props.profile, 'settings.fonts[' + this.props.fontType + ']', '');

        if (this.props.fontSelectionArea !== this.props.fontType) {
            let font = this.props.fonts.find(f => { return f.Name === selectedFont });

            if (typeof font !== 'undefined' && font !== null) {
                return (
                    <div className='font-selected' key={font.Id} style={{background: `url('/content/images/fontsprite.png') 0px -${font.SpritePosition}px`}} onClick={this.showFontSelectionArea.bind(this)}>
                        <FontAwesome name='caret-down' style={{paddingRight: "4px"}} />
                    </div>
                );
            } else {
                return (
                    <div className='font-selected' onClick={this.showFontSelectionArea.bind(this)}>Select</div>
                );
            }
        }

        let fontDivSet = this.getFontSelectionArea();

        return (
            <div>
                {fontDivSet}
            </div>
        );
    }

    updateFont(event) {
        let fontName = event.target.attributes.getNamedItem('data-font-name').value;

        let profile = this.props.profile;
        profile.settings.fonts[this.props.fontType] = fontName;
        this.props.actions.updateProfile(profile);
        this.props.actions.hideFontSelectionArea();
    }

    showFontSelectionArea(event) {
        this.props.actions.showFontSelectionArea(this.props.fontType)
    }

    getFontSelectionArea() {
        let fontDivSet = null;
        if (typeof this.props.fonts !== 'undefined' && this.props.fonts !== null) {
            fontDivSet = this.props.fonts.map((font) => {
                return (
                    <div className='font-div' key={font.Id} data-font-name={font.Name} style={{background: `url('/content/images/fontsprite.png') 0px -${font.SpritePosition}px`}} onClick={this.updateFont.bind(this)}></div>
                );
            });
        }

        return (
            <div className='font-list'>
                { fontDivSet }
            </div>
        );
    }
}

export default FontSelect;
