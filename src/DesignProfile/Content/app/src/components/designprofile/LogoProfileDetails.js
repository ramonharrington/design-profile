import React from 'react';
import Modal from 'react-modal';
import _ from 'lodash';
import { MoonLoader } from 'react-spinners';

import LogoSelection from '../components/LogoSelection';

import '../styles/app.css';

class LogoProfileDetails extends React.Component {
    render() {
        return (
            <div>
                <div className="row panel-section-title">
                    Your Logo
                </div>
                <div className="row">
                    { this.getLogo(this.props.profile) }
                </div>
                <Modal isOpen={this.props.logoSelectionDialogVisible} className='logo-selection-modal' contentLabel="Modal">
                    <LogoSelection profile={this.props.profile} portfolioImages={this.props.portfolioImages} actions={this.props.actions} />
                </Modal>
            </div>
        );
    }

    componentDidUpdate() {
        if (this.props.loadingLogo !== null) {
            setTimeout(function() { this.updatePreviewsIfLogoUploadIsComplete() }.bind(this), 1000);
        }
    }

    getLogo(profile) {
        let logo = _.get(this.props, 'profile.settings.images.logo', null);
        if (logo === null) {
            return <div className='logo-thumbnail-blank' onClick={this.displayLogoSelectionDialog.bind(this)}>&nbsp;</div>
        }

        return (
            <div>
                <MoonLoader color={'#123abc'} loading={this.props.loadingLogo !== null}/>
                <div className='logo-thumbnail' onClick={this.displayLogoSelectionDialog.bind(this)} style={{backgroundImage: 'url(' + logo.url + ')'}}></div>
            </div>
        );
    }

    displayLogoSelectionDialog() {
        if (this.props.loadingLogo === null) {
            this.props.actions.displayLogoSelectionDialog();
        }
    }

    updatePreviewsIfLogoUploadIsComplete() {
        this.props.actions.updatePreviewsIfLogoUploadIsComplete(this.props.profile, this.props.loadingLogo);
    }
}

export default LogoProfileDetails;
