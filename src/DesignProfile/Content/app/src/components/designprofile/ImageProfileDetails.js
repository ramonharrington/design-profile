import React from 'react';
import _ from 'lodash';

import '../styles/app.css';

import ImageElement from './ImageElement';

class ImageProfileDetails extends React.Component {
    render() {
        let imageArray = this.getImageArray();
        let text = this.props.expandImageProfileDetails ? "CANCEL" : "EDIT";
        let action = this.props.expandImageProfileDetails ? this.props.actions.minimizeImageProfileDetails : this.props.actions.expandImageProfileDetails;

        if (this.props.expandImageProfileDetails) {
            imageArray.push({"id": null, imageType: null, url: null})
        }
        
        return (
            <div>
                <div className="row">
                    <div className="ten columns panel-section-title">Images</div>
                    <div className="two columns panel-edit-link" onClick={action}>{text}</div>
                </div>
                <div className="row">
                    { imageArray.map((i) => { return <ImageElement key={i.id} id={i.id} url={i.url} profile={this.props.profile} edit={this.props.expandImageProfileDetails} actions={this.props.actions} /> }) }
                </div>
        </div>
        );
    }

    getImageArray() {
        let images = _.get(this.props, 'profile.settings.images', {});
        let logoId = _.get(this.props, 'profile.settings.images.logo.id', 0);

        let pictures = [];
        if (typeof images.pictures !== 'undefined' && images.pictures !== null) {
            pictures = images.pictures.filter(p => p.id !== logoId).map((i) => { return({ "id": i.id, "imageType": "picture", "url": i.url }) });
        }

        return pictures;
    }
}

export default ImageProfileDetails;
