import React from 'react';
import Dropzone from 'react-dropzone';

import '../styles/app.css';

class ImageElement extends React.Component {
    render() {
        if (this.props.edit === false) {
            return <img className="profile-image-view" src={this.props.url} alt='Profile' />;
        }

        if (this.props.url === null) {
            return (
                <Dropzone onDrop={this.onDrop.bind(this)} accept=".jpeg,.jpg,.png" className='add-image-drop-zone'>
                    <div className='add-image-box-text'>+ ADD PHOTO</div>
                </Dropzone>
            );
        }

        return (
            <div className='image-box-outer'>
                <div className='image-box'><img className="profile-image" src={this.props.url} alt='Profile' /></div>
                <div className='image-link-row'><span className='image-link' onClick={this.removeImage.bind(this, this.props.id)}>Remove</span></div>
            </div>
        );
    }

    removeImage(imageId, event) {
        if (this.props != null && this.props.actions != null) {
            this.props.actions.deleteImage(this.props.profile, imageId);
        }
    }

    onDrop(acceptedFiles, rejectedFiles) {
        if (this.props != null && this.props.actions != null) {
            this.props.actions.uploadPictures(this.props.profile, acceptedFiles);
        }
    }
}

export default ImageElement;
