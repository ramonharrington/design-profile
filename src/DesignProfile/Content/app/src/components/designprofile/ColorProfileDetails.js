import React from 'react';
import _ from 'lodash';

import '../styles/app.css';

import ColorSwatch from './ColorSwatch';

class ColorProfileDetails extends React.Component {
    render() {
        if (this.props.expandColorProfileDetails === true) {
            return this.renderEdit();
        } else {
            return this.renderView();
        }
    }

    renderView() {
        let colors = _.get(this.props.profile, 'settings.colors', null);
        let colorArray = [];
        if (colors !== null && typeof colors.primary !== 'undefined' && colors.primary !== null && colors.primary.color !== null) {
            colorArray.push({ "priority": 'primary', "color": colors.primary });
            if (typeof colors.secondary !== 'undefined' && colors.secondary !== null) {
                colorArray.push({ "priority": 'secondary', "color": colors.secondary });
                if (typeof colors.tertiary !== 'undefined' && colors.tertiary !== null) {
                    colorArray.push({ "priority": 'tertiary', "color": colors.tertiary });
                }
            }
        }

        return (
            <div>
                <div className="row">
                    <div className="ten columns">
                        <div className="panel-section-title">Color Palette</div>
                    </div>
                    <div className="two columns">
                        <div className="one column panel-edit-link">
                            <div onClick={this.props.actions.expandColorProfileDetails}>EDIT</div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    { colorArray.map((c) => { return <div key={c.priority} className='color-swatch-thumbnail' style={{backgroundColor: c.color}} >&nbsp;</div> }) }
                </div>
            </div>
        );
    }

    renderEdit() {
        let buttonClass = 'button-primary';
        if (!this.props.profileModified) {
            buttonClass = 'button-primary-disabled';
        }
        let colorProfileContent = <div></div>;
        let sectionEnd = <div></div>;

        let colors = _.get(this.props.profile, 'settings.colors', null);

        if (colors == null || colors.primary === null) {
            colorProfileContent = this.renderNewPalette();
        } else {
            colorProfileContent = this.renderExisting();
            sectionEnd = (
                <div className="row">
                    <input type="button" className={buttonClass} value="Save Palette" onClick={this.persistProfileSettings.bind(this)} />
                </div>
            );
        }

        return (
            <div>
                <form>
                    <div className="row">
                        <div className="ten columns panel-section-title">Color Palette</div>
                        <div className="two columns panel-edit-link" onClick={this.props.actions.minimizeColorProfileDetails}>CANCEL</div>
                    </div>
                    <div className="row">
                        { colorProfileContent }
                    </div>
                    { sectionEnd }
                </form>
        </div>
        );
    }

    renderNewPalette() {
        if (_.get(this.props.profile, 'settings.images.logo', null) === null) {
            return (
                <div className="color-palette-box">
                    <div className='panel-edit-link' onClick={this.createCustomPalette.bind(this)}>Create Custom Palette</div>
                </div>
            );
        } else {
            return (
                <div className="color-palette-box">
                    <div className="panel-prefix centered">Recommended:<br /><br /></div>
                    <input type="button" className="button-primary" value="Create Palette from Logo" onClick={this.createColorPaletteFromLogo.bind(this)} />
                    <hr className="panel-separator" />
                    or <div className='panel-edit-link' onClick={this.createCustomPalette.bind(this)}>Create Custom Palette</div>
                </div>
            );
        }
    }

    renderExisting() {
        let colors = _.get(this.props.profile, 'settings.colors', null);

        if (colors === null) {
            return <div></div>;
        }

        let colorArray = [];
        if (typeof colors.primary !== 'undefined' && colors.primary !== null && colors.primary.color !== null) {
            colorArray.push({ "priority": 'primary', "color": colors.primary });
            if (typeof colors.secondary !== 'undefined' && colors.secondary !== null) {
                colorArray.push({ "priority": 'secondary', "color": colors.secondary });
                if (typeof colors.tertiary !== 'undefined' && colors.tertiary !== null) {
                    colorArray.push({ "priority": 'tertiary', "color": colors.tertiary });
                } else { colorArray.push({ "priority": 'tertiary', "color": null }); }
            } else { colorArray.push({ "priority": 'secondary', "color": null }); }
        } else { colorArray.push({ "priority": 'primary', "color": null }); }

        return (
            <div>
                { colorArray.map((c) => { return <ColorSwatch key={c.priority} actions={this.props.actions} profile={this.props.profile} priority={c.priority} color={c.color} renderColorPicker={this.props.colorPickerAttachedElement === c.priority} /> }) }
            </div>
        );
    }

    createColorPaletteFromLogo() {
        this.props.actions.createColorPaletteFromLogo(this.props.profile.id);
    }

    createCustomPalette() {
        let profile = this.props.profile;
        if (typeof profile.settings.colors === 'undefined') {
            profile.settings.colors = {};
        }
        profile.settings.colors["primary"] = { "priority": "primary", "color": null };
        this.props.actions.updateProfile(profile);
    }

    persistProfileSettings() {
        if (this.props.profileModified) {
            this.props.actions.persistProfileSettings(this.props.profile);
            this.props.actions.minimizeColorProfileDetails();
        }
    }
}

export default ColorProfileDetails;
