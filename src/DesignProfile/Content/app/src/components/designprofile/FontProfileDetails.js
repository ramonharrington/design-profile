import React from 'react';
import _ from 'lodash';
import FontSelect from './FontSelect';

import '../styles/app.css';

class FontProfileDetails extends React.Component {
    render() {
        if (this.props.expandFontProfileDetails === true) {
            return this.renderEdit();
        } else {
            return this.renderView();
        }
    }

    renderView() {
        let fonts = _.get(this.props.profile, 'settings.fonts', null);
        let fontElement = null;

        if (_.get(this.props, 'fonts', null) === null) {
            let fontArray = [];

            if (fonts !== null) {
                if (fonts.main !== null) {
                    fontArray.push(fonts.main);
                }
                if (fonts.secondary !== null) {
                    fontArray.push(fonts.secondary);
                }
            }
            fontElement = <div className='font-summary'>{ fontArray.join(', ') }</div>;
        } else if (typeof fonts !== 'undefined' && fonts !== null) {
            let mainFont = this.props.fonts.find(f => { return f.Name === fonts.main });
            let secondaryFont = this.props.fonts.find(f => { return f.Name === fonts.secondary });
            let mainFontElement = null;
            let secondaryFontElement = null;

            if (typeof mainFont !== 'undefined' && mainFont !== null) {
                mainFontElement = <div className='font-view' key={'main_' + mainFont.Id} style={{background: `url('/content/images/fontsprite.png') 0px -${mainFont.SpritePosition}px`}}></div>;
            }
            if (typeof secondaryFont !== 'undefined' && secondaryFont !== null) {
                secondaryFontElement = <div className='font-view' key={'secondary_' + secondaryFont.Id} style={{background: `url('/content/images/fontsprite.png') 0px -${secondaryFont.SpritePosition}px`}}></div>;
            }

            fontElement = (
                <div className='font-summary'>
                    { mainFontElement }
                    { secondaryFontElement }
                </div>
            );
        }

        return (
            <div>
                <div className="row">
                    <div className="ten columns">
                        <div className="panel-section-title">Fonts</div>
                    </div>
                    <div className="two columns">
                        <div className="one column panel-edit-link">
                            <div onClick={this.props.actions.expandFontProfileDetails}>EDIT</div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    { fontElement }
                </div>
            </div>
        );
        
    }

    renderEdit() {
        let buttonClass = 'button-primary';
        if (!this.props.profileModified) {
            buttonClass = 'button-primary-disabled';
        }
        return (
            <div>
                <form>
                    <div className="row">
                        <div className="ten columns panel-section-title">Fonts</div>
                        <div className="two columns panel-edit-link" onClick={this.props.actions.minimizeFontProfileDetails}>CANCEL</div>
                    </div>
                    <div className="row">
                        <div className="ten columns font-selection-label">Main Font</div>
                    </div>
                    <div className="row">
                        <div className="ten columns font-selection-label">
                            <FontSelect fontType="main" fonts={this.props.fonts} actions={this.props.actions} profile={this.props.profile} fontSelectionArea={this.props.fontSelectionArea} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="ten columns font-selection-label">Secondary Font</div>
                    </div>
                    <div className="row">
                        <div className="ten columns font-selection-label">
                            <FontSelect fontType="secondary" fonts={this.props.fonts} actions={this.props.actions} profile={this.props.profile} fontSelectionArea={this.props.fontSelectionArea} />
                        </div>
                    </div>
                    <div className="row">
                        <input type="button" className={buttonClass} onClick={this.persistProfileSettings.bind(this)} value="Save Info" />
                    </div>
                </form>
        </div>
        );
    }

    persistProfileSettings() {
        if (this.props.profileModified) {
            this.props.actions.persistProfileSettings(this.props.profile);
            this.props.actions.minimizeFontProfileDetails();
        }
    }
}

export default FontProfileDetails;
