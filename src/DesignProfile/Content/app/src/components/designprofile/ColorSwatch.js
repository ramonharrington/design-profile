import React from 'react';
import { SwatchesPicker } from 'react-color'

import '../styles/app.css';

class ColorSwatch extends React.Component {
    render() {
        let colorPicker = <div></div>;

        if (this.props.renderColorPicker) {
            colorPicker = (
                <div className='react-color-swatch'>
                    <SwatchesPicker color='#ffffff' onChange={this.updateColor.bind(this)} />
                </div>
            );

        }

        if (this.props.color === null || this.props.color === {}) {
            return (
                <div>
                    <div className='color-swatch' style={{backgroundColor: '#fff'}} onClick={this.showColorPicker.bind(this)}>+</div>
                    {colorPicker}
                </div>
            );
        }

        return (
            <div>
                <div className='color-swatch' style={{backgroundColor: this.props.color}} onClick={this.showColorPicker.bind(this)}>&nbsp;</div>
                {colorPicker}
            </div>
        );
    }

    showColorPicker() {
        this.props.actions.displayColorPicker(this.props.priority);
    }

    updateColor(color, event) {
        let profile = this.props.profile;

        if (profile !== null && typeof profile.settings !== 'undefined' && profile.settings !== null) {
            profile.settings.colors[this.props.priority] = color.hex;
            this.props.actions.updateProfile(profile);
            this.props.actions.hideColorPicker();
        }
    }
}

export default ColorSwatch;
