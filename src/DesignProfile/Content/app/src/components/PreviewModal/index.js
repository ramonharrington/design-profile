import React from 'react';
import _ from 'lodash';

import './PreviewModal.sass';

class PreviewModal extends React.Component {
  constructor() {
    super();
    this.state = {
      cartItems: {},
      successCart: false,
      successPortfolio: false
    }
    this.buildSizeAndQuantitySelectorGroup = this.buildSizeAndQuantitySelectorGroup.bind(this);
  }

  handleQuantityUpdate(event, product) {
    this.updateCartItem(event.target.value, product, this.props.modalPreview.AltDocId)
  }

  updateCartItem(quantity, product, document){
    let stateClone;
    if(quantity === "0"){
      stateClone = this.state.cartItems;
      delete stateClone[product];
      this.setState({cartItems: stateClone});
      return;
    }
    var cartItem = {"AltDocId": document, "Quantity": quantity, "Product": product}
    stateClone = this.state.cartItems;
    stateClone[product] = cartItem;
    this.setState({cartItems: stateClone});
  }

  componentDidMount() {
    if(_.isEmpty(this.props.modalPreview.AvailableSizes)){
      this.updateCartItem(this.props.modalPreview.AvailableQuantities[0], this.props.modalPreview.ProductId, this.props.modalPreview.AltDocId);
    }
  }

  addToPortfolio(event) {
    this.props.actions.addToPortfolio(this.props.profileId, this.props.modalPreview);
    this.setState({successPortfolio: true})
    if (typeof window.environment !== 'undefined') {
      this.props.actions.publishToSlack(this.props.profileId, this.props.modalPreview.PreviewUrl, "portfolio", window.environment);
    }
  }

  addToCart(event) {
    if (_.isEmpty(this.state.cartItems)) {
      return; //something went wrong
    }
    var clonedState = this.state.cartItems;
    var vals = Object.keys(clonedState).map(function (key) {
        return clonedState[key];
      });
    this.props.actions.addToCart(this.props.profileId, vals, this.props.modalPreview);
    this.setState({successCart: true})
  }

  buildSizeAndQuantitySelectorGroup(){
    var availableQuantities = this.props.modalPreview.AvailableQuantities;
    availableQuantities.unshift(0);
    var sizes = Object.keys(this.props.modalPreview.AvailableSizes);
    return sizes.map((size => {
      return <div key={size} className="size-selector-group"> 
        <div className="size-text">{size}</div>
      <select className="size-quantity-selector" onChange={(evt) => this.handleQuantityUpdate(evt, this.props.modalPreview.AvailableSizes[size])}>
        <option selected disabled>Select a quantity</option>
      { availableQuantities.map((quantity) => {
            return <option key={quantity.toString() + size.toString()} value={quantity}>{quantity}</option>;
          })}
      </select>
      </div>;
    }))
  }

  goToCart(event){
    this.props.actions.trackGenericEvent(this.props.profileId, "gotocart");
    window.parent.postMessage("goToCart", "*");
  }

  continueShopping(event){
    this.props.actions.trackGenericEvent(this.props.profileId, "continueshopping");
    window.parent.postMessage("continueShopping", "*");
    this.props.close();
  }

  render() {
    let selector = null;
    if (_.isEmpty(this.props.modalPreview.AvailableSizes)) { //only dealing with sizes
      selector = <div>
        <h3>Select your quantity</h3>
        <select onChange={(evt) => this.handleQuantityUpdate(evt, this.props.modalPreview.ProductId)}>
          {this.props.modalPreview.AvailableQuantities.map((quantity => {
              return <option key={quantity} value={quantity}>{quantity}</option>;
            }))}
        </select>
      </div>;
    } else {
      selector = this.buildSizeAndQuantitySelectorGroup();
    }

    let customStyle = {};
    if(window.self !== window.top){
      customStyle.top = this.props.topPos+'px';
    }

    let priceString = "";
    if (typeof window.pricingDictionary !== 'undefined' && window.pricingDictionary !== null) {
        priceString = window.pricingDictionary[this.props.modalPreview.ProductId];
    }
    
    let addToPortfolioButton = null;
    if(this.state.successPortfolio){
      addToPortfolioButton = <span className='secondary-action secondary-action-off'>Saved to portfolio</span>
    }
    else{
      addToPortfolioButton = <a className='secondary-action' onClick={this.addToPortfolio.bind(this)}>Save for later</a>
    }

    let body = null;
    if(this.state.successCart){
      body = <div style={customStyle} className="preview-modal-container">
          <div className="sucess-message-container">
            <div className="top">
              <img src='/Content/images/ppage/success.svg' alt='Success'/>
              <h1>Success! That product has been added to your cart.</h1>
            </div>
            <div className="bottom">
              <button className="continue" onClick={this.continueShopping.bind(this)}>Continue shopping</button>
              <button className="go-to-cart" onClick={this.goToCart.bind(this)}>Go to cart</button>
            </div>
          </div>
        </div>
    }
    else{
      body=<div style={customStyle} className="preview-modal-container">
      <div className='close-btn' onClick={this.props.close}>x</div>
      <div className="modal-image-container">
        <img
          ref="previewImage"
          className="modal-preview-image"
          src={this.props.modalPreview.PreviewData}
          width={this.props.modalPreview.Width}

          alt="Product Preview"/>
        </div>
      <div className='right-content'>
        <h1>{this.props.modalPreview.ProductName}</h1>
        <h2 dangerouslySetInnerHTML={{__html: priceString}}></h2>
        {selector}
        <button className='primary-button' onClick={this.addToCart.bind(this)}>Add to cart</button>
        {addToPortfolioButton}
      </div>
    </div>
    }

    return (
      <div className="preview-modal-parent-container">
        <div className='veil' onClick={this.props.close}></div>
        {body}
      </div>
    );
  }
}

export default PreviewModal;
