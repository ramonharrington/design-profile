import React from 'react';
import ReactJson from 'react-json-view';

class ItemMetadata extends React.Component {
    render(){
        return(
            <div className="doc-item" name={this.props.itemKey}>
                <p>Template Item ID: <span className="key">{this.props.itemKey}</span></p>
                <p>Item Type: <span className="key">{this.props.itemType}</span></p>
                <div className="image-container">
                    <img className="item-img" src={this.props.imageUrl} alt='template'/>
                </div>
                <label>
                    <ReactJson src={this.props.itemMetadata} onEdit={(e)=> this.props.updateHandler(this.props.itemKey, e)} theme="monokai"/>
                </label>
            </div>
        );
    }
}
export default ItemMetadata;