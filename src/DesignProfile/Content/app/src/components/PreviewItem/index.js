import React from 'react';

// eslint-disable-next-line
import styles from './PreviewItem.sass';

class PreviewItem extends React.Component {
    render() {
        if (typeof this.props.preview === 'undefined' || this.props.preview === null || (this.props.preview.PlaceholderUrl === null && this.props.preview.PreviewData === null)) {
            return (
                <div></div>
            );
        }

        let previewImage = null;
        if (typeof this.props.preview.PreviewData !== 'undefined' && this.props.preview.PreviewData !== null) {
            previewImage = <img ref="previewImage" className="preview-image" src={this.props.preview.PreviewData} data-height={this.props.preview.Height} data-width={this.props.preview.Width} alt="Product Preview" onClick={this.editDocument.bind(this)} />;
        } else {
            previewImage = <img ref="previewImage" className="preview-image-placeholder" src={this.props.preview.PlaceholderUrl} data-height={this.props.preview.Height} data-width={this.props.preview.Width} alt="Product Preview" onClick={this.editDocument.bind(this)} />;
        }

        let priceString = "";
        if (typeof window.pricingDictionary !== 'undefined' && window.pricingDictionary !== null) {
            priceString = window.pricingDictionary[this.props.preview.ProductId];
        }

        return (
            <div ref="previewItem" className="preview-item-hidden">
                <div className="preview-overlay" onClick={this.openModal.bind(this)}>
                </div>
                { previewImage }
                <div className="preview-item-footer">
                    <div className="preview-item-footer-product">{this.props.preview.ProductName}</div>
                    <div className="preview-item-footer-price" dangerouslySetInnerHTML={{ __html: priceString }}></div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        if (typeof this.refs.previewItem !== 'undefined') {
            this.refs.previewItem.className = "preview-item";
            if (parseInt(this.refs.previewImage.getAttribute('data-width'), 10) !== 0) {
                //var newHeight = $(this.refs.previewImage).width() * parseInt(this.refs.previewImage.getAttribute('data-height'), 10) / parseInt(this.refs.previewImage.getAttribute('data-width'), 10);
                //$(this.refs.previewImage).height(newHeight);
                setTimeout(function() {this.props.actions.resizeFrame()}.bind(this), 100);
            }

            if (this.props.preview.PreviewData === null) {
                this.props.actions.updatePreview(this.props.profile.id, this.props.preview);
            }
        }
    }

    componentDidUpdate() {
        if (this.props.preview.PreviewData === null) {
            this.props.actions.updatePreview(this.props.profile.id, this.props.preview);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.preview.PreviewData !== nextProps.preview.PreviewData) {
            return true;
        }
        if (this.props.preview.PreviewUrl !== nextProps.preview.PreviewUrl) {
            return true;
        }
        if (this.props.preview.PlaceholderUrl !== nextProps.preview.PlaceholderUrl)
        {
            return true;
        }
        return false;
    }

    editDocument() {
        this.props.actions.editDocument(this.props.profile.id, this.props.preview);

        if (typeof window.environment !== 'undefined') {
            this.props.actions.publishToSlack(this.props.profile.id, this.props.preview.PreviewUrl, "studio", window.environment);
        }
    }

    openModal(ele){
        this.props.handlePreviewClick(this.props.preview, ele);
    }

    viewPortfolio(event) {
        this.props.actions.viewPortfolio(this.props.profile.id, this.props.preview);
    }
}

export default PreviewItem;
