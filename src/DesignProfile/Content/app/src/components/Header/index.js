import React from 'react';
import './Header.sass';

class Header extends React.Component {
    render() {
        return (
            <div className='designprofile-header'>
              <h3>Upload your company's logo or icon to see it on a variety of products to promote your brand.</h3>
            </div>
        );
    }

    componentDidUpdate() {
        if (typeof this.props.loadingLogo !== 'undefined' && this.props.loadingLogo !== null && this.props.loadingLogo !== 0) {
            setTimeout(function() { this.updatePreviewsIfLogoUploadIsComplete() }.bind(this), 1000);
        }
    }

    onDrop(acceptedFiles, rejectedFiles) {
        this.uploadLogo(acceptedFiles).bind(this);
        setTimeout(function() { this.updatePreviewsIfLogoUploadIsComplete() }.bind(this), 1000);
    }

    updatePreviewsIfLogoUploadIsComplete() {
        this.props.actions.updatePreviewsIfLogoUploadIsComplete(this.props.profile, this.props.loadingLogo);
    }

    uploadLogo(acceptedFiles) {
        this.props.actions.uploadLogo(this.props.profile, acceptedFiles);
    }
}

export default Header;
