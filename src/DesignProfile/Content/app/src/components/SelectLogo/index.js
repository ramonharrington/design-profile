import React from 'react';
import Dropzone from 'react-dropzone';
import { PulseLoader } from 'react-spinners';
import $ from 'jquery';

import './SelectLogo.sass';

class LogoSelection extends React.Component {
    constructor(){
        super();
        this.state = {
            zoneImage: "",
            uploadSuccess: false
        }
    }
    render() {
      let dropzoneContent = null;
      if(this.state.uploadSuccess){
          dropzoneContent = <div className="logo-container">              
                    <div className="logo-success-overlay initial-logo-state">
                        <img src='/Content/images/ppage/upload-icon.svg' alt='' key="dropIcon" />
                        <span key="dropIconText">Change your logo</span>
                    </div>
                    <img src={this.state.zoneImage} alt='' key="dropIcon" className="logo-success-image"/>
                </div>
      }
      else{
          dropzoneContent = <div className="logo-container initial-logo-state">
              <img src='/Content/images/ppage/upload-icon.svg' alt='' key="dropIcon" />
              <span key="dropIconText">Add your logo</span>
          </div>
      }
      if(this.props.loadingLogo !== null && this.props.loadingLogo !== 0){
        dropzoneContent = <PulseLoader color={'#0199e0'} loading={true} />
      }

        return (
            <div className='select-logo-container text-shadow'>
                <h1>To get started, upload your logo</h1>
                <Dropzone onDrop={this.onDrop.bind(this)} accept=".jpeg,.jpg,.png" className='dropzone'>
                  {dropzoneContent}
                </Dropzone>
            </div>
        );
    }

    componentDidUpdate() {
        if (this.props.loadingLogo !== null) {
            setTimeout(function() { this.updatePreviewsIfLogoUploadIsComplete() }.bind(this), 1000);
        }
    }

    onDrop(acceptedFiles, rejectedFiles) {
        if (this.props != null && this.props.actions != null) {
            this.props.actions.uploadLogo(this.props.profile, acceptedFiles);
            setTimeout(function() { this.updatePreviewsIfLogoUploadIsComplete(); }.bind(this), 1000);
            this.setState({zoneImage: acceptedFiles[0].preview, uploadSuccess: true});
            setTimeout(function() { this.props.toggleHeader(); }.bind(this), 3000);
        }
    }

    updatePreviewsIfLogoUploadIsComplete() {
        this.props.actions.updatePreviewsIfLogoUploadIsComplete(this.props.profile, this.props.loadingLogo);
    }

    componentDidMount(){
        if (window.matchMedia('(max-width: 772px)').matches) {
            var elem = $($('.dropzone').find('input')[0]);
            elem.attr('style', function(i, style){return style && style.replace(/display[^;]+;?/g, '');});
            elem.css("opacity", "0");
            elem.css("width", "100%");
            elem.css("height", "100");
            elem.css("position", "absolute");
            elem.css("top", "0");
        }
    }
}

export default LogoSelection;
