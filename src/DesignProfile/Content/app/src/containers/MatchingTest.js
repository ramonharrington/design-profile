import React, {Component} from 'react';
import '../styles/matching.css';
import axios from 'axios';
import {ChromePicker} from 'react-color';

class Matching extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companyname: "",
      industry: "",
      fullname: "",
      jobtitle: "",
      email: "",
      phone: "",
      web: "",
      image: "",
      livePreview: "",
      designTemplateToUse: "1126784_B73",
      forceUploadId: 3118094,
      colors: {}
    };

    this.inputChangeHandler = this
      .inputChangeHandler
      .bind(this);
    this.performAssetTransfer = this
      .performAssetTransfer
      .bind(this);
    this.getTextTransferModelFromState = this
      .getTextTransferModelFromState
      .bind(this);
    this.handleChangeColor = this
      .handleChangeColor
      .bind(this);
  }

  componentDidMount() {
    if (this.state.designTemplateToUse.trim() !== "") {
      this.setState({
        livePreview: "http://www.vistaprint.com/lp.aspx?width=600&height=400&template=" + this.state.designTemplateToUse
      });
    }
  }

  inputChangeHandler(event) {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  performAssetTransfer() {
    axios.post("/api/MatchingTestPreviews/4", this.getTextTransferModelFromState(), {
      headers: {
        'Accept': 'application/json',
        "Content-Type": "application/json"
      }
    }).then((responseJson) => {
      this.setState({livePreview: responseJson.data[0].PreviewUrl});
    }).catch((error) => {
      console.error(error);
    });
  }

  handleChangeColor(event, key) {
    var colorobj = this.state.colors;
    colorobj[key] = event.hex;
    this.setState({colors: colorobj})
    this.performAssetTransfer();
  }

  getTextTransferModelFromState() {
    return {
      "TextFieldPurposeNameToValue": {
        companyname: this.state.companyname,
        fullname: this.state.fullname,
        jobtitle: this.state.jobtitle,
        email: this.state.email,
        phone: this.state.phone,
        web: this.state.web
      },
      "designTemplate": this.state.designTemplateToUse,
      "images": [this.state.forceUploadId],
      "colors": this.state.colors,
      "previewHeight": 600,
      "previewWidth": 800,
      "fonts": {
        "Main": "Impact",
        "Secondary": "Arial"
      }
    };
  }

  render() {
    return (
      <div className="matching">
        <div>
          <img className="proto-image" src={this.state.livePreview} alt="live-preview"/>
        </div>
        <label>
          Company Name:
          <input
            id="companyname"
            type="text"
            value={this.state.companyname}
            onChange={this.inputChangeHandler}
            onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Industry:
          <input
            id="industry"
            type="text"
            value={this.state.industry}
            onChange={this.inputChangeHandler}
            onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Owner:
          <input
            id="fullname"
            type="text"
            value={this.state.fullname}
            onChange={this.inputChangeHandler}
            onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Job Title:
          <input
            id="jobtitle"
            type="text"
            value={this.state.jobtitle}
            onChange={this.inputChangeHandler}
            onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Email:
          <input
            id="email"
            type="text"
            value={this.state.email}
            onChange={this.inputChangeHandler}
            onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Phone:
          <input
            id="phone"
            type="text"
            value={this.state.phone}
            onChange={this.inputChangeHandler}
            onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Web Address:
          <input
            id="web"
            type="text"
            value={this.state.web}
            onChange={this.inputChangeHandler}
            onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Image:
          <input
            id="image"
            type="text"
            value={this.state.image}
            onChange={this.inputChangeHandler}
            onBlur={this.performImageTransfer}/>
        </label>
        <br/>
        <label>
          Designt Template:
          <input
            id="designTemplateToUse"
            type="text"
            value={this.state.designTemplateToUse}
            onChange={this.inputChangeHandler}
            onBlur={this.performAssetTransfer}/>
        </label>
        <span>Other good ones: 1047987_B73_023, 2564708_B73_023, 1377355_B73_023,
          3106237_B73_023</span>
        <br/>
        <label>
          Force Upload id:
          <input
            id="forceUploadId"
            type="text"
            value={this.state.forceUploadId}
            onChange={this.inputChangeHandler}
            onBlur={this.performAssetTransfer}/>
        </label>
        <div className="color-picker">
          <span>Primary: {this.state.colors["Primary"]}</span>
          <br/>
          <ChromePicker
            color={this.state.colors.Primary}
            onChangeComplete={(e) => this.handleChangeColor(e, "Primary")}
            className="color-picker"/>
        </div>
        <div className="color-picker">
          <span>Secondary: {this.state.colors["Secondary"]}</span>
          <br/>
          <ChromePicker
            color={this.state.colors.Secondary}
            onChangeComplete={(e) => this.handleChangeColor(e, "Secondary")}
            className="color-picker"/>
        </div>
        <div className="color-picker">
          <span>Tertiary: {this.state.colors["Tertiary"]}</span>
          <br/>
          <ChromePicker
            color={this.state.colors.Tertiary}
            onChangeComplete={(e) => this.handleChangeColor(e, "Tertiary")}
            className="color-picker"/>
        </div>
      </div>
    );
  }
}

export default Matching;