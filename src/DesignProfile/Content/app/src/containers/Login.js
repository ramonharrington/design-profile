import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import * as AuthActions from '../actions/auth';

const validate = values => {
    return {};
};

class Login extends React.Component {
    handleFormSubmit(values){
        this.props.signInUser(values);
    };

    renderAuthenticationError() {
        if (this.props.authenticationError) {
            return <div className="alert alert-danger">{ this.props.authenticationError }</div>;
        }
        return <div></div>;
    }

    render() {
        return(
            <div className="container">
                <div className="col-md-6 col-md-offset-3">
                    <h2 className="text-center">Log In</h2>

                    { this.renderAuthenticationError() }

                    <form onSubmit={this.props.handleSubmit(this.handleFormSubmit)}>
                        <Field name="email" component={this.renderField} className="form-control" type="text" label="Email"/>
                        <Field name="password" component={this.renderField} className="form-control" type="password" label="Password"/>

                        <button action="submit" className="btn btn-primary">Sign In</button>
                    </form>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        authenticationError: state.auth.error
    }
}

export default connect(mapStateToProps, AuthActions)(reduxForm({
    form: 'login',
    validate
})(Login));
