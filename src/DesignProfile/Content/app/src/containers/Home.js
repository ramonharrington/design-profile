import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as TypeActions from '../actions/type';
import * as ProfileActions from '../actions/profile';
import * as NavigationActions from '../actions/navigation';

import { getCookie, createCookie } from '../utils/cookieHelper';
import '../styles/app.css';

class Home extends React.Component {
    render() {
        let shopperKey = getCookie('shopperKey');

        return (
            <div>
                <br />
                Shopper Key: <input type="text" defaultValue={shopperKey} onChange={this.updateShopperKey.bind(this)} />
                <hr />
                <br />
                <a href="profile">Create/Edit Your Design Profile</a><br />
                <hr />
                <a href="profile/1">Edit Test Profile</a><br />
                <a href="matchingtest">Matching Test Page</a><br />
                <a href="itemMetadata">Item Metadata </a>
            </div>
        );
    }

    updateShopperKey(event) {
        createCookie('shopperKey', event.target.value, 7);
    }
}

function mapStateToProps(state) {
    return {
        authenticated: state.auth.authenticated
    };
}

function mapDispatchToProps(dispatch) {
    let actions = {};
    Object.assign(actions, {}, TypeActions, ProfileActions, NavigationActions);
    return { actions: bindActionCreators(actions, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
