import React, {Component} from 'react';
import axios from 'axios';
import ItemComponent from '../components/ItemMetadata/ItemComponent';
import {RingLoader} from 'react-spinners';

class ItemMetadata extends Component {
    constructor() {
        super();
        this.state = {
            templates: null,
            designTemplate: "",
            items: [],
            Isloading: false,
            loadingErr: "",
            IsSaving: false,
            savingErr: "",
            templatePreviewImage: null,
            renderedPreviewImage: null
        };

        this.inputChangeHandler = this
            .inputChangeHandler
            .bind(this);
        this.retrieveItemsPreviews = this
            .retrieveItemsPreviews
            .bind(this);
        this.loadDesignTemplate = this
            .loadDesignTemplate
            .bind(this);
        this.inputChangeHandlerForMetadata = this
            .inputChangeHandlerForMetadata
            .bind(this);
        this.onButtonPress = this
            .onButtonPress
            .bind(this);
        
    }

    componentWillMount() {
        this.getTemplates();
    }
    inputChangeHandler(event) {
        this.setState({
            [event.target.id]: event.target.value
        });
    }
    designTemplateUpdated(event) {
        this.setState({ designTemplate: event.target.value })
    }
    inputChangeHandlerForMetadata(id, event) {
        var copyOfItems = this.state.items;
        for(var i in copyOfItems){
            if(copyOfItems[i].ItemId === id){
                copyOfItems[i].Metadata = event.updated_src;
                break;
            }
        }
        this.setState({items: copyOfItems});
    }
    retrieveItemsPreviews() {
        this.setState({templatePreviewImage: "http://www.vpdev.com/vp/ns/livepreview.aspx?alt_doc_id=" + this.state.designTemplate + "&page=1" });
        this.getRenderedImage(this.state.designTemplate);
        axios
            .get("/api/GetItemMetadataAndXml/" + this.state.designTemplate)
            .then((responseJson) => {
                this.setState({Isloading: false, items: responseJson.data});
            })
            .catch((error) => {
                console.log(error);
                this.setState({loadingErr: "Error retrieving previews"});
                this.setState({items: []});
                this.setState({Isloading: false});
            });
    }
    loadDesignTemplate() {
        this.setState({Isloading: true, items: [], templatePreviewImage: null, renderedPreviewImage: null});
        this.retrieveItemsPreviews();
    }
    onButtonPress() {
        this.setState({IsSaving: true});
        axios
            .post("/api/UpsertItemMetadataV2/" + this.state.designTemplate, this.state.items)
            .then(() => {
                console.log("save data success!");
                this.setState({IsSaving: false});
                this.getRenderedImage(this.state.designTemplate)
            })
            .catch((error) => {
                console.log(error);
                this.setState({IsSaving: false});
                this.setState({savingErr: "failed saving data"});
            });
    }
    render() {
        let templateItems = null;
        if (typeof this.state.templates !== 'undefined' && this.state.templates !== null) {
            templateItems = this.state.templates.map((template) => {
                return <option key={template} id={template}>{template}</option>
            });
        }

        let previewImage = null;
        if (typeof this.state.templatePreviewImage !== 'undefined' && this.state.templatePreviewImage !== null && this.state.templatePreviewImage !== '') {
            previewImage = <img style={{maxHeight:300, maxWidth:300}} src={this.state.templatePreviewImage} alt="rendered-preview" />
        }

        let renderedPreview = null;
        if (typeof this.state.renderedPreviewImage !== 'undefined' && this.state.renderedPreviewImage !== null && this.state.renderedPreviewImage !== '') {
            renderedPreview = <img style={{maxHeight:300, maxWidth:300}} src={this.state.renderedPreviewImage} alt="rendered-preview" />
        }

        return (
            <div>
                <h1>Doc Items</h1>
                <br/>
                <div style={{display: "table"}}>
                    <div style={{display: "table-cell", overflow: "hidden", verticalAlign: "top"}}>
                        <label>Select Existing Design Template:
                        <select id="templateId" onChange={this.updateTemplate.bind(this)}>
                            { templateItems }
                        </select></label>
                        <label>
                            Or Import New Template:
                            <input
                                id="designTemplate"
                                type="text"
                                onChange={this.designTemplateUpdated.bind(this)}
                                value={this.state.designTemplate}
                                />
                            <span>
                                {this.state.loadingErr}</span>
                        </label>
                        <RingLoader color={'#123abc'} loading={this.state.Isloading}/>
                        <button type="button" onClick={this.loadDesignTemplate}>Load Data</button><br />
                        {previewImage}<br />
                        {renderedPreview}<br />
                        <button type="button" onClick={this.onButtonPress}>Save Data</button>
                        <RingLoader color={'#123abc'} loading={this.state.IsSaving}/>
                        <span>{this.state.savingErr}</span>
                    </div>
                    <div style={{display: "table-cell", overflow: "scroll", verticalAlign: "top"}}>
                        <div className="doc-items" style={{height: "800px", minWidth: "500px"}}>
                            {this
                                .state
                                .items
                                .map((itemData) => {
                                    return <ItemComponent key={itemData.ItemId}
                                        imageUrl={itemData.ItemPreviewUrl}
                                        itemKey={itemData.ItemId}
                                        itemType={itemData.Type}
                                        itemMetadata={itemData.Metadata}
                                        updateHandler={this.inputChangeHandlerForMetadata}/>
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    updateTemplate(event) {
        this.setState({designTemplate: event.target.value});
    }

    getTemplates() {
        axios
            .get("/api/Template")
            .then((responseJson) => {
                this.setState({templates: responseJson.data});
            })
            .catch((error) => {
                console.log(error);
                this.setState({loadingErr: "Error retrieving templates"});
                this.setState({templates: {}});
            });;
    }

    getRenderedImage(selectedTemplate) {
        if (typeof selectedTemplate !== 'undefined' && selectedTemplate !== null) {
            axios
                .get("/api/Profile/1/CompletePreviews/1?templateIdToUse=" + selectedTemplate)
                .then((responseJson) => {
                    if (responseJson.data.length > 0) {
                        this.setState({renderedPreviewImage: responseJson.data[0].PreviewUrl});
                    }
                })
                .catch((error) => {
                    console.log("error retrieving image from profile:  " + error);
                    this.setState({loadingErr: "Error retrieving profile preview"});
                    this.setState({renderedPreviewImage: null});
                });;
        }
    }
}

export default ItemMetadata;