import React from 'react';
import { ConnectedRouter } from 'react-router-redux';
import { Route, Redirect } from 'react-router-dom'
import { history } from '../store/configureStore';
import { connect } from 'react-redux';

import Login from './Login';
import Home from './Home';
import Profile from './Profile';
import PreviewPanel from '../components/PreviewPanel';
import MatchingTest from './MatchingTest';
import Dashboard from './Dashboard';
import ItemMetadata from './ItemMetadata';

const PrivateRoute = ({component: Component, authenticated, ...props}) => {
    return (
        <Route
            {...props}
            render={(props) => authenticated === true
                ? <Component {...props} />
                : <Redirect to={{pathname: '/login', state: {from: props.location}}} />}
        />
    );
};

const PublicRoute = ({component: Component, authenticated, ...props}) => {
    return (
        <Route
            {...props}
            render={(props) => authenticated === false
                ? <Component {...props} />
                : <Redirect to='/favorites' />}
        />
    );
};

class App extends React.Component {
    render() {
        return (
            <ConnectedRouter history={history}>
                <div>
                    <div className="container">
                        <Route exact path="/" component={ Profile }/>
                        <Route exact path="/debug" component={ Home }/>
                        <Route path="/profile/:id?" component={ Profile } />
                        <Route path="/previewpanel" component={ PreviewPanel }/>
                        <Route path="/matchingtest" component={ MatchingTest }/>
                        <Route path="/itemMetadata" component={ ItemMetadata } />
                        <Route path="/insta" component={null} />
                        <PublicRoute authenticated={ this.props.authenticated }  path="/login" component={ Login } />
                        <PrivateRoute authenticated={ this.props.authenticated }  path="/dashboard" component={ Dashboard } />
                    </div>
                </div>
            </ConnectedRouter>
        );
    }
}

const mapStateToProps = (state) => {
    return { authenticated: state.auth.authenticated };
};

export default connect(mapStateToProps)(App);
