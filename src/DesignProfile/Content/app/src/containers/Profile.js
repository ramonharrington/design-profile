import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import $ from 'jquery';

import * as TypeActions from '../actions/type';
import * as ProfileActions from '../actions/profile';
import * as NavigationActions from '../actions/navigation';
import * as LoggingActions from '../actions/logging';

import LogoSelection from '../components/SelectLogo/';
import PreviewPanel from '../components/PreviewPanel';
import PreviewModal from '../components/PreviewModal';
import Header from '../components/Header';

import { getCookie } from '../utils/cookieHelper';

// eslint-disable-next-line
import styles from './Profile.sass';
import '../styles/app.css';

class Profile extends React.Component {
    constructor(){
      super();

      this.state = {
        showPreviewModal: false,
        modalPreview: {},
        modalYPos: 30,
        showCollapsedHeader: false
      }
    }

    componentDidMount() {
        window.addEventListener("resize", this.props.actions.resizeFrame);
        window.addEventListener("shopperKeyUpdated", function() { this.loadShopper() }.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.props.actions.resizeFrame);
        window.removeEventListener("shopperKeyUpdated", function() { this.loadShopper() }.bind(this));
    }

    componentWillMount() {
        this.props.actions.getIndustries();
        this.props.actions.getFonts();
        let profileId = _.get(this.props, 'match.params.id', null);
        if (profileId !== null) {
            this.props.actions.getProfile(profileId);
        } else {
            setTimeout(function() { this.loadShopper(0) }.bind(this), 1000);
        }
    }

    loadShopper(attempt) {
        let shopperKey = getCookie('shopperKey');
        if (typeof shopperKey !== 'undefined' && shopperKey !== '0' && shopperKey !== 0) {
            this.props.actions.getProfileByShopperKey(shopperKey);
        } else {
            if (attempt < 3) {
                setTimeout(function() { this.loadShopper(attempt + 1) }.bind(this), 1000);
            // } else {
            //     window.location.reload();
            }
        }
    }

    render() {
        if (_.get(this.props, 'profile', null) == null) {
            return <div></div>;
        }

        let previewModal = <PreviewModal
          close={() => this.setState({showPreviewModal: false})}
          modalPreview ={ this.state.modalPreview}
          profileId= {this.props.profile.id}
          actions = {this.props.actions}
          topPos = {this.state.modalYPos}
        />;
        let topSection = null;

        if(this.state.showCollapsedHeader){
            if (typeof this.props.loadingLogo !== 'undefined' && this.props.loadingLogo !== null) {
                setTimeout(function() { this.updatePreviewsIfLogoUploadIsComplete() }.bind(this), 1000);
            }
            topSection = <div className='top-section-collapsed'>
                <div className='header'>BRAND SHOP</div>
                <button className="button-primary" onClick={this.toggleCollapsedHeader.bind(this)}>Change Logo</button>
            </div>
        }else{
            topSection = <div className='top-section text-shadow'>
            <Header actions={this.props.actions} profile={this.props.profile} loadingLogo={this.props.loadingLogo} lastUploadCompletionCheck={this.props.lastUploadCompletionCheck} />
            <LogoSelection actions={this.props.actions} profile={this.props.profile} loadingLogo={this.props.loadingLogo} 
            toggleHeader={this.toggleCollapsedHeader.bind(this)}/>
          </div>
        }

        return (
            <div className='profile-container' onClick={this.handleClick.bind(this)}>
              {this.state.showPreviewModal ? previewModal : <div></div>}
              {topSection}

              <PreviewPanel actions={this.props.actions} profile={this.props.profile} previews={this.props.previews} handlePreviewClick={this.handlePreviewClick.bind(this)}/>
            </div>
        );
    }

    toggleCollapsedHeader(){
        this.setState({showCollapsedHeader: !this.state.showCollapsedHeader})
    }

    updatePreviewsIfLogoUploadIsComplete() {
        this.props.actions.updatePreviewsIfLogoUploadIsComplete(this.props.profile, this.props.loadingLogo);
    }

    handlePreviewClick(preview, ele){
        if(preview.PreviewData === null){
            return;
        }
        this.setState({modalPreview: preview});
        this.setState({showPreviewModal: true});

        // if we are in an iframe then fixed content does not work
        // need to calculate the correct placement of the modal
        if(window.parent){
            var ypos = (ele.pageY - ele.screenY + 100);
            if(ypos < 100){
                ypos = ele.pageY;
            }            
          this.setState({modalYPos: ypos});
        }

        this.props.actions.trackPreviewEvent(this.props.profile.id, preview, 'ViewProductDetail');
    }

    handleClick(event) {
        if (event.target.className !== 'color-swatch' && this.props.colorPickerAttachedElement !== null && $(event.target).parents('.react-color-swatch').length === 0) {
            this.props.actions.hideColorPicker();
        }
        if (this.props.fontSelectionArea !== null && $(event.target).parents('.font-list').length === 0) {
            this.props.actions.hideFontSelectionArea();
        }
    }
}

const mapStateToProps = (state) => {
    return {
        industries: state.type.industries,
        fonts: state.type.fonts,
        authenticated: state.auth.authenticated,
        profile: state.profile.profile,
        profileModified: state.profile.profileModified,
        previews: state.profile.previews,
        lastProfileUpdate: state.profile.lastUpdate,
        previewsGenerated: state.profile.previewsGenerated,
        lastUploadCompletionCheck: state.profile.lastUploadCompletionCheck,
        portfolioImages: state.profile.portfolioImages,
        loadingLogo: state.profile.loadingLogo,
        expandBusinessProfileDetails: state.navigation.expandBusinessProfileDetails,
        expandColorProfileDetails: state.navigation.expandColorProfileDetails,
        expandFontProfileDetails: state.navigation.expandFontProfileDetails,
        expandImageProfileDetails: state.navigation.expandImageProfileDetails,
        colorPickerAttachedElement: state.navigation.colorPickerAttachedElement,
        logoSelectionDialogVisible: state.navigation.logoSelectionDialogVisible,
        fontSelectionArea: state.navigation.fontSelectionArea
    };
};

const mapDispatchToProps = (dispatch) => {
    let actions = {};
    Object.assign(actions, {}, TypeActions, ProfileActions, NavigationActions, LoggingActions);
    return { actions: bindActionCreators(actions, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
