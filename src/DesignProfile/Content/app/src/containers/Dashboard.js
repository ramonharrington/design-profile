import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as TypeActions from '../actions/type';
import * as ProfileActions from '../actions/profile';
import * as NavigationActions from '../actions/navigation';
import * as LoggingActions from '../actions/logging';

import '../styles/app.css';

class Dashboard extends React.Component {
  render() {
    return (
      <div>
      </div>
    );
  }
}

function mapStateToProps(state) {
    return {
        authenticated: state.auth.authenticated,
    };
}

function mapDispatchToProps(dispatch) {
    let actions = {};
    Object.assign(actions, {}, TypeActions, ProfileActions, NavigationActions, LoggingActions);
    return { actions: bindActionCreators(actions, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
