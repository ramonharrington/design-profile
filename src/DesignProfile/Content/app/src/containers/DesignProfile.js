import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import $ from 'jquery';

import * as TypeActions from '../actions/type';
import * as ProfileActions from '../actions/profile';
import * as NavigationActions from '../actions/navigation';
import * as LoggingActions from '../actions/logging';

import LogoProfileDetails from '../components/LogoProfileDetails';
import BusinessProfileDetails from '../components/BusinessProfileDetails';
import ColorProfileDetails from '../components/ColorProfileDetails';
import FontProfileDetails from '../components/FontProfileDetails';
import ImageProfileDetails from '../components/ImageProfileDetails';
import PreviewPanel from '../components/PreviewPanel';

import { getCookie } from '../utils/cookieHelper';

// eslint-disable-next-line
import styles from './Profile.sass';
import '../styles/app.css';

class Profile extends React.Component {
    componentDidMount() {
        window.addEventListener("resize", this.props.actions.resizeFrame);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.props.actions.resizeFrame);
    }

    componentWillMount() {
        this.props.actions.getIndustries();
        this.props.actions.getFonts();
        let profileId = _.get(this.props, 'match.params.id', null);
        if (profileId !== null) {
            this.props.actions.getProfile(profileId);
        } else {
            setTimeout(function() { this.loadShopper(0) }.bind(this), 1000);
        }
    }

    loadShopper(attempt) {
        let shopperKey = getCookie('shopperKey');
        if (typeof shopperKey !== 'undefined' && shopperKey !== '0' && shopperKey !== 0) {
            this.props.actions.getProfileByShopperKey(shopperKey);
        } else {
            if (attempt < 3) {
                setTimeout(function() { this.loadShopper(attempt + 1) }.bind(this), 1000);
            // } else {
            //     window.location.reload();
            }
        }
    }

    render() {
        if (_.get(this.props, 'profile', null) == null) {
            return <div></div>;
        }

        return (
            <div className='profile' onClick={this.handleClick.bind(this)}>
                {/* <div className="row">
                    <div className="three columns">
                        <div className="panel edit-panel">
                            <div className="profile-section">
                                <div className="panel-prefix">BRAND CENTER</div>
                                <div className="panel-title">Edit Your Design Profile</div>
                            </div>
                            <div className="profile-section">
                                <LogoProfileDetails actions={this.props.actions} profile={this.props.profile} logoSelectionDialogVisible={this.props.logoSelectionDialogVisible} portfolioImages={this.props.portfolioImages} loadingLogo={this.props.loadingLogo} lastUploadCompletionCheck={this.props.lastUploadCompletionCheck} />
                            </div>
                            <div className="profile-section">
                                <BusinessProfileDetails actions={this.props.actions} expandBusinessProfileDetails={this.props.expandBusinessProfileDetails} profile={this.props.profile} industries={this.props.industries} profileModified={this.props.profileModified} />
                            </div>
                            <div className="profile-section">
                                <ColorProfileDetails actions={this.props.actions} expandColorProfileDetails={this.props.expandColorProfileDetails} profile={this.props.profile} colorPickerAttachedElement={this.props.colorPickerAttachedElement} profileModified={this.props.profileModified} />
                            </div>
                            <div className="profile-section">
                                <FontProfileDetails actions={this.props.actions} expandFontProfileDetails={this.props.expandFontProfileDetails} profile={this.props.profile} fonts={this.props.fonts} profileModified={this.props.profileModified} fontSelectionArea={this.props.fontSelectionArea} />
                            </div>
                            <div className="profile-section">
                                <ImageProfileDetails actions={this.props.actions} expandImageProfileDetails={this.props.expandImageProfileDetails} profile={this.props.profile} profileModified={this.props.profileModified} />
                            </div>
                        </div>
                    </div>
                </div> */}
                <div className="row">
                    <div className="twelve columns">
                    </div>
                </div>
                <div className="row">
                    <div className="twelve columns">
                        <PreviewPanel actions={this.props.actions} profile={this.props.profile} previews={this.props.previews} />
                    </div>
                </div>
            </div>
        );
    }

    handleClick(event) {
        if (event.target.className !== 'color-swatch' && this.props.colorPickerAttachedElement !== null && $(event.target).parents('.react-color-swatch').length === 0) {
            this.props.actions.hideColorPicker();
        }
        if (this.props.fontSelectionArea !== null && $(event.target).parents('.font-list').length === 0) {
            this.props.actions.hideFontSelectionArea();
        }
    }
}

const mapStateToProps = (state) => {
    return {
        industries: state.type.industries,
        fonts: state.type.fonts,
        authenticated: state.auth.authenticated,
        profile: state.profile.profile,
        profileModified: state.profile.profileModified,
        previews: state.profile.previews,
        lastProfileUpdate: state.profile.lastUpdate,
        previewsGenerated: state.profile.previewsGenerated,
        lastUploadCompletionCheck: state.profile.lastUploadCompletionCheck,
        portfolioImages: state.profile.portfolioImages,
        loadingLogo: state.profile.loadingLogo,
        expandBusinessProfileDetails: state.navigation.expandBusinessProfileDetails,
        expandColorProfileDetails: state.navigation.expandColorProfileDetails,
        expandFontProfileDetails: state.navigation.expandFontProfileDetails,
        expandImageProfileDetails: state.navigation.expandImageProfileDetails,
        colorPickerAttachedElement: state.navigation.colorPickerAttachedElement,
        logoSelectionDialogVisible: state.navigation.logoSelectionDialogVisible,
        fontSelectionArea: state.navigation.fontSelectionArea
    };
};

const mapDispatchToProps = (dispatch) => {
    let actions = {};
    Object.assign(actions, {}, TypeActions, ProfileActions, NavigationActions, LoggingActions);
    return { actions: bindActionCreators(actions, dispatch) };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
