import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Link, browserHistory } from 'react-router';
import { AppContainer } from 'react-hot-loader';
import classNames from 'classnames'

//adding this for now to load the matching prototype, we can delete these later
import './index.css';
import Home from './Home';
import Profile from './Profile';
import MatchingTester from './MatchingTester';

import AuthService from './utils/AuthService'
const auth = new AuthService('GvqUerGUIuqIIHqEpNlyiEMEcswombTp', 'cimpress.auth0.com');

import GlobalStyle from './Global.scss';

var DesignProfile = require('./Profile')

//Authentication required paths
const requireAuth = (nextState, replace) => {
  if (!auth.loggedIn()) {
    replace({ pathname: '/login' })
  }
}

render((
  <div>
    <Router history={browserHistory} auth={auth}>
      <Route path="/" component={Home} />
      <Route path="/profile" component={Profile} />
      <Route path="/matchingTester" component={MatchingTester} />
    </Router>
  </div>
), document.getElementById('page-content'));