import React, {Component} from 'react';
import './App.css';
import axios from 'axios';
import { ChromePicker } from 'react-color';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companyname: "",
      industry: "",
      fullname: "",
      jobtitle: "",
      email: "",
      phone: "",
      web: "",
      image: "",
      livePreview: "",
      designTemplateToUse: "1377355_B73",
      forceUploadId: 0,
      color: "",
      fonts: [],
      selectedFont: "",
      saturationWeight: 50,
      lightnessWeight: 50
    };

    this.inputChangeHandler = this
      .inputChangeHandler
      .bind(this);
    this.performAssetTransfer = this
      .performAssetTransfer
      .bind(this);
    this.getTextTransferModelFromState = this
      .getTextTransferModelFromState
      .bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.getAvailabeFonts = this.getAvailabeFonts.bind(this);
    this.createSelectItems = this.createSelectItems.bind(this);
    this.onDropdownSelected = this.onDropdownSelected.bind(this);
  }

  componentDidMount(){
    if(this.state.designTemplateToUse.trim() != ""){
    this.setState({livePreview: "http://www.vistaprint.com/lp.aspx?width=600&height=400&template=" + this.state.designTemplateToUse});
    };
    this.getAvailabeFonts();
  }

  inputChangeHandler(event) {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleColorChange(color) {
    this.setState({color: color.hex}, function(){
      this.performAssetTransfer();
    });
  }

  performAssetTransfer() {
     axios.post("/api/ProxyPost", 
     {
       "monolithEndpoint" : "/hatchery/rpc/assettransferapi/AssetTransfer",
	      "payload" : this.getTextTransferModelFromState()
     }, 
     {
      headers: {
        'Accept': 'application/json',
        "Content-Type": "application/json",
      }})
    .then((responseJson) => {
        this.setState({livePreview: responseJson.data.PreviewUrl});
        this.setState({forceUploadId: responseJson.data.ForceUploadId});
    })
    .catch((error) => {
      console.error(error);
    }); 
  }

  getTextTransferModelFromState() {
    return {"TextFieldPurposeNameToValue": {
      companyname: this.state.companyname,
      fullname: this.state.fullname,
      jobtitle: this.state.jobtitle,
      email: this.state.email,
      phone: this.state.phone,
      web: this.state.web
    }, 
    "DesignTemplateToTransfer": this.state.designTemplateToUse,
    "images": [this.state.image],
    "forceUploadId": this.state.forceUploadId,
    "color" : this.state.color,
    "selectedFont" : this.state.selectedFont,
    "saturationWeight": this.state.saturationWeight /100,
    "lightnessWeight": this.state.lightnessWeight / 100
  };
  }

  getAvailabeFonts(){
    axios.get("/api/getfonts")
    .then((responseJson) => {
      this.setState({fonts: this.createSelectItems(responseJson.data)});
    }).catch((error) => {
      console.error(error);
    });
  }

  createSelectItems(fonts){
    let items = [];
    for(let i= 0; i <= fonts.length; i++){
      items.push(<option key={i} value={fonts[i]}>{fonts[i]}</option>);
    }
    return items;
  }

  onDropdownSelected(event) {
    this.setState({selectedFont: event.target.value}, function(){
      this.performAssetTransfer();
    });        
}

  render() {
    return (
      <div className="App">
        <label>
          Company Name:
          <input
            id="companyname"
            type="text"
            value={this.state.companyname}
            onChange={this.inputChangeHandler}
            onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Industry:
          <input
            id="industry"
            type="text"
            value={this.state.industry}
            onChange={this.inputChangeHandler} onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Owner:
          <input
            id="fullname"
            type="text"
            value={this.state.fullname}
            onChange={this.inputChangeHandler} onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Job Title:
          <input
            id="jobtitle"
            type="text"
            value={this.state.jobtitle} 
            onChange={this.inputChangeHandler} onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Email:
          <input
            id="email"
            type="text"
            value={this.state.email}
            onChange={this.inputChangeHandler} onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Phone:
          <input
            id="phone"
            type="text"
            value={this.state.phone}
            onChange={this.inputChangeHandler} onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Web Address:
          <input
            id="web"
            type="text"
            value={this.state.web}
            onChange={this.inputChangeHandler} onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Image:
          <input
            id="image"
            type="text"
            value={this.state.image}
            onChange={this.inputChangeHandler} onBlur={this.performImageTransfer}/>
        </label>
        <br/>
        <label>
          Designt Template:
          <input
            id="designTemplateToUse"
            type="text"
            value={this.state.designTemplateToUse}
            onChange={this.inputChangeHandler} onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <label>
          Force Upload id:
          <input
            id="forceUploadId"
            type="text"
            value={this.state.forceUploadId}
            onChange={this.inputChangeHandler} onBlur={this.performAssetTransfer}/>
        </label>
        <br />
        <label>
          Saturation Weight (%):
          <input
            id="saturationWeight"
            type="text"
            value={this.state.saturationWeight}
            onChange={this.inputChangeHandler} onBlur={this.performAssetTransfer}/>
        </label>
        <br />
        <label>
          Lightness Weight (%):
          <input
            id="lightnessWeight"
            type="text"
            value={this.state.lightnessWeight}
            onChange={this.inputChangeHandler} onBlur={this.performAssetTransfer}/>
        </label>
        <br/>
        <select id="selectedFont" value={this.state.fonts.first} 
          onChange={this.onDropdownSelected}>
          {this.state.fonts}
        </select>
        <br/>
        <ChromePicker color={this.state.color} onChangeComplete={this.handleColorChange}/>
        <div>
          <img src={this.state.livePreview}/>
        </div>
      </div>
    );
  }
}

export default App;
