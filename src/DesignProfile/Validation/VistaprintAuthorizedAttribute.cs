﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using DesignProfile.Controllers.Extensions;

namespace DesignProfile.Validation
{
    /// <summary>
    /// Used to determine whether the user has been authorized by the Vistaprint login popup.  This is determined by
    /// checking for the existence of a "shopperkey" cookie.  Bypass for local requests
    /// </summary>
    /// <seealso cref="System.Web.Http.Filters.ActionFilterAttribute" />
    public class VistaprintAuthorizedAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            ///TODO: Remove escape clause

            return;
            if (!actionContext.Request.Headers.GetCookies("shopperkey").Any())
            {
                var ipAddress = AuthorizationHelpers.GetIpAddress(actionContext);
                if (ipAddress != "127.0.0.1" && ipAddress != "::1")
                {
                    throw new HttpResponseException(HttpStatusCode.Unauthorized);
                }
            }
        }
    }
}