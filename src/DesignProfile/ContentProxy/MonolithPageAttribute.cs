using System.Web.Mvc;

namespace DesignProfile.ContentProxy
{
    public class MonolithPageOptions : ActionFilterAttribute
    {
        public string PageTitle { get; set; }
        public string Layout { get; set; }
        public string FavIcon { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!string.IsNullOrEmpty(Layout))
            {
                filterContext.RequestContext.HttpContext.Response.Headers["X-VpProxy-LayoutType"] = Layout;
            }

            if (!string.IsNullOrEmpty(PageTitle))
            {
                filterContext.RequestContext.HttpContext.Response.Headers["X-VpProxy-RootLayoutOptions-PageTitle"] = PageTitle;
            }

            if (!string.IsNullOrEmpty(FavIcon))
            {
                ProxyHelper.Link("icon", FavIcon);
            }
        }
    }

    public static class MonolithLayout
    {
        /// <summary>
        /// This is an empty layout and allows Hatchery to control the overall layout of the page.
        /// </summary>
        public const string Simple = "Simple";

        /// <summary>
        /// This includes the Vistaprint header and footer.
        /// </summary>
        public const string Standard = "Standard";
    }
}
