using System.Web;

namespace DesignProfile.ContentProxy
{
    public static class ProxyHelper
    {
        private const string JAVASCRIPT_HEADER = "X-VpProxy-Javascripts";
        private const string LINK_HEADER = "X-VpProxy-LinkTags";
        private const string META_HEADER = "X-VpProxy-MetaTags";

        public static IHtmlString Css(string area, params string[] paths)
        {
            var ctx = HttpContext.Current;
            foreach (var path in paths)
            {
                AppendPathToHeader(ctx, "X-VpProxy-Stylesheets", path, "Content", area);
            }
            return new HtmlString("");
        }

        public static IHtmlString Js(string area, params string[] paths)
        {
            var ctx = HttpContext.Current;
            foreach (var path in paths)
            {
                AppendPathToHeader(ctx, JAVASCRIPT_HEADER, path, "Scripts", area);
            }
            return new HtmlString("");
        }

        public static IHtmlString Js(string fullRelativePath)
        {
            var ctx = HttpContext.Current;
            string existingHeader = ctx.Response.Headers[JAVASCRIPT_HEADER] ?? "";
            if (!string.IsNullOrEmpty(existingHeader))
            {
                existingHeader += ",";
            }
            existingHeader += fullRelativePath;
            ctx.Response.Headers[JAVASCRIPT_HEADER] = existingHeader;

            return new HtmlString("");
        }

        public static void Meta(string name, string content)
        {
            var ctx = HttpContext.Current;
            // Version two uses query string serialization
            ctx.Response.Headers["X-VpProxy-Version"] = "2";
            string existingHeader = ctx.Response.Headers[META_HEADER] ?? "";
            if (!string.IsNullOrEmpty(existingHeader))
            {
                existingHeader += "&";
            }
            existingHeader += name + "=" + HttpUtility.UrlEncode(content);
            ctx.Response.Headers[META_HEADER] = existingHeader;
        }

        public static void Link(string rel, string href)
        {
            var ctx = HttpContext.Current;
            // Version two uses query string serialization
            ctx.Response.Headers["X-VpProxy-Version"] = "2";
            string existingHeader = ctx.Response.Headers[LINK_HEADER] ?? "";
            if (!string.IsNullOrEmpty(existingHeader))
            {
                existingHeader += "&";
            }
            existingHeader += rel + "=" + HttpUtility.UrlEncode(href);
            ctx.Response.Headers[LINK_HEADER] = existingHeader;
        }

        private static void AppendPathToHeader(HttpContext ctx, string header, string path, string prefix, string area)
        {
            string existingHeader = ctx.Response.Headers[header] ?? "";
            if (!string.IsNullOrEmpty(existingHeader))
            {
                existingHeader += ",";
            }
            existingHeader += "/beta-api/static/?area=" + area + "&path=/" + prefix.Trim('/') + '/' + path.TrimStart('/');
            ctx.Response.Headers[header] = existingHeader;
        }

        public static IHtmlString ImageUrl(string area, string path)
        {
            string url = "/beta-api/static/?area=" + area + "&path=Content/" + path.TrimStart('/');
            return new HtmlString(url);
        }
    }
}
