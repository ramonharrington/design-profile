﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using DesignProfile.Adapters;
using DesignProfile.data;
using DesignProfile.Models;
using DesignProfile.Properties;
using DesignProfile.Utils;
using Newtonsoft.Json;
using Profile = DesignProfile.Models.Profile;

namespace DesignProfile.Builders
{
    internal class ProfileBuilder
    {
        private VpProfileAdapter _vpProfileAdapter;
        private ImagingAdapter _imagingAdapter;

        internal ProfileBuilder(VpProfileAdapter vpProfileAdapter)
        {
            _vpProfileAdapter = vpProfileAdapter;
            _imagingAdapter = new ImagingAdapter();
        }

        internal Profile IntializeNullShopper()
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                using (var tx = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        var account = new data.Account
                        {
                            ShopperKey = null,
                            Created = DateTime.Now
                        };
                        ctx.Accounts.Add(account);
                        ctx.SaveChanges();

                        var business = new data.Business
                        {
                            AccountId = account.Id
                        };
                        ctx.Businesses.Add(business);
                        ctx.SaveChanges();

                        var profile = new data.Profile
                        {
                            BusinessId = business.Id,
                            Name = "Empty profile",
                            Created = DateTime.Now,
                            Settings = BuildProfileSettingsString(null, null),
                            ProfileTypeId = 1 //we should prob remove this
                        };
                        ctx.Profiles.Add(profile);
                        ctx.SaveChanges();
                        tx.Commit();
                        return profile.ToModel();
                    }
                    catch (Exception e)
                    {
                        tx.Rollback();

                        return null;
                    }
                }
            }
        }

        internal Profile Initialize(int shopperKey, bool searchImages = false, bool searchVpHistory = false)
        {
            ShopperProfileData shopperProfile;
            if (searchVpHistory)
            {
                shopperProfile = _vpProfileAdapter.GetShopperProfile(shopperKey);

                if (shopperProfile?.ShopperId == null)
                {
                    return null;
                }
            }
            else
            {
                shopperProfile = new ShopperProfileData() { ShopperId = shopperKey.ToString() };

                if (shopperProfile?.ShopperId == null)
                {
                    return null;
                }
            }

            var document = shopperProfile.GetDocument();

            var analyzedImages = new List<AnalyzedImage>();
            if (shopperProfile.LastImage != null)
            {
                try
                {
                    var newImage = new AnalyzedImage
                    {
                        SourceId = shopperProfile.LastImage.ImageId,
                        Uploaded = shopperProfile.LastImage.UploadDate,
                        Url = shopperProfile.LastImage.ImagePreview,
                        IsLogo = true
                    };

                    Bitmap image =
                        (Bitmap)System.Drawing.Image.FromStream(
                            new MemoryStream(ImageRetrievalUtils.GetImageBytes(newImage.Url)));
                    newImage.Colors = _imagingAdapter.GetImageColors(image).Take(5)
                        .Select(c => "#" + c.Key.R.ToString("X2") + c.Key.G.ToString("X2") +
                                     c.Key.B.ToString("X2"));

                    analyzedImages.Add(newImage);
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Unable to retrieve colors from image", ex);
                }
            }
            else if (searchImages)
            {
                analyzedImages = GetAnalyzedImages(document).ToList();
            }

            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                using (var tx = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        var account = ctx.Accounts.FirstOrDefault(p => p.ShopperKey == shopperKey);

                        // Create account if it doesn't exist
                        if (account == null)
                        {
                            ctx.Accounts.Add(new data.Account
                            {
                                Email = GetText(document, PurposeField.Email)?.TruncateString(100),
                                LastName = GetText(document, PurposeField.FullName)?.TruncateString(100),
                                ShopperKey = shopperKey,
                                Created = DateTime.Now
                            });

                            ctx.SaveChanges();
                            account = ctx.Accounts.FirstOrDefault(a => a.ShopperKey == shopperKey);
                        }

                        // Get business
                        var business = ctx.Businesses.FirstOrDefault(b => b.AccountId == account.Id);
                        if (business == null)
                        {
                            business = new data.Business
                            {
                                AccountId = account.Id,
                                Email = account.Email,
                            };

                            if (document != null)
                            {
                                var textClassificationAdapter = new TextClassificationAdapter();
                                textClassificationAdapter.UpdateBusinessInformation(business,
                                    document.DocumentToTextFieldData
                                        .Select(t => new Tuple<string, string>(t.Value, t.PurposeTextId)).ToList());
                            }
                            ctx.Businesses.Add(business);
                            ctx.SaveChanges();
                            business = ctx.Businesses.FirstOrDefault(b => b.AccountId == account.Id);
                        }

                        if (analyzedImages.Any())
                        {
                            foreach (var analyzedImage in analyzedImages)
                            {
                                ctx.Images.Add(new data.Image
                                {
                                    BusinessId = business.Id,
                                    ImageTypeId = analyzedImage.SourceType == "LibraryImage"
                                        ? 3
                                        : (analyzedImage.IsLogo ? 1 : 2),
                                    Title = analyzedImage.SourceType ?? "Uploaded Image",
                                    Url = analyzedImage.Url,
                                    Uploaded = analyzedImage.Uploaded ?? DateTime.Now,
                                    UploadId = analyzedImage.SourceId
                                });
                            }
                            ctx.SaveChanges();

                            var analyzedImagesUploadIds = analyzedImages.Select(i => i.SourceId).ToList();
                            var newImages = ctx.Images
                                .Where(i => i.UploadId != null && analyzedImagesUploadIds.Contains(i.UploadId.Value))
                                .ToList();
                            foreach (var newImage in newImages)
                            {
                                analyzedImages.FirstOrDefault(i => i.SourceId == newImage.UploadId).Id = newImage.Id;
                            }
                        }

                        // Get profile
                        var profile = ctx.Profiles.FirstOrDefault(p => p.BusinessId == business.Id);
                        if (profile == null)
                        {
                            ctx.Profiles.Add(new data.Profile
                            {
                                BusinessId = business.Id,
                                Name = "",
                                Settings = BuildProfileSettingsString(document, analyzedImages),
                                Created = DateTime.Now,
                                ProfileTypeId = 1
                            });

                            ctx.SaveChanges();
                            profile = ctx.Profiles.FirstOrDefault(p => p.BusinessId == business.Id);
                        }

                        tx.Commit();

                        return profile.ToModel();
                    }
                    catch (Exception e)
                    {
                        tx.Rollback();

                        return null;
                    }
                }
            }
        }

        private string BuildProfileSettingsString(DocumentData document, IEnumerable<AnalyzedImage> analyzedImages)
        {
            if (document == null && (analyzedImages == null || !analyzedImages.Any()))
            {
                return "{ \"fonts\": {}, \"colors\": {}, \"images\": {} }";
            }

            var settings = new ProfileSettings();
            settings.fonts = new font();
            settings.colors = new color();
            settings.images = new image();

            var fonts = new List<string>();
            IDictionary<string, int> colors = new Dictionary<string, int>();

            if (document != null)
            {
                fonts = document.GetFonts().ToList().OrderByDescending(f => f.Value).Select(f => f.Key).ToList();
                colors = document.GetFontColors();
            }

            var logo = analyzedImages.FirstOrDefault(i => i.IsLogo);
            if (logo != null)
            {
                settings.images.logo = new imageData
                {
                    id = logo.Id,
                    url = logo.Url
                };

                var primaryColor = logo.Colors.ElementAtOrDefault(0);
                if (primaryColor != null)
                {
                    AddColorToWeightedDictionary(colors, primaryColor, 100);
                }
                var secondaryColor = logo.Colors.ElementAtOrDefault(1);
                if (secondaryColor != null)
                {
                    AddColorToWeightedDictionary(colors, secondaryColor, 50);
                }
                var tertiaryColor = logo.Colors.ElementAtOrDefault(2);
                if (tertiaryColor != null)
                {
                    AddColorToWeightedDictionary(colors, tertiaryColor, 30);
                }
            }

            var pictures = new List<imageData>();
            foreach (var analyzedImage in analyzedImages)
            {
                if (logo == null || logo.Id != analyzedImage.Id)
                {
                    pictures.Add(new imageData
                    {
                        id = analyzedImage.Id,
                        url = analyzedImage.Url
                    });
                }
            }
            settings.images.pictures = pictures;

            if (fonts != null && fonts.Any())
            {
                settings.fonts.main = fonts.First();
            }
            if (fonts.ElementAtOrDefault(1) != null)
            {
                settings.fonts.secondary = fonts.ElementAt(1);
            }

            var colorNames = colors.ToList().OrderByDescending(f => f.Value).Select(f => f.Key);
            var colorsToUse = new ImagingAdapter().DeprioritizeGreyColors(colorNames);

            if (colorsToUse.Any())
            {
                settings.colors.primary = colorsToUse.First();
            }
            if (colorsToUse.ElementAtOrDefault(1) != null)
            {
                settings.colors.secondary = colorsToUse.ElementAt(1);
            }
            if (colorsToUse.ElementAtOrDefault(2) != null)
            {
                settings.colors.tertiary = colorsToUse.ElementAt(2);
            }

            return JsonConvert.SerializeObject(settings);
        }

        private IEnumerable<AnalyzedImage> GetAnalyzedImages(DocumentData document)
        {
            if (document?.DocumentToUploadIds == null || !document.DocumentToUploadIds.Any())
            {
                return new List<AnalyzedImage>();
            }

            var images = new List<AnalyzedImage>();
            foreach (var upload in document.DocumentToUploadIds.Where(d => d.Type != "LibraryImage"))
            {
                try
                {
                    var newImage = new AnalyzedImage
                    {
                        SourceId = upload.ImageId,
                        Uploaded = upload.UploadDate,
                        Url = upload.ImagePreview
                    };

                    if (!images.Any(i => i.SourceId == newImage.SourceId))
                    {
                        Bitmap image =
                            (Bitmap) System.Drawing.Image.FromStream(
                                new MemoryStream(ImageRetrievalUtils.GetImageBytes(newImage.Url)));
                        newImage.IsLogo = _imagingAdapter.IsLogo(image);
                        if (newImage.IsLogo)
                        {
                            newImage.Colors = _imagingAdapter.GetImageColors(image).Take(5)
                                .Select(c => "#" + c.Key.R.ToString("X2") + c.Key.G.ToString("X2") +
                                             c.Key.B.ToString("X2"));
                        }

                        images.Add(newImage);
                    }
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Unable to retrieve image from url", ex);
                }
            }

            /*
            if (document.ComboFamilyImageId != null)
            {
                var newImage = new AnalyzedImage
                {
                    SourceId = document.ComboFamilyImageId.Value,
                    SourceType = "LibraryImage",
                    Uploaded = DateTime.Now,
                    Url =
                        $"{Settings.Default.VpSecureBaseUrl}/vp/ns/imagepreview.aspx?contentserver=1&image_id={document.ComboFamilyImageId}&maxwidth=400&maxheight=400",
                    IsLogo = true
                };

                Bitmap image =
                    (Bitmap)System.Drawing.Image.FromStream(new MemoryStream(ImageRetrievalUtils.GetImageBytes(newImage.Url)));

                newImage.Colors = _imagingAdapter.GetImageColors(image).Take(5)
                    .Select(c => "#" + c.Key.R.ToString("X2") + c.Key.G.ToString("X2") + c.Key.B.ToString("X2"));

                images.Add(newImage);
            }
            */

            return images;
        }

        private void AddColorToWeightedDictionary(IDictionary<string, int> colors, string newColor, int weight, int tolerance = 20)
        {
            var newC = ColorTranslator.FromHtml(newColor);
            foreach (var color in colors.OrderByDescending(c => c.Value).Select(c => c.Key))
            {
                if (_imagingAdapter.AreSameColor(ColorTranslator.FromHtml(color), newC, tolerance))
                {
                    colors[color] += weight;
                    return;
                }
            }

            colors.Add(newColor, weight);
        }

        private class AnalyzedImage
        {
            public int Id { get; set; }
            public int? SourceId { get; set; }
            public string SourceType { get; set; }
            public string Url { get; set; }
            public bool IsLogo { get; set; }
            public IEnumerable<string> Colors { get; set; }
            public DateTime? Uploaded { get; set; }
        }

        private string GetText(DocumentData document, string textFieldId)
        {
            if (document?.DocumentToTextFieldData == null)
            {
                return null;
            }

            return document.DocumentToTextFieldData.FirstOrDefault(t => t.PurposeTextId == textFieldId)?.Value.Trim();
        }

        internal static class PurposeField
        {
            internal static string CompanyName = "companyname";
            internal static string CompanyMessage = "companymessage";
            internal static string FullName = "fullname";
            internal static string JobTitle = "jobtitle";
            internal static string Address1 = "address1";
            internal static string Address2 = "address2";
            internal static string Address3 = "address3";
            internal static string Phone = "phone";
            internal static string Fax = "fax";
            internal static string Email = "email";
            internal static string Web = "web";
        }
    }

    internal static class FieldHelper
    {
        public static string TruncateString(this string str, int maxLength)
        {
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }
    }
}
