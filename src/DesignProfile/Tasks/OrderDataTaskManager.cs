﻿using System;
using System.Data;
using System.Data.SqlClient;
using DesignProfile.Notifications;
using DesignProfile.Properties;

namespace DesignProfile.Tasks
{
    public static class OrderDataTaskManager
    {
        public static void ReportOrderedTemplates()
        {
            using (var c = new SqlConnection(Settings.Default.ReportingDbConnectionString))
            {
                c.Open();
                var query = c.CreateCommand();
                query.CommandType = CommandType.Text;
                query.CommandText = @"SELECT ri.order_id, d.alt_doc_id, s.email, ri.declared_value_usd /100 as 'price'
FROM vp_receipt_item ri (nolock)
LEFT JOIN vp_receipt r (nolock) on ri.order_id = r.order_id
LEFT JOIN vp_shopper s (nolock) on r.shopper_key = s.shopper_key
LEFT JOIN vp_document d (nolock) on d.doc_id = ri.doc_id
WHERE 
	r.created > DateADD(mi, -5, GETDATE()) 
AND 
	ri.doc_id IS NOT NULL
AND (
	EXISTS (select 1 from std_document_audit da (nolock) where d.doc_id = da.doc_id and studio_mode_id = 64) 
	OR
	EXISTS (select 1 from std_document_audit da (nolock)  where d.parent_doc_id = da.doc_id and studio_mode_id = 64)
)

";
                try
                {
                    var slackPublisher = new SlackPublisher(slackRoomDev: "designpro-order-dev", slackRoomProd: "designpro-order-prd");
                    var reader = query.ExecuteReader();
                    while (reader.Read())
                    {
                        var orderId = (reader["order_id"] as int?).GetValueOrDefault();
                        var altDocId = (reader["alt_doc_id"] as string);
                        var email = (reader["email"] as string);
                        var price = "$"+(reader["price"] as decimal?);
                        slackPublisher.PublishMessageAsync(message: "Order ID:" + orderId + " | Item price: " + price, author: email,
                            image_url: Settings.Default.VpBaseUrl + "/lp.aspx?admin=1&alt_doc_id=" + altDocId);
                    }
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Warn("Could not send message to slack for ordered product", ex);
                }
                c.Close();
            }
        }
    }
}