﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using DesignProfile.Adapters;
using DesignProfile.data;
using DesignProfile.Models;
using DesignProfile.Properties;
using DesignProfile.Utils;
using Hangfire;
using Hatchery.Imaging;
using Newtonsoft.Json;
using RestSharp;

namespace DesignProfile.Tasks
{
    public static class ImageTransfer
    {
        /// <summary>
        /// Adds the image to portfolio.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static async Task AddImageToPortfolio(int id)
        {
            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var image = ctx.Images.Find(id);

                if (image == null || image.UploadId != null)
                {
                    return;
                }

                var isLogo = image.ImageTypeId == 1;
                await UploadToCimpress(id, image.Url);

                if (isLogo && Settings.Default.EnableCrispify)
                {
                    try
                    {
                        var urlBase = Settings.Default.CimpressOpenApiUrl + "/process/crispify";
                        var request = (HttpWebRequest) WebRequest.Create(urlBase);
                        request.ContentType = "application/json";
                        request.Method = "POST";

                        var body = Encoding.UTF8.GetBytes(
                            JsonConvert.SerializeObject(new CrispifyRequest {ImageUrl = image.Url}));
                        request.ContentLength = body.Length;

                        var bodyStream = request.GetRequestStream();
                        bodyStream.Write(body, 0, body.Length);
                        bodyStream.Close();

                        using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                        using (Stream stream = response.GetResponseStream())
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            var jobQueuedResponse =
                                JsonConvert.DeserializeObject<JobQueuedResponse>(reader.ReadToEnd());

                            if (!string.IsNullOrEmpty(jobQueuedResponse.JobId))
                            {
                                BackgroundJob.Enqueue(() => RetrieveCrispifiedImage(jobQueuedResponse.JobId, id, 0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        NLog.LogManager.GetCurrentClassLogger().Error("Error trying to submit Crispify job", ex);
                    }
                }
            }
        }

        public static async Task<int?> UploadToCimpress(int imageId, string imageUrl)
        {
            int uploadId;

            byte[] imageData;
            // Get the image and parse through the colors
            using (var client = new WebClient())
            {
                imageData = ImageRetrievalUtils.GetImageBytes(imageUrl);
                var ms = new MemoryStream(imageData);
            }

            using (var ctx = new DesignProfileEntities(ConnectionString.Current))
            {
                var image = ctx.Images.Find(imageId);

                if (image == null)
                {
                    return null;
                }

                using (var client = new HttpClient())
                {
                    using (var content =
                        new MultipartFormDataContent("Upload----" +
                                                     DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                    {
                        var prefix = image.ImageTypeId == 1 ? "brandshop_" : (image.ImageTypeId == 2 ? "giftshop_" : "");
                        content.Add(new StreamContent(new MemoryStream(imageData)), "image", prefix + image.Title);
                        var cimpressUploadResponse = await client.PostAsync(
                            $"https://uploads.documents.cimpress.io/upload/process.aspx?tenant={Settings.Default.CimpressTenant}",
                            content);

                        try
                        {
                            var response = JsonConvert.DeserializeObject<CimpressUploadResponse>(await cimpressUploadResponse.Content.ReadAsStringAsync());
                            uploadId = response.Success.Images.FirstOrDefault().Id;
                            image.UploadId = uploadId;
                            image.UploadCluster = response.Success.Images.FirstOrDefault().McpClusterId.ToString();
                            ctx.SaveChanges();
                        }
                        catch(Exception ex)
                        {
                            NLog.LogManager.GetCurrentClassLogger().Error("Error trying to upload file", ex);
                            return null;
                        }
                    }
                }
            }

            return uploadId;
        }

        public static async Task RetrieveCrispifiedImage(string jobId, int imageId, int numAttempts)
        {
            try
            {
                var urlBase = Settings.Default.CimpressOpenApiUrl + "/status/" + jobId;
                var request = (HttpWebRequest) WebRequest.Create(urlBase);
                request.ContentType = "application/json";
                request.Method = "GET";
                request.Headers.Add("Authorization", "Bearer " + GetCimpressAuth0Token());

                using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    var crispifyResponse =
                        JsonConvert.DeserializeObject<CrispifyResponse>(reader.ReadToEnd());

                    if (crispifyResponse.JobStatus != "Complete")
                    {
                        if (numAttempts < 10)
                        {
                            BackgroundJob.Schedule(
                                () => RetrieveCrispifiedImage(jobId, imageId, numAttempts + 1),
                                TimeSpan.FromSeconds(2));
                        }
                        else
                        {
                            NLog.LogManager.GetCurrentClassLogger().Error("Maximum number of Crispify polling attempts reached for image " + imageId + ", job " + jobId);
                        }
                    }
                    else
                    {
                        await UploadToCimpress(imageId, crispifyResponse.ResultImage);
                    }
                }
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Error trying to conclude Crispified job", ex);
            }
        }

        private static string GetCimpressAuth0Token()
        {
            var client = new RestClient("https://cimpress.auth0.com");

            var request = new RestRequest("oauth/token", Method.POST);
            request.AddParameter("client_id", Settings.Default.CimpressAuth0ClientId);
            request.AddParameter("client_secret", Settings.Default.CimpressAuth0ClientSecret);
            request.AddParameter("audience", "https://api.cimpress.io/");
            request.AddParameter("grant_type", "client_credentials");

            IRestResponse response = client.Execute(request);
            dynamic json = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
            return json["access_token"];
        }

        public class ImageUploadIdRequest
        {
            public int ShopperKey { get; set; }
            public string ImageUrl { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public int FileSize { get; set; }
            public string FileName { get; set; }
            public bool IsLogo { get; set; }
        }

        public class FileUpload
        {
            public string FileName { get; set; }
            public string FileData { get; set; }
        }

        public class CimpressUploadResponse
        {
            public CimpressUploadResponseSuccess Success { get; set; }
        }

        public class CimpressUploadResponseSuccess
        {
            public IEnumerable<CimpressUploadResponseImage> Images { get; set; }
        }

        public class CimpressUploadResponseImage
        {
            public int Id { get; set; }
            public bool IsLogo { get; set; }
            public int McpClusterId { get; set; }
        }

        public class CrispifyRequest
        {
            public string ImageUrl { get; set; }
        }

        public class JobQueuedResponse
        {
            public string JobId { get; set; }
        }

        public class CrispifyResponse
        {
            public string ConsoleVersion { get; set; }
            public string ImageUrl { get; set; }
            public string JobStatus { get; set; }
            public string Processor { get; set; }
            public string ResultImage { get; set; }
        }
    }
}