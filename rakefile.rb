require 'bundler/setup'
require 'rake'
require 'rake/clean'
require 'vp_devkit_dotnet'

version = DevKit::Version.new('1.0.*')
@version=version

desc "You know what this does."
task :preflight => [:build, "db:migrate"]
task :default => [:preflight]

nuget_retrieve :retrieve do |t|
  t.input_glob = '**/packages.config'
end

msbuild :build => [:retrieve] do |t|
	t.input_file = 'src\DesignProfile.sln'
	t.build_configuration = 'Release'
	t.version = version
end

CLEAN.include('src\DesignProfile\bin', 'nuget-packages')

task :jenkins_build => ["deploy:publish"]
task :jenkins_dev => ["deploy:dev"]
task :jenkins_test => ["deploy:test"]
task :jenkins_prod => ["deploy:prod"]

namespace :db do
	@flyway = 'tools\flyway-4.0.1\flyway.cmd'

	def migrate(server, user, password)
		puts "Migrating to #{server}"
		puts "Current path #{File.dirname(__FILE__)}"
		cmd = "#{@flyway} \"-url=jdbc:jtds:sqlserver://#{server}\" \"-user=#{user}\" \"-password=#{password}\" \"-locations=filesystem:data\" -jarDirs= migrate"
		puts cmd
		raise "Migration failed" unless system cmd
	end

	task :migrate do
		migrate('localhost/design_profile;instance=DBLOCAL', 'designprofiledb', 'designprofiledb')
	end

	task :clean do
		server = 'localhost/design_profile;instance=DBLOCAL'
		user = 'designprofiledb'
		password = 'designprofiledb'
		puts "Cleaning #{server}"
		cmd = "#{@flyway} \"-url=jdbc:jtds:sqlserver://#{server}\" \"-user=#{user}\" \"-password=#{password}\" -jarDirs= clean"
		puts cmd
		raise "Clean failed" unless system cmd
	end

	namespace :dev do
		task :migrate do
			migrate('INTIRLDBOSQL001.vistaprint.svc/design_profile', 'udesign_profilerm', '77353731DA2A4CB')
		end
	end

	namespace :test do
		task :migrate do
			migrate('TSTIRLDBOSQL001.vistaprint.svc/design_profile', 'udesign_profilerm', '8E5FE157A4B949E')
		end
	end

	namespace :prod do
		task :migrate do
			migrate('PRDIRLDBOSQL001.vistaprint.svc/design_profile', 'udesign_profilerm', '86845661C5DC487')
		end
	end
end

namespace :deploy do
	def zip_and_upload
		require 'zip'
		require 'aws-sdk'
		
		ENV['AWS_ACCESS_KEY_ID'] = 'AKIAIR5NNYJ6BS2C3PKA'
		ENV['AWS_SECRET_ACCESS_KEY'] = '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'
		
		# Create zip file
		archive = create_archive

		# Upload zip file
		name = File.basename archive
		Aws.use_bundled_cert!
		puts "Uploading archive"
		s3 = Aws::S3::Client.new(region: 'eu-west-1')
		File.open(archive, 'rb') do |file|
			x = s3.put_object(bucket: 'design-profile-app-revisions-dev', key: name, body: file)
		end

		name
	end

	def zip_then_deploy(environment)
		name = zip_and_upload
		deploy_revision(environment,name)
	end

	def deploy_revision(environment, name)
		require 'aws-sdk'
		
		ENV['AWS_ACCESS_KEY_ID'] = 'AKIAIR5NNYJ6BS2C3PKA'
		ENV['AWS_SECRET_ACCESS_KEY'] = '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'

		# Start deployment
		puts "Starting deployment"
		Aws.use_bundled_cert!
		codedeploy = Aws::CodeDeploy::Client.new()
		deployment = codedeploy.create_deployment({
			application_name: 'DesignProfile',
			deployment_group_name: environment,
			revision: {
				revision_type: 'S3',
				s3_location: {
					bucket: environment == 'PROD' ? 'design-profile-app-revisions-prod' : 'design-profile-app-revisions-dev',
					key: name,
					bundle_type: 'zip',
				}
			}
		})

		# Wait for deployment to finish
		deploy_id = deployment.deployment_id
		puts "Deployment id is #{deploy_id}"
		puts "Logs: https://console.aws.amazon.com/codedeploy/home?region=#{ENV['AWS_REGION']}#/deployments/#{deploy_id}"
		puts "Polling for completion..."
		deploy_status = nil
		(0..30).each do
			deploy_status = codedeploy.get_deployment({deployment_id: deploy_id})
			case deploy_status.deployment_info.status
			when 'Succeeded', 'Failed', 'Stopped'
				break
			end
			sleep 30
		end
		puts 'Done'

		# Logging & status assessment
		if deploy_status.deployment_info.error_information
			puts "Error information"
			puts "#{deploy_status.deployment_info.error_information.code} - #{deploy_status.deployment_info.error_information}"
		end

		case deploy_status.deployment_info.status
			when 'Succeeded'
				puts 'Deployment successful'
			when 'Failed', 'Stopped'
				raise 'Deployment failed'
			when 'Created', 'Queued', 'InProgress'
				raise 'Deployment timed out'
			else
				raise "Unknown deployment status #{deploy_status.deployment_info.status}"
		end
	end

	def create_archive
		puts "Creating archive"
		file_name = 'DesignProfile_' + @version.to_s + '.zip'
		FileUtils.mkdir_p 'output'
		zip_path = 'output\\' + file_name

		File.delete zip_path if File.exists? zip_path

		# todo: we "should" msbuild /t:publish to a temp directory and use that instead
		in_zip_folders = ['cd-scripts', 'src/DesignProfile', 'configs']
		in_zip_files = ['appspec.yml']

		Zip::File.open(zip_path, Zip::File::CREATE) do |zipfile|
			in_zip_files.each do |f|
				zipfile.add(f, f)
			end
			in_zip_folders.each do |d|
				Dir["#{d}/**/*"].each do |f|
					# Ignore the obj folder
					zipfile.add(f, f) unless /\/obj\//.match f
				end
			end
		end

		zip_path
	end

	def copy_revision_to_prod(revision)
		require 'aws-sdk'
	
		ENV['AWS_ACCESS_KEY_ID'] = 'AKIAIR5NNYJ6BS2C3PKA'
		ENV['AWS_SECRET_ACCESS_KEY'] = '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'
		Aws.use_bundled_cert!
		
		to_bucket = Aws::S3::Bucket.new('design-profile-app-revisions-prod')
		to = to_bucket.object(revision)

		if !to.exists?
			from_bucket = Aws::S3::Bucket.new('design-profile-app-revisions-dev')
			from = from_bucket.object(revision)
			puts "Copying new revision to prod bucket"
			from.copy_to(to)
		end
	end

	task :publish => [:build] do
		ENV['DESIGNPROFILE_REVISION_NAME'] = zip_and_upload
		File.open('revision.txt', 'w') do |f|
			f.write(ENV['DESIGNPROFILE_REVISION_NAME'])
		end
	end

	task :dev => ["db:dev:migrate"] do
		ENV['AWS_REGION'] = 'eu-west-1'
		revision = File.open('revision.txt','r').read.strip
		fail 'No revision specified (revision.txt)' if !revision
		deploy_revision('DEV', revision)
	end

	task :test => ["db:test:migrate"] do
		ENV['AWS_REGION'] = 'eu-west-1'
		revision = File.open('revision.txt','r').read.strip
		fail 'No revision specified (revision.txt)' if !revision
		deploy_revision('TEST', revision)
	end

	task :prod => ["db:prod:migrate"] do
		ENV['AWS_REGION'] = 'eu-west-1'
		revision = File.open('revision.txt','r').read.strip
		fail 'No revision specified (revision.txt)' if !revision

		copy_revision_to_prod(revision)
		deploy_revision('PROD', revision)
	end
end
