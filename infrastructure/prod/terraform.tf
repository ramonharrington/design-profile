provider "aws" {
  access_key = "AKIAIR5NNYJ6BS2C3PKA"
  secret_key = "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"
  region     = "eu-west-1"
}

resource "aws_instance" "design-profile-proxy" {
  ami           = "ami-250ce05c"
  instance_type = "t2.small"
  subnet_id     = "subnet-0c6d0e7a"
  associate_public_ip_address = "true"
  key_name      = "hatchery-ec2_eu-west-1"
  vpc_security_group_ids = ["sg-39ee3840"]
  iam_instance_profile = "hatchery_ec2"

  tags {
    Name = "design-profile-proxy"
  }
}

resource "aws_instance" "design-profile-prod-1" {
  ami           = "ami-23dc325a"
  instance_type = "t2.medium"
  subnet_id     = "subnet-0f6d0e79"
  associate_public_ip_address = "false"
  key_name      = "hatchery-ec2_eu-west-1"
  vpc_security_group_ids = ["sg-48e40a2e", "sg-192d327e"]
  iam_instance_profile = "hatchery_ec2"

  tags {
	DesignProfileEnvironment = "Prod"
    Name = "design-profile-prod-1"
  }
}

resource "aws_instance" "design-profile-prod-2" {
  ami           = "ami-23dc325a"
  instance_type = "t2.medium"
  subnet_id     = "subnet-0f6d0e79"
  associate_public_ip_address = "false"
  key_name      = "hatchery-ec2_eu-west-1"
  vpc_security_group_ids = ["sg-48e40a2e", "sg-192d327e"]
  iam_instance_profile = "hatchery_ec2"

  tags {
	DesignProfileEnvironment = "Prod"
    Name = "design-profile-prod-2"
  }
}

resource "aws_instance" "design-profile-prod-3" {
  ami           = "ami-23dc325a"
  instance_type = "t2.medium"
  subnet_id     = "subnet-0f6d0e79"
  associate_public_ip_address = "false"
  key_name      = "hatchery-ec2_eu-west-1"
  vpc_security_group_ids = ["sg-48e40a2e", "sg-192d327e"]
  iam_instance_profile = "hatchery_ec2"

  tags {
	DesignProfileEnvironment = "Prod"
    Name = "design-profile-prod-3"
  }
}

resource "aws_route53_record" "prod_1" {
  zone_id = "Z3I3H84DHUD3N1"
  name    = "webserver1"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.design-profile-prod-1.private_ip}"]
}

resource "aws_route53_record" "prod_2" {
  zone_id = "Z3I3H84DHUD3N1"
  name    = "webserver2"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.design-profile-prod-2.private_ip}"]
}

resource "aws_route53_record" "prod_3" {
  zone_id = "Z3I3H84DHUD3N1"
  name    = "webserver3"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.design-profile-prod-3.private_ip}"]
}

resource "aws_route53_record" "prod" {
  zone_id = "Z3I3H84DHUD3N1"
  name    = "."
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.design-profile-proxy.public_ip}"]
}

output "proxy_ip" {
  value = "${aws_instance.design-profile-proxy.public_ip}"
}

output "webserver1_ip" {
  value = "${aws_instance.design-profile-prod-1.private_ip}"
}

output "webserver2_ip" {
  value = "${aws_instance.design-profile-prod-2.private_ip}"
}

output "webserver3_ip" {
  value = "${aws_instance.design-profile-prod-3.private_ip}"
}
