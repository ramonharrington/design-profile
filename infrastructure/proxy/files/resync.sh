HOST="designprofile.vistaprint.io"
USER="ubuntu"
IDENTITY="~/.ssh/hatchery-ec2_eu-west-1.pem"

ssh -i $IDENTITY $USER@$HOST "sudo tar czpf - /etc/letsencrypt" > letsencrypt.tgz
scp -i $IDENTITY $USER@$HOST:/etc/ssl/certs/dhparam.pem ./
scp -i $IDENTITY $USER@$HOST:/etc/nginx/sites-enabled/designprofile-prod ./designprofile-prod
scp -i $IDENTITY $USER@$HOST:/etc/nginx/sites-enabled/designprofile-test ./designprofile-test
scp -i $IDENTITY $USER@$HOST:/etc/nginx/sites-enabled/designprofile-dev ./designprofile-dev


